package com.ingenio.trust.service;

import com.ingenio.trust.model.LibBookInventoryModel;

public interface LibBookInventoryService {

	LibBookInventoryModel getLibBookInventoryByName(String cName, Integer schoolId, String authorName, String bookEdition);

	LibBookInventoryModel addLibBookInventory(LibBookInventoryModel libBookSupplierModel);

}
