package com.ingenio.trust.service;

import java.util.List;

import com.ingenio.trust.bean.DashBoardBean;
import com.ingenio.trust.bean.SchoolBean;
import com.ingenio.trust.bean.StaffDetailsNewBean;
import com.ingenio.trust.bean.StudentCountBean;

public interface StudentCountService {
	public List<StudentCountBean> getSchoolWiseStrength(String sansthaKey, String yearName, String renewDate, Integer groupOfSchoolId,String mobileNo, String profileRole) ;
	public List<StudentCountBean> getstandardWiseStrength(Integer schoolId, Integer yearId, String renewDate);
	public List<StudentCountBean> getDivisionwiseStrength(Integer schoolId, Integer yearId, String renewDate,Integer standardId);
	public List<DashBoardBean> getDashboardCount(String sansthaKey, String yearName, String renewDate, String language);
	public List<StudentCountBean> getRelisionWiseCount(String sansthaKey, String yearName, String renewDate);
	public List<StudentCountBean> getgroupOfSchoolWiseStrength(String sansthaKey, String yearName, String renewDate,String mobileNo,
			String profileRole);
	public List<String> getYearForSanstha(String sansthaKey, String renewDate);
	public List<DashBoardBean> getNewDashboardCount(String sansthaKey, String yearName, String renewDate,
			String language, String mobileNo, String profileRole, Integer isReact);
	public List<DashBoardBean> getNewDashboardCount1(String sansthaKey, String yearName, String renewDate,
			String language, String mobileNo, String profileRole, Integer isReact);
	public List<StaffDetailsNewBean> getSchoolWiseStaffDetails(Integer schoolid);
	public List<SchoolBean> getAllSchools(String sansthaKey, Integer groupOfSchoolId);

}
