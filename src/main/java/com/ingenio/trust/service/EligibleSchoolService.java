package com.ingenio.trust.service;

import com.ingenio.trust.bean.EligibleSchoolPage;
import com.ingenio.trust.bean.IntakeFilterParam;

public interface EligibleSchoolService {

	EligibleSchoolPage getEligibleSchoolPageDetails(IntakeFilterParam queryParams);

}
