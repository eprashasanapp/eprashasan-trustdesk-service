package com.ingenio.trust.service;

import com.ingenio.trust.bean.AppliedSchoolFilterParam;
import com.ingenio.trust.bean.AppliedSchoolPage;

public interface AppliedSchoolService {

	AppliedSchoolPage getAppliedSchoolPageDetails(AppliedSchoolFilterParam queryParams);

}
