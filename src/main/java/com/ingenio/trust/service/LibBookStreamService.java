package com.ingenio.trust.service;

import com.ingenio.trust.model.LibBookStreamModel;

public interface LibBookStreamService {

	LibBookStreamModel getLibBookStreamByName(String cName, Integer schoolId);

	LibBookStreamModel addLibBookStream(LibBookStreamModel libBookSupplierModel);

}
