package com.ingenio.trust.service;

import com.ingenio.trust.model.LibBookTypeModel;

public interface LibBookTypeService {

	LibBookTypeModel getLibBookTypeByName(String cName, Integer schoolId);

	LibBookTypeModel addLibBookType(LibBookTypeModel libBookSupplierModel);

}
