package com.ingenio.trust.service;

import java.util.List;

import com.ingenio.trust.bean.ExcelBean;

public interface AttachmentService {

	public boolean uploadBookInventoryinLibrary(Integer schoolId, List<ExcelBean> excelBeanList);

}
