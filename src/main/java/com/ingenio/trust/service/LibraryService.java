package com.ingenio.trust.service;

import java.util.List;

import com.ingenio.trust.bean.LibStudentAdmissionFilterParam;
import com.ingenio.trust.bean.LibStudentAdmissionPage;
import com.ingenio.trust.bean.LibStudentAdmissionRequest;
import com.ingenio.trust.bean.LibStudentAdmissionResponse;

public interface LibraryService {

	public List<LibStudentAdmissionResponse> addStudentToLib(LibStudentAdmissionRequest addStudentToLibraryRequest);

	public LibStudentAdmissionPage getAdmittedStudentsFromLib(LibStudentAdmissionFilterParam queryParams);

}
