package com.ingenio.trust.service;

import java.util.List;

import com.ingenio.trust.bean.FeeBean;

public interface PaidFeeService {

	public List<FeeBean> getPaidFeeStandardWise(String sansthaKey, String yearName, String feeFromDate,String feeToDate, Integer groupId,
			String mobileNo, String profileRole);

	public List<FeeBean> getPaidFeeGroupWise(String sansthaKey, String yearName, String feeFromDate, String feeToDate,String mobileNo, String profileRole);
	
}
