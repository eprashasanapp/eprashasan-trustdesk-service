package com.ingenio.trust.service;

import com.ingenio.trust.bean.BookIssuedDetails;
import com.ingenio.trust.bean.BookIssuedFilterParam;
import com.ingenio.trust.bean.BookIssuedPage;
import com.ingenio.trust.bean.UpdateBookIssued;

public interface LibBookIssuedService {

	BookIssuedPage getBookIssuedDetails(BookIssuedFilterParam queryParams);

	BookIssuedDetails addBookIssuedDetails(BookIssuedDetails bookIssue);

	UpdateBookIssued updateBookIssuedDetails(UpdateBookIssued bookIssue);

}
