package com.ingenio.trust.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.ingenio.trust.bean.AnnouncementRequestBean;
import com.ingenio.trust.bean.AnnouncementResponseBean;
import com.ingenio.trust.bean.FeeBean;
import com.ingenio.trust.bean.SansthaUserBean;
import com.ingenio.trust.bean.SchoolBean;
import com.ingenio.trust.bean.StaffDetailsBean;
import com.ingenio.trust.bean.UserBean;

public interface AnnouncementService {

	List<SchoolBean> getSchoolList(String sansthaKey, Integer sansthaUserId);

	List<SansthaUserBean> getSansthaUserList(String sansthaKey);

	List<StaffDetailsBean> getSchoolWisePrincipalList(String sansthaKey, Integer sansthaUserId);

	List<UserBean> getAllUserList(String sansthaKey, Integer sansthaUserId);

	Integer saveSansthaAnnouncement(AnnouncementRequestBean announcementRequestBean);

	List<AnnouncementResponseBean> getMyAnnouncement(Integer sansthauserId, Integer yearId, String announcementDate,
			Integer offset);

	Integer saveAttachmentsWithId(MultipartFile file, String fileName, String yearName, Integer sansthaAnnouncmentId,
			Integer sansthakey);

	List<AnnouncementResponseBean> getAttachmentList(Integer announcementId);


}
