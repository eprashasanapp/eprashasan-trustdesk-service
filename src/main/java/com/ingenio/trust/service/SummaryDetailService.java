package com.ingenio.trust.service;

import java.util.List;

import com.ingenio.trust.bean.StudentCountBean;
import com.ingenio.trust.bean.SummaryMenuBean;

public interface SummaryDetailService {

	List<StudentCountBean> getSansthaWiseSummaryDetails(String sansthaKey, String yearName, String renewDate, String summaryType, String mobileNo, String profileRole);

	List<StudentCountBean> getSummaryWiseSchoolList(String sansthaKey, String yearName, String summaryType,
			String summaryId, String renewDate,Integer groupId, String mobileNo, String profileRole);

	List<StudentCountBean> groupWiseSummaryList(String sansthaKey, String yearName, String summaryType, String summaryId, String renewDate, String mobileNo, String profileRole);

	List<SummaryMenuBean> getSummaryMenuList();


	
}
