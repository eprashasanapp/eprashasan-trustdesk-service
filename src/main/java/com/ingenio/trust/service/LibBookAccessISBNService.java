package com.ingenio.trust.service;

import com.ingenio.trust.bean.LibBookAccessISBNFilterParam;
import com.ingenio.trust.bean.LibBookAccessISBNPage;
import com.ingenio.trust.model.LibBookAccessISBNModel;

public interface LibBookAccessISBNService {

	public LibBookAccessISBNPage getLibBookAccessISBN(LibBookAccessISBNFilterParam queryParams);
	
	LibBookAccessISBNModel addLibBookAccessISBN(LibBookAccessISBNModel libBookAccessISBNModel);
}
