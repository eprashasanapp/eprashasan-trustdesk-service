package com.ingenio.trust.service;

import com.ingenio.trust.bean.LibSchoolTypeFilterParam;
import com.ingenio.trust.bean.LibSchoolTypePage;

public interface LibSchoolTypeService {

	public LibSchoolTypePage getSchoolType(LibSchoolTypeFilterParam queryParams);

}
