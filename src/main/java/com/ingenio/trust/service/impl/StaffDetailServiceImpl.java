package com.ingenio.trust.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ingenio.trust.ConfigurationProperties;
import com.ingenio.trust.bean.StaffDetailsBean;
import com.ingenio.trust.constants.IngenioCodes;
import com.ingenio.trust.constants.TrustDeskConstants;
import com.ingenio.trust.repository.StaffDetailsRepository;
import com.ingenio.trust.service.StaffDetailService;
import com.ingenio.trust.util.CommonUtlitity;
import com.ingenio.trust.util.CryptoUtilLib;
import com.ingenio.trust.util.Utillity;

@Component
public class StaffDetailServiceImpl implements StaffDetailService {

	@Autowired
	private StaffDetailsRepository staffDetailsRepository;

	@Autowired
	private CommonUtlitity commonUtility;
	
	CryptoUtilLib cry = new CryptoUtilLib();

	@Override
	public List<StaffDetailsBean> getStaffDetails(Integer schoolId, String yearname, String renewDate) {

		List<StaffDetailsBean> staffdetailsList = new ArrayList<>();
		StaffDetailsBean staffDetailsBean = new StaffDetailsBean();
		staffDetailsBean = staffDetailsRepository.getStaffCount(schoolId, renewDate);

		List<StaffDetailsBean> staffPresentList = staffDetailsRepository.getPresentStaffDetails(schoolId, renewDate);
		for (int i = 0; i < staffPresentList.size(); i++) {		
			staffPresentList.set(i, setStaffPhotoUrl(schoolId, staffPresentList.get(i)));
		}
		staffDetailsBean.setPresentStaffList(staffPresentList);

		List<StaffDetailsBean> staffAbsentList = staffDetailsRepository.getAbsentStaffDetails(schoolId, renewDate);
		for (int i = 0; i < staffAbsentList.size(); i++) {			
			staffAbsentList.set(i, setStaffPhotoUrl(schoolId, staffAbsentList.get(i)));
		}
	
		staffDetailsBean.setAbsentStaffList(staffAbsentList);
		staffdetailsList.add(staffDetailsBean);

		return staffdetailsList;
	}

	@Override
	public List<StaffDetailsBean> getSansthaWiseStaffDetails(String sansthaKey, String yearname, String renewDate,Integer groupOfSchoolId) {
		List<StaffDetailsBean> staffdetailsList = new ArrayList<>();
		StaffDetailsBean staffDetailsBean = new StaffDetailsBean();
		staffDetailsBean = staffDetailsRepository.getGroupWiseStaffCount(sansthaKey, renewDate,groupOfSchoolId);
		Long totalStaffCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + staffDetailsBean.getTotalCount()));
		Long presentStaffCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + staffDetailsBean.getPresentCount()));
		Long absentStaffCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + staffDetailsBean.getAbsentCount()));
		staffDetailsBean.setTotalCountInWords("" + totalStaffCountInWords);
		staffDetailsBean.setPresentCountInWords("" + presentStaffCountInWords);
		staffDetailsBean.setAbsentCountInWords("" + absentStaffCountInWords);

		List<StaffDetailsBean> schoolWiseList = staffDetailsRepository.getSchoolWiseAttendance(sansthaKey, renewDate,groupOfSchoolId);

		for (int i = 0; i < schoolWiseList.size(); i++) {
			StaffDetailsBean staffDetailsBean2 = schoolWiseList.get(i);
			 try {
				 staffDetailsBean2.setLongSchoolName(cry.decrypt(IngenioCodes.getSchoolname(), StringEscapeUtils.escapeJava(staffDetailsBean2.getLongSchoolName().replaceAll("[\\r\\n]+", ""))));
			} catch (Exception e) {
				e.printStackTrace();
			} 
			Long totalSchoolCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + staffDetailsBean2.getTotalCount()));
			Long presentSchoolCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + staffDetailsBean2.getPresentCount()));
			Long absentSchoolCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + staffDetailsBean2.getAbsentCount()));
			staffDetailsBean2.setTotalCountInWords("" + totalSchoolCountInWords);
			staffDetailsBean2.setPresentCountInWords("" + presentSchoolCountInWords);
			staffDetailsBean2.setAbsentCountInWords("" + absentSchoolCountInWords);
			staffDetailsBean2.setYearId(commonUtility.getCurrentYear(staffDetailsBean2.getSchoolid(), renewDate));
			schoolWiseList.set(i, staffDetailsBean2);
		}

		staffDetailsBean.setSchoolWiseList(schoolWiseList);
		staffdetailsList.add(staffDetailsBean);

		return staffdetailsList;

	}

	private StaffDetailsBean setStaffPhotoUrl(Integer schoolId, StaffDetailsBean userDetailsBean) {
		String directoryPath = "" + ConfigurationProperties.staffPhotoUrl +File.separator+ schoolId;
		String extension = "jpg";
		userDetailsBean.setStaffPhotoUrl(directoryPath + File.separator + userDetailsBean.getStaffRegNo() + TrustDeskConstants.DOT + extension);
		return userDetailsBean;
	}

	@Override
	public List<StaffDetailsBean> getgroupWiseStaffDetails(String sansthaKey, String yearname, String renewDate) {
		List<StaffDetailsBean> staffdetailsList = new ArrayList<>();
		StaffDetailsBean staffDetailsBean = new StaffDetailsBean();
		staffDetailsBean = staffDetailsRepository.getSansthaWiseStaffCount(sansthaKey, renewDate);
		Long totalStaffCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + staffDetailsBean.getTotalCount()));
		Long presentStaffCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + staffDetailsBean.getPresentCount()));
		Long absentCount=staffDetailsBean.getTotalCount()-staffDetailsBean.getPresentCount();
		Long absentStaffCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + absentCount));
		staffDetailsBean.setTotalCountInWords("" + totalStaffCountInWords);
		staffDetailsBean.setPresentCountInWords("" + presentStaffCountInWords);
		staffDetailsBean.setAbsentCountInWords("" + absentStaffCountInWords);

		List<StaffDetailsBean> groupWiseList = staffDetailsRepository.getGroupWiseStaffAttendance(sansthaKey,
				renewDate);

		for (int i = 0; i < groupWiseList.size(); i++) {
			StaffDetailsBean staffDetailsBean2 = groupWiseList.get(i);

			Long totalSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + staffDetailsBean2.getTotalCount()));
			Long presentSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + staffDetailsBean2.getPresentCount()));
			Long absentCount1=staffDetailsBean2.getTotalCount()-staffDetailsBean2.getPresentCount();
			Long absentSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + absentCount1));
			staffDetailsBean2.setTotalCountInWords("" + totalSchoolCountInWords);
			staffDetailsBean2.setPresentCountInWords("" + presentSchoolCountInWords);
			staffDetailsBean2.setAbsentCountInWords("" + absentSchoolCountInWords);
			staffDetailsBean2.setYearId(commonUtility.getCurrentYear(staffDetailsBean2.getSchoolid(), renewDate));
			groupWiseList.set(i, staffDetailsBean2);
		}

		staffDetailsBean.setGroupWiseList(groupWiseList);
		staffdetailsList.add(staffDetailsBean);

		return staffdetailsList;
	}

}
