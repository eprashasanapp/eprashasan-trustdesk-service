package com.ingenio.trust.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ingenio.trust.bean.FeeBean;
import com.ingenio.trust.bean.SchoolBean;
import com.ingenio.trust.constants.IngenioCodes;
import com.ingenio.trust.model.AppLanguageModel;
import com.ingenio.trust.repository.AppLanguageRepository;
import com.ingenio.trust.repository.SchoolMasterRepository;
import com.ingenio.trust.repository.impl.FeeRepositoryImpl;
import com.ingenio.trust.service.FeeService;
import com.ingenio.trust.util.CommonUtlitity;
import com.ingenio.trust.util.CryptoUtilLib;
import com.ingenio.trust.util.Utillity;

@Component
public class FeeServiceImpl implements FeeService {

	@Autowired
	FeeRepositoryImpl feeRepositoryImpl;
	
	@Autowired
	CommonUtlitity commonUtility;
	
	@Autowired
	private AppLanguageRepository appLanguageRepository;

	CryptoUtilLib cry = new CryptoUtilLib();
	
	@Autowired
	SchoolMasterRepository schoolMasterRepository;
	
	@Override
	public List<FeeBean> getTotalFeeSchoolWise(String sansthaKey, String yearName,
			String feeToDate,Integer groupId,String mobNum ,String profileRole) {

		List<FeeBean> feeResultList = feeRepositoryImpl.getTotalFee(sansthaKey,feeToDate,"",groupId);
		List<FeeBean> schoolList = new ArrayList<>();
		List<FeeBean> feeCountWithSchoolList = new ArrayList<>();
		FeeBean feeBean = new FeeBean();
		BigDecimal assignedFee = new BigDecimal(0);
		BigDecimal assignedFine = new BigDecimal(0);
		BigDecimal assignedChequeBounce=new BigDecimal(0);
		BigDecimal assignedTotal =new BigDecimal(0);
		
		BigDecimal paidFee =new BigDecimal(0);
		BigDecimal paidFine = new BigDecimal(0);
		BigDecimal paidChequeBounce=new BigDecimal(0);
		BigDecimal paidTotal =new BigDecimal(0);
		BigDecimal excessFee =new BigDecimal(0);
		BigDecimal otherFee =new BigDecimal(0);
		
		BigDecimal remainingFee =new BigDecimal(0);
		BigDecimal remainingFine = new BigDecimal(0);
		BigDecimal remainingChequeBounce=new BigDecimal(0);
		BigDecimal remainingTotal =new BigDecimal(0);
	
		BigDecimal clearanceFee =new BigDecimal(0);
		BigDecimal clearanceFine = new BigDecimal(0);
		BigDecimal clearanceChequeBounce=new BigDecimal(0);
		BigDecimal clearanceTotal =new BigDecimal(0);
	
		if(CollectionUtils.isNotEmpty(feeResultList)) {
			for(FeeBean feeResult : feeResultList) {
				Integer yearId =  commonUtility.getCurrentYear(feeResult.getSchoolId(), feeToDate);
				if(feeResult.getYearId().equals(yearId)) {
					FeeBean schoolBean = new FeeBean();
					schoolBean.setYearId(feeResult.getYearId());
					schoolBean.setYearName(feeResult.getYearName());
					schoolBean.setSchoolId(feeResult.getSchoolId());
					schoolBean.setYearId(yearId);
					schoolBean.setSchoolName(feeResult.getSchoolName());
					try {
						schoolBean.setLongSchoolName(cry.decrypt(IngenioCodes.getSchoolname(), StringEscapeUtils.escapeJava(feeResult.getLongSchoolName().replaceAll("[\\r\\n]+", ""))));
					} catch (Exception e) {
						e.printStackTrace();
					} 
					schoolBean.setAssignedFee(feeResult.getAssignedFee());
					schoolBean.setAssignedFine(feeResult.getAssignedFine());
					List<FeeBean> chequeBounceList = feeRepositoryImpl.getChequeBounceChargesSchoolWise(feeResult.getSchoolId(),yearId,feeToDate,"",groupId);
					schoolBean.setAssignedChequeBounceCharges("0");
					if(CollectionUtils.isNotEmpty(chequeBounceList)) {
						schoolBean.setAssignedChequeBounceCharges(chequeBounceList.get(0).getAssignedChequeBounceCharges());
					}
				
					schoolBean.setAssignedTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getAssignedFee()))).
							add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getAssignedFine()))).
							add(BigDecimal.valueOf((long)Double.parseDouble(schoolBean.getAssignedChequeBounceCharges()))));
					schoolBean.setAssignedFeeInWords(Utillity.convertNumberToWord(feeResult.getAssignedFee()));
					schoolBean.setAssignedFineInWords(Utillity.convertNumberToWord(feeResult.getAssignedFine()));
					schoolBean.setAssignedChequeBounceChargesInWords(Utillity.convertNumberToWord(schoolBean.getAssignedChequeBounceCharges()));
					schoolBean.setAssignedTotalInWords(Utillity.convertNumberToWord(schoolBean.getAssignedTotal()));
					
					assignedFee=assignedFee.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getAssignedFee())));
					assignedFine= assignedFine.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getAssignedFine())));
					assignedChequeBounce=assignedChequeBounce.add(BigDecimal.valueOf((long)Double.parseDouble("0")));
					assignedTotal=assignedTotal.add(BigDecimal.valueOf((long)Double.parseDouble(schoolBean.getAssignedTotal())));

					schoolBean.setPaidFee(feeResult.getPaidFee());
					schoolBean.setPaidFine(feeResult.getPaidFine());
					schoolBean.setExcessFee(feeResult.getExcessFee());
					schoolBean.setOtherFee(feeResult.getOtherFee());
					schoolBean.setPaidChequeBounceCharges("0");
					if(CollectionUtils.isNotEmpty(chequeBounceList)) {
						schoolBean.setPaidChequeBounceCharges(chequeBounceList.get(0).getPaidChequeBounceCharges());
					}
					schoolBean.setPaidTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getPaidFee())))
										.add(BigDecimal.valueOf((long)Double.parseDouble((feeResult.getPaidFine()))))
										.add(BigDecimal.valueOf((long)Double.parseDouble((schoolBean.getPaidChequeBounceCharges())))));
					schoolBean.setPaidFeeInWords(Utillity.convertNumberToWord(feeResult.getPaidFee()));
					schoolBean.setPaidFineInWords(Utillity.convertNumberToWord(feeResult.getPaidFine()));
					schoolBean.setPaidChequeBounceChargesInWords(Utillity.convertNumberToWord(schoolBean.getPaidChequeBounceCharges()));
					schoolBean.setPaidTotalInWords(Utillity.convertNumberToWord(schoolBean.getPaidTotal()));
					
					paidFee=paidFee.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getPaidFee())));
					paidFine= paidFine.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getPaidFine())));
					paidChequeBounce=paidChequeBounce.add(BigDecimal.valueOf((long)Double.parseDouble("0")));
					paidTotal=paidTotal.add(BigDecimal.valueOf((long)Double.parseDouble(schoolBean.getPaidTotal())));
					excessFee=excessFee.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getExcessFee())));
					otherFee=otherFee.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getOtherFee())));
					
					schoolBean.setRemainingFee(feeResult.getRemainingFee());
					schoolBean.setRemainingFine(feeResult.getRemainingFine());
					schoolBean.setRemainingChequeBounceCharges("0");
					schoolBean.setRemainingTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getRemainingFee()))).add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getRemainingFine()))));
					schoolBean.setRemainingFeeInWords(Utillity.convertNumberToWord(feeResult.getRemainingFee()));
					schoolBean.setRemainingFineInWords(Utillity.convertNumberToWord(feeResult.getRemainingFine()));
					schoolBean.setRemainingChequeBounceChargesInWords(Utillity.convertNumberToWord(schoolBean.getRemainingChequeBounceCharges()));
					schoolBean.setRemainingTotalInWords(Utillity.convertNumberToWord(schoolBean.getRemainingTotal()));
					
					remainingFee=remainingFee.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getRemainingFee())));
					remainingFine=remainingFine.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getRemainingFine())));
					remainingChequeBounce=remainingChequeBounce.add(BigDecimal.valueOf((long)Double.parseDouble("0")));
					remainingTotal=remainingTotal.add(BigDecimal.valueOf((long)Double.parseDouble(schoolBean.getRemainingTotal())));

					schoolBean.setClearanceFee(feeResult.getClearanceFee());
					schoolBean.setClearanceFine(feeResult.getClearanceFine());
					schoolBean.setClearanceChequeBounceCharges("0");
					schoolBean.setClearanceTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getClearanceFee()))).add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getClearanceFine())))
							.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getDiscount()))));
					schoolBean.setClearanceFeeInWords(Utillity.convertNumberToWord(feeResult.getClearanceFee()));
					schoolBean.setClearanceFineInWords(Utillity.convertNumberToWord(feeResult.getClearanceFine()));
					schoolBean.setClearanceChequeBounceChargesInWords(Utillity.convertNumberToWord(schoolBean.getClearanceChequeBounceCharges()));
					schoolBean.setClearanceTotalInWords(Utillity.convertNumberToWord(schoolBean.getClearanceTotal()));
					
					clearanceFee=clearanceFee.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getClearanceFee())));
					clearanceFine=clearanceFine.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getClearanceFine())));
					clearanceChequeBounce=clearanceChequeBounce.add(BigDecimal.valueOf((long)Double.parseDouble("0")));
					clearanceTotal=clearanceTotal.add(BigDecimal.valueOf((long)Double.parseDouble(schoolBean.getClearanceTotal())));

					schoolList.add(schoolBean);
				}
				
			}
			
		}
		
		feeBean.setAssignedFee(""+assignedFee);
		feeBean.setAssignedFine(""+assignedFine);
		feeBean.setAssignedChequeBounceCharges("0");
		feeBean.setAssignedTotal(""+(assignedFee.add(assignedFine)));
		feeBean.setAssignedFeeInWords(Utillity.convertNumberToWord(""+assignedFee));
		feeBean.setAssignedFineInWords(Utillity.convertNumberToWord(""+assignedFine));
		feeBean.setAssignedChequeBounceChargesInWords(Utillity.convertNumberToWord(""+assignedChequeBounce));
		feeBean.setAssignedTotalInWords(Utillity.convertNumberToWord(""+assignedTotal));
		
		feeBean.setPaidFee(""+paidFee);
		feeBean.setPaidFine(""+paidFine);
		feeBean.setExcessFee(""+excessFee);
		feeBean.setOtherFee(""+otherFee);
		feeBean.setPaidChequeBounceCharges("0");
		feeBean.setPaidTotal(""+(paidFee.add(paidFine)));
		feeBean.setPaidFeeInWords(Utillity.convertNumberToWord(""+paidFee));
		feeBean.setPaidFineInWords(Utillity.convertNumberToWord(""+paidFine));
		feeBean.setPaidChequeBounceChargesInWords(Utillity.convertNumberToWord(""+paidChequeBounce));
		feeBean.setPaidTotalInWords(Utillity.convertNumberToWord(""+paidTotal));
		
		feeBean.setRemainingFee(""+remainingFee);
		feeBean.setRemainingFine(""+remainingFine);
		feeBean.setRemainingChequeBounceCharges("0");
		feeBean.setRemainingTotal(""+(remainingFee.add(remainingFine)));
		feeBean.setRemainingFeeInWords(Utillity.convertNumberToWord(""+remainingFee));
		feeBean.setRemainingFineInWords(Utillity.convertNumberToWord(""+remainingFine));
		feeBean.setRemainingChequeBounceChargesInWords(Utillity.convertNumberToWord(""+remainingChequeBounce));
		feeBean.setRemainingTotalInWords(Utillity.convertNumberToWord(""+remainingTotal));
		
		feeBean.setClearanceFee(""+clearanceFee);
		feeBean.setClearanceFine(""+clearanceFine);
		feeBean.setClearanceChequeBounceCharges("0");
		feeBean.setClearanceTotal(""+clearanceTotal);
		feeBean.setClearanceFeeInWords(Utillity.convertNumberToWord(""+clearanceFee));
		feeBean.setClearanceFineInWords(Utillity.convertNumberToWord(""+clearanceFine));
		feeBean.setClearanceChequeBounceChargesInWords(Utillity.convertNumberToWord(""+clearanceChequeBounce));
		feeBean.setClearanceTotalInWords(Utillity.convertNumberToWord(""+clearanceTotal));
		
		feeBean.setPaidClearanceTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(feeBean.getPaidTotal())).add(BigDecimal.valueOf((long)Double.parseDouble(feeBean.getClearanceTotal())))));
		feeBean.setPaidClearanceTotalInWords(Utillity.convertNumberToWord(""+feeBean.getPaidClearanceTotal()));
		
		
		
//		if (!profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
//				|| profileRole.equalsIgnoreCase("Super officer")) {
//			List<Integer> schoolList1 = getSchoolList(mobNum, sansthaKey);
//			List<FeeBean> newSchoolWiseList = new ArrayList<FeeBean>();
//			for (int i = 0; i < schoolList.size(); i++) {
//				if (schoolList1.contains(schoolList.get(i).getSchoolId())){
//					newSchoolWiseList.add(schoolList.get(i));
//				}
//			}	
//			schoolList.clear();
//			schoolList = newSchoolWiseList; 
//		}

		feeBean.setSchoolList(schoolList);
		feeCountWithSchoolList.add(feeBean);
		return feeCountWithSchoolList;
	}
	
	
	public List<Integer> getSchoolList(String MobileNo, String SansthaKey) {
		List<SchoolBean> schoolMasterList = schoolMasterRepository.getSchoolList(SansthaKey, MobileNo);
		List<Integer> schoolList = new ArrayList<>();
		for (int i = 0; i < schoolMasterList.size(); i++) {
			schoolList.add(schoolMasterList.get(i).getSchoolId());
		}
		return schoolList;
	}
	
	@Override
	public List<FeeBean> getTotalFeeGroupWise(String sansthaKey, String yearName, String feeToDate,String mobNum ,String profileRole) {
		List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
		List<String> yearList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(appLanguageList)) {
			for (int i = 0; i < appLanguageList.size(); i++) {
				yearList.add(Utillity.convertEnglishToOther(yearName, "" + appLanguageList.get(i).getLanguageId()));
			}
		}
		List<FeeBean> feeResultList = feeRepositoryImpl.getTotalFeeGroupWise(sansthaKey,feeToDate,"",yearList);
		List<FeeBean> schoolList = new ArrayList<>();
		List<FeeBean> feeCountWithSchoolList = new ArrayList<>();
		FeeBean feeBean = new FeeBean();
		BigDecimal assignedFee = new BigDecimal(0);
		BigDecimal assignedFine = new BigDecimal(0);
		BigDecimal assignedChequeBounce=new BigDecimal(0);
		BigDecimal assignedTotal =new BigDecimal(0);
		
		BigDecimal paidFee =new BigDecimal(0);
		BigDecimal paidFine = new BigDecimal(0);
		BigDecimal paidChequeBounce=new BigDecimal(0);
		BigDecimal paidTotal =new BigDecimal(0);
		BigDecimal otherFee = new BigDecimal(0);
		BigDecimal excessFee = new BigDecimal(0);
		
		BigDecimal remainingFee =new BigDecimal(0);
		BigDecimal remainingFine = new BigDecimal(0);
		BigDecimal remainingChequeBounce=new BigDecimal(0);
		BigDecimal remainingTotal =new BigDecimal(0);
	
		BigDecimal clearanceFee =new BigDecimal(0);
		BigDecimal clearanceFine = new BigDecimal(0);
		BigDecimal clearanceChequeBounce=new BigDecimal(0);
		BigDecimal clearanceTotal =new BigDecimal(0);
	
		if(CollectionUtils.isNotEmpty(feeResultList)) {
			for(FeeBean feeResult : feeResultList) {
					FeeBean schoolBean = new FeeBean();
					schoolBean.setYearId(feeResult.getYearId());
					schoolBean.setYearName(feeResult.getYearName());
					schoolBean.setGroupId(feeResult.getGroupId());
					schoolBean.setGroupName(feeResult.getGroupName());
					schoolBean.setAssignedFee(feeResult.getAssignedFee());
					schoolBean.setAssignedFine(feeResult.getAssignedFine());
					List<FeeBean> chequeBounceList = feeRepositoryImpl.getChequeBounceChargesGroupWise(Integer.parseInt(feeResult.getGroupId()),yearList,feeToDate,"");
					schoolBean.setAssignedChequeBounceCharges("0");
					if(CollectionUtils.isNotEmpty(chequeBounceList)) {
						schoolBean.setAssignedChequeBounceCharges(chequeBounceList.get(0).getAssignedChequeBounceCharges());
					}
					schoolBean.setAssignedTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getAssignedFee())))
							.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getAssignedFine())))
							.add(BigDecimal.valueOf((long)Double.parseDouble(schoolBean.getAssignedChequeBounceCharges()))));
					schoolBean.setAssignedFeeInWords(Utillity.convertNumberToWord(feeResult.getAssignedFee()));
					schoolBean.setAssignedFineInWords(Utillity.convertNumberToWord(feeResult.getAssignedFine()));
					schoolBean.setAssignedChequeBounceChargesInWords(Utillity.convertNumberToWord(schoolBean.getAssignedChequeBounceCharges()));
					schoolBean.setAssignedTotalInWords(Utillity.convertNumberToWord(schoolBean.getAssignedTotal()));
					
					assignedFee=assignedFee.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getAssignedFee())));
					assignedFine= assignedFine.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getAssignedFine())));
					assignedChequeBounce=assignedChequeBounce.add(BigDecimal.valueOf((long)Double.parseDouble("0")));
					assignedTotal=assignedTotal.add(BigDecimal.valueOf((long)Double.parseDouble(schoolBean.getAssignedTotal())));

					schoolBean.setPaidFee(feeResult.getPaidFee());
					schoolBean.setPaidFine(feeResult.getPaidFine());
					schoolBean.setOtherFee(feeResult.getOtherFee());
					schoolBean.setExcessFee(feeResult.getExcessFee());
					schoolBean.setPaidChequeBounceCharges("0");
					if(CollectionUtils.isNotEmpty(chequeBounceList)) {
						schoolBean.setPaidChequeBounceCharges(chequeBounceList.get(0).getPaidChequeBounceCharges());
					}
					schoolBean.setPaidTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getPaidFee())))
							.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getPaidFine())))
							.add(BigDecimal.valueOf((long)Double.parseDouble(schoolBean.getPaidChequeBounceCharges()))));
					schoolBean.setPaidFeeInWords(Utillity.convertNumberToWord(feeResult.getPaidFee()));
					schoolBean.setPaidFineInWords(Utillity.convertNumberToWord(feeResult.getPaidFine()));
					schoolBean.setPaidChequeBounceChargesInWords(Utillity.convertNumberToWord(schoolBean.getPaidChequeBounceCharges()));
					schoolBean.setPaidTotalInWords(Utillity.convertNumberToWord(schoolBean.getPaidTotal()));
					
					paidFee=paidFee.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getPaidFee())));
					paidFine= paidFine.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getPaidFine())));
					paidChequeBounce=paidChequeBounce.add(BigDecimal.valueOf((long)Double.parseDouble("0")));
					paidTotal=paidTotal.add(BigDecimal.valueOf((long)Double.parseDouble(schoolBean.getPaidTotal())));
					otherFee=otherFee.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getOtherFee())));
					excessFee=excessFee.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getExcessFee())));
					
					schoolBean.setRemainingFee(feeResult.getRemainingFee());
					schoolBean.setRemainingFine(feeResult.getRemainingFine());
					schoolBean.setRemainingChequeBounceCharges("0");
					schoolBean.setRemainingTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getRemainingFee()))).add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getRemainingFine()))));
					schoolBean.setRemainingFeeInWords(Utillity.convertNumberToWord(feeResult.getRemainingFee()));
					schoolBean.setRemainingFineInWords(Utillity.convertNumberToWord(feeResult.getRemainingFine()));
					schoolBean.setRemainingChequeBounceChargesInWords(Utillity.convertNumberToWord(schoolBean.getRemainingChequeBounceCharges()));
					schoolBean.setRemainingTotalInWords(Utillity.convertNumberToWord(schoolBean.getRemainingTotal()));
					
					remainingFee=remainingFee.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getRemainingFee())));
					remainingFine=remainingFine.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getRemainingFine())));
					remainingChequeBounce=remainingChequeBounce.add(BigDecimal.valueOf((long)Double.parseDouble("0")));
					remainingTotal=remainingTotal.add(BigDecimal.valueOf((long)Double.parseDouble(schoolBean.getRemainingTotal())));


					schoolBean.setClearanceFee(feeResult.getClearanceFee());
					schoolBean.setClearanceFine(feeResult.getClearanceFine());
					schoolBean.setClearanceChequeBounceCharges("0");
					schoolBean.setClearanceTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getClearanceFee()))).add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getClearanceFine())))
							.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getDiscount()))));
					schoolBean.setClearanceFeeInWords(Utillity.convertNumberToWord(feeResult.getClearanceFee()));
					schoolBean.setClearanceFineInWords(Utillity.convertNumberToWord(feeResult.getClearanceFine()));
					schoolBean.setClearanceChequeBounceChargesInWords(Utillity.convertNumberToWord(schoolBean.getClearanceChequeBounceCharges()));
					schoolBean.setClearanceTotalInWords(Utillity.convertNumberToWord(schoolBean.getClearanceTotal()));
					
					clearanceFee=clearanceFee.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getClearanceFee())));
					clearanceFine=clearanceFine.add(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getClearanceFine())));
					clearanceChequeBounce=clearanceChequeBounce.add(BigDecimal.valueOf((long)Double.parseDouble("0")));
					clearanceTotal=clearanceTotal.add(BigDecimal.valueOf((long)Double.parseDouble(schoolBean.getClearanceTotal())));

					schoolList.add(schoolBean);
			}
		}
		
		feeBean.setAssignedFee(""+assignedFee);
		feeBean.setAssignedFine(""+assignedFine);
		feeBean.setAssignedChequeBounceCharges("0");
		feeBean.setAssignedTotal(""+(assignedFee.add(assignedFine)));
		feeBean.setAssignedFeeInWords(Utillity.convertNumberToWord(""+assignedFee));
		feeBean.setAssignedFineInWords(Utillity.convertNumberToWord(""+assignedFine));
		feeBean.setAssignedChequeBounceChargesInWords(Utillity.convertNumberToWord(""+assignedChequeBounce));
		feeBean.setAssignedTotalInWords(Utillity.convertNumberToWord(""+assignedTotal));
		
		feeBean.setPaidFee(""+paidFee);
		feeBean.setPaidFine(""+paidFine);
		feeBean.setOtherFee(""+otherFee);
		feeBean.setExcessFee(""+excessFee);
		feeBean.setPaidChequeBounceCharges("0");
		feeBean.setPaidTotal(""+(paidFee.add(paidFine)));
		feeBean.setPaidFeeInWords(Utillity.convertNumberToWord(""+paidFee));
		feeBean.setPaidFineInWords(Utillity.convertNumberToWord(""+paidFine));
		feeBean.setPaidChequeBounceChargesInWords(Utillity.convertNumberToWord(""+paidChequeBounce));
		feeBean.setPaidTotalInWords(Utillity.convertNumberToWord(""+paidTotal));
		
		feeBean.setRemainingFee(""+remainingFee);
		feeBean.setRemainingFine(""+remainingFine);
		feeBean.setRemainingChequeBounceCharges("0");
		feeBean.setRemainingTotal(""+(remainingFee.add(remainingFine)));
		feeBean.setRemainingFeeInWords(Utillity.convertNumberToWord(""+remainingFee));
		feeBean.setRemainingFineInWords(Utillity.convertNumberToWord(""+remainingFine));
		feeBean.setRemainingChequeBounceChargesInWords(Utillity.convertNumberToWord(""+remainingChequeBounce));
		feeBean.setRemainingTotalInWords(Utillity.convertNumberToWord(""+remainingTotal));
		
		feeBean.setClearanceFee(""+clearanceFee);
		feeBean.setClearanceFine(""+clearanceFine);
		feeBean.setClearanceChequeBounceCharges("0");
		feeBean.setClearanceTotal(""+clearanceTotal);
		feeBean.setClearanceFeeInWords(Utillity.convertNumberToWord(""+clearanceFee));
		feeBean.setClearanceFineInWords(Utillity.convertNumberToWord(""+clearanceFine));
		feeBean.setClearanceChequeBounceChargesInWords(Utillity.convertNumberToWord(""+clearanceChequeBounce));
		feeBean.setClearanceTotalInWords(Utillity.convertNumberToWord(""+clearanceTotal));

		feeBean.setPaidClearanceTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(feeBean.getPaidTotal())).add(BigDecimal.valueOf((long)Double.parseDouble(feeBean.getClearanceTotal())))));
		feeBean.setPaidClearanceTotalInWords(Utillity.convertNumberToWord(""+feeBean.getPaidClearanceTotal()));
		
		
		

//		if (!profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
//				|| profileRole.equalsIgnoreCase("Super officer")) {
//			List<Integer> schoolList1 = getSchoolList(mobNum, sansthaKey);
//			List<FeeBean> newSchoolWiseList = new ArrayList<FeeBean>();
//			for (int i = 0; i < schoolList.size(); i++) {
//				if (schoolList1.contains(schoolList.get(i).getSchoolId())){
//					newSchoolWiseList.add(schoolList.get(i));
//				}
//			}	
//			schoolList.clear();
//			schoolList = newSchoolWiseList; 
//		}
		
		feeBean.setGroupList(schoolList);
		feeCountWithSchoolList.add(feeBean);
		return feeCountWithSchoolList;
	}

	
}
