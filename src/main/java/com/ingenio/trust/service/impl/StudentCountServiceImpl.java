package com.ingenio.trust.service.impl;

import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.ingenio.trust.ConfigurationProperties;
import com.ingenio.trust.bean.AttendanceReportBean1;
import com.ingenio.trust.bean.DashBoardBean;
import com.ingenio.trust.bean.FeeBean;
import com.ingenio.trust.bean.GroupOfSchoolBean;
import com.ingenio.trust.bean.PrincipalDetailsBean;
import com.ingenio.trust.bean.PrincipalMenuBean;
import com.ingenio.trust.bean.SchoolBean;
import com.ingenio.trust.bean.StaffDetailsNewBean;
import com.ingenio.trust.bean.StudentCountBean;
import com.ingenio.trust.constants.IngenioCodes;
import com.ingenio.trust.constants.TrustDeskConstants;
import com.ingenio.trust.model.AppLanguageModel;
import com.ingenio.trust.model.SchoolMasterModel;
import com.ingenio.trust.model.StaffBasicDetailsModel;
import com.ingenio.trust.repository.AppLanguageRepository;
import com.ingenio.trust.repository.AppUserRepository;
import com.ingenio.trust.repository.PrincipalMenuRepository;
import com.ingenio.trust.repository.SchoolMasterRepository;
import com.ingenio.trust.repository.StaffBasicDetailsRepository;
import com.ingenio.trust.repository.StudentMasterRepository;
import com.ingenio.trust.repository.TrustMenuRepository;
import com.ingenio.trust.repository.impl.AttendanceDetailsRepositoryImpl;
import com.ingenio.trust.service.FeeService;
import com.ingenio.trust.service.StudentCountService;
import com.ingenio.trust.util.CommonUtlitity;
import com.ingenio.trust.util.CryptoUtilLib;
import com.ingenio.trust.util.Utillity;

@Component
//@PropertySource("file:C:/updatedwars/configuration/messages_en.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_mr.properties")
//@PropertySource("file:C:/updatedwars/configuration/messages_hn.properties")
@PropertySource("classpath:messages_en.properties")
@PropertySource("classpath:messages_mr.properties")
@PropertySource("classpath:messages_hn.properties")
public class StudentCountServiceImpl implements StudentCountService {

	@Autowired
	private StudentMasterRepository studentCountRepository;

	@Autowired
	private TrustMenuRepository trustMenuRepository;

	@Autowired
	private PrincipalMenuRepository principalMenuRepository;

	@Autowired
	private AttendanceDetailsRepositoryImpl attendanceRepositoryImpl;

	@Autowired
	private FeeService feeService;

	@Autowired
	private CommonUtlitity commonUtlitity;

	@Autowired
	private AppLanguageRepository appLanguageRepository;

	@Autowired
	private SchoolMasterRepository schoolMasterRepository;
	
	@Autowired
	StaffBasicDetailsRepository staffBasicDetailsRepository;
	@Autowired
	AppUserRepository appUserRepository;

	@Autowired
	private Environment env;

	CryptoUtilLib cry = new CryptoUtilLib();

	@Override
	public List<StudentCountBean> getSchoolWiseStrength(String sansthaKey, String yearName, String renewDate,
			Integer groupOfSchoolId, String mobileNo, String profileRole) {

				
		List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
		List<String> yearList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(appLanguageList)) {
			for (int i = 0; i < appLanguageList.size(); i++) {
				yearList.add(Utillity.convertEnglishToOther(yearName, "" + appLanguageList.get(i).getLanguageId()));
			}
		}
		StudentCountBean studentCountBean = new StudentCountBean();
		List<StudentCountBean> studentcountlist = new ArrayList<>();
		if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
				|| profileRole.equalsIgnoreCase("Super officer")) {
			if (groupOfSchoolId != 0) {
				studentCountBean = studentCountRepository.getGroupWiseTotalCount(sansthaKey, yearList, renewDate,
						groupOfSchoolId);
			} else {
				studentCountBean = studentCountRepository.getGroupWiseTotalCount(sansthaKey, yearList, renewDate);
			}

		} else {
			List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
			if (groupOfSchoolId != 0) {
				studentCountBean = studentCountRepository.getGroupWiseTotalCount(sansthaKey, yearList, renewDate,
						groupOfSchoolId, schoolList);
			} else {
				studentCountBean = studentCountRepository.getGroupWiseTotalCount(sansthaKey, yearList, renewDate,
						schoolList);
			}
		}

		Long totalCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
		Long boysCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
		Long girlsCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
		studentCountBean.setBoysCountInWords("" + boysCountInWords);
		studentCountBean.setGirlsCountInWords("" + girlsCountInWords);
		studentCountBean.setTotalCountInWords("" + totalCountInWords);

		StudentCountBean studentCountBean1 = new StudentCountBean();
		List<StudentCountBean> schoolWiseList = new ArrayList<>();
		if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
				|| profileRole.equalsIgnoreCase("Super officer")) {
			if (groupOfSchoolId != 0) {
				schoolWiseList = studentCountRepository.getGroupWiseSchoolStrength(sansthaKey, yearList, renewDate,
						groupOfSchoolId);
			} else {
				schoolWiseList = studentCountRepository.getGroupWiseSchoolStrength(sansthaKey, yearList, renewDate);
			}
		} else {
			List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
			if (groupOfSchoolId != 0) {
				schoolWiseList = studentCountRepository.getGroupWiseSchoolStrength(sansthaKey, yearList, renewDate,
						groupOfSchoolId, schoolList);
			} else {
				schoolWiseList = studentCountRepository.getGroupWiseSchoolStrength(sansthaKey, yearList, renewDate,
						schoolList);
			}
		}

		for (int i = 0; i < schoolWiseList.size(); i++) {
			
			try {
			StudentCountBean schoolNameBean = schoolWiseList.get(i);
			Long totalSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + schoolNameBean.getTotalCount()));
			Long boysSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + schoolNameBean.getBoysCount()));
			Long girlsSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + schoolNameBean.getGirlsCount()));
			schoolNameBean.setBoysCountInWords("" + boysSchoolCountInWords);
			schoolNameBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
			schoolNameBean.setTotalCountInWords("" + totalSchoolCountInWords);
			schoolNameBean.setYearId(commonUtlitity.getCurrentYear(schoolWiseList.get(i).getSchoolid(), renewDate));
			Integer staffCount=staffBasicDetailsRepository.getStaffCount(schoolWiseList.get(i).getSchoolid());
			schoolNameBean.setStaffCount(staffCount);
			
			
				schoolNameBean.setLongSchoolName(cry.decrypt(IngenioCodes.getSchoolname(),
						StringEscapeUtils.escapeJava(schoolNameBean.getLongSchoolName().replaceAll("[\\r\\n]+", ""))));
				schoolNameBean.setSchoolAdreess(cry.decrypt(IngenioCodes.getAddress(),
						StringEscapeUtils.escapeJava(schoolNameBean.getSchoolAdreess().replaceAll("[\\r\\n]+", ""))));
			
			
		//---------------------------------------principal Details---------------------------------------//	
			PrincipalDetailsBean principalDetailsBean=staffBasicDetailsRepository.getPrincipalDetails(schoolNameBean.getSchoolid());
			Integer appuserId=appUserRepository.getUserId(principalDetailsBean.getStaffId(),schoolNameBean.getSchoolid());
			
			principalDetailsBean.setPrincipalAppuserID(appuserId);

			principalDetailsBean.setPrincipalName(principalDetailsBean.getInitialName()+" "+principalDetailsBean.getFirstName()+" "+principalDetailsBean.getSecondName()+" "+principalDetailsBean.getLastName());
			String directoryPath = "" + ConfigurationProperties.staffPhotoUrl +File.separator+ schoolNameBean.getSchoolid();
			String extension = "jpg";
			principalDetailsBean.setPrincipalPhoto(directoryPath + File.separator + principalDetailsBean.getPrincipalregId() + TrustDeskConstants.DOT + extension);
			schoolNameBean.setPrincipalDetailsBean(principalDetailsBean);
	//----------------------------------Principal Details Over------------------------------------//
			
	//----------------------------------staff Details-----------------------------------------------//
			
			
//			List<StaffDetailsNewBean> staffDetailsNewBean=staffBasicDetailsRepository.getStaffDetails(schoolNameBean.getSchoolid());
//			for (StaffDetailsNewBean staffBasicDetailsModel2 : staffDetailsNewBean) {
//				Integer staffappuserId=appUserRepository.getUserId(staffBasicDetailsModel2.getStaffId(),schoolNameBean.getSchoolid());
//				staffBasicDetailsModel2.setStaffAppuserID(staffappuserId);
//
//				staffBasicDetailsModel2.setStaffName(staffBasicDetailsModel2.getInitialName()+" "+staffBasicDetailsModel2.getFirstName()+" "+staffBasicDetailsModel2.getSecondName()+" "+staffBasicDetailsModel2.getLastName());
//				String directoryPath1 = "" + ConfigurationProperties.staffPhotoUrl +File.separator+ schoolNameBean.getSchoolid();
//				String extension1 = "jpg";
//				staffBasicDetailsModel2.setStaffPhoto(directoryPath1 + File.separator + staffBasicDetailsModel2.getStaffregId() + TrustDeskConstants.DOT + extension1);
//			}
//			
			System.out.println("adsfadf");
	//-----------------------------------staff Details Over-------------------------------------------//		
		//	schoolNameBean.setStaffDetailsBean(staffDetailsNewBean);
			schoolWiseList.set(i, schoolNameBean);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		studentCountBean1.setSchoolWiseStudentList(schoolWiseList);
		studentcountlist.add(studentCountBean);
		studentcountlist.add(studentCountBean1);
		return studentcountlist;
	}

	@Override
	public List<StudentCountBean> getstandardWiseStrength(Integer schoolId, Integer yearId, String renewDate) {
		List<StudentCountBean> studentcountlist = new ArrayList<>();
		StudentCountBean studentCountBean = studentCountRepository.getSchoolWiseStrengthId(schoolId, yearId, renewDate);
		Long totalCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
		Long boysCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
		Long girlsCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
		studentCountBean.setBoysCountInWords("" + boysCountInWords);
		studentCountBean.setGirlsCountInWords("" + girlsCountInWords);
		studentCountBean.setTotalCountInWords("" + totalCountInWords);

		StudentCountBean studentCountBean1 = new StudentCountBean();
		List<StudentCountBean> standardWiseList = new ArrayList<>();
		standardWiseList = studentCountRepository.getStandardwiseStrength(schoolId, yearId, renewDate);
		for (int i = 0; i < standardWiseList.size(); i++) {
			StudentCountBean standardWiseBean = standardWiseList.get(i);
			Long totalSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + standardWiseBean.getTotalCount()));
			Long boysSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + standardWiseBean.getBoysCount()));
			Long girlsSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + standardWiseBean.getGirlsCount()));
			standardWiseBean.setBoysCountInWords("" + totalSchoolCountInWords);
			standardWiseBean.setGirlsCountInWords("" + boysSchoolCountInWords);
			standardWiseBean.setTotalCountInWords("" + girlsSchoolCountInWords);
			standardWiseList.set(i, standardWiseBean);
		}
		studentCountBean1.setStandardwiseStudentList(standardWiseList);
		studentcountlist.add(studentCountBean);
		studentcountlist.add(studentCountBean1);
		return studentcountlist;
	}

	@Override
	public List<StudentCountBean> getDivisionwiseStrength(Integer schoolId, Integer yearId, String renewDate,
			Integer standardId) {

		List<StudentCountBean> studentcountlist = new ArrayList<>();
		StudentCountBean standardCountBean = new StudentCountBean();
		standardCountBean = studentCountRepository.getStandardwiseStrengthwithId(schoolId, yearId, standardId,
				renewDate);
		Long totalCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + standardCountBean.getTotalCount()));
		Long boysCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + standardCountBean.getBoysCount()));
		Long girlsCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + standardCountBean.getGirlsCount()));
		standardCountBean.setBoysCountInWords("" + boysCountInWords);
		standardCountBean.setGirlsCountInWords("" + girlsCountInWords);
		standardCountBean.setTotalCountInWords("" + totalCountInWords);
		studentcountlist.add(standardCountBean);

		StudentCountBean studentCountBean1 = new StudentCountBean();
		List<StudentCountBean> divisionWiseList = new ArrayList<>();
		divisionWiseList = studentCountRepository.getDivisionwiseStrength(schoolId, yearId, standardId, renewDate);
		for (int i = 0; i < divisionWiseList.size(); i++) {
			StudentCountBean standardWiseBean = divisionWiseList.get(i);
			Long totalSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + standardWiseBean.getTotalCount()));
			Long boysSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + standardWiseBean.getBoysCount()));
			Long girlsSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + standardWiseBean.getGirlsCount()));
			standardWiseBean.setBoysCountInWords("" + boysSchoolCountInWords);
			standardWiseBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
			standardWiseBean.setTotalCountInWords("" + totalSchoolCountInWords);
			divisionWiseList.set(i, standardWiseBean);
		}
		studentCountBean1.setDivisionwiseList(divisionWiseList);
		studentcountlist.add(studentCountBean1);
		return studentcountlist;
	}

	@Override
	public List<StudentCountBean> getRelisionWiseCount(String sansthaKey, String yearName, String renewDate) {

		List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
		List<String> yearList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(appLanguageList)) {
			for (int i = 0; i < appLanguageList.size(); i++) {
				yearList.add(Utillity.convertEnglishToOther(yearName, "" + appLanguageList.get(i).getLanguageId()));
			}
		}
		StudentCountBean studentCountBean = studentCountRepository.getSansthaWiseTotalCount(sansthaKey, yearList,
				renewDate);

		StudentCountBean religionCountBean = studentCountRepository.getSansthaWiseReligionCount(sansthaKey, yearName,
				renewDate);

		List<StudentCountBean> updatedList = new ArrayList<>();
		updatedList.add(studentCountBean);
		updatedList.add(religionCountBean);

		return updatedList;

	}

	@Override
	public List<DashBoardBean> getDashboardCount(String sansthaKey, String yearName, String renewdate,
			String language) {
		List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
		List<String> yearList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(appLanguageList)) {
			for (int i = 0; i < appLanguageList.size(); i++) {
				yearList.add(Utillity.convertEnglishToOther(yearName, ""+ appLanguageList.get(i).getLanguageId()));
			}
		}
		StudentCountBean menuListCountBean = new StudentCountBean();
		List<PrincipalMenuBean> principalMenuList = principalMenuRepository.getMenuList();
		if (CollectionUtils.isNotEmpty(principalMenuList)
				&& (language.equalsIgnoreCase("Mr") || language.equalsIgnoreCase("Hi"))) {
			principalMenuList.stream().forEach(menu -> {
				String menuName = env
						.getProperty(language + TrustDeskConstants.DOT + menu.getMenuName().replaceAll("\\s+", ""));
				if (StringUtils.isNotEmpty(menuName)) {
					menu.setMenuName(menuName);
				}
			});
		}
		menuListCountBean.setPrincipalMenuList(principalMenuList);

		StudentCountBean studentCountBean = studentCountRepository.getSansthaWiseTotalCount(sansthaKey, yearList,
				renewdate);
		List<AttendanceReportBean1> presentList = attendanceRepositoryImpl.getSansthaWisePresentReport(sansthaKey,
				renewdate);
		List<AttendanceReportBean1> absentList = attendanceRepositoryImpl.getSansthaWiseAbsentReport(sansthaKey,
				renewdate);

		List<DashBoardBean> finalList = new ArrayList<>();
		DashBoardBean dashBoardBean = new DashBoardBean();
		dashBoardBean.setMenuList(principalMenuList);
		dashBoardBean.setTitle("Till Date # Student Summary");

		List<DashBoardBean> countList = new ArrayList<>();
		DashBoardBean countBean = new DashBoardBean();
		countBean.setLabel(TrustDeskConstants.TOTAL);
		if ((language.equalsIgnoreCase("Mr") || language.equalsIgnoreCase("Hi"))) {
			String total = env.getProperty(language + "." + TrustDeskConstants.TOTAL);
			if (StringUtils.isNotEmpty(total)) {
				countBean.setLabel(total);
			}
		}
		countBean.setCount("" + studentCountBean.getTotalCount());
		countBean.setCountInWord("" + studentCountBean.getTotalCount());
		countList.add(countBean);

		DashBoardBean countBean1 = new DashBoardBean();
		countBean1.setLabel("Girls");
		countBean1.setCount("" + studentCountBean.getGirlsCount());
		countBean1.setCountInWord("" + studentCountBean.getGirlsCount());
		countList.add(countBean1);

		DashBoardBean countBean2 = new DashBoardBean();
		countBean2.setLabel("Boys");
		countBean2.setCount("" + studentCountBean.getBoysCount());
		countBean2.setCountInWord("" + studentCountBean.getBoysCount());
		countList.add(countBean2);
		finalList.add(dashBoardBean);

		dashBoardBean.setCountList(countList);

		dashBoardBean = new DashBoardBean();
		dashBoardBean.setMenuList(principalMenuList);
		dashBoardBean.setTitle("Today's # Attendance");

		countList = new ArrayList<>();
		countBean = new DashBoardBean();
		countBean.setLabel("Total Students");
		countBean.setCount("" + (Integer.parseInt(presentList.get(0).getTotalCount())
				+ Integer.parseInt(absentList.get(0).getTotalCount())));
		countBean.setCountInWord("" + (Integer.parseInt(presentList.get(0).getTotalCount())
				+ Integer.parseInt(absentList.get(0).getTotalCount())));
		countList.add(countBean);

		countBean1 = new DashBoardBean();
		countBean1.setLabel("Present");
		countBean1.setCount("" + presentList.get(0).getTotalCount());
		countBean1.setCountInWord("" + presentList.get(0).getTotalCount());
		countList.add(countBean1);

		countBean2 = new DashBoardBean();
		countBean2.setLabel("Absent");
		countBean2.setCount("" + (Integer.parseInt(absentList.get(0).getTotalCount())));
		countBean2.setCountInWord("" + (Integer.parseInt(absentList.get(0).getTotalCount())));
		countList.add(countBean2);

		dashBoardBean.setCountList(countList);
		finalList.add(dashBoardBean);

		List<FeeBean> feeList = feeService.getTotalFeeGroupWise(sansthaKey, yearName, renewdate,null,null);
		dashBoardBean = new DashBoardBean();
		dashBoardBean.setMenuList(principalMenuList);
		dashBoardBean.setTitle("Till Date # Fee Collection");

		countList = new ArrayList<>();
		countBean = new DashBoardBean();
		countBean.setLabel(TrustDeskConstants.TOTAL);
		countBean.setCount(feeList.get(0).getAssignedTotal());
		countBean.setCountInWord(feeList.get(0).getAssignedTotalInWords());
		countList.add(countBean);

		countBean1 = new DashBoardBean();
		countBean1.setLabel("Received");
		countBean1.setCount(feeList.get(0).getPaidClearanceTotal());
		countBean1.setCountInWord(feeList.get(0).getPaidClearanceTotalInWords());
		countList.add(countBean1);

		countBean2 = new DashBoardBean();
		countBean2.setLabel("Remaining");
		countBean2.setCount(feeList.get(0).getRemainingTotal());
		countBean2.setCountInWord(feeList.get(0).getRemainingTotalInWords());
		countList.add(countBean2);

		dashBoardBean.setCountList(countList);
		finalList.add(dashBoardBean);

		if (CollectionUtils.isNotEmpty(finalList)
				&& (language.equalsIgnoreCase("Mr") || language.equalsIgnoreCase("Hi"))) {
			finalList.stream().forEach(menu -> {
				String menuName = env.getProperty(
						language + TrustDeskConstants.DOT + menu.getTitle().replaceAll("\\s+", "").replaceAll("#", ""));
				if (StringUtils.isNotEmpty(menuName)) {
					menu.setTitle(menuName);
				}

				if (CollectionUtils.isNotEmpty(menu.getCountList())) {
					menu.getCountList().stream().forEach(count -> {
						String label = env.getProperty(language + TrustDeskConstants.DOT
								+ count.getLabel().replaceAll("\\s+", "").replaceAll("#", ""));
						if (StringUtils.isNotEmpty(label)) {
							count.setLabel(label);
						}
					});
				}
			});
		}

		return finalList;

	}

	@Override
	public List<StudentCountBean> getgroupOfSchoolWiseStrength(String sansthaKey, String yearName, String renewDate,
			String mobileNo, String profileRole) {
		 profileRole=profileRole.replace("+", "@");
			if(profileRole.equals("Super@Officer")||profileRole.equals("Super@Admin")){
				String[] role=profileRole.split("@");
				profileRole=role[0]+" "+role[1];
				}
		List<StudentCountBean> studentcountlist = new ArrayList<>();
		StudentCountBean studentCountBean = new StudentCountBean();
		List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
		List<String> yearList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(appLanguageList)) {
			for (int i = 0; i < appLanguageList.size(); i++) {
				yearList.add(Utillity.convertEnglishToOther(yearName, "" + appLanguageList.get(i).getLanguageId()));
			}
		}

		if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
				|| profileRole.equalsIgnoreCase("Super Officer")) {
			studentCountBean = studentCountRepository.getSansthaWiseTotalCount(sansthaKey, yearList, renewDate);
		} else {
			List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
			studentCountBean = studentCountRepository.getSansthaWiseTotalCount(sansthaKey, yearList, renewDate,
					schoolList);
		}
		
//		String numericPart = Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()).replaceAll("[^0-9.]", ""); 
//		double numericValue = Double.parseDouble(numericPart); 
//		Long totalCountInWords = (long) (numericValue * 1000); 
//		
////		Long totalCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
//		Long boysCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
//		Long girlsCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
//		studentCountBean.setBoysCountInWords("" + boysCountInWords);
//		studentCountBean.setGirlsCountInWords("" + girlsCountInWords);
//		studentCountBean.setTotalCountInWords("" + totalCountInWords);

		StudentCountBean groupWiseCountBean = new StudentCountBean();

		List<GroupOfSchoolBean> GroupOfSchoolFilterList = new ArrayList<>();

		if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
				|| profileRole.equalsIgnoreCase("Super officer")) {
			GroupOfSchoolFilterList = studentCountRepository.getGroupOfSchoolFilterList(sansthaKey, yearList,
					renewDate);
		} else {
			List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
			GroupOfSchoolFilterList = studentCountRepository.getGroupOfSchoolFilterList(sansthaKey, yearList, renewDate,
					schoolList);
		}
		for (int i = 0; i < GroupOfSchoolFilterList.size(); i++) {
			GroupOfSchoolBean groupOfSchoolBean = GroupOfSchoolFilterList.get(i);
//			Long totalSchoolCountInWords = Long
//					.parseLong(Utillity.convertNumberToWord("" + groupOfSchoolBean.getTotalCount()));
//			Long boysSchoolCountInWords = Long
//					.parseLong(Utillity.convertNumberToWord("" + groupOfSchoolBean.getBoysCount()));
//			Long girlsSchoolCountInWords = Long
//					.parseLong(Utillity.convertNumberToWord("" + groupOfSchoolBean.getGirlsCount()));
//			groupOfSchoolBean.setBoysCountInWords("" + boysSchoolCountInWords);
//			groupOfSchoolBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
//			groupOfSchoolBean.setTotalCountInWords("" + totalSchoolCountInWords);
			GroupOfSchoolFilterList.set(i, groupOfSchoolBean);
		}
		groupWiseCountBean.setGroupWiseList(GroupOfSchoolFilterList);
		studentcountlist.add(studentCountBean);
		studentcountlist.add(groupWiseCountBean);
		return studentcountlist;
	}

	@Override
	public List<String> getYearForSanstha(String sansthaKey, String renewDate) {
		List<SchoolMasterModel> schoolMasterList = schoolMasterRepository.findBySansthaKey(sansthaKey);
		List<String> yearList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(schoolMasterList)) {
			for (int i = 0; i < schoolMasterList.size(); i++) {
				String yearName = commonUtlitity.getCurrentYearName(schoolMasterList.get(i).getSchoolid(), renewDate);
				if (!yearList.contains(Utillity.convertOtherToEnglish(yearName))) {
					yearList.add(yearName);
				}
			}
		}
		return yearList;
	}

	public List<Integer> getSchoolList(String MobileNo, String SansthaKey) {
		List<SchoolBean> schoolMasterList = schoolMasterRepository.getSchoolList(SansthaKey, MobileNo);
		List<Integer> schoolList = new ArrayList<>();
		for (int i = 0; i < schoolMasterList.size(); i++) {
			schoolList.add(schoolMasterList.get(i).getSchoolId());
		}
		return schoolList;
	}

	@Override
	public List<DashBoardBean> getNewDashboardCount1(String sansthaKey, String yearName, String renewdate,
			String language, String mobileNo,String profileRole,Integer isReact) {
		
		System.out.println(profileRole);
		 profileRole=profileRole.replace("+", "@");
		if(profileRole.equals("Super@Officer")||profileRole.equals("Super@Admin")){
			String[] role=profileRole.split("@");
			profileRole=role[0]+" "+role[1];
			}

		List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
		System.out.println("--------------------------------"+appLanguageList);
		List<String> yearList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(appLanguageList)) {
			for (int i = 0; i < appLanguageList.size(); i++) {
				yearList.add(Utillity.convertEnglishToOther(yearName, "" + appLanguageList.get(i).getLanguageId()));
			}
		}
		
		System.out.println("yearList------------------------"+yearList);
		StudentCountBean menuListCountBean = new StudentCountBean();
		List<PrincipalMenuBean> principalMenuList1 = new ArrayList<>();
		List<PrincipalMenuBean> groupList = trustMenuRepository.getGroups();
		String isReactFlag;
		if(isReact == 1) {
			isReactFlag = "'1','2'";
		}else {
			isReactFlag = "'0','2'";
		}
		if (CollectionUtils.isNotEmpty(groupList)) {
			groupList.stream().forEach(group -> {
				PrincipalMenuBean principalMenuBean = new PrincipalMenuBean();
				
//				List<PrincipalMenuBean> principalMenuList = trustMenuRepository.getMenuList(group.getMenuId());
				List<PrincipalMenuBean> principalMenuList = attendanceRepositoryImpl.getMenuList1(group.getMenuId(),isReactFlag,sansthaKey);
				
				List<PrincipalMenuBean> globalPrincipalMenuList = attendanceRepositoryImpl.getGlobalMenuList1(group.getMenuId(),isReactFlag);
				
				List<Integer> notForSchoolIds = attendanceRepositoryImpl.getMenuListNotForSchool(sansthaKey);
				for (PrincipalMenuBean globalMenu : globalPrincipalMenuList) {
					if(notForSchoolIds.contains(globalMenu.getMenuId())) {
						continue;
					}else {
					principalMenuList.add(globalMenu);
					}
				}
				
				if (CollectionUtils.isNotEmpty(principalMenuList)
						&& (language.equalsIgnoreCase("Mr") || language.equalsIgnoreCase("Hi"))) {
					principalMenuList.stream().forEach(menu -> {
						String menuName = env.getProperty(
								language + TrustDeskConstants.DOT + menu.getMenuName().replaceAll("\\s+", ""));
						if (StringUtils.isNotEmpty(menuName)) {
							menu.setMenuName(menuName);
						}
					});
				}
				if (CollectionUtils.isNotEmpty(principalMenuList)) {
					principalMenuBean.setPrincipalMenuList(principalMenuList);
					principalMenuBean.setGroupdId(group.getMenuId());
					principalMenuBean.setGroupName(group.getMenuName());
					principalMenuBean.setIconPath(group.getIconPath());
					
					principalMenuList1.add(principalMenuBean);
				}

			});
		}

		menuListCountBean.setPrincipalMenuList(principalMenuList1);
		StudentCountBean studentCountBean =new StudentCountBean();
		if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
				|| profileRole.equalsIgnoreCase("Super Officer") || profileRole.equalsIgnoreCase("ROLE SUPEROFFICER")) {
			 studentCountBean = studentCountRepository.getSansthaWiseTotalCount(sansthaKey, yearList,
					renewdate);
		} else {
			List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
			 studentCountBean = studentCountRepository.getSansthaWiseTotalCount(sansthaKey, yearList,
					renewdate, schoolList);
		}
	
		List<AttendanceReportBean1> presentList = attendanceRepositoryImpl.getSansthaWisePresentReport(sansthaKey,
				renewdate);
		List<AttendanceReportBean1> absentList = attendanceRepositoryImpl.getSansthaWiseAbsentReport(sansthaKey,
				renewdate);

		List<DashBoardBean> finalList = new ArrayList<>();
		DashBoardBean dashBoardBean = new DashBoardBean();
		dashBoardBean.setMenuList(principalMenuList1);
		dashBoardBean.setTitle("Till Date # Student Summary");

		List<DashBoardBean> countList = new ArrayList<>();
		DashBoardBean countBean = new DashBoardBean();
		countBean.setLabel(TrustDeskConstants.TOTAL);
		if ((language.equalsIgnoreCase("Mr") || language.equalsIgnoreCase("Hi"))) {
			String total = env.getProperty(language + TrustDeskConstants.DOT + TrustDeskConstants.TOTAL);
			if (StringUtils.isNotEmpty(total)) {
				countBean.setLabel(total);
			}
		}
		countBean.setCount("" + studentCountBean.getTotalCount());
		countBean.setCountInWord("" + studentCountBean.getTotalCount());
		countList.add(countBean);

		DashBoardBean countBean1 = new DashBoardBean();
		countBean1.setLabel("Girls");
		countBean1.setCount("" + studentCountBean.getGirlsCount());
		countBean1.setCountInWord("" + studentCountBean.getGirlsCount());
		countList.add(countBean1);

		DashBoardBean countBean2 = new DashBoardBean();
		countBean2.setLabel("Boys");
		countBean2.setCount("" + studentCountBean.getBoysCount());
		countBean2.setCountInWord("" + studentCountBean.getBoysCount());
		countList.add(countBean2);
		finalList.add(dashBoardBean);

		dashBoardBean.setCountList(countList);

		dashBoardBean = new DashBoardBean();
		dashBoardBean.setMenuList(principalMenuList1);
		dashBoardBean.setTitle("Today's # Attendance");

		countList = new ArrayList<>();
		countBean = new DashBoardBean();
		countBean.setLabel("Total Students");
		countBean.setCount("" + (Integer.parseInt(presentList.get(0).getTotalCount())
				+ Integer.parseInt(absentList.get(0).getTotalCount())));
		countBean.setCountInWord("" + (Integer.parseInt(presentList.get(0).getTotalCount())
				+ Integer.parseInt(absentList.get(0).getTotalCount())));
		countList.add(countBean);

		countBean1 = new DashBoardBean();
		countBean1.setLabel("Present");
		countBean1.setCount("" + presentList.get(0).getTotalCount());
		countBean1.setCountInWord("" + presentList.get(0).getTotalCount());
		countList.add(countBean1);

		countBean2 = new DashBoardBean();
		countBean2.setLabel("Absent");
		countBean2.setCount("" + (Integer.parseInt(absentList.get(0).getTotalCount())));
		countBean2.setCountInWord("" + (Integer.parseInt(absentList.get(0).getTotalCount())));
		countList.add(countBean2);

		dashBoardBean.setCountList(countList);
		finalList.add(dashBoardBean);

		List<FeeBean> feeList = feeService.getTotalFeeGroupWise(sansthaKey, yearName, renewdate,mobileNo,profileRole);
		dashBoardBean = new DashBoardBean();
		dashBoardBean.setMenuList(principalMenuList1);
		dashBoardBean.setTitle("Till Date # Fee Collection");

		countList = new ArrayList<>();
		countBean = new DashBoardBean();
		countBean.setLabel(TrustDeskConstants.TOTAL);
		countBean.setCount(feeList.get(0).getAssignedTotal());
		countBean.setCountInWord(feeList.get(0).getAssignedTotalInWords());
		countList.add(countBean);

		countBean1 = new DashBoardBean();
		countBean1.setLabel("Received");
		countBean1.setCount(feeList.get(0).getPaidClearanceTotal());
		countBean1.setCountInWord(feeList.get(0).getPaidClearanceTotalInWords());
		countList.add(countBean1);

		countBean2 = new DashBoardBean();
		countBean2.setLabel("Remaining");
		countBean2.setCount(feeList.get(0).getRemainingTotal());
		countBean2.setCountInWord(feeList.get(0).getRemainingTotalInWords());
		countList.add(countBean2);

		dashBoardBean.setCountList(countList);
		finalList.add(dashBoardBean);

		if (CollectionUtils.isNotEmpty(finalList)
				&& (language.equalsIgnoreCase("Mr") || language.equalsIgnoreCase("Hi"))) {
			finalList.stream().forEach(menu -> {
				String menuName = env.getProperty(
						language + TrustDeskConstants.DOT + menu.getTitle().replaceAll("\\s+", "").replaceAll("#", ""));
				if (StringUtils.isNotEmpty(menuName)) {
					menu.setTitle(menuName);
				}

				if (CollectionUtils.isNotEmpty(menu.getCountList())) {
					menu.getCountList().stream().forEach(count -> {
						String label = env.getProperty(language + TrustDeskConstants.DOT
								+ count.getLabel().replaceAll("\\s+", "").replaceAll("#", ""));
						if (StringUtils.isNotEmpty(label)) {
							count.setLabel(label);
						}
					});
				}
			});
		}

		return finalList;
	}

	@Override
	public List<DashBoardBean> getNewDashboardCount(String sansthaKey, String yearName, String renewdate,
			String language, String mobileNo,String profileRole,Integer isReact) {
		List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
		System.out.println("--------------------------------"+appLanguageList);
		List<String> yearList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(appLanguageList)) {
			for (int i = 0; i < appLanguageList.size(); i++) {
				yearList.add(Utillity.convertEnglishToOther(yearName, "" + appLanguageList.get(i).getLanguageId()));
			}
		}
		
		System.out.println("yearList------------------------"+yearList);
		StudentCountBean menuListCountBean = new StudentCountBean();
		List<PrincipalMenuBean> principalMenuList1 = new ArrayList<>();
		List<PrincipalMenuBean> groupList = trustMenuRepository.getGroups();
		String isReactFlag;
		if(isReact == 1) {
			isReactFlag = "'1','2'";
		}else {
			isReactFlag = "'0','2'";
		}
		if (CollectionUtils.isNotEmpty(groupList)) {
			groupList.stream().forEach(group -> {
				PrincipalMenuBean principalMenuBean = new PrincipalMenuBean();
				
//				List<PrincipalMenuBean> principalMenuList = trustMenuRepository.getMenuList(group.getMenuId());
				List<PrincipalMenuBean> principalMenuList = attendanceRepositoryImpl.getMenuList(group.getMenuId(),isReactFlag);
				
				
				if (CollectionUtils.isNotEmpty(principalMenuList)
						&& (language.equalsIgnoreCase("Mr") || language.equalsIgnoreCase("Hi"))) {
					principalMenuList.stream().forEach(menu -> {
						String menuName = env.getProperty(
								language + TrustDeskConstants.DOT + menu.getMenuName().replaceAll("\\s+", ""));
						if (StringUtils.isNotEmpty(menuName)) {
							menu.setMenuName(menuName);
						}
					});
				}
				if (CollectionUtils.isNotEmpty(principalMenuList)) {
					principalMenuBean.setPrincipalMenuList(principalMenuList);
					principalMenuBean.setGroupdId(group.getMenuId());
					principalMenuBean.setGroupName(group.getMenuName());
					principalMenuBean.setIconPath(group.getIconPath());
					
					principalMenuList1.add(principalMenuBean);
				}

			});
		}

		menuListCountBean.setPrincipalMenuList(principalMenuList1);
		StudentCountBean studentCountBean =new StudentCountBean();
		if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
				|| profileRole.equalsIgnoreCase("Super Officer") || profileRole.equalsIgnoreCase("ROLE SUPEROFFICER")) {
			 studentCountBean = studentCountRepository.getSansthaWiseTotalCount(sansthaKey, yearList,
					renewdate);
		} else {
			List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
			 studentCountBean = studentCountRepository.getSansthaWiseTotalCount(sansthaKey, yearList,
					renewdate, schoolList);
		}
		
		List<AttendanceReportBean1> presentList = attendanceRepositoryImpl.getSansthaWisePresentReport(sansthaKey,
				renewdate);
		List<AttendanceReportBean1> absentList = attendanceRepositoryImpl.getSansthaWiseAbsentReport(sansthaKey,
				renewdate);

		List<DashBoardBean> finalList = new ArrayList<>();
		DashBoardBean dashBoardBean = new DashBoardBean();
		dashBoardBean.setMenuList(principalMenuList1);
		dashBoardBean.setTitle("Till Date # Student Summary");

		List<DashBoardBean> countList = new ArrayList<>();
		DashBoardBean countBean = new DashBoardBean();
		countBean.setLabel(TrustDeskConstants.TOTAL);
		if ((language.equalsIgnoreCase("Mr") || language.equalsIgnoreCase("Hi"))) {
			String total = env.getProperty(language + TrustDeskConstants.DOT + TrustDeskConstants.TOTAL);
			if (StringUtils.isNotEmpty(total)) {
				countBean.setLabel(total);
			}
		}
		countBean.setCount("" + studentCountBean.getTotalCount());
		countBean.setCountInWord("" + studentCountBean.getTotalCount());
		countList.add(countBean);

		DashBoardBean countBean1 = new DashBoardBean();
		countBean1.setLabel("Girls");
		countBean1.setCount("" + studentCountBean.getGirlsCount());
		countBean1.setCountInWord("" + studentCountBean.getGirlsCount());
		countList.add(countBean1);

		DashBoardBean countBean2 = new DashBoardBean();
		countBean2.setLabel("Boys");
		countBean2.setCount("" + studentCountBean.getBoysCount());
		countBean2.setCountInWord("" + studentCountBean.getBoysCount());
		countList.add(countBean2);
		finalList.add(dashBoardBean);

		dashBoardBean.setCountList(countList);

		dashBoardBean = new DashBoardBean();
		dashBoardBean.setMenuList(principalMenuList1);
		dashBoardBean.setTitle("Today's # Attendance");

		countList = new ArrayList<>();
		countBean = new DashBoardBean();
		countBean.setLabel("Total Students");
		countBean.setCount("" + (Integer.parseInt(presentList.get(0).getTotalCount())
				+ Integer.parseInt(absentList.get(0).getTotalCount())));
		countBean.setCountInWord("" + (Integer.parseInt(presentList.get(0).getTotalCount())
				+ Integer.parseInt(absentList.get(0).getTotalCount())));
		countList.add(countBean);

		countBean1 = new DashBoardBean();
		countBean1.setLabel("Present");
		countBean1.setCount("" + presentList.get(0).getTotalCount());
		countBean1.setCountInWord("" + presentList.get(0).getTotalCount());
		countList.add(countBean1);

		countBean2 = new DashBoardBean();
		countBean2.setLabel("Absent");
		countBean2.setCount("" + (Integer.parseInt(absentList.get(0).getTotalCount())));
		countBean2.setCountInWord("" + (Integer.parseInt(absentList.get(0).getTotalCount())));
		countList.add(countBean2);

		dashBoardBean.setCountList(countList);
		finalList.add(dashBoardBean);

		List<FeeBean> feeList = feeService.getTotalFeeGroupWise(sansthaKey, yearName, renewdate,mobileNo,profileRole);
		dashBoardBean = new DashBoardBean();
		dashBoardBean.setMenuList(principalMenuList1);
		dashBoardBean.setTitle("Till Date # Fee Collection");

		countList = new ArrayList<>();
		countBean = new DashBoardBean();
		countBean.setLabel(TrustDeskConstants.TOTAL);
		countBean.setCount(feeList.get(0).getAssignedTotal());
		countBean.setCountInWord(feeList.get(0).getAssignedTotalInWords());
		countList.add(countBean);

		countBean1 = new DashBoardBean();
		countBean1.setLabel("Received");
		countBean1.setCount(feeList.get(0).getPaidClearanceTotal());
		countBean1.setCountInWord(feeList.get(0).getPaidClearanceTotalInWords());
		countList.add(countBean1);

		countBean2 = new DashBoardBean();
		countBean2.setLabel("Remaining");
		countBean2.setCount(feeList.get(0).getRemainingTotal());
		countBean2.setCountInWord(feeList.get(0).getRemainingTotalInWords());
		countList.add(countBean2);

		dashBoardBean.setCountList(countList);
		finalList.add(dashBoardBean);

		if (CollectionUtils.isNotEmpty(finalList)
				&& (language.equalsIgnoreCase("Mr") || language.equalsIgnoreCase("Hi"))) {
			finalList.stream().forEach(menu -> {
				String menuName = env.getProperty(
						language + TrustDeskConstants.DOT + menu.getTitle().replaceAll("\\s+", "").replaceAll("#", ""));
				if (StringUtils.isNotEmpty(menuName)) {
					menu.setTitle(menuName);
				}

				if (CollectionUtils.isNotEmpty(menu.getCountList())) {
					menu.getCountList().stream().forEach(count -> {
						String label = env.getProperty(language + TrustDeskConstants.DOT
								+ count.getLabel().replaceAll("\\s+", "").replaceAll("#", ""));
						if (StringUtils.isNotEmpty(label)) {
							count.setLabel(label);
						}
					});
				}
			});
		}

		return finalList;
	}

	@Override
	public List<StaffDetailsNewBean> getSchoolWiseStaffDetails(Integer schoolid) {
		List<StaffDetailsNewBean> staffDetailsNewBean=staffBasicDetailsRepository.getStaffDetails(schoolid);
		for (StaffDetailsNewBean staffBasicDetailsModel2 : staffDetailsNewBean) {
			Integer staffappuserId=appUserRepository.getUserId(staffBasicDetailsModel2.getStaffId(),schoolid);
			staffBasicDetailsModel2.setStaffAppuserID(staffappuserId);

			staffBasicDetailsModel2.setStaffName(staffBasicDetailsModel2.getInitialName()+" "+staffBasicDetailsModel2.getFirstName()+" "+staffBasicDetailsModel2.getSecondName()+" "+staffBasicDetailsModel2.getLastName());
			String directoryPath1 = "" + ConfigurationProperties.staffPhotoUrl +File.separator+ schoolid;
			String extension1 = "jpg";
			staffBasicDetailsModel2.setStaffPhoto(directoryPath1 + File.separator + staffBasicDetailsModel2.getStaffregId() + TrustDeskConstants.DOT + extension1);
		}

		return staffDetailsNewBean;
	}

	@Override
	public List<SchoolBean> getAllSchools(String sansthaKey,Integer groupOfSchoolId) {

		List<SchoolBean> schoolBean=new ArrayList<SchoolBean>();
		if(groupOfSchoolId>0) {
			 schoolBean= schoolMasterRepository.getSchoolsAccordingGroupOfSchoolId(sansthaKey,groupOfSchoolId);
		}else {
			 schoolBean= schoolMasterRepository.getSchools(sansthaKey);
		}
		for (SchoolBean schoolBean2 : schoolBean) {
			
			try {
				schoolBean2.setSchoolName(cry.decrypt(IngenioCodes.getSchoolname(),
						StringEscapeUtils.escapeJava(schoolBean2.getSchoolName().replaceAll("[\\r\\n]+", ""))));
			} catch (Exception  e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
			
			}
		}
		
		return schoolBean;
	}

}