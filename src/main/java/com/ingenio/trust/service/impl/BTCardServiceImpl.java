package com.ingenio.trust.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections4.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.trust.bean.BTCardRecordDetails;
import com.ingenio.trust.bean.BTCardRecordFilterParam;
import com.ingenio.trust.bean.BTCardRecordPage;
import com.ingenio.trust.bean.Paging;
import com.ingenio.trust.model.BTCardModel;
import com.ingenio.trust.model.BTCardRecordModel;
import com.ingenio.trust.repository.BTCardReportRepository;
import com.ingenio.trust.repository.BTCardRepository;
import com.ingenio.trust.service.BTCardService;

@Service
@Transactional
public class BTCardServiceImpl implements BTCardService {

	@Autowired
	BTCardReportRepository bTCardReportRepository;
	
	@Autowired
	BTCardRepository bTCardRepository;

	@Autowired
	private ModelMapper mapper;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public BTCardRecordPage getBTCardDetails(BTCardRecordFilterParam filterParams) {
		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<BTCardModel> criteriaQuery = criteria.createQuery(BTCardModel.class);
		Root<BTCardModel> rootBTCardModel = criteriaQuery
				.from(BTCardModel.class);
		Predicate filterPredicate = criteria.conjunction();
		
		if (CollectionUtils.isNotEmpty(filterParams.getBTNumbers())) {
            In<String> btNumber_in = criteria.in(rootBTCardModel.get("btNumber"));
            for (String btNumber : filterParams.getBTNumbers()) {
            	btNumber_in.value(btNumber);
            }
            filterPredicate = criteria.and(filterPredicate, btNumber_in);
        }

		if (filterParams.getSchoolId() != null) {
			Predicate schoolIdFilter = criteria.equal(rootBTCardModel.get("schoolId"),
					filterParams.getSchoolId());
			filterPredicate = criteria.and(filterPredicate, schoolIdFilter);
		}
		if (filterParams.getSchoolTypeId() != null) {
			Predicate schoolTypeIdFilter = criteria.equal(rootBTCardModel.get("schoolTypeId"),
					filterParams.getSchoolTypeId());
			filterPredicate = criteria.and(filterPredicate, schoolTypeIdFilter);
		}
		if (filterParams.getStatus() != null) {
			Predicate statusFilter = criteria.equal(rootBTCardModel.get("status"),
					filterParams.getStatus());
			filterPredicate = criteria.and(filterPredicate, statusFilter);
		}
		criteriaQuery.where(filterPredicate);
		TypedQuery<BTCardModel> query = entityManager.createQuery(criteriaQuery);

		if (filterParams.getSize() > 0) {
			/*
			 * my mysql - http://www.mysqltutorial.org/mysql-limit.aspx
			 */
			int offset = 0;
			if (filterParams.getPage() > 0) {
				offset = filterParams.getPage() * filterParams.getSize();
			}

			query.setFirstResult(offset);
			query.setMaxResults(filterParams.getSize());
		}

		List<BTCardModel> entities = query.getResultList();

		// Count query to get the total number of pages
		CriteriaQuery<Long> countQuery = criteria.createQuery(Long.class);
		countQuery.select(criteria.count(countQuery.from(BTCardModel.class)));
		countQuery.where(filterPredicate);
		Long count = entityManager.createQuery(countQuery).getSingleResult();
		int totalPages = 0;
		if (filterParams.getSize() == 0) {
			totalPages = 1;
		} else if (null != count) {
			totalPages = (int) Math.ceil((double) count.intValue() / filterParams.getSize());
		}

		Paging paging = new Paging();
		paging.setPageSize(filterParams.getSize());
		paging.setTotalPages(totalPages);
		List<BTCardRecordDetails> data = new ArrayList<BTCardRecordDetails>();
		entities.forEach(entity -> {
			data.add(mapper.map(entity, BTCardRecordDetails.class));
		});

		BTCardRecordPage bTCardPage = new BTCardRecordPage();
		bTCardPage.setData(data);
		bTCardPage.setPaging(paging);

		return bTCardPage;

	}

	@Override
	public BTCardRecordPage getBTCardRenewedRecordDetails(BTCardRecordFilterParam filterParams) {
		
		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<BTCardRecordModel> criteriaQuery = criteria.createQuery(BTCardRecordModel.class);
		Root<BTCardRecordModel> rootBTCardModel = criteriaQuery
				.from(BTCardRecordModel.class);
		Predicate filterPredicate = criteria.conjunction();
		
		if (CollectionUtils.isNotEmpty(filterParams.getBTNumbers())) {
            In<String> btNumber_in = criteria.in(rootBTCardModel.get("btNumber"));
            for (String btNumber : filterParams.getBTNumbers()) {
            	btNumber_in.value(btNumber);
            }
            filterPredicate = criteria.and(filterPredicate, btNumber_in);
        }

		if (filterParams.getSchoolId() != null) {
			Predicate schoolIdFilter = criteria.equal(rootBTCardModel.get("schoolId"),
					filterParams.getSchoolId());
			filterPredicate = criteria.and(filterPredicate, schoolIdFilter);
		}
		
		if (filterParams.getType() != null) {
			Predicate typeFilter = criteria.equal(rootBTCardModel.get("type"),
					filterParams.getType());
			filterPredicate = criteria.and(filterPredicate, typeFilter);
		}
		if (filterParams.getSchoolTypeId() != null) {
			Predicate schoolTypeIdFilter = criteria.equal(rootBTCardModel.get("schoolTypeId"),
					filterParams.getSchoolTypeId());
			filterPredicate = criteria.and(filterPredicate, schoolTypeIdFilter);
		}
		if (filterParams.getStatus() != null) {
			Predicate statusFilter = criteria.equal(rootBTCardModel.get("status"),
					filterParams.getStatus());
			filterPredicate = criteria.and(filterPredicate, statusFilter);
		}
		criteriaQuery.where(filterPredicate);
		TypedQuery<BTCardRecordModel> query = entityManager.createQuery(criteriaQuery);

		if (filterParams.getSize() > 0) {
			/*
			 * my mysql - http://www.mysqltutorial.org/mysql-limit.aspx
			 */
			int offset = 0;
			if (filterParams.getPage() > 0) {
				offset = filterParams.getPage() * filterParams.getSize();
			}

			query.setFirstResult(offset);
			query.setMaxResults(filterParams.getSize());
		}

		List<BTCardRecordModel> entities = query.getResultList();

		// Count query to get the total number of pages
		CriteriaQuery<Long> countQuery = criteria.createQuery(Long.class);
		countQuery.select(criteria.count(countQuery.from(BTCardRecordModel.class)));
		countQuery.where(filterPredicate);
		Long count = entityManager.createQuery(countQuery).getSingleResult();
		int totalPages = 0;
		if (filterParams.getSize() == 0) {
			totalPages = 1;
		} else if (null != count) {
			totalPages = (int) Math.ceil((double) count.intValue() / filterParams.getSize());
		}

		Paging paging = new Paging(); 
		paging.setPageSize(filterParams.getSize());
		paging.setTotalPages(totalPages);
		List<BTCardRecordDetails> data = new ArrayList<BTCardRecordDetails>();
		entities.forEach(entity -> {
			data.add(mapper.map(entity, BTCardRecordDetails.class));
		});

		BTCardRecordPage bTCardPage = new BTCardRecordPage();
		bTCardPage.setData(data);
		bTCardPage.setPaging(paging);

		return bTCardPage;

	}

	@Override
	public BTCardRecordDetails addBTCard(BTCardRecordDetails bTCardDetails) {
		BTCardModel entity = mapper.map(bTCardDetails, BTCardModel.class);
		entity = bTCardRepository.save(entity);
		return mapper.map(entity, BTCardRecordDetails.class);
	}
}
