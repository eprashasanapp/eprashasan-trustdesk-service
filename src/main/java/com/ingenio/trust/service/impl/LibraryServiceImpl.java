package com.ingenio.trust.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.ingenio.trust.bean.LibStudentAdmissionFilterParam;
import com.ingenio.trust.bean.LibStudentAdmissionInfo;
import com.ingenio.trust.bean.LibStudentAdmissionPage;
import com.ingenio.trust.bean.LibStudentAdmissionRequest;
import com.ingenio.trust.bean.LibStudentAdmissionResponse;
import com.ingenio.trust.bean.Paging;
import com.ingenio.trust.model.LibStudentAdmissionModel;
import com.ingenio.trust.repository.LibraryRepository;
import com.ingenio.trust.service.LibraryService;

@Service
@Transactional
public class LibraryServiceImpl implements LibraryService {

	@Autowired
	LibraryRepository libraryRepository;

	@Autowired
	private ModelMapper mapper;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<LibStudentAdmissionResponse> addStudentToLib(LibStudentAdmissionRequest addStudentToLibraryRequest) {
		Assert.isTrue(addStudentToLibraryRequest != null, "Payload is required");
		Assert.notEmpty(addStudentToLibraryRequest.getStudentsInfo(), "Payload is required");
		addStudentToLibraryRequest.getStudentsInfo().forEach(studentInfo -> {
			LibStudentAdmissionModel entity = mapper.map(studentInfo, LibStudentAdmissionModel.class);
			libraryRepository.save(entity);
		});
		return null;
	}

	@Override
	public LibStudentAdmissionPage getAdmittedStudentsFromLib(LibStudentAdmissionFilterParam filterParams) {
		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<LibStudentAdmissionModel> criteriaQuery = criteria.createQuery(LibStudentAdmissionModel.class);
		Root<LibStudentAdmissionModel> rootLibStudentAdmissionModel = criteriaQuery
				.from(LibStudentAdmissionModel.class);
		Predicate filterPredicate = criteria.conjunction();

		if (filterParams.getIsGuest() != null) {
			Predicate isGuestFilter = criteria.equal(rootLibStudentAdmissionModel.get("isGuest"),
					filterParams.getIsGuest());
			filterPredicate = criteria.and(filterPredicate, isGuestFilter);
		}
		if (filterParams.getSchoolId() != null) {
			Predicate schoolIdFilter = criteria.equal(rootLibStudentAdmissionModel.get("schoolId"),
					filterParams.getSchoolId());
			filterPredicate = criteria.and(filterPredicate, schoolIdFilter);
		}
		if (filterParams.getSchoolTypeId() != null) {
			Predicate schoolTypeIdFilter = criteria.equal(rootLibStudentAdmissionModel.get("schoolTypeId"),
					filterParams.getSchoolTypeId());
			filterPredicate = criteria.and(filterPredicate, schoolTypeIdFilter);
		}
		if (filterParams.getStatus() != null) {
			Predicate statusFilter = criteria.equal(rootLibStudentAdmissionModel.get("status"),
					filterParams.getStatus());
			filterPredicate = criteria.and(filterPredicate, statusFilter);
		}
		if (filterParams.getStudentAdmissionId() != null) {
			Predicate studentAdmissionIdFilter = criteria.equal(rootLibStudentAdmissionModel.get("studentAdmissionId"),
					filterParams.getStudentAdmissionId());
			filterPredicate = criteria.and(filterPredicate, studentAdmissionIdFilter);
		}
		if (filterParams.getStudentId() != null) {
			Predicate studentIdFilter = criteria.equal(rootLibStudentAdmissionModel.get("studentId"),
					filterParams.getStudentId());
			filterPredicate = criteria.and(filterPredicate, studentIdFilter);
		}
		if (filterParams.getStudentRenewId() != null) {
			Predicate studentRenewIdFilter = criteria.equal(rootLibStudentAdmissionModel.get("studentRenewId"),
					filterParams.getStudentRenewId());
			filterPredicate = criteria.and(filterPredicate, studentRenewIdFilter);
		}
		if (filterParams.getStudentschoolId() != null) {
			Predicate studentschoolIdFilter = criteria.equal(rootLibStudentAdmissionModel.get("studentschoolId"),
					filterParams.getStudentRenewId());
			filterPredicate = criteria.and(filterPredicate, studentschoolIdFilter);
		}
		if (filterParams.getIsGuest() != null) {
			Predicate isGuestFilter = criteria.equal(rootLibStudentAdmissionModel.get("isGuest"),
					filterParams.getIsGuest());
			filterPredicate = criteria.and(filterPredicate, isGuestFilter);
		}
		criteriaQuery.where(filterPredicate);
		TypedQuery<LibStudentAdmissionModel> query = entityManager.createQuery(criteriaQuery);

		if (filterParams.getSize() > 0) {
			/*
			 * my mysql - http://www.mysqltutorial.org/mysql-limit.aspx
			 */
			int offset = 0;
			if (filterParams.getPage() > 0) {
				offset = filterParams.getPage() * filterParams.getSize();
			}

			query.setFirstResult(offset);
			query.setMaxResults(filterParams.getSize());
		}

		List<LibStudentAdmissionModel> entities = query.getResultList();

		// Count query to get the total number of pages
		CriteriaQuery<Long> countQuery = criteria.createQuery(Long.class);
		countQuery.select(criteria.count(countQuery.from(LibStudentAdmissionModel.class)));
		countQuery.where(filterPredicate);
		Long count = entityManager.createQuery(countQuery).getSingleResult();
		int totalPages = 0;
		if (filterParams.getSize() == 0) {
			totalPages = 1;
		} else if (null != count) {
			totalPages = (int) Math.ceil((double) count.intValue() / filterParams.getSize());
		}

		Paging paging = new Paging();
		paging.setPageSize(filterParams.getSize());
		paging.setTotalPages(totalPages);
		List<LibStudentAdmissionInfo> data = new ArrayList<LibStudentAdmissionInfo>();
		entities.forEach(entity -> {
			data.add(mapper.map(entity, LibStudentAdmissionInfo.class));
		});

		LibStudentAdmissionPage libStudentAdmissionPage = new LibStudentAdmissionPage();
		libStudentAdmissionPage.setData(data);
		libStudentAdmissionPage.setPaging(paging);

		return libStudentAdmissionPage;

	}
}
