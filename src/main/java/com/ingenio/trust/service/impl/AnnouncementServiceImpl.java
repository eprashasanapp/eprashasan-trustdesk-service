package com.ingenio.trust.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;


import com.ingenio.trust.bean.AnnoucementLabelBean;
import com.ingenio.trust.bean.AnnouncementLabelBean;
import com.ingenio.trust.bean.AnnouncementRequestBean;
import com.ingenio.trust.bean.AnnouncementResponseBean;
import com.ingenio.trust.bean.SansthaUserBean;
import com.ingenio.trust.bean.SchoolBean;
import com.ingenio.trust.bean.StaffDetailsBean;
import com.ingenio.trust.bean.UserBean;
import com.ingenio.trust.model.AppUserRoleModel;
import com.ingenio.trust.model.SansthaAnnouncementAssignToModel;
import com.ingenio.trust.model.SansthaAnnouncementAttachmentModel;
import com.ingenio.trust.model.SansthaAnnouncementModel;
import com.ingenio.trust.model.SansthaRegistrationModel;
import com.ingenio.trust.model.SansthaUserRegistrationModel;
import com.ingenio.trust.model.StaffBasicDetailsModel;
import com.ingenio.trust.model.YearMasterModel;
import com.ingenio.trust.repository.SansthaAnnouncementAssignedToRepository;
import com.ingenio.trust.repository.SansthaAnnouncementAttachmentRepository;
import com.ingenio.trust.repository.SansthaAnnouncementRepository;
import com.ingenio.trust.repository.StaffBasicDetailsRepository;
import com.ingenio.trust.repository.impl.AttendanceDetailsRepositoryImpl;
import com.ingenio.trust.service.AnnouncementService;
import com.ingenio.trust.util.CommonUtlitity;
import com.ingenio.trust.util.CryptoUtilLib;
import com.ingenio.trust.util.HttpClientUtil;

@Component
public class AnnouncementServiceImpl implements AnnouncementService {

	@Autowired
	private SansthaAnnouncementRepository announcementRepository;
	
	@Autowired
	private SansthaAnnouncementAssignedToRepository sansthaAnnouncementAssignedToRepository;
	
	@Autowired
	private SansthaAnnouncementAttachmentRepository sansthaAnnouncementAttachmentRepository;

	@Autowired
	private AttendanceDetailsRepositoryImpl attendanceRepositoryImpl;

	@Autowired
	private CommonUtlitity CommonUtlitity;
	
	@Autowired
	private StaffBasicDetailsRepository staffBasicDetailsRepository;

	CryptoUtilLib cry = new CryptoUtilLib();

	

	@Autowired
	private RestTemplate restTemplate;
	
	
	@Override
	public List<SchoolBean> getSchoolList(String sansthaKey, Integer sansthaUserId) {
		List<SchoolBean> schoolList = announcementRepository.getSchoolList(sansthaKey, sansthaUserId);
		schoolList.stream().forEach(school -> {

		});
		return schoolList;
	}

	@Override
	public List<SansthaUserBean> getSansthaUserList(String sansthaKey) {
		return announcementRepository.getSansthaUserList(sansthaKey);

	}

	@Override
	public List<StaffDetailsBean> getSchoolWisePrincipalList(String sansthaKey, Integer sansthaUserId) {
		return announcementRepository.getSchoolWisePrincipalList(sansthaKey, sansthaUserId);
	}

	@Override
	public List<UserBean> getAllUserList(String sansthaKey, Integer sansthaUserId) {

		List<UserBean> userbeanList = new ArrayList<>();
		
	
		
		UserBean userBean = new UserBean();
		userBean.setRoleName("School");
		List<AnnouncementLabelBean> labelList2 = new ArrayList<>();
		List<SchoolBean> schoolList = announcementRepository.getSchoolList(sansthaKey, sansthaUserId);
		schoolList.stream().forEach(school -> {
			AnnouncementLabelBean annoucementLabelBean2 = new AnnouncementLabelBean();
			annoucementLabelBean2.setId("" + school.getSchoolId());
			annoucementLabelBean2.setName(school.getSchoolName());
			labelList2.add(annoucementLabelBean2);
		});
		userBean.setDisplayList(labelList2);
		userbeanList.add(userBean);

		UserBean userBean1 = new UserBean();
		userBean1.setRoleName("SansthaUser");
		List<AnnouncementLabelBean> labelList = new ArrayList<>();
		List<SansthaUserBean> sansthaUserList = announcementRepository.getSansthaUserList(sansthaKey);
		sansthaUserList.stream().forEach(sanstha -> {
			AnnouncementLabelBean annoucementLabelBean2 = new AnnouncementLabelBean();
			annoucementLabelBean2.setId("" + sanstha.getSansthaUserId());
			annoucementLabelBean2.setName(sanstha.getLastName());
			labelList.add(annoucementLabelBean2);
		});
		userBean1.setDisplayList(labelList);
		userbeanList.add(userBean1);

		UserBean userBean2 = new UserBean();
		userBean2.setRoleName("principal");
		List<AnnouncementLabelBean> labelList3 = new ArrayList<>();
		List<StaffDetailsBean> principalList = announcementRepository.getSchoolWisePrincipalList(sansthaKey,
				sansthaUserId);
		principalList.stream().forEach(principal -> {
			AnnouncementLabelBean annoucementLabelBean2 = new AnnouncementLabelBean();
			annoucementLabelBean2.setId("" + principal.getStaffId());
			annoucementLabelBean2.setName(principal.getLastName());
			labelList3.add(annoucementLabelBean2);
		});
		userBean2.setDisplayList(labelList3);
		userbeanList.add(userBean2);

		return userbeanList;
	}

	@Override
	public Integer saveSansthaAnnouncement(AnnouncementRequestBean announcementRequestBean) {
		
		
		List<SansthaUserBean> sansthaDetailsList = announcementRepository.getSansthaDetails("");

			YearMasterModel yearMasterModel = new YearMasterModel();
			yearMasterModel.setYearId(announcementRequestBean.getYearId());

			SansthaRegistrationModel sansthaRegistrationModel = new SansthaRegistrationModel();
			sansthaRegistrationModel.setSansthaId(sansthaDetailsList.get(0).getSansthaId());
			
			SansthaUserRegistrationModel sansthaUserRegistrationModel = new SansthaUserRegistrationModel();
			sansthaUserRegistrationModel.setSansthaUserId(announcementRequestBean.getSansthaUserId());

			AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
			appUserRoleModel.setAppUserRoleId(announcementRequestBean.getUserId());

			String announcementId = "0";

			SansthaAnnouncementModel announcementModel = new SansthaAnnouncementModel();
			announcementModel.setUserId(appUserRoleModel);
			announcementModel.setIpAddress(announcementRequestBean.getIPAddress());
			announcementModel.setDeviceType(announcementRequestBean.getDeviceType());
			announcementModel.setAnnouncementTitle(announcementRequestBean.getAnnouncementTitle());
			announcementModel.setAnnouncement(announcementRequestBean.getAnnouncement());
			announcementModel.setStartDate(announcementRequestBean.getStartDate());	
			announcementModel.setSansthaRegistrationModel(sansthaRegistrationModel);
			announcementModel.setSansthaUserRegistrationModel(sansthaUserRegistrationModel);
			
			if (StringUtils.isNotEmpty(announcementId) && !announcementId.equals("0")) {
				announcementModel.setSansthaAnnouncementId(Integer.parseInt(announcementId));
			}
			
		
		  announcementRepository.saveAndFlush(announcementModel);
			
			sansthaAnnouncementAssignedToRepository.deleteBySansthaAnnouncementModel(announcementModel);

		      Integer responseSize =  announcementRequestBean.getAnnoucementResponseList().size();
		      for(int i =0;i<responseSize ;i++) {
		    	  AnnouncementResponseBean announcementResponseBean = announcementRequestBean.getAnnoucementResponseList().get(i);
					if (announcementResponseBean.getRoleName().equals("Principal")) {
						Date date = new Date();
						String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
					
						SansthaAnnouncementAssignToModel AnnouncementAssignToModel = new SansthaAnnouncementAssignToModel();
						AnnouncementAssignToModel.setRole(announcementResponseBean.getRoleName());
						AnnouncementAssignToModel.setUserFlag(announcementResponseBean.getFlag());
						List<AnnoucementLabelBean> displaylist = announcementResponseBean.getDisplayList();
						displaylist.stream().forEach(display->{
							
							StaffBasicDetailsModel staffBasicDetailsModel = new StaffBasicDetailsModel();
							staffBasicDetailsModel .setstaffId(Integer.parseInt(display.getId()));
							AnnouncementAssignToModel.setStaffBasicDetailsModel(staffBasicDetailsModel);
							
							AnnouncementAssignToModel.setAnnouncementStatus("0");
							AnnouncementAssignToModel.setSansthaRegistrationModel(sansthaRegistrationModel);
							AnnouncementAssignToModel.setYearMasterModel(null);
							AnnouncementAssignToModel.setUserId(appUserRoleModel);
							AnnouncementAssignToModel.setSansthaAnnouncementModel(announcementModel);
							AnnouncementAssignToModel.setSansthaUserRegistrationModel(null);
							sansthaAnnouncementAssignedToRepository.saveAndFlush(AnnouncementAssignToModel);
							
						});
						

		      }else if (announcementResponseBean.getRoleName().equals("SansthaUser")) {
					Date date = new Date();
					String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
				
					SansthaAnnouncementAssignToModel AnnouncementAssignToModel = new SansthaAnnouncementAssignToModel();
					AnnouncementAssignToModel.setRole(announcementResponseBean.getRoleName());
					AnnouncementAssignToModel.setUserFlag(announcementResponseBean.getFlag());
					List<AnnoucementLabelBean> displaylist = announcementResponseBean.getDisplayList();
					displaylist.stream().forEach(display->{
						
						SansthaUserRegistrationModel sansthaUserRegistrationModel1 = new SansthaUserRegistrationModel();
						sansthaUserRegistrationModel1.setSansthaUserId(announcementRequestBean.getSansthaUserId());						
						AnnouncementAssignToModel.setStaffBasicDetailsModel(null);						
						AnnouncementAssignToModel.setAnnouncementStatus("0");
						AnnouncementAssignToModel.setSansthaRegistrationModel(sansthaRegistrationModel);
						AnnouncementAssignToModel.setYearMasterModel(null);
						AnnouncementAssignToModel.setUserId(appUserRoleModel);
						AnnouncementAssignToModel.setSansthaAnnouncementModel(announcementModel);
						AnnouncementAssignToModel.setSansthaUserRegistrationModel(sansthaUserRegistrationModel1);
						sansthaAnnouncementAssignedToRepository.saveAndFlush(AnnouncementAssignToModel);
						
					});
		      }else {
		    	  
		    	  	Date date = new Date();
					String currentDate= new SimpleDateFormat("yyyy-MM-dd").format(date);		
				
					SansthaAnnouncementAssignToModel AnnouncementAssignToModel = new SansthaAnnouncementAssignToModel();
					AnnouncementAssignToModel.setRole(announcementResponseBean.getRoleName());
					AnnouncementAssignToModel.setUserFlag(announcementResponseBean.getFlag());
					List<AnnoucementLabelBean> displaylist = announcementResponseBean.getDisplayList();
					displaylist.stream().forEach(display->{						
						List<StaffBasicDetailsModel> staffList = staffBasicDetailsRepository.findStaff(Integer.parseInt(display.getId()));	
						staffList.stream().forEach(staff->{							
							StaffBasicDetailsModel staffBasicDetailsModel = new StaffBasicDetailsModel();
							staffBasicDetailsModel.setstaffId(staff.getstaffId());
							AnnouncementAssignToModel.setStaffBasicDetailsModel(staffBasicDetailsModel);						
							AnnouncementAssignToModel.setAnnouncementStatus("0");
							AnnouncementAssignToModel.setSansthaRegistrationModel(sansthaRegistrationModel);
							AnnouncementAssignToModel.setYearMasterModel(null);
							AnnouncementAssignToModel.setUserId(appUserRoleModel);
							AnnouncementAssignToModel.setSansthaAnnouncementModel(announcementModel);
							AnnouncementAssignToModel.setSansthaUserRegistrationModel(null);
							sansthaAnnouncementAssignedToRepository.saveAndFlush(AnnouncementAssignToModel);
						});
				
		      });
		      }
		}

			return 1;
	}

	@Override
	public List<AnnouncementResponseBean> getMyAnnouncement(Integer sansthauserId, Integer yearId,
			String announcementDate, Integer offset) {
		List<AnnouncementResponseBean> myAnnouncementList = announcementRepository.getMyannouncement(sansthauserId);
		myAnnouncementList.stream().forEach(announcement -> {
			announcement.setAssignmentStatus("");
			announcement.setAnnouncedByImgUrl("");
			announcement.setShowcount(false);
			announcement.setCompleteCount(0l);
			announcement.setDeliveredCount(0l);
			announcement.setSeenCount(0l);
			announcement.setUnseenCount(0l);
			announcement.setTotalStudentCount(0l);
			List<AnnouncementResponseBean> attachmentList = announcementRepository.getAttachments(announcement.getAnnouncementId());
			announcement.setAttachmentList(attachmentList);
			announcement.setAnnouncementAssignedtoId(0);
		});
		return myAnnouncementList;
	}
	
	
	@Override
	public Integer saveAttachmentsWithId(MultipartFile file, String fileName, String yearName ,Integer sansthaAnnouncmentId,
			Integer sansthakey) {
		try {
			
			SansthaAnnouncementAttachmentModel attachmentModel = new SansthaAnnouncementAttachmentModel();
			SansthaAnnouncementModel announcementModel = new SansthaAnnouncementModel();
			announcementModel.setSansthaAnnouncementId(sansthaAnnouncmentId);
			attachmentModel.setSansthaAnnouncementModel(announcementModel);
			attachmentModel.setFileName(fileName);		
			
			StringBuilder attachmentFileName = new StringBuilder();	
		
		
			if (!file.isEmpty() && StringUtils.isNotEmpty(fileName)) {
				attachmentFileName.append(yearName).append("_").append(fileName);
				String folderName = "cms" + File.separator + "SansthaAnnouncement" + File.separator+ sansthakey + File.separator + yearName;
				String target = HttpClientUtil.uploadImage(file, fileName, folderName, ""+sansthakey,1);
				if (!target.isEmpty()) {
					attachmentModel.setFileName(attachmentFileName.toString());
					attachmentModel.setServerFilePath(target);
					sansthaAnnouncementAttachmentRepository.saveAndFlush(attachmentModel);
					return attachmentModel.getSansthaAnnouncementAttachmentId();
				}
			}

		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return 0;

	}

	@Override
	public List<AnnouncementResponseBean> getAttachmentList(Integer announcementId) {
		return announcementRepository.getAttachments(announcementId);
	}

	

}
