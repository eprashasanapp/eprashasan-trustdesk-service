package com.ingenio.trust.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.trust.bean.AppliedSchool;
import com.ingenio.trust.bean.AppliedSchoolDetails;
import com.ingenio.trust.bean.AppliedSchoolFilterParam;
import com.ingenio.trust.bean.AppliedSchoolPage;
import com.ingenio.trust.bean.Paging;
import com.ingenio.trust.model.AppliedSchoolModel;
import com.ingenio.trust.model.SchoolMasterModel;
import com.ingenio.trust.repository.SchoolMasterRepository;
import com.ingenio.trust.service.AppliedSchoolService;

@Service
@Transactional
public class AppliedSchoolServiceImpl implements AppliedSchoolService {

	@Autowired
	private ModelMapper mapper;

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	SchoolMasterRepository schoolMasterRepository;

	@Override
	public AppliedSchoolPage getAppliedSchoolPageDetails(AppliedSchoolFilterParam filterParams) {
		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<AppliedSchoolModel> criteriaQuery = criteria.createQuery(AppliedSchoolModel.class);
		Root<AppliedSchoolModel> rootBTCardModel = criteriaQuery
				.from(AppliedSchoolModel.class);
		Predicate filterPredicate = criteria.conjunction();
		
		if (filterParams.getFromSchoolId() != null) {
			Predicate schoolIdFilter = criteria.equal(rootBTCardModel.get("fromSchoolId"),
					filterParams.getFromSchoolId());
			filterPredicate = criteria.and(filterPredicate, schoolIdFilter);
		}
		if (filterParams.getAppliedSchoolId() != null) {
			Predicate schoolTypeIdFilter = criteria.equal(rootBTCardModel.get("appliedSchoolId"),
					filterParams.getAppliedSchoolId());
			filterPredicate = criteria.and(filterPredicate, schoolTypeIdFilter);
		}
		if (filterParams.getStudentId() != null) {
			Predicate statusFilter = criteria.equal(rootBTCardModel.get("studentId"),
					filterParams.getStudentId());
			filterPredicate = criteria.and(filterPredicate, statusFilter);
		}
		criteriaQuery.where(filterPredicate);
		TypedQuery<AppliedSchoolModel> query = entityManager.createQuery(criteriaQuery);

		if (filterParams.getSize() > 0) {
			/*
			 * my mysql - http://www.mysqltutorial.org/mysql-limit.aspx
			 */
			int offset = 0;
			if (filterParams.getPage() > 0) {
				offset = filterParams.getPage() * filterParams.getSize();
			}

			query.setFirstResult(offset);
			query.setMaxResults(filterParams.getSize());
		}

		List<AppliedSchoolModel> entities = query.getResultList();

		// Count query to get the total number of pages
		CriteriaQuery<Long> countQuery = criteria.createQuery(Long.class);
		countQuery.select(criteria.count(countQuery.from(AppliedSchoolModel.class)));
		countQuery.where(filterPredicate);
		Long count = entityManager.createQuery(countQuery).getSingleResult();
		int totalPages = 0;
		if (filterParams.getSize() == 0) {
			totalPages = 1;
		} else if (null != count) {
			totalPages = (int) Math.ceil((double) count.intValue() / filterParams.getSize());
		}

		Paging paging = new Paging();
		paging.setPageSize(filterParams.getSize());
		paging.setTotalPages(totalPages);
		List<AppliedSchool> data = new ArrayList<AppliedSchool>();
		List<AppliedSchool> response = new ArrayList<AppliedSchool>();
		entities.forEach(entity -> {
			data.add(mapper.map(entity, AppliedSchool.class));
		});

		AppliedSchoolPage bTCardPage = new AppliedSchoolPage();
		
		for(AppliedSchool entity : data) {
			SchoolMasterModel se = schoolMasterRepository.findBySchoolid(entity.getAppliedSchoolId());
			if(se != null) {
				AppliedSchoolDetails appliedSchoolDetails = new AppliedSchoolDetails();
			appliedSchoolDetails.setSchoolid(se.getSchoolid());
			appliedSchoolDetails.setAddress(se.getSchoolAddressDecrypted());
			appliedSchoolDetails.setSchoolName(se.getSchoolNameDecrypted());
			appliedSchoolDetails.setSchoolSaunsthaName(se.getSansthaNameDecrypted());
			entity.setAppliedSchoolDetails(appliedSchoolDetails);
			}
			response.add(entity);
		}
		
		
		bTCardPage.setData(response);
		bTCardPage.setPaging(paging);

		return bTCardPage;

	}

}
