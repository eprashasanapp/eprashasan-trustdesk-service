package com.ingenio.trust.service.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.trust.model.LibBookInventoryModel;
import com.ingenio.trust.repository.LibBookInventoryRepository;
import com.ingenio.trust.service.LibBookInventoryService;

@Service
@Transactional
public class LibBookInventoryServiceImpl implements LibBookInventoryService {

	@Autowired
	LibBookInventoryRepository libBookInventoryRepository;
	

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public LibBookInventoryModel getLibBookInventoryByName(String cName, Integer schoolId, String authorName, String bookEdition) {
		LibBookInventoryModel entity = libBookInventoryRepository.getEntityByName(schoolId, cName, authorName, bookEdition);
		return entity;
	}

	@Override
	public LibBookInventoryModel addLibBookInventory(LibBookInventoryModel libBookSupplierModel) {
		libBookSupplierModel = libBookInventoryRepository.save(libBookSupplierModel);
		return libBookSupplierModel;
	}
}
