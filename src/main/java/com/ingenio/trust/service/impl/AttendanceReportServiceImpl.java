package com.ingenio.trust.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ingenio.trust.bean.AttendanceReportBean;
import com.ingenio.trust.bean.AttendanceReportBean1;
import com.ingenio.trust.bean.SchoolBean;
import com.ingenio.trust.constants.IngenioCodes;
import com.ingenio.trust.constants.SpecialCharacter;
import com.ingenio.trust.repository.AttendanceReportRepository;
import com.ingenio.trust.repository.SchoolMasterRepository;
import com.ingenio.trust.repository.impl.AttendanceDetailsRepositoryImpl;
import com.ingenio.trust.service.AttendanceReportService;
import com.ingenio.trust.util.CommonUtlitity;
import com.ingenio.trust.util.CryptoUtilLib;
import com.ingenio.trust.util.Utillity;

@Component
public class AttendanceReportServiceImpl implements AttendanceReportService {

	@Autowired
	private AttendanceReportRepository attendanceReportRepository;

	@Autowired
	private AttendanceDetailsRepositoryImpl attendanceRepositoryImpl;

	@Autowired
	private CommonUtlitity CommonUtlitity;
	
	CryptoUtilLib cry = new CryptoUtilLib();
	
	@Autowired
	SchoolMasterRepository schoolMasterRepository;

	@Override
	public List<AttendanceReportBean> getSchoolWiseAttendance(String sansthaKey, String attendedDate,
			Integer groupOfSchoolId,String mobileNo,String profileRole) {
		List<AttendanceReportBean> attendanceList = new ArrayList<>();
		AttendanceReportBean attendanceReportBean = new AttendanceReportBean();
		List<AttendanceReportBean1> presentList = new ArrayList<AttendanceReportBean1>();
		if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
				|| profileRole.equalsIgnoreCase("Super officer")) {
		presentList = attendanceRepositoryImpl.getGroupWisePresentReport(sansthaKey, attendedDate,groupOfSchoolId);
		} else {
			List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
			presentList = attendanceRepositoryImpl.getGroupWisePresentReport(sansthaKey, attendedDate,groupOfSchoolId,schoolList);
		}
		AttendanceReportBean1 presentAttendanceReport = presentList.get(0);
		Long totalPresentInWords = Long.parseLong(Utillity.convertNumberToWord("" + presentAttendanceReport.getTotalCount()));
		Long malePresentInWords = Long.parseLong(Utillity.convertNumberToWord("" + presentAttendanceReport.getMaleCount()));
		Long femalePresentInWords = Long.parseLong(Utillity.convertNumberToWord("" + presentAttendanceReport.getFemalCount()));
		presentAttendanceReport.setTotalCountInWords("" + totalPresentInWords);
		presentAttendanceReport.setMaleCountInWords("" + malePresentInWords);
		presentAttendanceReport.setFemalCountInWords("" + femalePresentInWords);
		presentList.set(0, presentAttendanceReport);

		List<AttendanceReportBean1> absentList = attendanceRepositoryImpl.getGroupWiseAbsentReport(sansthaKey, attendedDate,groupOfSchoolId);
		AttendanceReportBean1 absentReportBean = absentList.get(0);
		Long totalAbsentInWords = Long.parseLong(Utillity.convertNumberToWord("" + absentReportBean.getTotalCount()));
		Long maleAbsentInWords = Long.parseLong(Utillity.convertNumberToWord("" + absentReportBean.getMaleCount()));
		Long femaleAbsentInWords = Long.parseLong(Utillity.convertNumberToWord("" + absentReportBean.getFemalCount()));
		absentReportBean.setTotalCountInWords("" + totalAbsentInWords);
		absentReportBean.setMaleCountInWords("" + maleAbsentInWords);
		absentReportBean.setFemalCountInWords("" + femaleAbsentInWords);
		absentList.set(0, absentReportBean);
		attendanceReportBean.setPresentList(presentList);
		attendanceReportBean.setAbsentList(absentList);
		attendanceList.add(attendanceReportBean);

		List<AttendanceReportBean> standardAndDivisionWiseList = new ArrayList<>();
		if(groupOfSchoolId !=0) {
//			standardAndDivisionWiseList = attendanceReportRepository.getSchoolWiseAttendance(sansthaKey, attendedDate,groupOfSchoolId);
			standardAndDivisionWiseList = attendanceRepositoryImpl.getSchoolWiseAttendance(sansthaKey, attendedDate,groupOfSchoolId);
		}
		else {
//			standardAndDivisionWiseList = attendanceReportRepository.getSchoolWiseAttendance(sansthaKey, attendedDate);
			standardAndDivisionWiseList = attendanceRepositoryImpl.getSchoolWiseAttendance(sansthaKey, attendedDate);
		}
	
		for (int i = 0; i < standardAndDivisionWiseList.size(); i++) {
			AttendanceReportBean attendanceReportBean2 = standardAndDivisionWiseList.get(i);
			try {
				attendanceReportBean2.setLongSchoolName(cry.decrypt(IngenioCodes.getSchoolname(), StringEscapeUtils.escapeJava(attendanceReportBean2.getLongSchoolName().replaceAll("[\\r\\n]+", ""))));
			} catch (Exception e) {
				e.printStackTrace();
			}
			Long totalCountInWords = Long.parseLong(attendanceReportBean2.getPresentCountInWords()) + Long.parseLong((attendanceReportBean2.getAbsentCountInWords()));
			Long presentCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + (attendanceReportBean2.getPresentCountInWords())));
			Long absentCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + (attendanceReportBean2.getAbsentCountInWords() )));
			Long exmptedCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + attendanceReportBean2.getExemptedCountInWords()));
			attendanceReportBean2.setTotalCount(Long.parseLong(attendanceReportBean2.getPresentCountInWords()) + Long.parseLong((attendanceReportBean2.getAbsentCountInWords())));
			attendanceReportBean2.setTotalCountInWords("" +totalCountInWords);
//			attendanceReportBean2.setPresentCountInWords("" + presentCountInWords);
//			attendanceReportBean2.setAbsentCountInWords("" + absentCountInWords);
			attendanceReportBean2.setPresentCount(presentCountInWords);
			attendanceReportBean2.setAbsentCount(absentCountInWords);
			attendanceReportBean2.setExemptedCount(exmptedCountInWords);
			attendanceReportBean2.setYearId(CommonUtlitity.getCurrentYear(attendanceReportBean2.getSchoolid(), attendedDate));
			attendanceReportBean2.setStandardId(0);
			attendanceReportBean2.setStdandardName("");
			int totalpercentage = 0;
			int eachDivisionpercentage = 100 / (standardAndDivisionWiseList.size());
//			for (int j = 0; j < standardAndDivisionWiseList.size(); j++) {
				if (Long.parseLong(standardAndDivisionWiseList.get(i).getAbsentCountInWords()) != 0
						|| Long.parseLong(standardAndDivisionWiseList.get(i).getPresentCountInWords()) != 0) {
					totalpercentage += eachDivisionpercentage;
				}
//			}
			attendanceReportBean2.setPercentAttendance("" + totalpercentage + SpecialCharacter.PERCENTAGE.getCharacterName());	
			standardAndDivisionWiseList.set(i, attendanceReportBean2);
		}
		AttendanceReportBean bean = new AttendanceReportBean();
		bean.setSchoolList(standardAndDivisionWiseList);
		attendanceList.add(bean);

		return attendanceList;
	}

	@Override
	public List<AttendanceReportBean> getstandardWiseAttendance(Integer schoolId, String attendedDate) 
	{
		List<AttendanceReportBean> attendanceList = new ArrayList<>();
		AttendanceReportBean attendanceReportBean = new AttendanceReportBean();		
		List<AttendanceReportBean1> presentList = attendanceRepositoryImpl.getSchoolWisePresentReport(schoolId, attendedDate);
		AttendanceReportBean1 presentAttendanceReport = presentList.get(0);
		Long totalPresentInWords = Long.parseLong(Utillity.convertNumberToWord("" + presentAttendanceReport.getTotalCount()));
		Long malePresentInWords = Long.parseLong(Utillity.convertNumberToWord("" + presentAttendanceReport.getMaleCount()));
		Long femalePresentInWords = Long.parseLong(Utillity.convertNumberToWord("" + presentAttendanceReport.getFemalCount()));
		presentAttendanceReport.setTotalCountInWords("" + totalPresentInWords);
		presentAttendanceReport.setMaleCountInWords("" + malePresentInWords);
		presentAttendanceReport.setFemalCountInWords("" + femalePresentInWords);
		presentList.set(0, presentAttendanceReport);

		List<AttendanceReportBean1> absentList = attendanceRepositoryImpl.getSchoolWiseAbsentReport(schoolId, attendedDate);
		AttendanceReportBean1 absentReportBean = absentList.get(0);
		Long totalAbsentInWords = Long.parseLong(Utillity.convertNumberToWord("" + absentReportBean.getTotalCount()));
		Long maleAbsentInWords = Long.parseLong(Utillity.convertNumberToWord("" + absentReportBean.getMaleCount()));
		Long femaleAbsentInWords = Long.parseLong(Utillity.convertNumberToWord("" + absentReportBean.getFemalCount()));
		absentReportBean.setTotalCountInWords("" + totalAbsentInWords);
		absentReportBean.setMaleCountInWords("" + maleAbsentInWords);
		absentReportBean.setFemalCountInWords("" + femaleAbsentInWords);
		absentList.set(0, absentReportBean);

		attendanceReportBean.setPresentList(presentList);
		attendanceReportBean.setAbsentList(absentList);

		attendanceList.add(attendanceReportBean);

//		List<AttendanceReportBean> standardwiseAttendanceList = attendanceReportRepository.getStandardWiseAttendance(schoolId, attendedDate);
		List<AttendanceReportBean> standardwiseAttendanceList = attendanceRepositoryImpl.getStandardWiseAttendance(schoolId, attendedDate);
		for (int i = 0; i < standardwiseAttendanceList.size(); i++) {
			AttendanceReportBean attendanceReportBean2 = standardwiseAttendanceList.get(i);
			Long exmptedCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + attendanceReportBean2.getExemptedCountInWords()));
			Long presentCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + attendanceReportBean2.getPresentCountInWords()));
			Long absentCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + attendanceReportBean2.getAbsentCountInWords()));
			attendanceReportBean2.setExemptedCountInWords(""+ exmptedCountInWords);
			attendanceReportBean2.setPresentCountInWords("" + presentCountInWords);
			attendanceReportBean2.setAbsentCountInWords("" + absentCountInWords);
			standardwiseAttendanceList.set(i, attendanceReportBean2);
		}

		AttendanceReportBean bean = new AttendanceReportBean();
		bean.setStandardList(standardwiseAttendanceList);
		attendanceList.add(bean);
		return attendanceList;

	}

	@Override
	public List<AttendanceReportBean> getdivisionWiseAttendance(Integer schoolId, String attendedDate,
			Integer standardId) {
		List<AttendanceReportBean> AttendanceList = new ArrayList<>();
		AttendanceReportBean attendanceReportBean = new AttendanceReportBean();
		List<AttendanceReportBean1> presentList = attendanceRepositoryImpl.getStandardWisePresentReport(schoolId, attendedDate, standardId);
		AttendanceReportBean1 presentAttendanceReport = presentList.get(0);
		Long totalPresentInWords = Long.parseLong(Utillity.convertNumberToWord("" + presentAttendanceReport.getTotalCount()));
		Long malePresentInWords = Long.parseLong(Utillity.convertNumberToWord("" + presentAttendanceReport.getMaleCount()));
		Long femalePresentInWords = Long.parseLong(Utillity.convertNumberToWord("" + presentAttendanceReport.getFemalCount()));
		presentAttendanceReport.setTotalCountInWords("" + totalPresentInWords);
		presentAttendanceReport.setMaleCountInWords("" + malePresentInWords);
		presentAttendanceReport.setFemalCountInWords("" + femalePresentInWords);
		presentList.set(0, presentAttendanceReport);

		List<AttendanceReportBean1> absentList = attendanceRepositoryImpl.getStandardWiseAbsentList(schoolId, attendedDate, standardId);
		AttendanceReportBean1 absentReportBean = absentList.get(0);
		Long totalAbsentInWords = Long.parseLong(Utillity.convertNumberToWord("" + absentReportBean.getTotalCount()));
		Long maleAbsentInWords = Long.parseLong(Utillity.convertNumberToWord("" + absentReportBean.getMaleCount()));
		Long femaleAbsentInWords = Long.parseLong(Utillity.convertNumberToWord("" + absentReportBean.getFemalCount()));
		absentReportBean.setTotalCountInWords("" + totalAbsentInWords);
		absentReportBean.setMaleCountInWords("" + maleAbsentInWords);
		absentReportBean.setFemalCountInWords("" + femaleAbsentInWords);
		absentList.set(0, absentReportBean);

		attendanceReportBean.setPresentList(presentList);
		attendanceReportBean.setAbsentList(absentList);
		AttendanceList.add(attendanceReportBean);

		//List<AttendanceReportBean> StandardAndDivisionWiseList = attendanceReportRepository.getStandardAndDivisionAttendance(schoolId, attendedDate, standardId);
		List<AttendanceReportBean> StandardAndDivisionWiseList = attendanceRepositoryImpl.getStandardAndDivisionAttendance(schoolId, attendedDate, standardId);
		for (int i = 0; i < StandardAndDivisionWiseList.size(); i++) {
			AttendanceReportBean attendanceReportBean2 = StandardAndDivisionWiseList.get(i);
			Long exmptedCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + attendanceReportBean2.getExemptedCountInWords()));
			Long presentCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + attendanceReportBean2.getPresentCountInWords()));
			Long absentCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + attendanceReportBean2.getAbsentCountInWords()));
			attendanceReportBean2.setExemptedCountInWords("" + exmptedCountInWords);
			attendanceReportBean2.setPresentCountInWords("" + presentCountInWords);
			attendanceReportBean2.setAbsentCountInWords("" + absentCountInWords);
			StandardAndDivisionWiseList.set(i, attendanceReportBean2);
		}

		AttendanceReportBean bean = new AttendanceReportBean();
		bean.setDivisionList(StandardAndDivisionWiseList);
		AttendanceList.add(bean);

		return AttendanceList;
	}

	@Override
	public List<AttendanceReportBean> getgGoupWiseAttendance(String sansthaKey, String attendedDate,String mobileNo,String profileRole) {
		List<AttendanceReportBean> AttendanceList = new ArrayList<>();
		AttendanceReportBean attendanceReportBean = new AttendanceReportBean();


		List<AttendanceReportBean1> presentList = attendanceRepositoryImpl.getSansthaWisePresentReport(sansthaKey, attendedDate);
		AttendanceReportBean1 presentAttendanceReport = presentList.get(0);
		Long totalPresentInWords = Long.parseLong(Utillity.convertNumberToWord("" + presentAttendanceReport.getTotalCount()));
		Long malePresentInWords = Long.parseLong(Utillity.convertNumberToWord("" + presentAttendanceReport.getMaleCount()));
		Long femalePresentInWords = Long.parseLong(Utillity.convertNumberToWord("" + presentAttendanceReport.getFemalCount()));
		presentAttendanceReport.setTotalCountInWords("" + totalPresentInWords);
		presentAttendanceReport.setMaleCountInWords("" + malePresentInWords);
		presentAttendanceReport.setFemalCountInWords("" + femalePresentInWords);
		presentList.set(0, presentAttendanceReport);
		
		List<AttendanceReportBean1> absentList = attendanceRepositoryImpl.getSansthaWiseAbsentReport(sansthaKey, attendedDate);
		AttendanceReportBean1 absentReportBean = absentList.get(0);
		Long totalAbsentInWords = Long.parseLong(Utillity.convertNumberToWord("" + absentReportBean.getTotalCount()));
		Long maleAbsentInWords = Long.parseLong(Utillity.convertNumberToWord("" + absentReportBean.getMaleCount()));
		Long femaleAbsentInWords = Long.parseLong(Utillity.convertNumberToWord("" + absentReportBean.getFemalCount()));
		absentReportBean.setTotalCountInWords("" + totalAbsentInWords);
		absentReportBean.setMaleCountInWords("" + maleAbsentInWords);
		absentReportBean.setFemalCountInWords("" + femaleAbsentInWords);
		absentList.set(0, absentReportBean);

		attendanceReportBean.setPresentList(presentList);
		attendanceReportBean.setAbsentList(absentList);
		AttendanceList.add(attendanceReportBean);

		
//		List<AttendanceReportBean> groupWiseSchoolAttendanceList = attendanceReportRepository.getgroupWiseAttendance(sansthaKey, attendedDate);
		List<AttendanceReportBean> groupWiseSchoolAttendanceList = attendanceRepositoryImpl.getgroupWiseAttendance(sansthaKey, attendedDate);
		for (int i = 0; i < groupWiseSchoolAttendanceList.size(); i++) {
			AttendanceReportBean attendanceReportBean2 = groupWiseSchoolAttendanceList.get(i);
			Long totalCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + (Long.parseLong(attendanceReportBean2.getPresentCountInWords()) + Long.parseLong(attendanceReportBean2.getAbsentCountInWords()))));
			Long presentCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + (attendanceReportBean2.getPresentCountInWords())));
			Long absentCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + (attendanceReportBean2.getAbsentCountInWords())));
			Long exemptedCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + (attendanceReportBean2.getExemptedCountInWords())));
			attendanceReportBean2.setTotalCount(Long.parseLong(attendanceReportBean2.getPresentCountInWords()) + Long.parseLong((attendanceReportBean2.getAbsentCountInWords())));
			attendanceReportBean2.setTotalCountInWords("" + totalCountInWords);
//			attendanceReportBean2.setPresentCountInWords("" + presentCountInWords);
//			attendanceReportBean2.setAbsentCountInWords("" + absentCountInWords);
			attendanceReportBean2.setPresentCount(presentCountInWords);
			attendanceReportBean2.setAbsentCount(absentCountInWords);
			attendanceReportBean2.setExemptedCount(exemptedCountInWords);
			groupWiseSchoolAttendanceList.set(i, attendanceReportBean2);
		}
		AttendanceReportBean bean = new AttendanceReportBean();
		bean.setGroupWiseAttendanceList(groupWiseSchoolAttendanceList);
		AttendanceList.add(bean);

		return AttendanceList;
	}
	
	
	public List<Integer> getSchoolList(String MobileNo, String SansthaKey) {
		List<SchoolBean> schoolMasterList = schoolMasterRepository.getSchoolList(SansthaKey, MobileNo);
		List<Integer> schoolList = new ArrayList<>();
		for (int i = 0; i < schoolMasterList.size(); i++) {
			schoolList.add(schoolMasterList.get(i).getSchoolId());
		}
		return schoolList;
	}

}
