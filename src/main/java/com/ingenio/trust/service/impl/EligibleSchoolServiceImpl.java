package com.ingenio.trust.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.trust.bean.EligibleSchool;
import com.ingenio.trust.bean.EligibleSchoolPage;
import com.ingenio.trust.bean.IntakeFilterParam;
import com.ingenio.trust.bean.Paging;
import com.ingenio.trust.model.AdmissionQuotaModel;
import com.ingenio.trust.model.CategoryMasterModel;
import com.ingenio.trust.model.SchoolMasterModel;
import com.ingenio.trust.model.StreamMasterModel;
import com.ingenio.trust.model.TalukaModel;
import com.ingenio.trust.repository.AdmissionQuotaRepository;
import com.ingenio.trust.repository.CategoryMasterRepository;
import com.ingenio.trust.repository.SchoolLogoRepository;
import com.ingenio.trust.repository.StreamMasterRepository;
import com.ingenio.trust.service.EligibleSchoolService;


@Service
@Transactional
public class EligibleSchoolServiceImpl implements EligibleSchoolService{

	@Autowired
	private ModelMapper mapper;

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private CategoryMasterRepository categoryMasterRepository;
	
	@Autowired
	private StreamMasterRepository streamMasterRepository;
	
	@Autowired
	private SchoolLogoRepository schoolLogoRepository;
	
	@Autowired
	private AdmissionQuotaRepository admissionQuotaRepository;
	
	@Override
	public EligibleSchoolPage getEligibleSchoolPageDetails(IntakeFilterParam queryParams) {
		
		if(StringUtils.isNotEmpty(queryParams.getCategoryName())) {
			CategoryMasterModel cat = categoryMasterRepository.findByName(queryParams.getSchoolId(), queryParams.getCategoryName());
		if(cat != null) {
			queryParams.setCategoryId(cat.getCategoryId());
		}
		}
		
		if(StringUtils.isNotEmpty(queryParams.getStreamName())) {
			StreamMasterModel cat = streamMasterRepository.findByName(queryParams.getSchoolId(), queryParams.getStreamName());
		if(cat != null) {
			queryParams.setStreamId(cat.getStreamId());
		}
		}
		
		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<SchoolMasterModel> criteriaQuery = criteria.createQuery(SchoolMasterModel.class);
		Root<SchoolMasterModel> rootBTCardModel = criteriaQuery
				.from(SchoolMasterModel.class);
		Predicate filterPredicate = criteria.conjunction();
		
		if (queryParams.getGroupOfSchoolId()!=null) {
			Predicate schoolIdFilter = criteria.equal(rootBTCardModel.get("groupOfSchoolId"),
					queryParams.getGroupOfSchoolId());
			filterPredicate = criteria.and(filterPredicate, schoolIdFilter);
        }

		if (queryParams.getSchoolId() != null) {
			Predicate schoolIdFilter = criteria.equal(rootBTCardModel.get("schoolid"),
					queryParams.getSchoolId());
			filterPredicate = criteria.and(filterPredicate, schoolIdFilter);
		}
		if (queryParams.getSansthakey() != null) {
			Predicate schoolTypeIdFilter = criteria.equal(rootBTCardModel.get("sansthaKey"),
					queryParams.getSansthakey());
			filterPredicate = criteria.and(filterPredicate, schoolTypeIdFilter);
		}
		if (queryParams.getGender() != null) {
			Predicate schoolTypeIdFilter = criteria.equal(rootBTCardModel.get("sinkingFlag"),
					queryParams.getGender());
			filterPredicate = criteria.and(filterPredicate, schoolTypeIdFilter);
		}
		if (queryParams.getTalukaId() != null) {
			Join<SchoolMasterModel, TalukaModel> join = rootBTCardModel.join("talukaModel");
            Predicate regulationFilter = criteria.equal(join.get("talukaId"),
            		queryParams.getTalukaId());
            filterPredicate = criteria.and(filterPredicate, regulationFilter);
		}
		if (queryParams.getDistrictId() != null) {
			Join<SchoolMasterModel, TalukaModel> join = rootBTCardModel.join("districtModel");
            Predicate regulationFilter = criteria.equal(join.get("city"),
            		queryParams.getDistrictId());
            filterPredicate = criteria.and(filterPredicate, regulationFilter);
		}
		criteriaQuery.where(filterPredicate);
		TypedQuery<SchoolMasterModel> query = entityManager.createQuery(criteriaQuery);

		if (queryParams.getSize() > 0) {
			/*
			 * my mysql - http://www.mysqltutorial.org/mysql-limit.aspx
			 */
			int offset = 0;
			if (queryParams.getPage() > 0) {
				offset = queryParams.getPage() * queryParams.getSize();
			}

			query.setFirstResult(offset);
			query.setMaxResults(queryParams.getSize());
		}

		List<SchoolMasterModel> entities = query.getResultList();

		// Count query to get the total number of pages
		CriteriaQuery<Long> countQuery = criteria.createQuery(Long.class);
		countQuery.select(criteria.count(countQuery.from(SchoolMasterModel.class)));
		countQuery.where(filterPredicate);
		Long count = entityManager.createQuery(countQuery).getSingleResult();
		int totalPages = 0;
		if (queryParams.getSize() == 0) {
			totalPages = 1;
		} else if (null != count) {
			totalPages = (int) Math.ceil((double) count.intValue() / queryParams.getSize());
		}

		Paging paging = new Paging();
		paging.setPageSize(queryParams.getSize());
		paging.setTotalPages(totalPages);
		List<EligibleSchool> data = new ArrayList<EligibleSchool>();
		entities.forEach(entity -> {
			EligibleSchool res = mapper.map(entity, EligibleSchool.class);
			Integer sId=entity.getSchoolid();
			AdmissionQuotaModel intake = admissionQuotaRepository.findByName(sId, queryParams.getStreamId(), queryParams.getCategoryId());
			if(intake!=null)
			res.setIntake(intake.getAdmissionQuota());
			res.setCategoryId(queryParams.getCategoryId());
			res.setStreamId(queryParams.getStreamId());
			String logo = schoolLogoRepository.getSchoolLogoPath(entity.getSchoolid());
			res.setLogo(logo);
			data.add(res);
		});

		EligibleSchoolPage bTCardPage = new EligibleSchoolPage();
		bTCardPage.setData(data);
		bTCardPage.setPaging(paging);

		return bTCardPage;

	}

	
	
	
}
