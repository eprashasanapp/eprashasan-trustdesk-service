package com.ingenio.trust.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ingenio.trust.bean.FeeBean;
import com.ingenio.trust.bean.SchoolBean;
import com.ingenio.trust.constants.IngenioCodes;
import com.ingenio.trust.constants.TrustDeskConstants;
import com.ingenio.trust.model.AppLanguageModel;
import com.ingenio.trust.model.SchoolMasterModel;
import com.ingenio.trust.repository.AppLanguageRepository;
import com.ingenio.trust.repository.SchoolMasterRepository;
import com.ingenio.trust.repository.impl.FeeRepositoryImpl;
import com.ingenio.trust.service.PaidFeeService;
import com.ingenio.trust.util.CommonUtlitity;
import com.ingenio.trust.util.CryptoUtilLib;
import com.ingenio.trust.util.Utillity;

@Component
public class PaidFeeServiceImpl implements PaidFeeService {

	@Autowired
	FeeRepositoryImpl feeRepositoryImpl;
	
	@Autowired
	CommonUtlitity commonUtlitity;
	
	@Autowired
	private AppLanguageRepository appLanguageRepository;
	
	CryptoUtilLib cry = new CryptoUtilLib();
	
	@Autowired
	SchoolMasterRepository schoolMasterRepository;
	 
	@Override
	public List<FeeBean> getPaidFeeStandardWise(String sansthaKey, String yearName, String fromDate,
			String toDate,Integer groupId,String mobileNo,String profileRole ) {
		
		List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
		List<String> yearListNew = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(appLanguageList)) {
			for (int i = 0; i < appLanguageList.size(); i++) {
				yearListNew.add(Utillity.convertEnglishToOther(yearName, "" + appLanguageList.get(i).getLanguageId()));
			}
		}
		List<FeeBean> payTypeResultList = feeRepositoryImpl.getPayTypeList(sansthaKey, yearListNew, fromDate, toDate,groupId);
		List<FeeBean> totalList = feeRepositoryImpl.getTotalFee(sansthaKey, fromDate,toDate,groupId);
		List<FeeBean> yearResultList = feeRepositoryImpl.getYearList(sansthaKey, yearListNew, fromDate, toDate,groupId);
	
		FeeBean feeBeanWithStandard = new FeeBean();
		List<FeeBean> beanList = new ArrayList<>();
		
		BigDecimal paidFee =new BigDecimal(0);
		BigDecimal paidFine = new BigDecimal(0);
		
		BigDecimal currentPaidFee =new BigDecimal(0);
		BigDecimal currentPaidFine = new BigDecimal(0);
		BigDecimal payTypeTotal = new BigDecimal(0);
		List<FeeBean> yearList = new ArrayList<>();
		for(int i=0;i<yearResultList.size();i++) {
			FeeBean feeBean = new FeeBean();
			feeBean.setYearId(yearResultList.get(i).getYearId());
			feeBean.setYearName(yearResultList.get(i).getYearName());
			feeBean.setPaidFee(yearResultList.get(i).getPaidFee());
			feeBean.setPaidFeeInWords(Utillity.convertNumberToWord(yearResultList.get(i).getPaidFee()));
			feeBean.setPaidFine(yearResultList.get(i).getPaidFine());
			feeBean.setPaidFineInWords(Utillity.convertNumberToWord(yearResultList.get(i).getPaidFine()));
			feeBean.setPaidChequeBounceCharges("0");
			feeBean.setPaidChequeBounceChargesInWords("0");
			feeBean.setPaidTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(yearResultList.get(i).getPaidFee()))).add(BigDecimal.valueOf((long)Double.parseDouble((yearResultList.get(i).getPaidFine())))));
			feeBean.setPaidTotalInWords(Utillity.convertNumberToWord(feeBean.getPaidTotal()));
			
			paidFee=paidFee.add(BigDecimal.valueOf((long)Double.parseDouble(yearResultList.get(i).getPaidFee())));
			paidFine= paidFine.add(BigDecimal.valueOf((long)Double.parseDouble(yearResultList.get(i).getPaidFine())));
			if(yearResultList.get(i).getYearName().equals(yearName)) {
				currentPaidFee=currentPaidFee.add(BigDecimal.valueOf((long)Double.parseDouble(yearResultList.get(i).getPaidFee())));
				currentPaidFine= currentPaidFine.add(BigDecimal.valueOf((long)Double.parseDouble(yearResultList.get(i).getPaidFine())));
			}
			yearList.add(feeBean);
		}
		
		List<FeeBean> payTypeList = new ArrayList<>();
		
		for(int i=0;i<payTypeResultList.size();i++) {
			FeeBean feeBean1 = new FeeBean();
			feeBean1.setPayTypeId(payTypeResultList.get(i).getPayTypeId());
			feeBean1.setPayTypeName(payTypeResultList.get(i).getPayTypeName());
			feeBean1.setPaidFee(payTypeResultList.get(i).getPaidFee());
			feeBean1.setPaidFine(payTypeResultList.get(i).getPaidFine());
			feeBean1.setPaidFineInWords(Utillity.convertNumberToWord(payTypeResultList.get(i).getPaidFine()));
			feeBean1.setPaidChequeBounceCharges("0");
			feeBean1.setPaidChequeBounceChargesInWords("0");
			feeBean1.setPaidTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(payTypeResultList.get(i).getPaidFee()))).add(BigDecimal.valueOf((long)Double.parseDouble((payTypeResultList.get(i).getPaidFine())))));
			feeBean1.setPaidTotalInWords(Utillity.convertNumberToWord(feeBean1.getPaidTotal()));
			payTypeTotal=payTypeTotal.add(BigDecimal.valueOf((long)Double.parseDouble(feeBean1.getPaidTotal())));
			payTypeList.add(feeBean1);
		}
		
		FeeBean feeBean1 = new FeeBean();
		feeBean1.setPayTypeId(0);
		feeBean1.setPayTypeName("Total");
		feeBean1.setPaidTotal(""+payTypeTotal);
		feeBean1.setPaidFee("0");
		feeBean1.setPaidFine("0");
		feeBean1.setPaidFineInWords("0");
		feeBean1.setPaidChequeBounceCharges("0");
		feeBean1.setPaidChequeBounceChargesInWords("0");
		feeBean1.setPaidTotalInWords(Utillity.convertNumberToWord(""+payTypeTotal));
		payTypeList.add(feeBean1);
		
		
		feeBeanWithStandard.setPaidFee(""+paidFee);
		feeBeanWithStandard.setPaidFeeInWords(Utillity.convertNumberToWord(""+paidFee));
		feeBeanWithStandard.setPaidFine(""+paidFine);
		feeBeanWithStandard.setPaidFineInWords(Utillity.convertNumberToWord(""+paidFine));
		feeBeanWithStandard.setPaidChequeBounceCharges("0");
		feeBeanWithStandard.setPaidChequeBounceChargesInWords("0");
		feeBeanWithStandard.setPaidTotal(""+(paidFee.add(paidFine)));
		feeBeanWithStandard.setPaidTotalInWords(Utillity.convertNumberToWord(""+(paidFee.add(paidFine))));
		
		feeBeanWithStandard.setCurrentPaidFee(""+currentPaidFee);
		feeBeanWithStandard.setCurrentPaidFeeInWords(Utillity.convertNumberToWord(""+currentPaidFee));
		feeBeanWithStandard.setCurrentPaidFine(""+currentPaidFine);
		feeBeanWithStandard.setCurrentPaidFineInWords(Utillity.convertNumberToWord(""+currentPaidFine));
		feeBeanWithStandard.setCurrentPaidChequeBounceCharges("0");
		feeBeanWithStandard.setCurrentPaidChequeBounceChargesInWords("0");
		feeBeanWithStandard.setCurrentPaidTotal(""+(currentPaidFee.add(currentPaidFine)));
		feeBeanWithStandard.setCurrentPaidTotalInWords(Utillity.convertNumberToWord(""+(currentPaidFee.add(currentPaidFine))));
		feeBeanWithStandard.setYearList(yearList);
		feeBeanWithStandard.setPayTypeList(payTypeList);
		
		List<FeeBean> schoolList = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(totalList)) {
			for(FeeBean feeResult : totalList) {
				if(yearListNew.contains(feeResult.getYearName())) {
					FeeBean standardBean = new FeeBean();
					standardBean.setSchoolId(feeResult.getSchoolId());
					standardBean.setSchoolName(feeResult.getSchoolName());
					try {
						standardBean.setLongSchoolName(cry.decrypt(IngenioCodes.getSchoolname(), StringEscapeUtils.escapeJava(feeResult.getLongSchoolName().replaceAll("[\\r\\n]+", ""))));
					} catch (Exception e) {
						e.printStackTrace();
					} 
					standardBean.setYearId(commonUtlitity.getCurrentYear(feeResult.getSchoolId(), toDate));
					
					standardBean.setPaidFee(feeResult.getPaidFee());
					standardBean.setPaidFine(feeResult.getPaidFine());
					standardBean.setPaidChequeBounceCharges("0");
					standardBean.setPaidTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getPaidFee()))).add(BigDecimal.valueOf((long)Double.parseDouble((feeResult.getPaidFine())))));
					standardBean.setPaidFeeInWords(Utillity.convertNumberToWord(feeResult.getPaidFee()));
					standardBean.setPaidFineInWords(Utillity.convertNumberToWord(feeResult.getPaidFine()));
					standardBean.setPaidChequeBounceChargesInWords(Utillity.convertNumberToWord(standardBean.getPaidChequeBounceCharges()));
					standardBean.setPaidTotalInWords(Utillity.convertNumberToWord(standardBean.getPaidTotal()));
					schoolList.add(standardBean);
				}
			}
		}
		
		if (!profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
				|| profileRole.equalsIgnoreCase("Super officer")) {
			List<Integer> schoolList1 = getSchoolListNew(mobileNo, sansthaKey).stream().distinct().collect(Collectors.toList());
			List<FeeBean> newSchoolWiseList = new ArrayList<FeeBean>();
			for (int i = 0; i < schoolList.size(); i++) {
				if (schoolList1.contains(schoolList.get(i).getSchoolId())){
					newSchoolWiseList.add(schoolList.get(i));
				}
			}	
			schoolList.clear();
			schoolList = newSchoolWiseList; 
		}
		
		feeBeanWithStandard.setSchoolList(schoolList);
		beanList.add(feeBeanWithStandard);
		return beanList;
	}
	
	private List<Integer> getSchoolListNew(String mobileNo, String sansthaKey) {
//		List<SchoolBean> schoolMasterList = schoolMasterRepository.getSchoolList(sansthaKey, mobileNo);
		List<SchoolMasterModel> schoolMasterList = schoolMasterRepository.findBySansthaKey(sansthaKey);
		List<Integer> schoolList = new ArrayList<>();
		for (int i = 0; i < schoolMasterList.size(); i++) {
			schoolList.add(schoolMasterList.get(i).getSchoolid());
		}
		return schoolList;
	}

	@Override
	public List<FeeBean> getPaidFeeGroupWise(String sansthaKey, String yearName, String fromDate,
			String toDate,String mobileNo,String profileRole ) {

		List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
		List<String> yearListNew = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(appLanguageList)) {
			for (int i = 0; i < appLanguageList.size(); i++) {
				yearListNew.add(Utillity.convertEnglishToOther(yearName, "" + appLanguageList.get(i).getLanguageId()));
			}
		}
		List<FeeBean> payTypeResultList = feeRepositoryImpl.getPayTypeListGroupWise(sansthaKey, yearListNew, fromDate, toDate);
		List<FeeBean> totalList = feeRepositoryImpl.getTotalFeeGroupWise(sansthaKey, fromDate,toDate,yearListNew);
		List<FeeBean> yearResultList = feeRepositoryImpl.getYearListGroupWise(sansthaKey, yearName, fromDate, toDate);
	
		FeeBean feeBeanWithStandard = new FeeBean();
		List<FeeBean> beanList = new ArrayList<>();
		
		BigDecimal paidFee =new BigDecimal(0);
		BigDecimal paidFine = new BigDecimal(0);
		
		BigDecimal currentPaidFee =new BigDecimal(0);
		BigDecimal currentPaidFine = new BigDecimal(0);
		BigDecimal payTypeTotal = new BigDecimal(0);
		List<FeeBean> yearList = new ArrayList<>();
		for(int i=0;i<yearResultList.size();i++) {
			FeeBean feeBean = new FeeBean();
			feeBean.setYearId(yearResultList.get(i).getYearId());
			feeBean.setYearName(yearResultList.get(i).getYearName());
			feeBean.setPaidFee(yearResultList.get(i).getPaidFee());
			feeBean.setPaidFeeInWords(Utillity.convertNumberToWord(yearResultList.get(i).getPaidFee()));
			feeBean.setPaidFine(yearResultList.get(i).getPaidFine());
			feeBean.setPaidFineInWords(Utillity.convertNumberToWord(yearResultList.get(i).getPaidFine()));
			feeBean.setPaidChequeBounceCharges("0");
			feeBean.setPaidChequeBounceChargesInWords("0");
			feeBean.setPaidTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(yearResultList.get(i).getPaidFee()))).add(BigDecimal.valueOf((long)Double.parseDouble((yearResultList.get(i).getPaidFine())))));
			feeBean.setPaidTotalInWords(Utillity.convertNumberToWord(feeBean.getPaidTotal()));
			paidFee=paidFee.add(BigDecimal.valueOf((long)Double.parseDouble(yearResultList.get(i).getPaidFee())));
			paidFine= paidFine.add(BigDecimal.valueOf((long)Double.parseDouble(yearResultList.get(i).getPaidFine())));
			if(yearListNew.contains(yearResultList.get(i).getYearName())) {
				currentPaidFee=currentPaidFee.add(BigDecimal.valueOf((long)Double.parseDouble(yearResultList.get(i).getPaidFee())));
				currentPaidFine= currentPaidFine.add(BigDecimal.valueOf((long)Double.parseDouble(yearResultList.get(i).getPaidFine())));
			}
			yearList.add(feeBean);
		}
		
		List<FeeBean> payTypeList = new ArrayList<>();
		for(int i=0;i<payTypeResultList.size();i++) {
			FeeBean feeBean1 = new FeeBean();
			feeBean1.setPayTypeId(payTypeResultList.get(i).getPayTypeId());
			feeBean1.setPayTypeName(payTypeResultList.get(i).getPayTypeName());
			feeBean1.setPaidFee(payTypeResultList.get(i).getPaidFee());
			feeBean1.setPaidFine(payTypeResultList.get(i).getPaidFine());
			feeBean1.setPaidFineInWords(Utillity.convertNumberToWord(payTypeResultList.get(i).getPaidFine()));
			feeBean1.setPaidChequeBounceCharges("0");
			feeBean1.setPaidChequeBounceChargesInWords("0");
			feeBean1.setPaidTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(payTypeResultList.get(i).getPaidFee()))).add(BigDecimal.valueOf((long)Double.parseDouble((payTypeResultList.get(i).getPaidFine())))));
			feeBean1.setPaidTotalInWords(Utillity.convertNumberToWord(feeBean1.getPaidTotal()));
			payTypeTotal=payTypeTotal.add(BigDecimal.valueOf((long)Double.parseDouble(feeBean1.getPaidTotal())));
			payTypeList.add(feeBean1);
		}
		
		FeeBean feeBean1 = new FeeBean();
		feeBean1.setPayTypeId(0);
		feeBean1.setPayTypeName(TrustDeskConstants.TOTAL);
		feeBean1.setPaidTotal(""+payTypeTotal);
		feeBean1.setPaidFee("0");
		feeBean1.setPaidFine("0");
		feeBean1.setPaidFineInWords("0");
		feeBean1.setPaidChequeBounceCharges("0");
		feeBean1.setPaidChequeBounceChargesInWords("0");
		feeBean1.setPaidTotalInWords(Utillity.convertNumberToWord(""+payTypeTotal));
		payTypeList.add(feeBean1);
		
		

		feeBeanWithStandard.setPaidFee(""+paidFee);
		feeBeanWithStandard.setPaidFeeInWords(Utillity.convertNumberToWord(""+paidFee));
		feeBeanWithStandard.setPaidFine(""+paidFine);
		feeBeanWithStandard.setPaidFineInWords(Utillity.convertNumberToWord(""+paidFine));
		feeBeanWithStandard.setPaidChequeBounceCharges("0");
		feeBeanWithStandard.setPaidChequeBounceChargesInWords("0");
		feeBeanWithStandard.setPaidTotal(""+(paidFee.add(paidFine)));
		feeBeanWithStandard.setPaidTotalInWords(Utillity.convertNumberToWord(""+(paidFee.add(paidFine))));
		
		feeBeanWithStandard.setCurrentPaidFee(""+currentPaidFee);
		feeBeanWithStandard.setCurrentPaidFeeInWords(Utillity.convertNumberToWord(""+currentPaidFee));
		feeBeanWithStandard.setCurrentPaidFine(""+currentPaidFine);
		feeBeanWithStandard.setCurrentPaidFineInWords(Utillity.convertNumberToWord(""+currentPaidFine));
		feeBeanWithStandard.setCurrentPaidChequeBounceCharges("0");
		feeBeanWithStandard.setCurrentPaidChequeBounceChargesInWords("0");
		feeBeanWithStandard.setCurrentPaidTotal(""+(currentPaidFee.add(currentPaidFine)));
		feeBeanWithStandard.setCurrentPaidTotalInWords(Utillity.convertNumberToWord(""+(currentPaidFee.add(currentPaidFine))));
		feeBeanWithStandard.setYearList(yearList);
		feeBeanWithStandard.setPayTypeList(payTypeList);
		
		List<FeeBean> schoolList = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(totalList)) {
			for(FeeBean feeResult : totalList) {
				if(feeResult.getYearName().equals(yearName)) {
					FeeBean standardBean = new FeeBean();
					standardBean.setGroupId(feeResult.getGroupId());
					standardBean.setGroupName(feeResult.getGroupName());
					standardBean.setPaidFee(feeResult.getPaidFee());
					standardBean.setPaidFine(feeResult.getPaidFine());
					standardBean.setPaidChequeBounceCharges("0");
					standardBean.setPaidTotal(""+(BigDecimal.valueOf((long)Double.parseDouble(feeResult.getPaidFee()))).add(BigDecimal.valueOf((long)Double.parseDouble((feeResult.getPaidFine())))));
					standardBean.setPaidFeeInWords(Utillity.convertNumberToWord(feeResult.getPaidFee()));
					standardBean.setPaidFineInWords(Utillity.convertNumberToWord(feeResult.getPaidFine()));
					standardBean.setPaidChequeBounceChargesInWords(Utillity.convertNumberToWord(standardBean.getPaidChequeBounceCharges()));
					standardBean.setPaidTotalInWords(Utillity.convertNumberToWord(standardBean.getPaidTotal()));
					schoolList.add(standardBean);
				}
			}
		}
		
		if (!profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
				|| profileRole.equalsIgnoreCase("Super officer")) {
			List<Integer> schoolList1 = getSchoolList(mobileNo, sansthaKey).stream().distinct().collect(Collectors.toList());
//			List<Integer> schoolList2 = schoolList1.stream().distinct().collect(Collectors.toList());
			List<FeeBean> newSchoolWiseList = new ArrayList<FeeBean>();
			for (int i = 0; i<schoolList.size(); i++) {
				System.out.println("@@@@@ "+schoolList.get(i).getGroupId());
				for(int j=0;j<schoolList1.size();j++) {
					System.out.println("#### "+schoolList1.get(j).toString());
					if(schoolList1.get(j).toString().equalsIgnoreCase(schoolList.get(i).getGroupId())) {
						System.out.println("Matched");
						newSchoolWiseList.add(schoolList.get(i));
					}
				}
			}	
			schoolList.clear();
			schoolList = newSchoolWiseList; 
		}
		
		feeBeanWithStandard.setGroupList(schoolList);
		beanList.add(feeBeanWithStandard);
		return beanList;
	}
	
	
	
	public List<Integer> getSchoolList(String MobileNo, String SansthaKey) {
//		List<SchoolBean> schoolMasterList = schoolMasterRepository.getSchoolList(SansthaKey, MobileNo);
		List<SchoolMasterModel> schoolMasterList = schoolMasterRepository.findBySansthaKey(SansthaKey);
		List<Integer> schoolList = new ArrayList<>();
		for (int i = 0; i < schoolMasterList.size(); i++) {
			schoolList.add(schoolMasterList.get(i).getGroupOfSchoolId());
		}
		return schoolList;
	}
}
