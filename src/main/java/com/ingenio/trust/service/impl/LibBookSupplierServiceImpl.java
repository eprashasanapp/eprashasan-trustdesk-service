package com.ingenio.trust.service.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.trust.model.LibBookSupplierModel;
import com.ingenio.trust.repository.LibBookSupplierRepository;
import com.ingenio.trust.service.LibBookSupplierService;

@Service
@Transactional
public class LibBookSupplierServiceImpl implements LibBookSupplierService {

	@Autowired
	LibBookSupplierRepository libBookSupplierRepository;
	

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public LibBookSupplierModel getLibBookSupplierDetailsByName(String cName, Integer schoolId) {
		LibBookSupplierModel entity = libBookSupplierRepository.getEntityByName(schoolId, cName);
		return entity;
	}

	@Override
	public LibBookSupplierModel addLibBookSupplier(LibBookSupplierModel libBookSupplierModel) {
		libBookSupplierModel = libBookSupplierRepository.save(libBookSupplierModel);
		return libBookSupplierModel;
	}
}
