package com.ingenio.trust.service.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.trust.model.LibBookTypeModel;
import com.ingenio.trust.repository.LibBookTypeRepository;
import com.ingenio.trust.service.LibBookTypeService;

@Service
@Transactional
public class LibBookTypeServiceImpl implements LibBookTypeService {

	@Autowired
	LibBookTypeRepository libBookTypeRepository;
	

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public LibBookTypeModel getLibBookTypeByName(String cName, Integer schoolId) {
		LibBookTypeModel entity = libBookTypeRepository.getEntityByName(schoolId, cName);
		return entity;
	}

	@Override
	public LibBookTypeModel addLibBookType(LibBookTypeModel libBookSupplierModel) {
		libBookSupplierModel = libBookTypeRepository.save(libBookSupplierModel);
		return libBookSupplierModel;
	}
}
