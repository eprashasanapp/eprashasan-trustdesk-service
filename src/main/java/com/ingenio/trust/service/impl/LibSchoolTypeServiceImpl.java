package com.ingenio.trust.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.trust.bean.LibSchoolTypeBean;
import com.ingenio.trust.bean.LibSchoolTypeFilterParam;
import com.ingenio.trust.bean.LibSchoolTypePage;
import com.ingenio.trust.bean.Paging;
import com.ingenio.trust.model.LibSchoolTypeModel;
import com.ingenio.trust.service.LibSchoolTypeService;

@Service
@Transactional
public class LibSchoolTypeServiceImpl implements LibSchoolTypeService {

	@Autowired
	private ModelMapper mapper;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public LibSchoolTypePage getSchoolType(LibSchoolTypeFilterParam filterParams) {
		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<LibSchoolTypeModel> criteriaQuery = criteria.createQuery(LibSchoolTypeModel.class);
		Root<LibSchoolTypeModel> rootLibSchoolTypeModel = criteriaQuery
				.from(LibSchoolTypeModel.class);
		Predicate filterPredicate = criteria.conjunction();

		if (filterParams.getSchoolId() != null) {
			Predicate schoolIdFilter = criteria.equal(rootLibSchoolTypeModel.get("schoolId"),
					filterParams.getSchoolId());
			filterPredicate = criteria.and(filterPredicate, schoolIdFilter);
		}
		if (filterParams.getSchoolTypeId() != null) {
			Predicate schoolTypeIdFilter = criteria.equal(rootLibSchoolTypeModel.get("schoolTypeId"),
					filterParams.getSchoolTypeId());
			filterPredicate = criteria.and(filterPredicate, schoolTypeIdFilter);
		}
		if (filterParams.getStatus() != null) {
			Predicate statusFilter = criteria.equal(rootLibSchoolTypeModel.get("status"),
					filterParams.getStatus());
			filterPredicate = criteria.and(filterPredicate, statusFilter);
		}
		criteriaQuery.where(filterPredicate);
		TypedQuery<LibSchoolTypeModel> query = entityManager.createQuery(criteriaQuery);

		if (filterParams.getSize() > 0) {
			/*
			 * my mysql - http://www.mysqltutorial.org/mysql-limit.aspx
			 */
			int offset = 0;
			if (filterParams.getPage() > 0) {
				offset = filterParams.getPage() * filterParams.getSize();
			}

			query.setFirstResult(offset);
			query.setMaxResults(filterParams.getSize());
		}

		List<LibSchoolTypeModel> entities = query.getResultList();

		// Count query to get the total number of pages
		CriteriaQuery<Long> countQuery = criteria.createQuery(Long.class);
		countQuery.select(criteria.count(countQuery.from(LibSchoolTypeModel.class)));
		countQuery.where(filterPredicate);
		Long count = entityManager.createQuery(countQuery).getSingleResult();
		int totalPages = 0;
		if (filterParams.getSize() == 0) {
			totalPages = 1;
		} else if (null != count) {
			totalPages = (int) Math.ceil((double) count.intValue() / filterParams.getSize());
		}

		Paging paging = new Paging();
		paging.setPageSize(filterParams.getSize());
		paging.setTotalPages(totalPages);
		List<LibSchoolTypeBean> data = new ArrayList<LibSchoolTypeBean>();
		entities.forEach(entity -> {
			data.add(mapper.map(entity, LibSchoolTypeBean.class));
		});

		LibSchoolTypePage libSchoolTypeModelPage = new LibSchoolTypePage();
		libSchoolTypeModelPage.setData(data);
		libSchoolTypeModelPage.setPaging(paging);

		return libSchoolTypeModelPage;

	}
}
