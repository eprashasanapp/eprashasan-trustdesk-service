package com.ingenio.trust.service.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.trust.model.LibBookAccessionCategoryModel;
import com.ingenio.trust.repository.LibBookAccessionCategoryRepository;
import com.ingenio.trust.service.LibBookAccessionCategoryService;

@Service
@Transactional
public class LibBookAccessionCategoryServiceImpl implements LibBookAccessionCategoryService {

	@Autowired
	LibBookAccessionCategoryRepository libBookAccessionCategoryRepository;
	

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public LibBookAccessionCategoryModel getLibBookAccessionCategoryDetailsByName(String cName, Integer schoolId) {
		LibBookAccessionCategoryModel entity = libBookAccessionCategoryRepository.getEntityByName(schoolId, cName);
		return entity;
	}

	@Override
	public LibBookAccessionCategoryModel addLibBookAccessionCategory(LibBookAccessionCategoryModel libBookAccessionCategoryModel) {
		libBookAccessionCategoryModel = libBookAccessionCategoryRepository.save(libBookAccessionCategoryModel);
		return libBookAccessionCategoryModel;
	}
}
