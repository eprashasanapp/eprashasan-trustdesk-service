package com.ingenio.trust.service.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.trust.model.LibBookStreamModel;
import com.ingenio.trust.repository.LibBookStreamRepository;
import com.ingenio.trust.service.LibBookStreamService;

@Service
@Transactional
public class LibBookStreamServiceImpl implements LibBookStreamService {

	@Autowired
	LibBookStreamRepository libBookStreamRepository;
	

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public LibBookStreamModel getLibBookStreamByName(String cName, Integer schoolId) {
		LibBookStreamModel entity = libBookStreamRepository.getEntityByName(schoolId, cName);
		return entity;
	}

	@Override
	public LibBookStreamModel addLibBookStream(LibBookStreamModel libBookSupplierModel) {
		libBookSupplierModel = libBookStreamRepository.save(libBookSupplierModel);
		return libBookSupplierModel;
	}
}
