package com.ingenio.trust.service.impl;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ingenio.trust.ConfigurationProperties;
import com.ingenio.trust.bean.AttendanceReportBean;
import com.ingenio.trust.bean.AttendanceReportBean1;
import com.ingenio.trust.bean.StudentCountBean;
import com.ingenio.trust.bean.StudentDetailsBean;
import com.ingenio.trust.constants.TrustDeskConstants;
import com.ingenio.trust.repository.StudentDetailsRepository;
import com.ingenio.trust.repository.StudentMasterRepository;
import com.ingenio.trust.repository.impl.AttendanceDetailsRepositoryImpl;
import com.ingenio.trust.service.StudentDetailService;




@Component
public class StudentDetailServiceImpl implements StudentDetailService {

	@Autowired
	private StudentDetailsRepository studentDetailsRepository;
	
	@Autowired
	private AttendanceDetailsRepositoryImpl attendanceRepositoryImpl;
	
	@Autowired
	private StudentMasterRepository studentCountRepository;
	


	@Override
	public List<AttendanceReportBean> getStudentAttendanceDetails(Integer schoolId, String label,
			String attendedDate, Integer standardId, Integer divisionId) {
		
		List<AttendanceReportBean> studentDetailsList =new ArrayList<>();	
		AttendanceReportBean attendanceReportBean =new AttendanceReportBean();	
		List<AttendanceReportBean1>presentList = attendanceRepositoryImpl.getDivisionWisePresentReport(schoolId,attendedDate,standardId,divisionId);		
		List<AttendanceReportBean1>absentList = attendanceRepositoryImpl.getDivisionWiseAbsentList(schoolId,attendedDate,standardId,divisionId);		
		List<AttendanceReportBean1>exemptedList = attendanceRepositoryImpl.getDivisionWiseExemptedList(schoolId,attendedDate,standardId,divisionId);
		
		attendanceReportBean.setPresentList(presentList);
		attendanceReportBean.setAbsentList(absentList);
		attendanceReportBean.setExemptedList(exemptedList);
		
		studentDetailsList.add(attendanceReportBean);
		
		AttendanceReportBean attendanceReportBean2 =new AttendanceReportBean();
		

		
		StudentDetailsBean presentDetailsBean = new StudentDetailsBean();
		List<StudentDetailsBean> presentDetailList= attendanceRepositoryImpl.getTotalPresentAttendanceDetails(attendedDate,schoolId,standardId,divisionId);
		List<StudentDetailsBean> presentDetailwithPhotoList= new ArrayList<>();
		for (int i=0;i<presentDetailList.size();i++)
		{
			presentDetailwithPhotoList.add(setStudentPhotoUrl(schoolId,presentDetailList.get(i)));
		}
		presentDetailsBean.setPresentStudentList(presentDetailwithPhotoList);
		
		StudentDetailsBean absentDetailsBean = new StudentDetailsBean();
		List<StudentDetailsBean> absentDetailList= attendanceRepositoryImpl.getTotalAbsentAttendanceDetails(attendedDate,schoolId,standardId,divisionId);
		List<StudentDetailsBean> absentDetailwithPhotoList= new ArrayList<>();
		for (int i=0;i<absentDetailList.size();i++)
		{
			absentDetailwithPhotoList.add(setStudentPhotoUrl(schoolId,absentDetailList.get(i)));
		}
		absentDetailsBean.setAbsentStudentList(absentDetailwithPhotoList);
		
		attendanceReportBean2.setAbsentStudentList(absentDetailList);
		attendanceReportBean2.setPresentStudentList(presentDetailList);
		
		studentDetailsList.add(attendanceReportBean2);
		
		
		return studentDetailsList;
	}


	@Override
	public List<StudentCountBean> getDivisionwiseStudentDetails(Integer schoolId, String label, Integer yearId,
			Integer standardId, Integer divisionId, String renewDate) {
		
		List<StudentCountBean> studentCountList =new ArrayList<>();
		StudentCountBean studentCountBean = new StudentCountBean();
		studentCountBean =  studentCountRepository.getDivisionStrengthwithId(schoolId, yearId, standardId,divisionId,renewDate);
		studentCountList.add(studentCountBean);
		
		List<StudentDetailsBean> studentRecordList = studentDetailsRepository.getDivisionwiseStudentDetails(schoolId,yearId,standardId,divisionId);
		StudentCountBean studentCountBean1 = new StudentCountBean();
		List<StudentDetailsBean> studentRecordwithPhotoList= new ArrayList<>();
		for(int i=0;i<studentRecordList.size();i++)
		{
			studentRecordwithPhotoList.add(setStudentPhotoUrl(schoolId,studentRecordList.get(i)));	
		}
		
		studentCountBean1.setStudentDetailsBean(studentRecordwithPhotoList);
		studentCountList.add(studentCountBean1);
		
		return studentCountList;
	}
	
	
	
	private StudentDetailsBean setStudentPhotoUrl( Integer schoolId, StudentDetailsBean userDetailsBean) {
		String directoryPath = ""+ConfigurationProperties.studentPhotoUrl+""+schoolId +File.separator+userDetailsBean.getStudentgrBookId();
		String extension="jpg";
		userDetailsBean.setStudentPhoto(directoryPath+File.separator+userDetailsBean.getStudentRegNo()+TrustDeskConstants.DOT+extension);
		return userDetailsBean;
	}
}
