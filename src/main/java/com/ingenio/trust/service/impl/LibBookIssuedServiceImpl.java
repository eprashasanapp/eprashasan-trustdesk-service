package com.ingenio.trust.service.impl;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.ingenio.trust.bean.BookIssuedDetails;
import com.ingenio.trust.bean.BookIssuedFilterParam;
import com.ingenio.trust.bean.BookIssuedPage;
import com.ingenio.trust.bean.Paging;
import com.ingenio.trust.bean.UpdateBookIssued;
import com.ingenio.trust.model.LibIssuedBookModel;
import com.ingenio.trust.repository.LibIssuedBookRepository;
import com.ingenio.trust.service.LibBookIssuedService;

@Service
@Transactional
public class LibBookIssuedServiceImpl implements LibBookIssuedService {

	@Autowired
	private ModelMapper mapper;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	LibIssuedBookRepository libIssuedBookRepository;

	@Override
	public BookIssuedPage getBookIssuedDetails(BookIssuedFilterParam filterParams) {
		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<LibIssuedBookModel> criteriaQuery = criteria.createQuery(LibIssuedBookModel.class);
		Root<LibIssuedBookModel> rootLibIssuedBookModel = criteriaQuery.from(LibIssuedBookModel.class);
		Predicate filterPredicate = criteria.conjunction();

		if (filterParams.getAccessionNo() != null) {
			Predicate accessionNoFilter = criteria.equal(rootLibIssuedBookModel.get("bookIsbnAccessId"),
					filterParams.getAccessionNo());
			filterPredicate = criteria.and(filterPredicate, accessionNoFilter);
		}
		if (filterParams.getSchoolId() != null) {
			Predicate schoolIdFilter = criteria.equal(rootLibIssuedBookModel.get("schoolId"),
					filterParams.getSchoolId());
			filterPredicate = criteria.and(filterPredicate, schoolIdFilter);
		}
		if (filterParams.getNameId() != null) {
			Predicate nameIdFilter = criteria.equal(rootLibIssuedBookModel.get("nameId"), filterParams.getNameId());
			filterPredicate = criteria.and(filterPredicate, nameIdFilter);
		}
		if (filterParams.getBookInventoryId() != null) {
			Predicate bookInventoryIdFilter = criteria.equal(rootLibIssuedBookModel.get("bookInventoryId"),
					filterParams.getBookInventoryId());
			filterPredicate = criteria.and(filterPredicate, bookInventoryIdFilter);
		}

		criteriaQuery.where(filterPredicate);
		TypedQuery<LibIssuedBookModel> query = entityManager.createQuery(criteriaQuery);

		if (filterParams.getSize() > 0) {
			
			int offset = 0;
			if (filterParams.getPage() > 0) {
				offset = filterParams.getPage() * filterParams.getSize();
			}

			query.setFirstResult(offset);
			query.setMaxResults(filterParams.getSize());
		}

		List<LibIssuedBookModel> entities = query.getResultList();

		// Count query to get the total number of pages
		CriteriaQuery<Long> countQuery = criteria.createQuery(Long.class);
		countQuery.select(criteria.count(countQuery.from(LibIssuedBookModel.class)));
		countQuery.where(filterPredicate);
		Long count = entityManager.createQuery(countQuery).getSingleResult();
		int totalPages = 0;
		if (filterParams.getSize() == 0) {
			totalPages = 1;
		} else if (null != count) {
			totalPages = (int) Math.ceil((double) count.intValue() / filterParams.getSize());
		}

		Paging paging = new Paging();
		paging.setPageSize(filterParams.getSize());
		paging.setTotalPages(totalPages);
		List<BookIssuedDetails> data = new ArrayList<BookIssuedDetails>();
		entities.forEach(entity -> {
			data.add(mapper.map(entity, BookIssuedDetails.class));
		});

		BookIssuedPage bookIssuedPage = new BookIssuedPage();
		bookIssuedPage.setData(data);
		bookIssuedPage.setPaging(paging);

		return bookIssuedPage;

	}

	@Override
	public BookIssuedDetails addBookIssuedDetails(BookIssuedDetails bookIssue) {
		LibIssuedBookModel model = mapper.map(bookIssue, LibIssuedBookModel.class);
		model = libIssuedBookRepository.save(model);
		return mapper.map(model, BookIssuedDetails.class);
	}

	@Override
	public UpdateBookIssued updateBookIssuedDetails(UpdateBookIssued bookIssue) {
		List<Integer> errorForIds = new ArrayList<Integer>();
		if (null != bookIssue && !CollectionUtils.isEmpty(bookIssue.getIds())) {
			bookIssue.getIds().forEach(id -> {
				try {
					Optional<LibIssuedBookModel> entity = libIssuedBookRepository.findById(id);
					if (!entity.isPresent())
						errorForIds.add(id);
					else {
						DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
						Date date = new java.sql.Date(new java.util.Date().getTime());
						LibIssuedBookModel updateEntity = entity.get();
						updateEntity.setActualReturnDate(df.format(date));
						updateEntity.setType(2);
						updateEntity.setUpdatedDate(df.format(date));
						updateEntity.setEditDate(date);
						updateEntity.setReturnDate(df.format(date));
					}
				} catch (Exception e) {
					errorForIds.add(id);
				}
			});

		}
		if (!CollectionUtils.isEmpty(errorForIds)) {
			bookIssue.setIds(errorForIds);
			bookIssue.setMessage("issue in update operation for this ids");
		} else {
			bookIssue = null;
		}
		return bookIssue;
	}
}
