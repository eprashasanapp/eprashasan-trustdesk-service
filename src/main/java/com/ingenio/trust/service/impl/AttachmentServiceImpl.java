package com.ingenio.trust.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.ingenio.trust.bean.ExcelBean;
import com.ingenio.trust.model.LibBookAccessISBNModel;
import com.ingenio.trust.model.LibBookAccessionCategoryModel;
import com.ingenio.trust.model.LibBookInventoryModel;
import com.ingenio.trust.model.LibBookStreamModel;
import com.ingenio.trust.model.LibBookSupplierModel;
import com.ingenio.trust.model.LibBookTypeModel;
import com.ingenio.trust.service.AttachmentService;
import com.ingenio.trust.service.LibBookAccessISBNService;
import com.ingenio.trust.service.LibBookAccessionCategoryService;
import com.ingenio.trust.service.LibBookInventoryService;
import com.ingenio.trust.service.LibBookStreamService;
import com.ingenio.trust.service.LibBookSupplierService;
import com.ingenio.trust.service.LibBookTypeService;

@Component
public class AttachmentServiceImpl implements AttachmentService {

	@Autowired
	private LibBookAccessionCategoryService libBookAccessionCategoryService;
	
	@Autowired
	private LibBookSupplierService libBookSupplierService;

	@Autowired
	private LibBookInventoryService libBookInventoryService;
	
	@Autowired
	private LibBookStreamService libBookStreamService;
	
	@Autowired
	private LibBookTypeService libBookTypeService;
	
	@Autowired
	private LibBookAccessISBNService libBookAccessISBNService;

	@Override
	public boolean uploadBookInventoryinLibrary(Integer schoolId, List<ExcelBean> excelBeanList) {

		if (!CollectionUtils.isEmpty(excelBeanList)) {
			excelBeanList.remove(0);
			excelBeanList.forEach(excel -> {
				Integer categoryId = 0;
				int supplierId = 0;
				int inventoryId = 0;
				int streamId = 0;
				int bookTypeId = 0;
				String categoryName = excel.getSeriesCode();
				if (!StringUtils.isEmpty(categoryName)) {
					LibBookAccessionCategoryModel categoryEntity = libBookAccessionCategoryService
							.getLibBookAccessionCategoryDetailsByName(categoryName, schoolId);
					if (categoryEntity != null) {
						categoryId = categoryEntity.getCategoryId();
					} else {
						LibBookAccessionCategoryModel entity = new LibBookAccessionCategoryModel();
						entity.setCategoryName(categoryName);
						entity.setSchoolId(schoolId);
						entity.setSinkingFlag("0");
						entity.setIsEdit("0");
						entity = libBookAccessionCategoryService.addLibBookAccessionCategory(entity);
						categoryId = entity.getCategoryId();
					}
				} else {
					LibBookAccessionCategoryModel entity = new LibBookAccessionCategoryModel();
					entity.setCategoryName("Default");
					entity.setSchoolId(schoolId);
					entity.setSinkingFlag("0");
					entity.setIsEdit("0");
					entity = libBookAccessionCategoryService.addLibBookAccessionCategory(entity);
					categoryId = entity.getCategoryId();
				}
				
				String supplierName = excel.getVendorCity();
				if (!StringUtils.isEmpty(supplierName)) {
					LibBookSupplierModel entity = libBookSupplierService
							.getLibBookSupplierDetailsByName(supplierName, schoolId);
					if (entity != null) {
						supplierId = entity.getSupplierID();
					} else {
						LibBookSupplierModel sup_entity = new LibBookSupplierModel();
						sup_entity.setSupplierName(supplierName);
						sup_entity.setSchoolId(schoolId);
						sup_entity.setSinkingFlag("0");
						sup_entity.setIsEdit("0");
						sup_entity = libBookSupplierService.addLibBookSupplier(sup_entity);
						supplierId = sup_entity.getSupplierID();
					}
				} else {
					LibBookSupplierModel entity = new LibBookSupplierModel();
					entity.setSupplierName("Default");
					entity.setSchoolId(schoolId);
					entity.setSinkingFlag("0");
					entity.setIsEdit("0");
					entity = libBookSupplierService.addLibBookSupplier(entity);
					supplierId = entity.getSupplierID();
				}
				
				String stream = excel.getSubSubjectName();
				if (!StringUtils.isEmpty(stream)) {
					LibBookStreamModel entity = libBookStreamService
							.getLibBookStreamByName(stream, schoolId);
					if (entity != null) {
						streamId = entity.getBookStreamId();
					} else {
						LibBookStreamModel sup_entity = new LibBookStreamModel();
						sup_entity.setBookStreamName(stream);
						sup_entity.setSchoolId(schoolId);
						sup_entity.setSinkingFlag("0");
						sup_entity.setIsEdit("0");
						sup_entity = libBookStreamService.addLibBookStream(sup_entity);
						streamId = sup_entity.getBookStreamId();
					}
				} else {
					LibBookStreamModel entity = new LibBookStreamModel();
					entity.setBookStreamName("Default");
					entity.setSchoolId(schoolId);
					entity.setSinkingFlag("0");
					entity.setIsEdit("0");
					entity = libBookStreamService.addLibBookStream(entity);
					streamId = entity.getBookStreamId();
				}
				
				
				String bookTypeName = excel.getSubSubjectName();
				if (!StringUtils.isEmpty(bookTypeName)) {
					LibBookTypeModel entity = libBookTypeService
							.getLibBookTypeByName(bookTypeName, schoolId);
					if (entity != null) {
						bookTypeId = entity.getBookTypeId();
					} else {
						LibBookTypeModel sup_entity = new LibBookTypeModel();
						sup_entity.setBookTypeName(bookTypeName);
						sup_entity.setSchoolId(schoolId);
						sup_entity.setSinkingFlag("0");
						sup_entity.setIsEdit("0");
						sup_entity = libBookTypeService.addLibBookType(sup_entity);
						bookTypeId = sup_entity.getBookTypeId();
					}
				} else {
					LibBookTypeModel entity = new LibBookTypeModel();
					entity.setBookTypeName("Default");
					entity.setSchoolId(schoolId);
					entity.setSinkingFlag("0");
					entity.setIsEdit("0");
					entity = libBookTypeService.addLibBookType(entity);
					bookTypeId = entity.getBookTypeId();
				}
				
				String auther = excel.getAuthor();
				String edition = excel.getEdition();
				String bookName = excel.getTitleName();
					LibBookInventoryModel entity = libBookInventoryService.getLibBookInventoryByName(bookName, schoolId,
							auther, edition);
					if (entity != null) {
						 inventoryId = entity.getBookInventoryID();
						 entity.setQuantity(entity.getQuantity()+1);
					} else {
						entity = new LibBookInventoryModel(); 
						entity.setBookName(bookName);
						entity.setBookEdition(edition);
						entity.setAuthorName(auther);
						entity.setQuantity(1);
						entity.setPublicationName(excel.getPublisherName());
						entity.setYear_of_publisher(excel.getPubYear());
						entity.setBill_no(excel.getInvoiceNo());
						entity.setBill_date(excel.getInvoiceDate());
						entity.setRack_No(excel.getRackName());
						entity.setNo_of_pages(excel.getPages());
						entity.setStatus(1);
						entity.setBook_stream_id(streamId);
						entity.setBookTypeID(bookTypeId);
						entity.setSchoolId(schoolId);
						entity.setSinkingFlag("0");
						entity.setIsEdit("0");
						entity.setStandardID(1000111111);// TODO
						entity = libBookInventoryService.addLibBookInventory(entity);
						inventoryId = entity.getBookInventoryID();
				}
					LibBookAccessISBNModel libBookAccessISBNModel = new LibBookAccessISBNModel();
					libBookAccessISBNModel.setAccessNumber(excel.getAccNo());
					libBookAccessISBNModel.setAvailabilitystatus(excel.getStatus());
					libBookAccessISBNModel.setCategoryId(categoryId.toString());
					libBookAccessISBNModel.setStatus(1);
					libBookAccessISBNModel.setSchoolId(schoolId);
					libBookAccessISBNModel.setBookDetails(entity);
					libBookAccessISBNModel.setType(1);
					libBookAccessISBNModel.setDeviceType("0");
					libBookAccessISBNModel.setIsDel("0");
					libBookAccessISBNModel.setIsEdit("0");
					libBookAccessISBNModel.setSinkingFlag("0");
					libBookAccessISBNService.addLibBookAccessISBN(libBookAccessISBNModel);
					
			});

		}

		return true;
	}
	
}
