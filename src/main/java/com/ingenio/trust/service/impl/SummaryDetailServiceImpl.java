package com.ingenio.trust.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ingenio.trust.bean.SchoolBean;
import com.ingenio.trust.bean.StudentCountBean;
import com.ingenio.trust.bean.SummaryBean;
import com.ingenio.trust.bean.SummaryMenuBean;
import com.ingenio.trust.constants.IngenioCodes;
import com.ingenio.trust.enums.SummaryType;
import com.ingenio.trust.model.AppLanguageModel;
import com.ingenio.trust.model.SchoolMasterModel;
import com.ingenio.trust.repository.AppLanguageRepository;
import com.ingenio.trust.repository.SchoolMasterRepository;
import com.ingenio.trust.repository.StudentDetailsRepository;
import com.ingenio.trust.repository.StudentMasterRepository;
import com.ingenio.trust.repository.SummaryDetailRepository;
import com.ingenio.trust.service.SummaryDetailService;
import com.ingenio.trust.util.CommonUtlitity;
import com.ingenio.trust.util.CryptoUtilLib;
import com.ingenio.trust.util.Utillity;

@SuppressWarnings("unused")
@Component
public class SummaryDetailServiceImpl implements SummaryDetailService {

	private static final String NOT_APPLICABLE = "NA";

	@Autowired
	private StudentMasterRepository studentCountRepository;

	@Autowired
	private SummaryDetailRepository summaryDetailRepository;

	@Autowired
	private StudentDetailsRepository studentDetailsRepository;

	@Autowired
	private CommonUtlitity commonUtility;

	@Autowired
	private AppLanguageRepository appLanguageRepository;

	@Autowired
	private SchoolMasterRepository schoolMasterRepository;

	@Override
	public List<StudentCountBean> getSansthaWiseSummaryDetails(String sansthaKey, String yearName, String renewDate,
			String summaryType, String mobileNo, String profileRole) {

		List<StudentCountBean> studentcountlist = new ArrayList<>();
		List<String> yearList = new ArrayList<>();
		List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
		if (CollectionUtils.isNotEmpty(appLanguageList)) {
			for (int i = 0; i < appLanguageList.size(); i++) {
				yearList.add(Utillity.convertEnglishToOther(yearName, "" + appLanguageList.get(i).getLanguageId()));
			}
		}
		StudentCountBean studentCountBean = new StudentCountBean();
		if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
				|| profileRole.equalsIgnoreCase("Super officer")) {
			studentCountBean = studentCountRepository.getSansthaWiseTotalCount(sansthaKey, yearList, renewDate);
		} else {
			List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
			studentCountBean = studentCountRepository.getSansthaWiseTotalCount(sansthaKey, yearList, renewDate,
					schoolList);
		}

		Long totalCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
		Long boysCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
		Long girlsCountInWords = Long.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
		studentCountBean.setBoysCountInWords("" + boysCountInWords);
		studentCountBean.setGirlsCountInWords("" + girlsCountInWords);
		studentCountBean.setTotalCountInWords("" + totalCountInWords);

		StudentCountBean summaryCountBean = new StudentCountBean();
		if (summaryType.equalsIgnoreCase(SummaryType.Category.getSummaryType())) {

			List<SummaryBean> categoryWiseCountList = new ArrayList<SummaryBean>();
			if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
					|| profileRole.equalsIgnoreCase("Super officer")) {
				categoryWiseCountList = summaryDetailRepository.getCategoryWiseSchoolStrength(sansthaKey, yearList,
						renewDate);
			} else {
				List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
				categoryWiseCountList = summaryDetailRepository.getCategoryWiseSchoolStrength(sansthaKey, yearList,
						renewDate, schoolList);
			}
			for (int i = 0; i < categoryWiseCountList.size(); i++) {
				SummaryBean summaryBean = categoryWiseCountList.get(i);
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + summaryBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + summaryBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + summaryBean.getGirlsCount()));
				summaryBean.setBoysCountInWords("" + boysSchoolCountInWords);
				summaryBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				summaryBean.setTotalCountInWords("" + totalSchoolCountInWords);
				categoryWiseCountList.set(i, summaryBean);
			}
			summaryCountBean.setSummaryWiseList(categoryWiseCountList);
		} else if (summaryType.equalsIgnoreCase(SummaryType.Minority.getSummaryType())) {
			List<SummaryBean> minorityWiseCountList = new ArrayList<SummaryBean>();
			if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
					|| profileRole.equalsIgnoreCase("Super officer")) {
				minorityWiseCountList = summaryDetailRepository.getMinorityWiseSchoolStrength(sansthaKey, yearList,
						renewDate);

			} else {
				List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
				minorityWiseCountList = summaryDetailRepository.getMinorityWiseSchoolStrength(sansthaKey, yearList,
						renewDate, schoolList);

			}
			for (int i = 0; i < minorityWiseCountList.size(); i++) {
				SummaryBean summaryBean = minorityWiseCountList.get(i);
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + summaryBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + summaryBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + summaryBean.getGirlsCount()));
				summaryBean.setBoysCountInWords("" + boysSchoolCountInWords);
				summaryBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				summaryBean.setTotalCountInWords("" + totalSchoolCountInWords);
				minorityWiseCountList.set(i, summaryBean);
			}
			summaryCountBean.setSummaryWiseList(minorityWiseCountList);
		} else if (summaryType.equalsIgnoreCase(SummaryType.Concession.getSummaryType())) {
			List<SummaryBean> concessionWiseCountList = new ArrayList<SummaryBean>();
			if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
					|| profileRole.equalsIgnoreCase("Super officer")) {
				concessionWiseCountList = summaryDetailRepository.getConcessionWiseSchoolStrength(sansthaKey, yearList,
						renewDate);
			} else {
				List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
				concessionWiseCountList = summaryDetailRepository.getConcessionWiseSchoolStrength(sansthaKey, yearList,
						renewDate, schoolList);

			}
			for (int i = 0; i < concessionWiseCountList.size(); i++) {
				SummaryBean summaryBean = concessionWiseCountList.get(i);
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + summaryBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + summaryBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + summaryBean.getGirlsCount()));
				summaryBean.setBoysCountInWords("" + boysSchoolCountInWords);
				summaryBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				summaryBean.setTotalCountInWords("" + totalSchoolCountInWords);
				concessionWiseCountList.set(i, summaryBean);
			}
			summaryCountBean.setSummaryWiseList(concessionWiseCountList);
		} else if (summaryType.equalsIgnoreCase(SummaryType.Religion.getSummaryType())) {
			List<SummaryBean> religionWiseCountList = new ArrayList<SummaryBean>();
			if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
					|| profileRole.equalsIgnoreCase("Super officer")) {
				religionWiseCountList = summaryDetailRepository.getReligionWiseSchoolStrength(sansthaKey, yearList,
						renewDate);
			} else {
				List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
				religionWiseCountList = summaryDetailRepository.getReligionWiseSchoolStrength(sansthaKey, yearList,
						renewDate, schoolList);

			}
			for (int i = 0; i < religionWiseCountList.size(); i++) {
				SummaryBean summaryBean = religionWiseCountList.get(i);
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + summaryBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + summaryBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + summaryBean.getGirlsCount()));
				summaryBean.setBoysCountInWords("" + boysSchoolCountInWords);
				summaryBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				summaryBean.setTotalCountInWords("" + totalSchoolCountInWords);
				religionWiseCountList.set(i, summaryBean);
			}
			summaryCountBean.setSummaryWiseList(religionWiseCountList);
		}
		studentcountlist.add(studentCountBean);
		studentcountlist.add(summaryCountBean);
		return studentcountlist;
	}

	@Override
	public List<StudentCountBean> getSummaryWiseSchoolList(String sansthaKey, String yearName, String summaryType,
			String summaryId, String renewDate, Integer groupOfSchoolId, String mobileNo, String profileRole) {

		List<String> yearList = new ArrayList<>();
		List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
		if (CollectionUtils.isNotEmpty(appLanguageList)) {
			for (int i = 0; i < appLanguageList.size(); i++) {
				yearList.add(Utillity.convertEnglishToOther(yearName, "" + appLanguageList.get(i).getLanguageId()));
			}
		}
		List<StudentCountBean> studentcountlist = new ArrayList<>();
		StudentCountBean studentCountBean = new StudentCountBean();
		if (!summaryId.equals(NOT_APPLICABLE) && groupOfSchoolId != 0) {
			if (summaryType.equalsIgnoreCase(SummaryType.Category.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseCategoryCount(sansthaKey, yearList, summaryId,
							renewDate, groupOfSchoolId);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseCategoryCount(sansthaKey, yearList, summaryId,
							renewDate, groupOfSchoolId, schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Minority.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseMinorityCount(sansthaKey, yearList, summaryId,
							renewDate, groupOfSchoolId);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseMinorityCount(sansthaKey, yearList, summaryId,
							renewDate, groupOfSchoolId, schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Concession.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseConcessionCount(sansthaKey, yearList,
							summaryId, renewDate, groupOfSchoolId);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseConcessionCount(sansthaKey, yearList,
							summaryId, renewDate, groupOfSchoolId, schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Religion.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseReligionCount(sansthaKey, yearList, summaryId,
							renewDate, groupOfSchoolId);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseReligionCount(sansthaKey, yearList, summaryId,
							renewDate, groupOfSchoolId, schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			}
		} else if (NOT_APPLICABLE.equals(summaryId) && groupOfSchoolId != 0) {
			if (summaryType.equalsIgnoreCase(SummaryType.Category.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseCategoryCount(sansthaKey, yearList, renewDate,
							groupOfSchoolId);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseCategoryCount(sansthaKey, yearList, renewDate,
							groupOfSchoolId, schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Minority.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseMinorityCount(sansthaKey, yearList, renewDate,
							groupOfSchoolId);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseMinorityCount(sansthaKey, yearList, renewDate,
							groupOfSchoolId,schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Concession.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseConcessionCount(sansthaKey, yearList, renewDate,
							groupOfSchoolId);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseConcessionCount(sansthaKey, yearList, renewDate,
							groupOfSchoolId,schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Religion.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseReligionCount(sansthaKey, yearList, renewDate,
							groupOfSchoolId);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseReligionCount(sansthaKey, yearList, renewDate,
							groupOfSchoolId,schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			}

		} else if (!summaryId.equals(NOT_APPLICABLE) && groupOfSchoolId == 0) {
			if (summaryType.equalsIgnoreCase(SummaryType.Category.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseCategoryCount(sansthaKey, yearList, summaryId,
							renewDate);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseCategoryCount(sansthaKey, yearList, summaryId,
							renewDate,schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Minority.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseMinorityCount(sansthaKey, yearList, summaryId,
							renewDate);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseMinorityCount(sansthaKey, yearList, summaryId,
							renewDate,schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Concession.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseConcessionCount(sansthaKey, yearList, summaryId,
							renewDate);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseConcessionCount(sansthaKey, yearList, summaryId,
							renewDate,schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Religion.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseReligionCount(sansthaKey, yearList, summaryId,
							renewDate);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseReligionCount(sansthaKey, yearList, summaryId,
							renewDate,schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			}

		} else if (summaryId.equals(NOT_APPLICABLE) && groupOfSchoolId == 0) {
			if (summaryType.equalsIgnoreCase(SummaryType.Category.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseCategoryCount(sansthaKey, yearList, renewDate);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseCategoryCount(sansthaKey, yearList, renewDate,schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Minority.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseMinorityCount(sansthaKey, yearList, renewDate);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseMinorityCount(sansthaKey, yearList, renewDate,schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Concession.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseConcessionCount(sansthaKey, yearList, renewDate);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseConcessionCount(sansthaKey, yearList, renewDate,schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Religion.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getGroupWiseReligionCount(sansthaKey, yearList, renewDate);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getGroupWiseReligionCount(sansthaKey, yearList, renewDate,schoolList);
				}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			}

		}

		StudentCountBean studentCountBean1 = new StudentCountBean();
		List<StudentCountBean> schoolWiseList = new ArrayList<>();
		CryptoUtilLib cry = new CryptoUtilLib();
		if (!summaryId.equals(NOT_APPLICABLE) && groupOfSchoolId != 0) {
			if (summaryType.equalsIgnoreCase(SummaryType.Category.getSummaryType())) {
				schoolWiseList = studentCountRepository.getCategoryWiseSchoolList(sansthaKey, yearList, summaryId,
						renewDate, groupOfSchoolId);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Minority.getSummaryType())) {
				schoolWiseList = studentCountRepository.getMinorityWiseSchoolList(sansthaKey, yearList, summaryId,
						renewDate, groupOfSchoolId);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Concession.getSummaryType())) {
				schoolWiseList = studentCountRepository.getConcessionWiseSchoolList(sansthaKey, yearList, summaryId,
						renewDate, groupOfSchoolId);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Religion.getSummaryType())) {
				schoolWiseList = studentCountRepository.getReligionWiseSchoolList(sansthaKey, yearList, summaryId,
						renewDate, groupOfSchoolId);
			}
		} else if (summaryId.equals(NOT_APPLICABLE) && groupOfSchoolId != 0) {
			if (summaryType.equalsIgnoreCase(SummaryType.Category.getSummaryType())) {
				schoolWiseList = studentCountRepository.getCategoryWiseSchoolList(sansthaKey, yearList, renewDate,
						groupOfSchoolId);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Minority.getSummaryType())) {
				schoolWiseList = studentCountRepository.getMinorityWiseSchoolList(sansthaKey, yearList, renewDate,
						groupOfSchoolId);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Concession.getSummaryType())) {
				schoolWiseList = studentCountRepository.getConcessionWiseSchoolList(sansthaKey, yearList, renewDate,
						groupOfSchoolId);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Religion.getSummaryType())) {
				schoolWiseList = studentCountRepository.getReligionWiseSchoolList(sansthaKey, yearList, renewDate,
						groupOfSchoolId);
			}
		} else if (!summaryId.equals(NOT_APPLICABLE) && groupOfSchoolId == 0) {
			if (summaryType.equalsIgnoreCase(SummaryType.Category.getSummaryType())) {
				schoolWiseList = studentCountRepository.getCategoryWiseSchoolList(sansthaKey, yearList, summaryId,
						renewDate);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Minority.getSummaryType())) {
				schoolWiseList = studentCountRepository.getMinorityWiseSchoolList(sansthaKey, yearList, summaryId,
						renewDate);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Concession.getSummaryType())) {
				schoolWiseList = studentCountRepository.getConcessionWiseSchoolList(sansthaKey, yearList, summaryId,
						renewDate);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Religion.getSummaryType())) {
				schoolWiseList = studentCountRepository.getReligionWiseSchoolList(sansthaKey, yearList, summaryId,
						renewDate);
			}
		} else if (summaryId.equals(NOT_APPLICABLE) && groupOfSchoolId == 0) {
			if (summaryType.equalsIgnoreCase(SummaryType.Category.getSummaryType())) {
				schoolWiseList = studentCountRepository.getCategoryWiseSchoolList(sansthaKey, yearList, renewDate);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Minority.getSummaryType())) {
				schoolWiseList = studentCountRepository.getMinorityWiseSchoolList(sansthaKey, yearList, renewDate);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Concession.getSummaryType())) {
				schoolWiseList = studentCountRepository.getConcessionWiseSchoolList(sansthaKey, yearList, renewDate);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Religion.getSummaryType())) {
				schoolWiseList = studentCountRepository.getReligionWiseSchoolList(sansthaKey, yearList, renewDate);

			}
		}

		if (!profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
				|| profileRole.equalsIgnoreCase("Super officer")) {
			List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
			List<StudentCountBean> newSchoolWiseList = new ArrayList<StudentCountBean>();
			for (int i = 0; i < schoolWiseList.size(); i++) {
				if (schoolList.contains(schoolWiseList.get(i).getSchoolid())) {
					newSchoolWiseList.add(schoolWiseList.get(i));
				}
			}
			schoolWiseList.clear();
			schoolWiseList = newSchoolWiseList;
		}

		for (int i = 0; i < schoolWiseList.size(); i++) {
			StudentCountBean schoolNameBean = schoolWiseList.get(i);
			try {
				schoolNameBean.setLongSchoolName(
						cry.decrypt(IngenioCodes.getSchoolname(), schoolNameBean.getLongSchoolName()));
			} catch (Exception e) {
				e.printStackTrace();
			}

			Long totalSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + schoolNameBean.getTotalCount()));
			Long boysSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + schoolNameBean.getBoysCount()));
			Long girlsSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + schoolNameBean.getGirlsCount()));
			schoolNameBean.setBoysCountInWords("" + boysSchoolCountInWords);
			schoolNameBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
			schoolNameBean.setTotalCountInWords("" + totalSchoolCountInWords);
			schoolNameBean.setYearId(commonUtility.getCurrentYear(schoolNameBean.getSchoolid(), renewDate));
			schoolWiseList.set(i, schoolNameBean);
		}
		studentCountBean1.setSchoolWiseStudentList(schoolWiseList);
		studentcountlist.add(studentCountBean);
		studentcountlist.add(studentCountBean1);
		return studentcountlist;
	}

	@Override
	public List<StudentCountBean> groupWiseSummaryList(String sansthaKey, String yearName, String summaryType,
			String summaryId, String renewDate, String mobileNo, String profileRole) {
		List<StudentCountBean> studentcountlist = new ArrayList<>();
		StudentCountBean studentCountBean = new StudentCountBean();
		List<String> yearList = new ArrayList<>();
		List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
		if (CollectionUtils.isNotEmpty(appLanguageList)) {
			for (int i = 0; i < appLanguageList.size(); i++) {
				yearList.add(Utillity.convertEnglishToOther(yearName, "" + appLanguageList.get(i).getLanguageId()));
			}
		}

		if (!summaryId.equals(NOT_APPLICABLE)) {
			if (summaryType.equalsIgnoreCase(SummaryType.Category.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getSummaryWiseCategoryCount(sansthaKey, yearList, summaryId,
							renewDate);					
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getSummaryWiseCategoryCount(sansthaKey, yearList, summaryId,
							renewDate,schoolList);
			   }
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Minority.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getSummaryWiseMinorityCount(sansthaKey, yearList, summaryId,
							renewDate);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getSummaryWiseMinorityCount(sansthaKey, yearList, summaryId,
							renewDate,schoolList);
			   }
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Concession.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getSummaryWiseConcessionCount(sansthaKey, yearList, summaryId,
							renewDate);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getSummaryWiseConcessionCount(sansthaKey, yearList, summaryId,
							renewDate,schoolList);
			   }
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Religion.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getSummaryWiseReligionCount(sansthaKey, yearList, summaryId,
							renewDate);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getSummaryWiseReligionCount(sansthaKey, yearList, summaryId,
							renewDate,schoolList);
			   }
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			}
		} else {
			if (summaryType.equalsIgnoreCase(SummaryType.Category.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getSummaryWiseCategoryCount(sansthaKey, yearList, renewDate);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getSummaryWiseCategoryCount(sansthaKey, yearList, renewDate,schoolList);
			   }
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Minority.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getSummaryWiseMinorityCount(sansthaKey, yearList, renewDate);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getSummaryWiseMinorityCount(sansthaKey, yearList, renewDate,schoolList);
			   }
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Concession.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getSummaryWiseConcessionCount(sansthaKey, yearList,
							renewDate);				
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getSummaryWiseConcessionCount(sansthaKey, yearList,
							renewDate,schoolList);	
					}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			} else if (summaryType.equalsIgnoreCase(SummaryType.Religion.getSummaryType())) {
				if (profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
						|| profileRole.equalsIgnoreCase("Super officer")) {
					studentCountBean = studentCountRepository.getSummaryWiseReligionCount(sansthaKey, yearList, renewDate);
				} else {
					List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
					studentCountBean = studentCountRepository.getSummaryWiseReligionCount(sansthaKey, yearList, renewDate,schoolList);
					}
				Long totalSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getTotalCount()));
				Long boysSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getBoysCount()));
				Long girlsSchoolCountInWords = Long
						.parseLong(Utillity.convertNumberToWord("" + studentCountBean.getGirlsCount()));
				studentCountBean.setBoysCountInWords("" + boysSchoolCountInWords);
				studentCountBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
				studentCountBean.setTotalCountInWords("" + totalSchoolCountInWords);
			}
		}

		StudentCountBean studentCountBean1 = new StudentCountBean();
		List<StudentCountBean> schoolWiseList = new ArrayList<>();
		if (!summaryId.equals(NOT_APPLICABLE)) {
			if (summaryType.equalsIgnoreCase(SummaryType.Category.getSummaryType())) {
				schoolWiseList = studentCountRepository.getCategoryWiseGroupList(sansthaKey, yearList, summaryId,
						renewDate);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Minority.getSummaryType())) {
				schoolWiseList = studentCountRepository.getMinorityWiseGroupList(sansthaKey, yearList, summaryId,
						renewDate);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Concession.getSummaryType())) {
				schoolWiseList = studentCountRepository.getConcessionWiseGroupList(sansthaKey, yearList, summaryId,
						renewDate);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Religion.getSummaryType())) {
				schoolWiseList = studentCountRepository.getReligionWiseGroupList(sansthaKey, yearList, summaryId,
						renewDate);

			}
		} else {
			if (summaryType.equalsIgnoreCase(SummaryType.Category.getSummaryType())) {
				schoolWiseList = studentCountRepository.getCategoryWiseGroupList(sansthaKey, yearList, renewDate);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Minority.getSummaryType())) {
				schoolWiseList = studentCountRepository.getMinorityWiseGroupList(sansthaKey, yearList, renewDate);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Concession.getSummaryType())) {
				schoolWiseList = studentCountRepository.getConcessionWiseGroupList(sansthaKey, yearList, renewDate);

			} else if (summaryType.equalsIgnoreCase(SummaryType.Religion.getSummaryType())) {
				schoolWiseList = studentCountRepository.getReligionWiseGroupList(sansthaKey, yearList, renewDate);

			}
		}

		if (!profileRole.equalsIgnoreCase("ROLE_SUPEROFFICER") || profileRole.equalsIgnoreCase("Super Admin")
				|| profileRole.equalsIgnoreCase("Super officer")) {
			List<Integer> schoolList = getSchoolList(mobileNo, sansthaKey);
			List<StudentCountBean> newSchoolWiseList = new ArrayList<StudentCountBean>();
			for (int i = 0; i < schoolWiseList.size(); i++) {
				if (schoolList.contains(schoolWiseList.get(i).getSchoolid())) {
					newSchoolWiseList.add(schoolWiseList.get(i));
				}
			}
			schoolWiseList.clear();
			schoolWiseList = newSchoolWiseList;
		}

		for (int i = 0; i < schoolWiseList.size(); i++) {
			StudentCountBean schoolNameBean = schoolWiseList.get(i);

			Long totalSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + schoolNameBean.getTotalCount()));
			Long boysSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + schoolNameBean.getBoysCount()));
			Long girlsSchoolCountInWords = Long
					.parseLong(Utillity.convertNumberToWord("" + schoolNameBean.getGirlsCount()));
			schoolNameBean.setBoysCountInWords("" + boysSchoolCountInWords);
			schoolNameBean.setGirlsCountInWords("" + girlsSchoolCountInWords);
			schoolNameBean.setTotalCountInWords("" + totalSchoolCountInWords);
			schoolWiseList.set(i, schoolNameBean);
		}
		studentCountBean1.setSummaryWiseGroupList(schoolWiseList);
		studentcountlist.add(studentCountBean);
		studentcountlist.add(studentCountBean1);
		return studentcountlist;

	}

	@Override
	public List<SummaryMenuBean> getSummaryMenuList() {
		return summaryDetailRepository.getSummaryMenuList();
	}

	public List<Integer> getSchoolList(String MobileNo, String SansthaKey) {
//		List<SchoolBean> schoolMasterList = schoolMasterRepository.getSchoolList(SansthaKey, MobileNo);
		List<SchoolMasterModel> schoolMasterList = schoolMasterRepository.findBySansthaKey(SansthaKey);
		List<Integer> schoolList = new ArrayList<>();
		for (int i = 0; i < schoolMasterList.size(); i++) {
			schoolList.add(schoolMasterList.get(i).getSchoolid());
		}
		return schoolList;
	}

}
