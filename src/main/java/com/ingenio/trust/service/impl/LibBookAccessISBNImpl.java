package com.ingenio.trust.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.trust.bean.LibBookAccessISBN;
import com.ingenio.trust.bean.LibBookAccessISBNFilterParam;
import com.ingenio.trust.bean.LibBookAccessISBNPage;
import com.ingenio.trust.bean.Paging;
import com.ingenio.trust.model.LibBookAccessISBNModel;
import com.ingenio.trust.repository.LibBookAccessISBNRepository;
import com.ingenio.trust.repository.LibraryRepository;
import com.ingenio.trust.service.LibBookAccessISBNService;

@Service
@Transactional
public class LibBookAccessISBNImpl implements LibBookAccessISBNService {

	@Autowired
	LibraryRepository libraryRepository;

	@Autowired
	private ModelMapper mapper;

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	LibBookAccessISBNRepository libBookAccessISBNRepository;

	
	@Override
	public LibBookAccessISBNPage getLibBookAccessISBN(LibBookAccessISBNFilterParam filterParams) {
		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<LibBookAccessISBNModel> criteriaQuery = criteria.createQuery(LibBookAccessISBNModel.class);
		Root<LibBookAccessISBNModel> rootLibStudentAdmissionModel = criteriaQuery
				.from(LibBookAccessISBNModel.class);
		Predicate filterPredicate = criteria.conjunction();

//		filterParams.getAccessionNo();
        String accessionNum = filterParams.getAccessionNo().replaceAll("\\D+", "");

		
//		if (filterParams.getAccessionNo() != null) {
//			Predicate accessionNoFilter = criteria.equal(rootLibStudentAdmissionModel.get("accessNumber"),
//					filterParams.getAccessionNo());
//			filterPredicate = criteria.and(filterPredicate, accessionNoFilter);
//		}
        
        if (filterParams.getAccessionNo() != null) {
			Predicate accessionNoFilter = criteria.equal(rootLibStudentAdmissionModel.get("accessNumber"),
					accessionNum);
			filterPredicate = criteria.and(filterPredicate, accessionNoFilter);
		}
        
		if (filterParams.getSchoolId() != null) {
			Predicate schoolIdFilter = criteria.equal(rootLibStudentAdmissionModel.get("schoolId"),
					filterParams.getSchoolId());
			filterPredicate = criteria.and(filterPredicate, schoolIdFilter);
		}
		if (filterParams.getCategoryId() != null) {
			Predicate categoryIdFilter = criteria.equal(rootLibStudentAdmissionModel.get("categoryId"),
					filterParams.getCategoryId());
			filterPredicate = criteria.and(filterPredicate, categoryIdFilter);
		}
		if (filterParams.getIsbnNumber() != null) {
			Predicate isbnNumberFilter = criteria.equal(rootLibStudentAdmissionModel.get("isbnNumber"),
					filterParams.getIsbnNumber());
			filterPredicate = criteria.and(filterPredicate, isbnNumberFilter);
		}
		
		criteriaQuery.where(filterPredicate);
		TypedQuery<LibBookAccessISBNModel> query = entityManager.createQuery(criteriaQuery);

		if (filterParams.getSize() > 0) {
			/*
			 * my mysql - http://www.mysqltutorial.org/mysql-limit.aspx
			 */
			int offset = 0;
			if (filterParams.getPage() > 0) {
				offset = filterParams.getPage() * filterParams.getSize();
			}

			query.setFirstResult(offset);
			query.setMaxResults(filterParams.getSize());
		}

		List<LibBookAccessISBNModel> entities = query.getResultList();

		// Count query to get the total number of pages
		CriteriaQuery<Long> countQuery = criteria.createQuery(Long.class);
		countQuery.select(criteria.count(countQuery.from(LibBookAccessISBNModel.class)));
		countQuery.where(filterPredicate);
		Long count = entityManager.createQuery(countQuery).getSingleResult();
		int totalPages = 0;
		if (filterParams.getSize() == 0) {
			totalPages = 1;
		} else if (null != count) {
			totalPages = (int) Math.ceil((double) count.intValue() / filterParams.getSize());
		}

		Paging paging = new Paging();
		paging.setPageSize(filterParams.getSize());
		paging.setTotalPages(totalPages);
		List<LibBookAccessISBN> data = new ArrayList<LibBookAccessISBN>();
		entities.forEach(entity -> {
			data.add(mapper.map(entity, LibBookAccessISBN.class));
		});

		LibBookAccessISBNPage libBookAccessISBNPage = new LibBookAccessISBNPage();
		libBookAccessISBNPage.setData(data);
		libBookAccessISBNPage.setPaging(paging);

		return libBookAccessISBNPage;

	}


	@Override
	public LibBookAccessISBNModel addLibBookAccessISBN(LibBookAccessISBNModel libBookAccessISBNModel) {
		libBookAccessISBNModel = libBookAccessISBNRepository.save(libBookAccessISBNModel);
		return libBookAccessISBNModel;
	}
}
