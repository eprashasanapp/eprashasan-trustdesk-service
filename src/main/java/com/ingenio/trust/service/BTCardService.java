package com.ingenio.trust.service;

import com.ingenio.trust.bean.BTCardRecordDetails;
import com.ingenio.trust.bean.BTCardRecordFilterParam;
import com.ingenio.trust.bean.BTCardRecordPage;

public interface BTCardService {

	BTCardRecordPage getBTCardDetails(BTCardRecordFilterParam queryParams);

	BTCardRecordPage getBTCardRenewedRecordDetails(BTCardRecordFilterParam queryParams);

	BTCardRecordDetails addBTCard(BTCardRecordDetails bTCardDetails);

}
