package com.ingenio.trust.service;

import java.util.List;

import com.ingenio.trust.bean.StaffDetailsBean;



public interface StaffDetailService {

	List<StaffDetailsBean> getStaffDetails(Integer schoolId, String yearname, String renewDate);

	List<StaffDetailsBean> getSansthaWiseStaffDetails(String sansthaKey, String yearname, String renewDate, Integer groupOfSchoolId);

	List<StaffDetailsBean> getgroupWiseStaffDetails(String sansthaKey, String yearname, String renewDate);



}
