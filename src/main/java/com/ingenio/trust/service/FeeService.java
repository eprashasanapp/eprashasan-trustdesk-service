package com.ingenio.trust.service;

import java.util.List;

import com.ingenio.trust.bean.FeeBean;

public interface FeeService {

	List<FeeBean> getTotalFeeSchoolWise(String sansthaKey, String yearName, String feeToDate, Integer groupId,String mobileNo, String profileRole);

	List<FeeBean> getTotalFeeGroupWise(String sansthaKey, String yearName, String feeToDate, String mobileNo, String profileRole);
}
