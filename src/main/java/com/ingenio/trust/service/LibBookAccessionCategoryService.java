package com.ingenio.trust.service;

import com.ingenio.trust.model.LibBookAccessionCategoryModel;

public interface LibBookAccessionCategoryService {

	LibBookAccessionCategoryModel getLibBookAccessionCategoryDetailsByName(String cName, Integer schoolId);

	LibBookAccessionCategoryModel addLibBookAccessionCategory(LibBookAccessionCategoryModel libBookAccessionCategoryModel);

}
