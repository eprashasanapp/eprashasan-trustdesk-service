package com.ingenio.trust.service;

import com.ingenio.trust.model.LibBookSupplierModel;

public interface LibBookSupplierService {

	LibBookSupplierModel getLibBookSupplierDetailsByName(String cName, Integer schoolId);

	LibBookSupplierModel addLibBookSupplier(LibBookSupplierModel libBookSupplierModel);

}
