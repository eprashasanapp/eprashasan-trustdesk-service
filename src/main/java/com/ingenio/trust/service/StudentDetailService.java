package com.ingenio.trust.service;

import java.util.List;

import com.ingenio.trust.bean.AttendanceReportBean;
import com.ingenio.trust.bean.StudentCountBean;



public interface StudentDetailService {


public List<AttendanceReportBean> getStudentAttendanceDetails(Integer schoolId, String label, String attendedDate, Integer standardId, Integer divisionId);

public List<StudentCountBean> getDivisionwiseStudentDetails(Integer schoolId, String label, Integer yearId,
		Integer standardId, Integer divisionId, String renewDate);

}
