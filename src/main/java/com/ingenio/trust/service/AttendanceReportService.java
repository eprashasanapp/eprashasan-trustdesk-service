package com.ingenio.trust.service;

import java.util.List;

import com.ingenio.trust.bean.AttendanceReportBean;









public interface AttendanceReportService {
	

	public List<AttendanceReportBean> getSchoolWiseAttendance(String sansthaKey, String attendedDate, Integer groupOfSchoolId, String mobileNo, String profileRole);
	public List<AttendanceReportBean> getstandardWiseAttendance(Integer schoolId, String attendedDate);
	public List<AttendanceReportBean> getdivisionWiseAttendance(Integer schoolId, String attendedDate, Integer standardId);
	public List<AttendanceReportBean> getgGoupWiseAttendance(String sansthaKey, String attendedDate, String mobileNo, String profileRole);





}
