package com.ingenio.trust;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
//@PropertySource("file:C:/updatedwars/configuration/trustDesk.properties")
public class ConfigurationProperties {

	public static  String uploadImageUrl;
	public static String studentPhotoUrl;
	public static String staffPhotoUrl;

	public String getUploadImageUrl() {
		return uploadImageUrl;
	}

	@Value("${uploadImageUrl}")
	public  void setUploadImageUrl(String uploadImageUrl) {
		ConfigurationProperties.uploadImageUrl = uploadImageUrl;
	}

	@Value("${studentPhotoUrl}")
	public  void setStudentPhotoUrl(String studentPhotoUrl) {
		ConfigurationProperties.studentPhotoUrl = studentPhotoUrl;
	}

}
