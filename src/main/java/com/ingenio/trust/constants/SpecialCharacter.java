package com.ingenio.trust.constants;

public enum SpecialCharacter {

	FILEPATH("/"),
	
	DOT("."),
	
	PERCENTAGE("%"),
	
	HASH("#"),
	
	DASH("-"),
	
	R_N_EXPRESSION("[\\r\\n]+");
	
	private final String characterName;
	
	private SpecialCharacter(String characterName) {
        this.characterName = characterName;
    }

    public String getCharacterName() {
        return characterName;
    }
}
