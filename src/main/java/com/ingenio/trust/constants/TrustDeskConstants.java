package com.ingenio.trust.constants;

public class TrustDeskConstants {
	private TrustDeskConstants() {
		super();
	}

	public static final String FILEPATH = "/";
	
	public static final String EXTENSION = "jpg";
	
	public static final String DOT = ".";
	
	public static final String TOTAL = "Total";
	
	public static final String TEACHER = "Teacher";

	public static final String GIRLS = "Girls";

	public static final String BOYS = "Boys";
	
	public static final String TILL_DATE_STUDENT_SUMMARY = "Till Date # Student Summary";

	public static final String TILL_DATE_FEE_COLLECTION = "Till Date # Fee Collection";

	public static final String REMAINING = "Remaining";

	public static final String RECEIVED = "Received";

	public static final String TILL_DATE_FEE_SUMMARY = "Till Date # Fee Summary";

	public static final String ABSENT = "Absent";

	public static final String PRESENT = "Present";

	public static final String TOTAL_STUDENTS = "Total Students";

	public static final String TODAYS_ATTENDANCE = "Today's # Attendance";

	public static final String PARENT = "Parent";

	public static final String SCHOOL_NAME = "SchoolName";

	public static final String PRINCIPAL_NAME = "PrincipalName";

	public static final String ENC_SCHOOL_NAME = "qwertyschoolname";

	public static final String R_N_EXPRESSION = "[\\r\\n]+";
	
	public static final String GLOBAL_DB_SCHOOL_KEY = "1";
	
	public static final String DASH = "-";

	public static final String NON_TEACHER = "Non Teacher";

	public static final String STUDENT = "Student";


}

