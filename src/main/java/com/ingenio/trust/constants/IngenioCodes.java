package com.ingenio.trust.constants;

public class IngenioCodes {
	private static String answer1="qwertyans";
	private static String settingStrin="qwertycheck";
	private static String pass1="fdo";
	private static String schoolname="qwertyschoolname";
	private static String sansthaname="qwertysansthaname";
	private static String address="qwertyschooladdress";
	private static String regId="qwertyregid";
	private static String host="qzxdghw";
	private static String port="nblxzkp";
	
	public static String getAnswer1() {
		return answer1;
	}
	public static void setAnswer1(String answer1) {
		IngenioCodes.answer1 = answer1;
	}
	public static String getSettingStrin() {
		return settingStrin;
	}
	public static void setSettingStrin(String settingStrin) {
		IngenioCodes.settingStrin = settingStrin;
	}
	public static String getPass1() {
		return pass1;
	}
	public static void setPass1(String pass1) {
		IngenioCodes.pass1 = pass1;
	}
	public static String getSchoolname() {
		return schoolname;
	}
	public static void setSchoolname(String schoolname) {
		IngenioCodes.schoolname = schoolname;
	}
	public static String getSansthaname() {
		return sansthaname;
	}
	public static void setSansthaname(String sansthaname) {
		IngenioCodes.sansthaname = sansthaname;
	}
	public static String getAddress() {
		return address;
	}
	public static void setAddress(String address) {
		IngenioCodes.address = address;
	}
	public static String getRegId() {
		return regId;
	}
	public static void setRegId(String regId) {
		IngenioCodes.regId = regId;
	}
	public static String getHost() {
		return host;
	}
	public static void setHost(String host) {
		IngenioCodes.host = host;
	}
	public static String getPort() {
		return port;
	}
	public static void setPort(String port) {
		IngenioCodes.port = port;
	}
	
}
