package com.ingenio.trust;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
@EntityScan(basePackages = "com.ingenio.trust.model")
public class EPrashashanTrustDeskApplication {

	public static void main(String[] args) {
		SpringApplication.run(EPrashashanTrustDeskApplication.class, args);
	}
	
	 @Bean
	    public RestTemplate restTemplate(RestTemplateBuilder builder) {
	        return builder.build();
	    }

	   @Bean
	    public ModelMapper getModelMapperBean(){
	        return new ModelMapper();
	    }
}
