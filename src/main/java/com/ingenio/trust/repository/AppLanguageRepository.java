package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.AppLanguageModel;

@Repository
public interface AppLanguageRepository extends JpaRepository<AppLanguageModel, Integer> {
	
}
