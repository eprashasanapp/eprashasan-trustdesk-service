package com.ingenio.trust.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.bean.PrincipalMenuBean;
import com.ingenio.trust.model.TrustMenuModel;

@Repository
public interface TrustMenuRepository extends JpaRepository<TrustMenuModel, Integer> {

	
	@Query(value = "select new com.ingenio.trust.bean.PrincipalMenuBean(tm.trustMenuId as menuId , tm.menuName as menuName, tm.imageFilePath as iconPath) from TrustMenuModel tm "
			+ " left join AndroidMenuMasterGroupModel b on b.androidMenuGroupId=tm.androidMenuMasterGroupModel.androidMenuGroupId "
			+ " where b.androidMenuGroupId=:groupId  " )
	List<PrincipalMenuBean> getMenuList(Integer groupId);

	
	@Query(value = "select new com.ingenio.trust.bean.PrincipalMenuBean(b.androidMenuGroupId,b.androidMenuGroupName ) "
			 + "from AndroidMenuMasterGroupModel b  ")	
	List<PrincipalMenuBean> getGroups();
	
}

	




