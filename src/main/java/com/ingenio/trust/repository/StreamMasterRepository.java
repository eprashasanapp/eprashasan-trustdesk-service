package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.StreamMasterModel;

@Repository
public interface StreamMasterRepository  extends JpaRepository<StreamMasterModel, Integer>{

	@Query("From StreamMasterModel c where c.schoolid = :schoolId AND c.stream = :stream ")
	StreamMasterModel findByName(@Param("schoolId") Integer schoolId, @Param("stream") String stream);

}
