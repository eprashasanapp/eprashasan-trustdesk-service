package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.SansthaAnnouncementAttachmentModel;

@Repository
public interface SansthaAnnouncementAttachmentRepository extends JpaRepository<SansthaAnnouncementAttachmentModel, Integer> {


}
