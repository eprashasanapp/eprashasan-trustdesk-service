package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.LibBookAccessionCategoryModel;

@Repository
public interface LibBookAccessionCategoryRepository  extends JpaRepository<LibBookAccessionCategoryModel, Integer>{

	
    @Query("From LibBookAccessionCategoryModel where schoolId = :schoolId AND categoryName = :categoryName")
	LibBookAccessionCategoryModel getEntityByName(@Param("schoolId") Integer schoolId, @Param("categoryName") String categoryName);

}
