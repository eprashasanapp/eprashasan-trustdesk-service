package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.BTCardModel;

@Repository
public interface BTCardRepository  extends JpaRepository<BTCardModel, Integer>{

}
