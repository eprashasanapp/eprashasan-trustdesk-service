package com.ingenio.trust.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.trust.bean.AnnouncementResponseBean;
import com.ingenio.trust.bean.SansthaUserBean;
import com.ingenio.trust.bean.SchoolBean;
import com.ingenio.trust.bean.StaffDetailsBean;
import com.ingenio.trust.model.SansthaAnnouncementModel;
import com.ingenio.trust.model.StaffBasicDetailsModel;

public interface SansthaAnnouncementRepository extends JpaRepository<SansthaAnnouncementModel, Integer>{

//	@Query(value="SELECT COALESCE(MAX(a.announcementId),0)+1 from AnnouncementModel a where a.schoolMasterModel.schoolid=:schoolId ")
//	String getMaxId(Integer schoolId);
//
//	@Query(value="SELECT COALESCE(MAX(a.announcementAssignToId),0)+1 from AnnouncementAssignToModel a where a.schoolMasterModel.schoolid=:schoolId ")
//	String getMaxAssignToId(Integer schoolId);
//
//	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(COALESCE(a.announcementId,0),COALESCE(a.announcement,''),"
//			+ "COALESCE(a.announcementTitle,''),COALESCE(a.startDate,''), "
//			+ "COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementModel.announcementId=a.announcementId group by a1.announcementModel.announcementId),0),"
//			+ "COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementStatus IN (1,2,3) and a1.announcementModel.announcementId=a.announcementId group by a1.announcementModel.announcementId),0),"
//			+ "(COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementStatus IN (1,2,3) and a1.announcementModel.announcementId=a.announcementId group by a1.announcementModel.announcementId),0) - "
//			+ "COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementStatus IN (2,3) and a1.announcementModel.announcementId=a.announcementId group by a1.announcementModel.announcementId),0)),"
//			+ "COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementStatus IN (2,3) and a1.announcementModel.announcementId=a.announcementId group by a1.announcementModel.announcementId),0),"
//			+ "COALESCE((select count(a1) from AnnouncementAssignToModel a1 where a1.announcementStatus=3 and a1.announcementModel.announcementId=a.announcementId group by a1.announcementModel.announcementId),0),"
//			+ "COALESCE((select count(c1.announcementAttachmentId) from AnnouncementAttachmentModel c1 where c1.announcementModel.announcementId=a.announcementId group by c1.announcementModel.announcementId),0))  "
//			+ "from AnnouncementModel a "
//			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
//			+ "left join AnnouncementAttachmentModel c on c.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid "
//			+ "where a.staffId=?1 and b.yearMasterModel.yearId=?2 and a.announcementId=?3 "
//			+ "group by a.announcementId  order by a.CDate desc ")
//	List<AnnouncementResponseBean> findAssignedAnnouncementCount(Integer staffId, Integer yearId,Integer announcementId);
//
//	@Query(value="select new com.ingenio.announcement.bean.StandardDivisionBean(COALESCE(a.standardId,0),COALESCE(a.standardName,''),COALESCE(b.divisionId,0),COALESCE(b.divisionName,'')) from StandardMasterModel a "
//			+ "left join DivisionMasterModel b on ( a.standardId=b.standardMasterModel.standardId and b.schoolMasterModel.schoolid=a.schoolMasterModel.schoolid ) where a.schoolMasterModel.schoolid=:schoolId order by a.standardPriority , b.divisionId ")
//	List<StandardDivisionBean> getStandardDivisionList(Integer schoolId);
//
	
//	
//	@Query(value="select new com.ingenio.announcement.bean.AnnouncementResponseBean(a.announcementId,COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
//			+ "COALESCE(a.startDate,''), "
//			+ "COALESCE((select COALESCE(count(c1.announcementAttachmentId),0) from AnnouncementAttachmentModel c1 "
//			+ "where c1.announcementModel.announcementId=a.announcementId "
//			+ "group by c1.announcementModel.announcementId),0),COALESCE(CONCAT(COALESCE(e.firstname,''),' ',COALESCE(e.lastname,'')),'') as staffName,"
//			+ "COALESCE(f.roleName,''),COALESCE(b.announcementStatus,''),COALESCE(b.announcementAssignToId,''),a.schoolMasterModel.schoolid,COALESCE(e.sregNo,''),COALESCE(a.staffId,0) ) "
//			+ "from AnnouncementModel a "
//			+ "left join AnnouncementAssignToModel b on b.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
//			+ "left join AnnouncementAttachmentModel c on c.announcementModel.announcementId=a.announcementId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid "
//			+ "left join StaffBasicDetailsModel e on e.staffId=a.staffId and a.schoolMasterModel.schoolid=e.schoolMasterModel.schoolid "
//			+ "left join AppUserModel f on f.appUserRoleId=a.userId.appUserRoleId "
//			+ "where ((b.staffStudentId=:staffStudentId and b.role=:profileRole) OR a.staffId=:staffStudentId) and a.startDate=:date and b.yearMasterModel.yearId=:yearId group by a.announcementId order by a.CDate desc ")
//	List<AnnouncementResponseBean> getStudentAnnouncement(Integer staffStudentId, Integer yearId, String profileRole,Date date, Pageable pageableo);
//
//	@Query(value="SELECT coalesce(MAX(a.announcementAttachmentId),0)+1 from AnnouncementAttachmentModel a where a.schoolMasterModel.schoolid=:schoolId ")
//	String getMaxAttachmentId(Integer schoolId);
//	
	@Query(value="select new com.ingenio.trust.bean.AnnouncementResponseBean(coalesce(a.serverFilePath,''),coalesce(a.sansthaAnnouncementAttachmentId,0)) from SansthaAnnouncementAttachmentModel a where a.sansthaAnnouncementModel.sansthaAnnouncementId=:announcementId ")
	List<AnnouncementResponseBean> getAttachments(Integer announcementId);
//
//	@Query(value="select new com.ingenio.announcement.bean.StudentDetailsBean( COALESCE(c.renewStudentId,0),COALESCE(d.studentId,0), "
//			+ "COALESCE(CONCAT(COALESCE(d.studInitialName,''),' ',COALESCE(d.studFName,''),' ',COALESCE(d.studMName,''),' ',COALESCE(d.studLName,'')),''), "
//			+ "COALESCE(c.rollNo,0),COALESCE(b.announcementStatus,''),COALESCE(d.studentRegNo,''),d.schoolMasterModel.schoolid,COALESCE(d.grBookName.grBookId,0) ) from AnnouncementModel a "
//			+ "left join AnnouncementAssignToModel b on a.announcementId=b.announcementModel.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
//			+ "left join StudentStandardRenewModel c on c.renewStudentId=b.staffStudentId and a.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid  "
//			+ "left join StudentMasterModel d on d.studentId=c.studentMasterModel.studentId and d.schoolMasterModel.schoolid=c.schoolMasterModel.schoolid  "
//			+ "where a.announcementId=?1 and b.role='Parent' and  d.isApproval='0' and d.isAdmissionCancelled='1' order by b.announcementStatus,c.rollNo  ")
//	List<StudentDetailsBean> getStudentDetailsForAnnouncement(Integer announcementId);
//
//	@Query(value="select new com.ingenio.announcement.bean.StudentDetailsBean( COALESCE(c.staffId,0),"
//			+ "COALESCE(CONCAT(COALESCE(c.initialname,''),' ',COALESCE(c.firstname,''),' ',COALESCE(c.secondname,''),' ',COALESCE(c.lastname,'')),''), "
//			+ "COALESCE(b.announcementStatus,''),COALESCE(c.sregNo,''),c.schoolMasterModel.schoolid) from AnnouncementModel a "
//			+ "left join AnnouncementAssignToModel b on a.announcementId=b.announcementModel.announcementId and a.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
//			+ "left join StaffBasicDetailsModel c on c.staffId=b.staffStudentId and c.schoolMasterModel.schoolid=b.schoolMasterModel.schoolid "
//			+ "where a.announcementId=?1 and b.role='Teacher' order by b.announcementStatus,c.staffId  ")
//	List<StudentDetailsBean> getStaffDetailsForAnnouncement(Integer announcementId);
//	
	
	@Query(value = "select new com.ingenio.trust.bean.SchoolBean(COALESCE(c.schoolid,0),COALESCE(c.schoolName,''),COALESCE(c.shortSchoolName,'') ) " 
			+"FROM SansthaUserMapToSchoolModel a "
			+"left join SansthaUserRegistrationModel b on (a.sansthaUserRegistrationModel.sansthaUserId = b.sansthaUserId ) "
			+"left join SchoolMasterModel c on (c.schoolid = a.schoolMasterModel.schoolid ) "
			+"where b.sansthaUserRegNo=:sansthaKey and a.sansthaUserRegistrationModel.sansthaUserId=:sansthaUserId ")
	List<SchoolBean> getSchoolList(String sansthaKey, Integer sansthaUserId);

	@Query(value = "select new com.ingenio.trust.bean.SansthaUserBean(COALESCE(c.sansthaUserId,0),COALESCE(c.sansthaRegistrationModel.sansthaId,0) , "
			+ "COALESCE(c.sansthaUserRegNo,'') , COALESCE(c.initialName,''), COALESCE(c.firstName,'') ,COALESCE(c.secondName,'') ,"
			+ "COALESCE(c.lastName,'') ,COALESCE(c.birthDate,'') ,COALESCE(c.gender,'')) " 
			+"FROM SansthaUserRegistrationModel c "		
			+"where c.sansthaUserRegNo=:sansthaKey ")
	List<SansthaUserBean> getSansthaUserList(String sansthaKey);

	
	@Query(value = "select new com.ingenio.trust.bean.StaffDetailsBean(COALESCE(c.schoolid,0),COALESCE(c.schoolName,''),COALESCE(d.firstName,'') , "
			+ "COALESCE(d.lastName,'') , COALESCE(d.staffId,0) ) " 
			+"FROM SansthaUserMapToSchoolModel a "
			+"left join SansthaUserRegistrationModel b on (a.sansthaUserRegistrationModel.sansthaUserId = b.sansthaUserId ) "
			+"left join SchoolMasterModel c on (c.schoolid = a.schoolMasterModel.schoolid ) "
			+"left join AppUserModel d on (c.schoolid = d.schoolMasterModel.schoolid and d.roleName='ROLE_PRINCIPAL' ) "
			+"where b.sansthaUserRegNo=:sansthaKey and a.sansthaUserRegistrationModel.sansthaUserId=:sansthaUserId ")
	List<StaffDetailsBean> getSchoolWisePrincipalList(String sansthaKey, Integer sansthaUserId);

	
//	@Query(value="select new com.ingenio.trust.bean.AnnouncementResponseBean(COALESCE(a.sansthaAnnouncementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
//			+ "COALESCE(a.startDate,''),COALESCE(b.role,'') , COALESCE(b.sansthaUserRegistrationModel.sansthaUserId,0) ,COALESCE(b.staffBasicDetailsModel.staffId,0) "
//			+ ",COALESCE(c.lastName,'') ,COALESCE(d.lastname,'')) "
//			+ "from SansthaAnnouncementModel a "
//			+ "left join SansthaAnnouncementAssignToModel b on b.sansthaAnnouncementModel.sansthaAnnouncementId=a.sansthaAnnouncementId "			
//			+ "left join SansthaUserRegistrationModel c on b.sansthaUserRegistrationModel.sansthaUserId=c.sansthaUserId "	
//			+ "left join StaffBasicDetailsModel d on b.staffBasicDetailsModel.staffId=d.staffId "
//			+ "where  a.sansthaUserRegistrationModel.sansthaUserId=:sansthauserId  ")
//	List<AnnouncementResponseBean> getMyannouncement(Integer sansthauserId);
	
	
	@Query(value="select new com.ingenio.trust.bean.AnnouncementResponseBean(COALESCE(a.sansthaAnnouncementId,0),COALESCE(a.announcement,''),COALESCE(a.announcementTitle,''),"
			+ "COALESCE(a.startDate,''), "
			+ "COALESCE((select COALESCE(count(c1.sansthaAnnouncementAttachmentId),0) from SansthaAnnouncementAttachmentModel c1 "
			+ "where c1.sansthaAnnouncementModel.sansthaAnnouncementId=a.sansthaAnnouncementId "
			+ "group by c1.sansthaAnnouncementModel.sansthaAnnouncementId),0),"
			+ " COALESCE(b.role,'') ,COALESCE(b.sansthaUserRegistrationModel.sansthaUserId,0) ,COALESCE(b.staffBasicDetailsModel.staffId,0) "
			+ ",COALESCE(c.lastName,'') ,COALESCE(d.lastname,'')) "
			+ "from SansthaAnnouncementModel a "
			+ "left join SansthaAnnouncementAssignToModel b on b.sansthaAnnouncementModel.sansthaAnnouncementId=a.sansthaAnnouncementId "	
			+ "left join SansthaAnnouncementAttachmentModel f on f.sansthaAnnouncementModel.sansthaAnnouncementId=a.sansthaAnnouncementId "
			+ "left join StaffBasicDetailsModel d on b.staffBasicDetailsModel.staffId=d.staffId "
			+ "left join SansthaUserRegistrationModel c on b.sansthaUserRegistrationModel.sansthaUserId=c.sansthaUserId "
			+ "where  a.sansthaUserRegistrationModel.sansthaUserId=:sansthauserId  ")
	List<AnnouncementResponseBean> getMyannouncement(Integer sansthauserId);

	
	@Query(value = "select new com.ingenio.trust.bean.SansthaUserBean(COALESCE(c.sansthaId,0),COALESCE(c.sansthaName,'') ) "
			+"FROM SansthaRegistrationModel c "		
			+"where c.sansthaKey=:sansthaKey ")
	List<SansthaUserBean> getSansthaDetails(String sansthaKey);


	
	
//	@Query(value="select a from StaffBasicDetailsModel a  "
//			+ "left join StaffRelivingModel s on ( s.staffBasicDetailsModel.staffId=a.staffId and s.isDel='0' ) "
//			+ "where a.schoolMasterModel.schoolid=:schoolId and (s.relievingDate is null or "
//			+ "(str_to_date(s.relievingDate,'%Y-%m-%d') > str_to_date(:currentDate,'%Y-%m-%d')))  ")
//	List<StaffBasicDetailsModel> findStaff(Integer schoolId, String currentDate);

}
