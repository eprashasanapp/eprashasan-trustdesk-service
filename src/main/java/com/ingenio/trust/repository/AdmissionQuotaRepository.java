package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.AdmissionQuotaModel;
@Repository
public interface AdmissionQuotaRepository  extends JpaRepository<AdmissionQuotaModel, Integer> {

	@Query("From AdmissionQuotaModel c where c.schoolid = :schoolId AND c.streamId = :streamId  AND c.categoryId = :categoryId ")
	AdmissionQuotaModel findByName(@Param("schoolId") Integer schoolId, @Param("streamId") Integer streamId, @Param("categoryId") Integer categoryId);

}
