package com.ingenio.trust.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.CategoryMasterModel;
import com.ingenio.trust.model.SchoolMasterModel;

@Repository
public interface CategoryMasterRepository  extends JpaRepository<CategoryMasterModel, Integer>{
	
	List<CategoryMasterModel> findBySchoolMasterModelOrderByCategoryId(SchoolMasterModel schoolMasterModel);

	@Query("From CategoryMasterModel c where c.schoolMasterModel.schoolid = :schoolId AND c.categoryName = :categoryName ")
	CategoryMasterModel findByName(@Param("schoolId") Integer schoolId, @Param("categoryName") String categoryName);

}
