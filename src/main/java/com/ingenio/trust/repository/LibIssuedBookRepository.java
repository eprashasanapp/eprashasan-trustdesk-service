package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.LibIssuedBookModel;

@Repository
public interface LibIssuedBookRepository  extends JpaRepository<LibIssuedBookModel, Integer>{

}
