package com.ingenio.trust.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.bean.SchoolBean;
import com.ingenio.trust.model.SchoolMasterModel;

@Repository
public interface SchoolMasterRepository extends JpaRepository<SchoolMasterModel, Integer>{
	
	SchoolMasterModel findBySchoolid(Integer schoolId);

	List<SchoolMasterModel> findBySansthaKey(String sansthaKey);

//	
//	@Query(value = "select new com.ingenio.trust.bean.SchoolBean(COALESCE(c.schoolid,0),COALESCE(c.schoolName,''),COALESCE(c.shortSchoolName,'') ) " 
//			+"FROM SansthaUserMapToSchoolModel a "
//			+"left join SansthaUserRegistrationModel b on (a.sansthaUserRegistrationModel.sansthaUserId = b.sansthaUserId ) "
//			+"left join SchoolMasterModel c on (c.schoolid = a.schoolMasterModel.schoolid ) "
//			+"where b.sansthaUserRegNo=:sansthaKey and b.contactNoSms=:mobileNo ")
	

	@Query(value = "select new com.ingenio.trust.bean.SchoolBean(COALESCE(c.schoolid,0),COALESCE(c.schoolName,''),COALESCE(c.shortSchoolName,'') ) " 
			+"FROM SansthaUserMapToSchoolModel a "
			+"left join SansthaRegistrationModel b on (a.sansthaRegistrationModel.sansthaId = b.sansthaId ) "
			+"left join SchoolMasterModel c on (c.schoolid = a.schoolMasterModel.schoolid ) "
			+"where b.sansthaKey=:sansthaKey and b.contactNo=:mobileNo ")
	List<SchoolBean> getSchoolList(String sansthaKey, String mobileNo);

	@Query("select new com.ingenio.trust.bean.SchoolBean(s.schoolid,s.schoolName) from SchoolMasterModel s "
			+ "where s.sansthaKey=:sansthaKey  ")
	List<SchoolBean> getSchools(String sansthaKey);


	@Query("select new com.ingenio.trust.bean.SchoolBean(s.schoolid,s.schoolName) from SchoolMasterModel s "
			+ "where s.sansthaKey=:sansthaKey and s.groupOfSchoolModel.groupOfSchoolId=:groupOfSchoolId ")
	List<SchoolBean> getSchoolsAccordingGroupOfSchoolId(String sansthaKey, Integer groupOfSchoolId);

	
}
