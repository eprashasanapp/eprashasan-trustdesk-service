package com.ingenio.trust.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.bean.AttendanceReportBean;
import com.ingenio.trust.model.AttendanceModel;



@Repository
public interface AttendanceReportRepository extends JpaRepository<AttendanceModel, Integer> {


	@Query(value = "select new com.ingenio.trust.bean.AttendanceReportBean(std.standardName as stdandardName, std.standardId , div.divisionName as divisionName , div.divisionId, COALESCE(SUM(LENGTH(att.absentStudentId) - LENGTH(REPLACE(att.absentStudentId, '|', '')) + 1),0) as absentCount ,"
			+ "COALESCE(SUM(LENGTH(att.presentStudentId) - LENGTH(REPLACE(att.presentStudentId, '|', '')) + 1),0) as presentCount ," 
			+ "COALESCE(SUM(LENGTH(att.exemptedStudentId) - LENGTH(REPLACE(att.exemptedStudentId, '|', '')) + 1),0) as exemptedCount ) " 
			+"FROM AttendanceModel att "
			+"left join StandardMasterModel std on (att.standardMasterModel.standardId = std.standardId and std.isDel='0' and att.schoolMasterModel.schoolid=std.schoolMasterModel.schoolid) "
			+"left join DivisionMasterModel div on (att.divId = div.divisionId and div.isDel='0' and div.schoolMasterModel.schoolid=att.schoolMasterModel.schoolid) "
			+"where att.isDel='0' and att.attendedDate =:attendedDate and att.standardMasterModel.standardId =:standardId and att.schoolMasterModel.schoolid=:schoolId group by 1,2 order by div.divisionId ")
	List<AttendanceReportBean> getStandardAndDivisionAttendance(Integer schoolId, String attendedDate, Integer standardId);



	@Query(value = "select new com.ingenio.trust.bean.AttendanceReportBean(std.standardName as stdandardName, std.standardId , COALESCE(SUM(LENGTH(att.absentStudentId) - LENGTH(REPLACE(att.absentStudentId, '|', '')) + 1),0) as absentCount ,"
			+ "COALESCE(SUM(LENGTH(att.presentStudentId) - LENGTH(REPLACE(att.presentStudentId, '|', '')) + 1),0) as presentCount ," 
			+ "COALESCE(SUM(LENGTH(att.exemptedStudentId) - LENGTH(REPLACE(att.exemptedStudentId, '|', '')) + 1),0) as exemptedCount ) " 
			+"FROM AttendanceModel att "
			+"left join StandardMasterModel std on (att.standardMasterModel.standardId = std.standardId and std.isDel='0' and att.schoolMasterModel.schoolid=std.schoolMasterModel.schoolid) "
			+"where att.isDel='0' and att.attendedDate =:attendedDate and att.schoolMasterModel.schoolid=:schoolId group by 1,2 order by std.standardId ")
	List<AttendanceReportBean> getStandardWiseAttendance(Integer schoolId, String attendedDate);
	
	
	@Query(value = "select new com.ingenio.trust.bean.AttendanceReportBean(ssm.schoolid as schoolid, COALESCE(ssm.shortSchoolName,'') as schoolName,ssm.schoolName, ssm.groupOfSchoolId, COALESCE(SUM(LENGTH(att.absentStudentId) - LENGTH(REPLACE(att.absentStudentId, '|', '')) + 1),0) as absentCount ,"
			+"COALESCE(SUM(LENGTH(att.presentStudentId) - LENGTH(REPLACE(att.presentStudentId, '|', '')) + 1),0) as presentCount ," 
			+"COALESCE(SUM(LENGTH(att.exemptedStudentId) - LENGTH(REPLACE(att.exemptedStudentId, '|', '')) + 1),0) as exemptedCount ) " 
			+"FROM AttendanceModel att "
			+"left join StandardMasterModel std on (att.standardMasterModel.standardId = std.standardId and std.isDel='0' and att.schoolMasterModel.schoolid=std.schoolMasterModel.schoolid) "
		    +"left join SchoolMasterModel ssm on (ssm.schoolid = att.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
		    +"where att.isDel='0' and att.attendedDate =:attendedDate and ssm.sansthaKey=:sansthaKey and gs.groupOfSchoolId=:groupOfSchoolId  group by 1,2 order by schoolid ")
	List<AttendanceReportBean> getSchoolWiseAttendance(String sansthaKey, String attendedDate, Integer groupOfSchoolId);
	
	@Query(value = "select new com.ingenio.trust.bean.AttendanceReportBean(ssm.schoolid as schoolid, COALESCE(ssm.shortSchoolName,'') as schoolName,ssm.schoolName, ssm.groupOfSchoolId, COALESCE(SUM(LENGTH(att.absentStudentId) - LENGTH(REPLACE(att.absentStudentId, '|', '')) + 1),0) as absentCount ,"
			+"COALESCE(SUM(LENGTH(att.presentStudentId) - LENGTH(REPLACE(att.presentStudentId, '|', '')) + 1),0) as presentCount ," 
			+"COALESCE(SUM(LENGTH(att.exemptedStudentId) - LENGTH(REPLACE(att.exemptedStudentId, '|', '')) + 1),0) as exemptedCount ) " 
			+"FROM AttendanceModel att "
			+"left join StandardMasterModel std on (att.standardMasterModel.standardId = std.standardId and std.isDel='0' and att.schoolMasterModel.schoolid=std.schoolMasterModel.schoolid) "
		    +"left join SchoolMasterModel ssm on (ssm.schoolid = att.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
		    +"where att.isDel='0' and att.attendedDate =:attendedDate and ssm.sansthaKey=:sansthaKey and gs.groupOfSchoolId is null  group by 1,2 order by schoolid ")
	List<AttendanceReportBean> getSchoolWiseAttendance(String sansthaKey, String attendedDate);


	
	@Query(value = "select new com.ingenio.trust.bean.AttendanceReportBean(COALESCE(gs.groupOfSchoolId,0) as groupOfSchoolId , COALESCE(gs.groupOfSchoolName,'Other') as groupOfSchoolName, COALESCE(SUM(LENGTH(att.absentStudentId) - LENGTH(REPLACE(att.absentStudentId, '|', '')) + 1),0) as absentCount ,"
			+"COALESCE(SUM(LENGTH(att.presentStudentId) - LENGTH(REPLACE(att.presentStudentId, '|', '')) + 1),0) as presentCount ," 
			+"COALESCE(SUM(LENGTH(att.exemptedStudentId) - LENGTH(REPLACE(att.exemptedStudentId, '|', '')) + 1),0) as exemptedCount ) " 
			+"FROM AttendanceModel att "
			+"left join StandardMasterModel std on (att.standardMasterModel.standardId = std.standardId and std.isDel='0' and att.schoolMasterModel.schoolid=std.schoolMasterModel.schoolid) "
			+"left join SchoolMasterModel ssm on (ssm.schoolid = att.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+"where att.isDel='0' and att.attendedDate =:attendedDate and ssm.sansthaKey=:sansthaKey group by 1,2 order by groupOfSchoolId ")
	List<AttendanceReportBean> getgroupWiseAttendance(String sansthaKey, String attendedDate);


}
