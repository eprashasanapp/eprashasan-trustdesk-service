package com.ingenio.trust.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.bean.StudentDetailsBean;
import com.ingenio.trust.model.StudentMasterModel;



@Repository
public interface StudentDetailsRepository extends JpaRepository<StudentMasterModel, Integer> {

	@Query(value = "select new com.ingenio.trust.bean.StudentDetailsBean(CONCAT(sm.studInitialName,' ',sm.studFName,' ',sm.studMName,' ',sm.studLName),sm.studentRegNo ,sm.studGender ,sm.castemaster.castName ," 
			+ "sm.status ,sm.grBookName.grBookId, ssr.concession , ssr.renewStudentId , " 
			+ "ssr.standardMasterModel.standardName , ssr.divisionMasterModel.divisionName ,ssr.yearMasterModel.year , ssr.rollNo   )"
			+" from StudentMasterModel sm " 
			+"left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+"left join CasteMasterModel cm on (cm.casteId = sm.castemaster.casteId and cm.isDel='0' and  cm.schoolMasterModel.schoolid=sm.schoolMasterModel.schoolid) "
			+"left join CategoryMasterModel cat on (cat.categoryId=sm.category.categoryId and cat.isDel='0' and  cat.schoolMasterModel.schoolid=sm.schoolMasterModel.schoolid) "
			+"left join ReligionMasterModel rl on (sm.religion.religionId=rl.religionId and rl.isDel='0' and  sm.schoolMasterModel.schoolid=rl.schoolMasterModel.schoolid) "
			+"left join StandardMasterModel std on (ssr.standardMasterModel.standardId=std.standardId and std.isDel='0' and  ssr.schoolMasterModel.schoolid=std.schoolMasterModel.schoolid) "
			+"left join DivisionMasterModel div on (ssr.divisionMasterModel.divisionId=div.divisionId and div.isDel='0' and  ssr.schoolMasterModel.schoolid=div.schoolMasterModel.schoolid) "
			+"left join YearMasterModel ym on (ssr.yearMasterModel.yearId=ym.yearId and ym.isDel='0') "
			+"where ssr.alreadyRenew='1' and (ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') "
			+"and sm.schoolMasterModel.schoolid=:schoolId and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+"and ssr.yearMasterModel.yearId=:yearId and ssr.standardMasterModel.standardId=:standardId and "
			+"ssr.divisionMasterModel.divisionId=:divisionId ")
	List<StudentDetailsBean> getDivisionwiseStudentDetails(Integer schoolId, Integer yearId, Integer standardId,
			Integer divisionId);


}