package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.LibBookStreamModel;

@Repository
public interface LibBookStreamRepository  extends JpaRepository<LibBookStreamModel, Integer>{

	
    @Query("From LibBookStreamModel where schoolId = :schoolId AND bookStreamName = :bookStreamName")
    LibBookStreamModel getEntityByName(@Param("schoolId") Integer schoolId, @Param("bookStreamName") String bookStreamName);

}
