package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.BTCardRecordModel;

@Repository
public interface BTCardReportRepository  extends JpaRepository<BTCardRecordModel, Integer>{

}
