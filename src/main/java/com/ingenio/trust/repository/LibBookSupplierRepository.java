package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.LibBookSupplierModel;

@Repository
public interface LibBookSupplierRepository  extends JpaRepository<LibBookSupplierModel, Integer>{

	
    @Query("From LibBookSupplierModel where schoolId = :schoolId AND supplierName = :supplierName")
    LibBookSupplierModel getEntityByName(@Param("schoolId") Integer schoolId, @Param("supplierName") String supplierName);

}
