package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.trust.model.AppUserModel;

public interface AppUserRepository extends JpaRepository<AppUserModel, Integer> {

	@Query("select a.appUserRoleId from AppUserModel a where a.staffId=:staffId and a.roleName='ROLE_PRINCIPAL' and a.schoolMasterModel.schoolid=:schoolid ")
	Integer getUserId(Integer staffId,Integer schoolid);

}
