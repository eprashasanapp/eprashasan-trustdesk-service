package com.ingenio.trust.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.bean.PrincipalMenuBean;
import com.ingenio.trust.model.PrincipalMenuModel;



@Repository
public interface PrincipalMenuRepository extends JpaRepository<PrincipalMenuModel, Integer> {

	
	@Query(value = "select new com.ingenio.trust.bean.PrincipalMenuBean(pm.principalMenuId as menuId , pm.menuName as menuName) from PrincipalMenuModel pm order by menuId " )
	List<PrincipalMenuBean> getMenuList();
	
}

	




