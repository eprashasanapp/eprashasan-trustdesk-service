package com.ingenio.trust.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.bean.GroupOfSchoolBean;
import com.ingenio.trust.bean.StudentCountBean;
import com.ingenio.trust.model.StudentMasterModel;

@Repository
public interface StudentMasterRepository extends JpaRepository<StudentMasterModel, Integer> {

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey  and sm.isDel='0' and sm.isAdmissionCancelled=1  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) "
			+ "and ym.year IN (:yearList) and ssm.schoolid IN:schoolist ")
	StudentCountBean getSansthaWiseTotalCount(String sansthaKey, List<String> yearList, String renewdate,List<Integer> schoolist );

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,'') as schoolName , ssm.shortSchoolName as shortSchoolName, count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  group by 1,2 order by schoolid ")
	List<StudentCountBean> getSchoolWiseStrength(String sansthaKey, List<String> yearList, String renewdate);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and sm.schoolMasterModel.schoolid=:schoolId and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ssr.yearMasterModel.yearId=:yearId  ")
	StudentCountBean getSchoolWiseStrengthId(Integer schoolId, Integer yearId, String renewdate);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(std.standardName as standardName,std.standardId as standardId, count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join StandardMasterModel std on (ssr.standardMasterModel.standardId=std.standardId and std.isDel='0' and std.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and sm.schoolMasterModel.schoolid=:schoolId and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ssr.yearMasterModel.yearId=:yearId group by 1,2 order by standardId ")
	List<StudentCountBean> getStandardwiseStrength(Integer schoolId, Integer yearId, String renewdate);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean( count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount , count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and sm.schoolMasterModel.schoolid=:schoolId and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ssr.yearMasterModel.yearId=:yearId and ssr.standardMasterModel.standardId=:standardId ")
	StudentCountBean getStandardwiseStrengthwithId(Integer schoolId, Integer yearId, Integer standardId, String renewdate);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(std.standardName as standardName,std.standardId as standardId, div.divisionName as divisionName, div.divisionId as divisionId ,count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount , count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join StandardMasterModel std on (ssr.standardMasterModel.standardId=std.standardId and std.isDel='0' and std.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join DivisionMasterModel div on (ssr.divisionMasterModel.divisionId=div.divisionId and div.isDel='0' and div.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ " and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and sm.schoolMasterModel.schoolid=:schoolId and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ssr.yearMasterModel.yearId=:yearId and ssr.standardMasterModel.standardId=:standardId group by 1,2,3,4 order by divisionId ")
	List<StudentCountBean> getDivisionwiseStrength(Integer schoolId, Integer yearId, Integer standardId, String renewdate);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount , count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and sm.schoolMasterModel.schoolid=:schoolId and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ssr.yearMasterModel.yearId=:yearId and ssr.standardMasterModel.standardId=:standardId and ssr.divisionMasterModel.divisionId=:divisionId ")
	StudentCountBean getDivisionStrengthwithId(Integer schoolId, Integer yearId, Integer standardId, Integer divisionId, String renewdate);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(rl.religionId,rl.religionName,count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0' and rl.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on (ssr.yearMasterModel.yearId = ym.yearId and ym.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year=:yearName group by 1,2 order by rl.religionId ")
	StudentCountBean getSansthaWiseReligionCount(String sansthaKey, String yearName, String renewdate);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and cm.categoryName=:summaryId  ")
	StudentCountBean getSummaryWiseCategoryCount(String sansthaKey, List<String> yearList, String summaryId, String renewDate);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and cm.categoryId is null  ")
	StudentCountBean getSummaryWiseCategoryCount(String sansthaKey, List<String> yearList, String renewDate);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ConcessionMaster cm on (cm.concessionId = ssr.concession and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and cm.concessionType=:summaryId  ")
	StudentCountBean getSummaryWiseConcessionCount(String sansthaKey, List<String> yearList, String summaryId, String renewDate);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ConcessionMaster cm on (cm.concessionId = ssr.concession and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and cm.concessionId is null  ")
	StudentCountBean getSummaryWiseConcessionCount(String sansthaKey, List<String> yearList, String renewDate);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityType=:summaryId  ")
	StudentCountBean getSummaryWiseMinorityCount(String sansthaKey, List<String> yearList, String summaryId, String renewDate);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityId is null ")
	StudentCountBean getSummaryWiseMinorityCount(String sansthaKey, List<String> yearList,  String renewDate);

	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0' and rl.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and rl.religionName=:summaryId  ")
	StudentCountBean getSummaryWiseReligionCount(String sansthaKey, List<String> yearList, String summaryId, String renewDate);
	

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0' and rl.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and rl.religionId is null  ")
	StudentCountBean getSummaryWiseReligionCount(String sansthaKey, List<String> yearList, String renewDate);
	

//*******************************************School List *************************************************************	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,'') ,ssm.schoolName ,  "
			+ "count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  and cm.categoryName=:summaryId and gs.groupOfSchoolId=:groupOfSchoolId group by 1,2  order by schoolid ")
	List<StudentCountBean> getCategoryWiseSchoolList(String sansthaKey, List<String> yearList, String summaryId, String renewDate, Integer groupOfSchoolId);
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid,COALESCE(ssm.shortSchoolName,''), ssm.schoolName as schoolName ,  count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  and cm.categoryId is null and gs.groupOfSchoolId=:groupOfSchoolId group by 1,2  order by schoolid ")
	List<StudentCountBean> getCategoryWiseSchoolList(String sansthaKey, List<String> yearList, String renewDate, Integer groupOfSchoolId);
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,''),ssm.schoolName as schoolName ,  count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  and cm.categoryName=:summaryId and gs.groupOfSchoolId is null group by 1,2  order by schoolid ")
	List<StudentCountBean> getCategoryWiseSchoolList(String sansthaKey, List<String> yearList, String summaryId, String renewDate);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,''),ssm.schoolName as schoolName ,  count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and cm.categoryId is null and gs.groupOfSchoolId is null group by 1,2  order by schoolid ")
	List<StudentCountBean> getCategoryWiseSchoolList(String sansthaKey, List<String> yearList, String renewDate);
	

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,''),ssm.schoolName as schoolName , count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityType=:summaryId and gs.groupOfSchoolId=:groupOfSchoolId group by 1,2 order by schoolid ")
	List<StudentCountBean> getMinorityWiseSchoolList(String sansthaKey, List<String> yearList, String summaryId,String renewDate, Integer groupOfSchoolId);
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,''),ssm.schoolName as schoolName , count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityId is null and gs.groupOfSchoolId=:groupOfSchoolId group by 1,2 order by schoolid ")
	List<StudentCountBean> getMinorityWiseSchoolList(String sansthaKey, List<String> yearList, String renewDate, Integer groupOfSchoolId);
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,''),ssm.schoolName as schoolName , count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityType=:summaryId and gs.groupOfSchoolId is null group by 1,2 order by schoolid ")
	List<StudentCountBean> getMinorityWiseSchoolList(String sansthaKey, List<String> yearList, String summaryId,String renewDate);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,''),ssm.schoolName as schoolName , count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityId is null and gs.groupOfSchoolId is null group by 1,2 order by schoolid ")
	List<StudentCountBean> getMinorityWiseSchoolList(String sansthaKey, List<String> yearList, 
			String renewDate);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,''),ssm.schoolName as schoolName ,  count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join ConcessionMaster cm on (cm.concessionId = ssr.concession and cm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and cm.concessionType=:summaryId and gs.groupOfSchoolId=:groupOfSchoolId group by 1,2 order by schoolid ")
	List<StudentCountBean> getConcessionWiseSchoolList(String sansthaKey, List<String> yearList, String summaryId,
			String renewDate, Integer groupOfSchoolId);
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,''),ssm.schoolName as schoolName ,  count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join ConcessionMaster cm on (cm.concessionId = ssr.concession and cm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and cm.concessionId is null and gs.groupOfSchoolId=:groupOfSchoolId group by 1,2 order by schoolid ")
	List<StudentCountBean> getConcessionWiseSchoolList(String sansthaKey, List<String> yearList, String renewDate, Integer groupOfSchoolId);
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,''),ssm.schoolName as schoolName ,  count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join ConcessionMaster cm on (cm.concessionId = ssr.concession and cm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and cm.concessionType=:summaryId and gs.groupOfSchoolId is null group by 1,2 order by schoolid ")
	List<StudentCountBean> getConcessionWiseSchoolList(String sansthaKey, List<String> yearList, String summaryId,
			String renewDate);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,''),ssm.schoolName as schoolName ,  count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join ConcessionMaster cm on (cm.concessionId = ssr.concession and cm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and cm.concessionId is null and gs.groupOfSchoolId is null group by 1,2 order by schoolid ")
	List<StudentCountBean> getConcessionWiseSchoolList(String sansthaKey, List<String> yearList,String renewDate);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,''),ssm.schoolName as schoolName ,  count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and rl.religionName=:summaryId and gs.groupOfSchoolId=:groupOfSchoolId group by 1,2 order by schoolid ")
	List<StudentCountBean> getReligionWiseSchoolList(String sansthaKey, List<String> yearList, String summaryId,String renewDate, Integer groupOfSchoolId);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,''),ssm.schoolName as schoolName ,  count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and rl.religionId is null and gs.groupOfSchoolId=:groupOfSchoolId group by 1,2 order by schoolid ")
	List<StudentCountBean> getReligionWiseSchoolList(String sansthaKey, List<String> yearList,String renewDate, Integer groupOfSchoolId);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,''),ssm.schoolName as schoolName ,  count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and rl.religionName=:summaryId and gs.groupOfSchoolId is null group by 1,2 order by schoolid ")
	List<StudentCountBean> getReligionWiseSchoolList(String sansthaKey, List<String> yearList, String summaryId,String renewDate);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,''),ssm.schoolName as schoolName ,  count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and rl.religionId is null and gs.groupOfSchoolId is null group by 1,2 order by schoolid ")
	List<StudentCountBean> getReligionWiseSchoolList(String sansthaKey, List<String> yearList,String renewDate);
	


	@Query(value = "select new com.ingenio.trust.bean.GroupOfSchoolBean(COALESCE(gs.groupOfSchoolId,0) as groupOfSchoolId , COALESCE(gs.groupOfSchoolName,'Other') as groupOfSchoolName, count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1'))  "
			+ "and ym.year IN (:yearList) and ssm.schoolid IN (:schoolist) group by 1,2 order by groupOfSchoolId")
List<GroupOfSchoolBean> getGroupOfSchoolFilterList(String sansthaKey, List<String> yearList, String renewDate ,List<Integer> schoolist);
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1'))  "
			+ "and ym.year IN (:yearName) and ssm.schoolid IN (:schoolList) and  ssm.groupOfSchoolId =:groupOfSchoolId  ")
	StudentCountBean getGroupWiseTotalCount(String sansthaKey, List<String> yearName, String renewdate,Integer groupOfSchoolId
			,List<Integer> schoolList);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1'))  "
			+ "and ym.year IN (:yearName) and ssm.schoolid IN (:schoolList) and  ssm.groupOfSchoolId is null  ")
	StudentCountBean getGroupWiseTotalCount(String sansthaKey, List<String> yearName, String renewdate,List<Integer> schoolList);


	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,'') as schoolName , ssm.schoolName, count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1'))  "
			+ "and ym.year IN (:yearName)  and ssm.schoolid IN (:schoolList) and ssm.groupOfSchoolId =:groupOfSchoolId  group by 1,2 order by schoolid")
	List<StudentCountBean> getGroupWiseSchoolStrength(String sansthaKey, List<String> yearName, String renewdate,
			Integer groupOfSchoolId,List<Integer> schoolList);
	

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,'') as schoolName , ssm.schoolName, count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) "
			+ "and ym.year IN (:yearName)  and ssm.schoolid IN (:schoolList) and ssm.groupOfSchoolId is null  group by 1,2 order by schoolid")
	List<StudentCountBean> getGroupWiseSchoolStrength(String sansthaKey, List<String> yearName, String renewdate,List<Integer> schoolList);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) "
			+ "and ym.year IN (:yearName) and  ssm.groupOfSchoolId =:groupOfSchoolId  ")
	StudentCountBean getGroupWiseTotalCount(String sansthaKey, List<String> yearName, String renewdate,Integer groupOfSchoolId);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1'))  "
			+ "and ym.year IN (:yearName)  and  ssm.groupOfSchoolId is null  ")
	StudentCountBean getGroupWiseTotalCount(String sansthaKey, List<String> yearName, String renewdate);

//--------------------------------------l.schoolLogo
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,'') as schoolName , ssm.schoolName, count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount,"
			+ "	 ssm.schoolBoardModel.schoolBoardId, "
			+ "	 ssm.schoolBoardModel.schoolBoard,"
			+ "	 ssm.language.languageId, "
			+ "	 ssm.language.language, " 
			+ "	 st.schoolType,"
			+ "	 st.schoolTypeId,"
			+ "	 ssm.address,"
			+ "	 ssm.stateModel.stateId,"
			+ "  ssm.stateModel.state,"
			+ "	 ssm.districtModel.districtId,"
			+ " ssm.districtModel.district,"
			+ "	 ssm.talukaModel.talukaId,"
			+ "ssm.talukaModel.taluka,"
			+ "sl.id,"
			+ "sl.imagePath "
			+ ") from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			
			
		
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join SchoolTypeModel st on st.schoolTypeId=ssm.schoolTypeId  "
			+ "left join SchoolLogo sl on sl.schoolId=ssm.schoolid "+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1'))  "
			+ "and ym.year IN (:yearName) and ssm.groupOfSchoolId =:groupOfSchoolId  group by 1,2 order by schoolid")
	List<StudentCountBean> getGroupWiseSchoolStrength(String sansthaKey, List<String> yearName, String renewdate,
			Integer groupOfSchoolId);
	

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(ssm.schoolid as schoolid ,COALESCE(ssm.shortSchoolName,'') as schoolName , ssm.schoolName, count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) "
			+ "and ym.year IN (:yearName)  and ssm.groupOfSchoolId is null  group by 1,2 order by schoolid")
	List<StudentCountBean> getGroupWiseSchoolStrength(String sansthaKey, List<String> yearName, String renewdate);
	
	
	
	
	
//	***************************************Group Summary********************************************************
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(COALESCE(gs.groupOfSchoolId,0) , COALESCE(gs.groupOfSchoolName,'Other') , ssm.schoolid as schoolid ,count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0') "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  and cm.categoryName=:summaryId group by 1,2 ")
	List<StudentCountBean> getCategoryWiseGroupList(String sansthaKey, List<String> yearList, String summaryId,String renewDate);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(COALESCE(gs.groupOfSchoolId,0) , COALESCE(gs.groupOfSchoolName,'Other') , ssm.schoolid as schoolid ,count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0') "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  and cm.categoryId is null group by 1,2 ")
	List<StudentCountBean> getCategoryWiseGroupList(String sansthaKey, List<String> yearList,String renewDate);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(COALESCE(gs.groupOfSchoolId,0) , COALESCE(gs.groupOfSchoolName,'Other') , ssm.schoolid as schoolid , count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0') "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityType=:summaryId group by 1,2 ")
	List<StudentCountBean> getMinorityWiseGroupList(String sansthaKey, List<String> yearList, String summaryId, String renewDate);
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(COALESCE(gs.groupOfSchoolId,0) , COALESCE(gs.groupOfSchoolName,'Other') , ssm.schoolid as schoolid , count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0') "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityId is null group by 1,2 ")
	List<StudentCountBean> getMinorityWiseGroupList(String sansthaKey, List<String> yearList, String renewDate);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(COALESCE(gs.groupOfSchoolId,0) , COALESCE(gs.groupOfSchoolName,'Other') , ssm.schoolid as schoolid , count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0') "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join ConcessionMaster cm on (cm.concessionId = ssr.concession and cm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and cm.concessionType=:summaryId group by 1,2 ")
	List<StudentCountBean> getConcessionWiseGroupList(String sansthaKey, List<String> yearList, String summaryId,
			String renewDate);
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(COALESCE(gs.groupOfSchoolId,0) , COALESCE(gs.groupOfSchoolName,'Other') , ssm.schoolid as schoolid , count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0') "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join ConcessionMaster cm on (cm.concessionId = ssr.concession and cm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and cm.concessionId is null group by 1,2 ")
	List<StudentCountBean> getConcessionWiseGroupList(String sansthaKey,List<String> yearList, String renewDate);


	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(COALESCE(gs.groupOfSchoolId,0) , COALESCE(gs.groupOfSchoolName,'Other') , ssm.schoolid as schoolid ,  count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0') "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and rl.religionName=:summaryId group by 1,2 ")
	List<StudentCountBean> getReligionWiseGroupList(String sansthaKey, List<String> yearList, String summaryId,
			String renewDate);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(COALESCE(gs.groupOfSchoolId,0) , COALESCE(gs.groupOfSchoolName,'Other') , ssm.schoolid as schoolid ,  count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0') "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and rl.religionId is null group by 1,2 ")
	List<StudentCountBean> getReligionWiseGroupList(String sansthaKey, List<String> yearList, String renewDate);


	
//	*****************************************GroupWise category Count**************************************************
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and cm.categoryName=:summaryId and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseCategoryCount(String sansthaKey, List<String> yearList, String summaryId, 
			String renewDate, Integer groupOfSchoolId);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and cm.categoryId is null and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseCategoryCount(String sansthaKey, List<String> yearList, String renewDate, Integer groupOfSchoolId);

	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and cm.categoryName=:summaryId and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseCategoryCount(String sansthaKey, List<String> yearList, String summaryId, String renewDate);
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and cm.categoryId is null and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseCategoryCount(String sansthaKey, List<String> yearList, String renewDate);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList) and cm.categoryName=:summaryId and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseCategoryCount(String sansthaKey, List<String> yearList, String summaryId, 
			String renewDate, Integer groupOfSchoolId,List<Integer> schoolList);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList) and cm.categoryId is null and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseCategoryCount(String sansthaKey, List<String> yearList, String renewDate, Integer groupOfSchoolId , List<Integer> schoolList);

	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList) and cm.categoryName=:summaryId and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseCategoryCount(String sansthaKey, List<String> yearList, String summaryId, String renewDate,List<Integer> schoolList);
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList) and cm.categoryId is null and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseCategoryCount(String sansthaKey, List<String> yearList, String renewDate,List<Integer> schoolList);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityType=:summaryId and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseMinorityCount(String sansthaKey, List<String> yearList, String summaryId,
			String renewDate, Integer groupOfSchoolId);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityId is null and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseMinorityCount(String sansthaKey, List<String> yearList, String renewDate, Integer groupOfSchoolId);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityType=:summaryId and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseMinorityCount(String sansthaKey, List<String> yearList, String summaryId,
			String renewDate);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityId is null and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseMinorityCount(String sansthaKey, List<String> yearList, String renewDate);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList) and mm.minorityType=:summaryId and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseMinorityCount(String sansthaKey, List<String> yearList, String summaryId,
			String renewDate, Integer groupOfSchoolId ,List<Integer> schoolList);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList) and mm.minorityId is null and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseMinorityCount(String sansthaKey, List<String> yearList, String renewDate,
			Integer groupOfSchoolId,List<Integer> schoolList);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList) and mm.minorityType=:summaryId and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseMinorityCount(String sansthaKey, List<String> yearList, String summaryId,
			String renewDate,List<Integer> schoolList);
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList) and mm.minorityId is null and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseMinorityCount(String sansthaKey, List<String> yearList, String renewDate,List<Integer> schoolList);
	

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityType=:summaryId and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseConcessionCount(String sansthaKey, List<String> yearList, String summaryId, String renewDate, Integer groupOfSchoolId);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityId is null  and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseConcessionCount(String sansthaKey, List<String> yearList,  String renewDate, Integer groupOfSchoolId);
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityType=:summaryId and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseConcessionCount(String sansthaKey, List<String> yearList,String summaryId, String renewDate);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and mm.minorityId is null and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseConcessionCount(String sansthaKey, List<String> yearList,  String renewDate);
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList)  and mm.minorityType=:summaryId and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseConcessionCount(String sansthaKey, List<String> yearList, String summaryId, 
			String renewDate, Integer groupOfSchoolId,List<Integer> schoolList);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList)  and mm.minorityId is null  and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseConcessionCount(String sansthaKey, List<String> yearList,  String renewDate, 
			Integer groupOfSchoolId,List<Integer> schoolList);
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList)  and mm.minorityType=:summaryId and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseConcessionCount(String sansthaKey, List<String> yearList,String summaryId, 
			String renewDate,List<Integer> schoolList);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList)  and mm.minorityId is null and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseConcessionCount(String sansthaKey, List<String> yearList,  String renewDate,List<Integer> schoolList);


	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0' and rl.schoolMasterModel.schoolid = sm.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList)  and rl.religionName=:summaryId and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseReligionCount(String sansthaKey, List<String> yearList, String summaryId, String renewDate, Integer groupOfSchoolId);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0' and rl.schoolMasterModel.schoolid = sm.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  and rl.religionId is null and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseReligionCount(String sansthaKey, List<String> yearList,  String renewDate, Integer groupOfSchoolId);


	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0' and rl.schoolMasterModel.schoolid = sm.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList)  and rl.religionName=:summaryId and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseReligionCount(String sansthaKey, List<String> yearList, String summaryId, String renewDate);


	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0' and rl.schoolMasterModel.schoolid = sm.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)   and rl.religionId is null and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseReligionCount(String sansthaKey, List<String> yearList,  String renewDate);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0' and rl.schoolMasterModel.schoolid = sm.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList)  and rl.religionName=:summaryId and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseReligionCount(String sansthaKey, List<String> yearList, String summaryId,
			String renewDate, Integer groupOfSchoolId,List<Integer> schoolList);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0' and rl.schoolMasterModel.schoolid = sm.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList)  and rl.religionId is null and gs.groupOfSchoolId=:groupOfSchoolId ")
	StudentCountBean getGroupWiseReligionCount(String sansthaKey, List<String> yearList,  String renewDate,
			Integer groupOfSchoolId,List<Integer> schoolList);


	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0' and rl.schoolMasterModel.schoolid = sm.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList)  and rl.religionName=:summaryId and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseReligionCount(String sansthaKey, List<String> yearList, String summaryId, 
			String renewDate,List<Integer> schoolList);


	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and sm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0' and rl.schoolMasterModel.schoolid = sm.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and  ssm.schoolid IN (:schoolList)  and rl.religionId is null and gs.groupOfSchoolId is null ")
	StudentCountBean getGroupWiseReligionCount(String sansthaKey, List<String> yearList,  String renewDate,List<Integer> schoolList);


	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey  and sm.isDel='0' and sm.isAdmissionCancelled=1  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) "
			+ "and ym.year IN (:yearList)  ")
	StudentCountBean getSansthaWiseTotalCount(String sansthaKey, List<String> yearList, String renewdate);

	
	@Query(value = "select new com.ingenio.trust.bean.GroupOfSchoolBean(COALESCE(gs.groupOfSchoolId,0) as groupOfSchoolId , COALESCE(gs.groupOfSchoolName,'Other') as groupOfSchoolName, count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount, count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1'))  "
			+ "and ym.year IN (:yearList) group by 1,2 order by groupOfSchoolId")
	List<GroupOfSchoolBean> getGroupOfSchoolFilterList(String sansthaKey, List<String> yearList, String renewDate);


	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  and  ssm.schoolid IN (:schoolList) and cm.categoryName=:summaryId  ")
	StudentCountBean getSummaryWiseCategoryCount(String sansthaKey, List<String> yearList, String summaryId, String renewDate,List<Integer> schoolList);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  and  ssm.schoolid IN (:schoolList) and cm.categoryId is null  ")
	StudentCountBean getSummaryWiseCategoryCount(String sansthaKey, List<String> yearList, String renewDate,List<Integer> schoolList);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ConcessionMaster cm on (cm.concessionId = ssr.concession and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  and  ssm.schoolid IN (:schoolList) and cm.concessionType=:summaryId  ")
	StudentCountBean getSummaryWiseConcessionCount(String sansthaKey, List<String> yearList, String summaryId, String renewDate,List<Integer> schoolList);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ConcessionMaster cm on (cm.concessionId = ssr.concession and cm.isDel='0' and cm.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  and  ssm.schoolid IN (:schoolList) and cm.concessionId is null  ")
	StudentCountBean getSummaryWiseConcessionCount(String sansthaKey, List<String> yearList, String renewDate,List<Integer> schoolList);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  and  ssm.schoolid IN (:schoolList) and mm.minorityType=:summaryId  ")
	StudentCountBean getSummaryWiseMinorityCount(String sansthaKey, List<String> yearList, String summaryId, String renewDate,List<Integer> schoolList);

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  and  ssm.schoolid IN (:schoolList) and mm.minorityId is null ")
	StudentCountBean getSummaryWiseMinorityCount(String sansthaKey, List<String> yearList,  String renewDate,List<Integer> schoolList);

	
	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0' and rl.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  and  ssm.schoolid IN (:schoolList) and rl.religionName=:summaryId  ")
	StudentCountBean getSummaryWiseReligionCount(String sansthaKey, List<String> yearList, String summaryId, String renewDate,List<Integer> schoolList);
	

	@Query(value = "select new com.ingenio.trust.bean.StudentCountBean(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm "
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0' and rl.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  and  ssm.schoolid IN (:schoolList) and rl.religionId is null  ")
	StudentCountBean getSummaryWiseReligionCount(String sansthaKey, List<String> yearList, String renewDate,List<Integer> schoolList);

	


	

}
