package com.ingenio.trust.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.bean.PrincipalDetailsBean;
import com.ingenio.trust.bean.StaffDetailsBean;
import com.ingenio.trust.bean.StaffDetailsNewBean;
import com.ingenio.trust.model.StaffAttendanceModel;
import com.ingenio.trust.model.StaffBasicDetailsModel;


@Repository
public interface StaffBasicDetailsRepository extends JpaRepository<StaffBasicDetailsModel, Integer> {
	
	@Query(value="select a from StaffBasicDetailsModel a  where a.schoolMasterModel.schoolid=:schoolId and a.isDel='0'  ")
	List<StaffBasicDetailsModel> findStaff(Integer schoolId);

//	@Query("select a from StaffBasicDetailsModel a where a.schoolMasterModel.schoolid=:schoolid and a.sregNo like 'P%' ")
//	StaffBasicDetailsModel getPrincipalDetails(Integer schoolid);

	@Query("select new com.ingenio.trust.bean.PrincipalDetailsBean(a.staffId,a.initialname,a.firstname,a.secondname,a.lastname,a.sregNo,a.birthdate,a.contactno,d.designationId,d.designationName,t.staffId,t.stafftype) from StaffBasicDetailsModel a "
			+ "left join DesignationModel d on d.designationId=a.designationModel.designationId "
			+ "left join StaffTypeModel t on t.staffId=a.staffTypeModel.staffId "
			+ "where a.schoolMasterModel.schoolid=:schoolid and a.sregNo like 'P%' ")
	PrincipalDetailsBean getPrincipalDetails(Integer schoolid);

//	@Query("select a from StaffBasicDetailsModel a where a.schoolMasterModel.schoolid=:schoolid and a.isDel='0' and a.sregNo not in  (select b.sregNo from StaffBasicDetailsModel b where b.schoolMasterModel.schoolid=:schoolid and  (b.sregNo like 'P%' or b.sregNo like 'T%') ) ")
//	List<StaffBasicDetailsModel> getStaffDetails(Integer schoolid);

	
	@Query("select new com.ingenio.trust.bean.StaffDetailsNewBean(a.staffId,a.initialname,a.firstname,a.secondname,a.lastname,a.sregNo,a.birthdate,a.contactno,d.designationId,d.designationName,t.staffId,t.stafftype) from StaffBasicDetailsModel a "
			+ "left join DesignationModel d on d.designationId=a.designationModel.designationId "
			+ "left join StaffTypeModel t on t.staffId=a.staffTypeModel.staffId "
			+ "where a.schoolMasterModel.schoolid=:schoolid and a.isDel='0' and a.sregNo not in  (select b.sregNo from StaffBasicDetailsModel b where b.schoolMasterModel.schoolid=:schoolid and  (b.sregNo like 'P%' or b.sregNo like 'T%') ) ")
	List<StaffDetailsNewBean> getStaffDetails(Integer schoolid);
	

	@Query("select count(a.staffId) from StaffBasicDetailsModel a "
			+ "left join DesignationModel d on d.designationId=a.designationModel.designationId "
			+ "left join StaffTypeModel t on t.staffId=a.staffTypeModel.staffId "
			+ "where a.schoolMasterModel.schoolid=:schoolid and a.isDel='0' and a.sregNo not in  (select b.sregNo from StaffBasicDetailsModel b where b.schoolMasterModel.schoolid=:schoolid and  (b.sregNo like 'P%' or b.sregNo like 'T%') ) ")
	Integer getStaffCount(Integer schoolid);


	



}