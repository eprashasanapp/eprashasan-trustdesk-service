package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.trust.model.SchoolLogo;

public interface SchoolLogoRepository  extends JpaRepository<SchoolLogo, Integer>{
	@Query("select COALESCE(a.imagePath,'abc') from SchoolLogo a where a.schoolId=:schoolId and a.isDel='0'  ")
	String getSchoolLogoPath(Integer schoolId);
}
