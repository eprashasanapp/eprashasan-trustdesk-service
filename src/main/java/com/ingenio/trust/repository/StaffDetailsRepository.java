package com.ingenio.trust.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.bean.StaffDetailsBean;
import com.ingenio.trust.model.StaffAttendanceModel;


@Repository
public interface StaffDetailsRepository extends JpaRepository<StaffAttendanceModel, Integer> {


	@Query(value = "select new com.ingenio.trust.bean.StaffDetailsBean(concat(sbd.initialname,' ',sbd.firstname,' ',sbd.secondname,' ',sbd.lastname) as staffName , sbd.staffId as staffId , sbd.sregNo as staffRegNo) "
			+" from StaffAttendanceModel sa " 
			+"left join StaffBasicDetailsModel sbd on (sa.staffID = sbd.staffId and sbd.isDel='0' and sa.schoolMasterModel.schoolid=sbd.schoolMasterModel.schoolid) "
			+"where sa.schoolMasterModel.schoolid=:schoolId and sa.date=:renewDate and  LENGTH(sa.inTime) > 0  order by staffId  ")
	List<StaffDetailsBean> getPresentStaffDetails(Integer schoolId, String renewDate);
	
	@Query(value = "select new com.ingenio.trust.bean.StaffDetailsBean(concat(sbd.initialname,' ',sbd.firstname,' ',sbd.secondname,' ',sbd.lastname) as staffName , sbd.staffId as staffId , sbd.sregNo as staffRegNo) "
			+" from StaffAttendanceModel sa " 
			+"left join StaffBasicDetailsModel sbd on (sa.staffID = sbd.staffId and sbd.isDel='0' and sa.schoolMasterModel.schoolid=sbd.schoolMasterModel.schoolid) "
			+"where sa.schoolMasterModel.schoolid=:schoolId and sa.date=:renewDate and (sa.inTime='' or sa.inTime is null )  order by staffId ")
	List<StaffDetailsBean> getAbsentStaffDetails(Integer schoolId, String renewDate);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StaffDetailsBean(COALESCE(count(case when(sa.inTime='' or sa.inTime is null) then 1 end),0) as absentCount , COALESCE(count(sa.inTime),0) as totalCount,COALESCE((count(sa.inTime) - count(case when(sa.inTime='' or sa.inTime is null) then 1 end)),0) as presentCount ) from StaffAttendanceModel sa " 
			+"where sa.schoolMasterModel.schoolid=:schoolId and sa.date=:renewDate ")
	StaffDetailsBean getStaffCount(Integer schoolId,  String renewDate);


	
	@Query(value = "select new com.ingenio.trust.bean.StaffDetailsBean(COALESCE((select count(*) from StaffBasicDetailsModel a where a.schoolMasterModel.schoolid=sa.schoolMasterModel.schoolid and a.isDel=0),0) as totalCount ,  COALESCE((count(sa.inTime) - count(case when(sa.inTime='' or sa.inTime is null) then 1 end)),0) as presentCount ) FROM StaffAttendanceModel sa " 
			+"left join SchoolMasterModel ssm on (ssm.schoolid = sa.schoolMasterModel.schoolid and ssm.isDel='0') "
		    +"where sa.isDel='0' and sa.date =:renewDate and ssm.sansthaKey=:sansthaKey ")
	StaffDetailsBean getSansthaWiseStaffCount(String sansthaKey, String renewDate);

	@Query(value = "select new com.ingenio.trust.bean.StaffDetailsBean(ssm.schoolid as schoolid, COALESCE(ssm.shortSchoolName,'') as schoolName,ssm.schoolName,"
			+ "COALESCE(count(case when(sa.inTime='' or sa.inTime is null) then 1 end),0) as absentCount , COALESCE(count(sa.inTime),0) as totalCount ,  COALESCE((count(sa.inTime) - count(case when(sa.inTime='' or sa.inTime is null) then 1 end)),0) as presentCount )  "
			+"FROM StaffAttendanceModel sa "
			+"left join SchoolMasterModel ssm on (ssm.schoolid = sa.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+"where sa.isDel='0' and sa.date =:attendedDate and ssm.sansthaKey=:sansthaKey and ssm.groupOfSchoolId =:groupOfSchoolId group by 1,2  order by schoolid ")
	List<StaffDetailsBean> getSchoolWiseAttendance(String sansthaKey, String attendedDate, Integer groupOfSchoolId);
	
	
	@Query(value = "select new com.ingenio.trust.bean.StaffDetailsBean(COALESCE(gs.groupOfSchoolName,'NA') , COALESCE(gs.groupOfSchoolId,0)  , "
			+ "coalesce((select count(*) from StaffBasicDetailsModel s where s.schoolMasterModel.schoolid=sa.schoolMasterModel.schoolid and s.isDel=0), 0)-COALESCE((count(sa.inTime) - count(case when(sa.inTime='' or sa.inTime is null) then 1 end)),0)  as absentCount , coalesce((select count(*) from StaffBasicDetailsModel s where s.schoolMasterModel.schoolid=sa.schoolMasterModel.schoolid and s.isDel=0), 0) as totalCount ,  COALESCE((count(sa.inTime) - count(case when(sa.inTime='' or sa.inTime is null) then 1 end)),0) as presentCount )  "
			+"FROM StaffAttendanceModel sa "
			+"left join SchoolMasterModel ssm on (ssm.schoolid = sa.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+"where sa.isDel='0' and sa.date =:renewDate and ssm.sansthaKey=:sansthaKey group by 1,2 order by gs.groupOfSchoolId ")
		List<StaffDetailsBean> getGroupWiseStaffAttendance(String sansthaKey, String renewDate);
	
	@Query(value = "select new com.ingenio.trust.bean.StaffDetailsBean(COALESCE(count(case when(sa.inTime='' or sa.inTime is null) then 1 end),0) as absentCount , COALESCE(count(sa.inTime),0) as totalCount ,  COALESCE((count(sa.inTime) - count(case when(sa.inTime='' or sa.inTime is null) then 1 end)),0) as presentCount ) FROM StaffAttendanceModel sa " 
			+"left join SchoolMasterModel ssm on (ssm.schoolid = sa.schoolMasterModel.schoolid and ssm.isDel='0') "
			+ "left join GroupOfSchoolModel gs on (gs.groupOfSchoolId = ssm.groupOfSchoolId and gs.isDel='0') "
			+"where sa.isDel='0' and sa.date =:renewDate and ssm.sansthaKey=:sansthaKey and ssm.groupOfSchoolId=:groupOfSchoolId ")
	StaffDetailsBean getGroupWiseStaffCount(String sansthaKey, String renewDate, Integer groupOfSchoolId);

	
	
}



