package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.LibBookInventoryModel;

@Repository
public interface LibBookInventoryRepository extends JpaRepository<LibBookInventoryModel, Integer> {

	@Query("From LibBookInventoryModel where schoolId = :schoolId AND bookName = :bookName AND authorName = :authorName AND bookEdition = :bookEdition")
	LibBookInventoryModel getEntityByName(@Param("schoolId") Integer schoolId, @Param("bookName") String bookName,
			@Param("authorName") String authorName, @Param("bookEdition") String bookEdition);

}
