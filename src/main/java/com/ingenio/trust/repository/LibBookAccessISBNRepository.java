package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.LibBookAccessISBNModel;

@Repository
public interface LibBookAccessISBNRepository  extends JpaRepository<LibBookAccessISBNModel, Integer>{


}
