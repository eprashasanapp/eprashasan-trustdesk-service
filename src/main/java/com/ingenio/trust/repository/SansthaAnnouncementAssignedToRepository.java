package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import com.ingenio.trust.model.SansthaAnnouncementAssignToModel;
import com.ingenio.trust.model.SansthaAnnouncementModel;

@Repository
public interface SansthaAnnouncementAssignedToRepository extends JpaRepository<SansthaAnnouncementAssignToModel, Integer>{

	void deleteBySansthaAnnouncementModel(SansthaAnnouncementModel announcementModel);

	
	
}
