package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.LibStudentAdmissionModel;

@Repository
public interface LibraryRepository extends JpaRepository<LibStudentAdmissionModel, Integer>{

}
