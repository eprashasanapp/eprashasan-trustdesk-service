package com.ingenio.trust.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.DynamicAcademicMonthsModel;
import com.ingenio.trust.model.SchoolMasterModel;

@Repository
public interface DynamicAcademicMonthsRepository extends JpaRepository<DynamicAcademicMonthsModel, Integer>{
	
	List<DynamicAcademicMonthsModel> findBySchoolMasterModel(SchoolMasterModel schoolMasterModel);

}
