package com.ingenio.trust.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.bean.AttendanceReportBean;
import com.ingenio.trust.bean.AttendanceReportBean1;
import com.ingenio.trust.bean.PrincipalMenuBean;
import com.ingenio.trust.bean.StudentAttendanceDetailsBean;
import com.ingenio.trust.bean.StudentDetailsBean;


@Repository
public class AttendanceDetailsRepositoryImpl {
	

  
  @PersistenceContext
  private EntityManager entitymanager;
	
	
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<AttendanceReportBean1> getSansthaWisePresentReport(String sansthaKey, String attendedDate) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select cast(count(sm.stud_id) as char) as totalCount , cast(count(case when sm.stud_gender='Male' then 1 end) as char) as maleCount,  cast(count(case when sm.stud_gender ='Female' then 1 end) as char) as femalCount ")
		.append("from student_master sm ")
		.append("left join student_standard_renew ssr on (sm.stud_id = ssr.studentId and ssr.isDel='0') ")
		.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' ")
		.append("left join school_master ssm on (ssm.schoolid = a.schoolid and ssm.isDel='0') ")
		.append("where a.attendanceDate = '"+attendedDate+"' and sm.isDel='0' and sm.isAdmissionCancelled='1' and ssr.alreadyRenew=1 and (ssr.college_leavingdate is null or ssr.college_leavingdate='')  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) ")
		.append("and a.isDel='0' and ssm.sansthaKey = '"+sansthaKey+"' AND a.p_AFlag='P'  ");
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return  query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(AttendanceReportBean1.class ) )
				 .getResultList();

	}


	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<AttendanceReportBean1> getSansthaWiseAbsentReport(String sansthaKey, String attendedDate) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select cast(count(sm.stud_id) as char) as totalCount , cast(count(case when sm.stud_gender='Male' then 1 end) as char) as maleCount,  cast(count(case when sm.stud_gender ='Female' then 1 end) as char) as femalCount ")
		.append("from student_master sm ")
		.append("left join student_standard_renew ssr on (sm.stud_id = ssr.studentId and ssr.isDel='0') ")
		.append("left join attendance_classwise_catalog a ON sm.stud_id=a.stud_id AND ssr.renewstudentId=a.renewstudentId AND sm.schoolid=a.schoolid " )
		.append("left join school_master ssm on (ssm.schoolid = a.schoolid and ssm.isDel='0') ")
		.append("where a.attendanceDate = '"+attendedDate+"' and sm.isDel='0' and sm.isAdmissionCancelled='1' and ssr.alreadyRenew=1 and (ssr.college_leavingdate is null or ssr.college_leavingdate='')  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) ")
		.append("and a.isDel='0' and ssm.sansthaKey = '"+sansthaKey+"' AND (a.p_AFlag='A' OR a.p_AType IS NOT NULL)  ");
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(AttendanceReportBean1.class ) )
				 .getResultList();

	}
	
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<AttendanceReportBean1> getGroupWisePresentReport(String sansthaKey, String attendedDate, Integer groupOfSchoolId) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select cast(count(sm.stud_id) as char) as totalCount , cast(count(case when sm.stud_gender='Male' then 1 end) as char) as maleCount,  cast(count(case when sm.stud_gender ='Female' then 1 end) as char) as femalCount ")
		.append("from student_master sm ")
		.append("left join student_standard_renew ssr on (sm.stud_id = ssr.studentId and ssr.isDel='0') ")
		.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' ")
		.append("left join school_master ssm on (ssm.schoolid = a.schoolid and ssm.isDel='0') ")
		.append("left join group_of_school gs on (ssm.groupOfSchoolId = gs.groupOfSchoolId and gs.isDel='0') ")
		.append("where a.attendanceDate = '"+attendedDate+"' ")
		.append("and a.isDel='0' and ssm.sansthaKey = '"+sansthaKey+"' AND a.p_AFlag='P' and sm.isDel='0' and sm.isAdmissionCancelled='1' and ssr.alreadyRenew=1 and (ssr.college_leavingdate is null or ssr.college_leavingdate='')  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1'))  ");
		
		if(groupOfSchoolId!=0) {
			attendenceListQuery.append("and gs.groupOfSchoolId='"+groupOfSchoolId+"' ");
		}
		else {
			attendenceListQuery.append("and gs.groupOfSchoolId is null ");
		}
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(AttendanceReportBean1.class ) )
				 .getResultList();

	}


	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<AttendanceReportBean1> getGroupWiseAbsentReport(String sansthaKey, String attendedDate, Integer groupOfSchoolId) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select cast(count(sm.stud_id) as char) as totalCount , cast(count(case when sm.stud_gender='Male' then 1 end) as char) as maleCount,  cast(count(case when sm.stud_gender ='Female' then 1 end) as char) as femalCount ")
		.append("from student_master sm ")
		.append("left join student_standard_renew ssr on (sm.stud_id = ssr.studentId and ssr.isDel='0') ")
		.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' ")
		.append("left join school_master ssm on (ssm.schoolid = a.schoolid and ssm.isDel='0') ")
		.append("left join group_of_school gs on (ssm.groupOfSchoolId = gs.groupOfSchoolId and gs.isDel='0') ")
		.append("where a.attendanceDate = '"+attendedDate+"' AND (a.p_AFlag='A' OR a.p_AType IS NOT NULL) ")
		.append("and a.isDel='0' and ssm.sansthaKey = '"+sansthaKey+"' and sm.isDel='0' and sm.isAdmissionCancelled='1' and ssr.alreadyRenew=1 and (ssr.college_leavingdate is null or ssr.college_leavingdate='')  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1'))  ");
		if(groupOfSchoolId!=0) {
			attendenceListQuery.append("and gs.groupOfSchoolId='"+groupOfSchoolId+"' ");
		}
		else {
			attendenceListQuery.append("and gs.groupOfSchoolId is null ");
		}
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(AttendanceReportBean1.class ) )
				 .getResultList();

	}
	
	
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<AttendanceReportBean1> getSchoolWisePresentReport(Integer schoolId, String attendedDate) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select cast(count(sm.stud_id) as char) as totalCount , cast(count(case when sm.stud_gender='Male' then 1 end) as char) as maleCount,  cast(count(case when sm.stud_gender ='Female' then 1 end) as char) as femalCount ")
		.append("from student_master sm ")
		.append("left join student_standard_renew ssr on (sm.stud_id = ssr.studentId and ssr.isDel='0') ")
		.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' ")
		.append("left join school_master ssm on (ssm.schoolid = ssr.schoolid and ssr.isDel='0') ")
		.append("where a.attendanceDate = '"+attendedDate+"' ")
		.append("and a.schoolid = '"+schoolId+"'  AND a.p_AFlag='P' ");
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(AttendanceReportBean1.class ) )
				 .getResultList();

	}


	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<AttendanceReportBean1> getSchoolWiseAbsentReport(Integer schoolId, String attendedDate) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select cast(count(sm.stud_id) as char) as totalCount , cast(count(case when sm.stud_gender='Male' then 1 end) as char) as maleCount,  cast(count(case when sm.stud_gender ='Female' then 1 end) as char) as femalCount ")
		.append("from student_master sm ")
		.append("left join student_standard_renew ssr on (sm.stud_id = ssr.studentId and ssr.isDel='0') ")
		.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' ")
		.append("left join school_master ssm on (ssm.schoolid = ssr.schoolid and ssr.isDel='0') ")
		.append("where a.attendanceDate = '"+attendedDate+"' ")
		.append("and a.schoolid = '"+schoolId+"' AND (a.p_AFlag='A' OR a.p_AType IS NOT NULL) ");
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(AttendanceReportBean1.class ) )
				 .getResultList();

	}



	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<StudentAttendanceDetailsBean> getTotalStudentsDetails(String attendedDate, Integer schoolId) {
		StringBuffer attendenceListQuery = new StringBuffer();
		attendenceListQuery.append("select CONCAT(IFNULL(sm.studInitialName,''),' ',IFNULL(sm.studFName,''),' ',IFNULL(sm.studMName,''),' ',IFNULL(sm.studLName,'')) as studentName ")
				.append("from student_master sm ")
				.append("left join student_standard_renew ssr on  (sm.stud_id = ssr.studentId and ssr.isDel='0')")
				.append("left join attendance_classwise_catalog a  ON sm.stud_id=a.stud_id AND ssr.renewstudentId=a.renewstudentId AND sm.schoolid=a.schoolid ")
				.append("where a.attendanceDate = '"+attendedDate+"' AND (a.p_AFlag='P' OR a.p_AFlag='A' OR a.p_AType IS NOT NULL) ");
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		List<StudentAttendanceDetailsBean> list = query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(StudentAttendanceDetailsBean.class ) )
				 .getResultList();
		
		
		return list;

		
	}	
	
	
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<StudentAttendanceDetailsBean> getPresentStudentDetails(String attendedDate) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select CONCAT(IFNULL(sm.studInitialName,''),' ',IFNULL(sm.studFName,''),' ',IFNULL(sm.studMName,''),' ',IFNULL(sm.studLName,'')) as studentName ")
				.append("from student_master sm ")
				.append("left join student_standard_renew ssr on  (sm.stud_id = ssr.studentId and ssr.isDel='0')")
				.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' ")
				.append("where a.attendanceDate = '"+attendedDate+"' AND a.p_AFlag='P' ");
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(StudentAttendanceDetailsBean.class ) )
				 .getResultList();
		
	}	
	
	
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<StudentAttendanceDetailsBean> getAbsentStudentDetails(String attendedDate) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select CONCAT(IFNULL(sm.studInitialName,''),' ',IFNULL(sm.studFName,''),' ',IFNULL(sm.studMName,''),' ',IFNULL(sm.studLName,'')) as studentName ")
				.append("from student_master sm ")
				.append("left join student_standard_renew ssr on  (sm.stud_id = ssr.studentId and ssr.isDel='0')")
				.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' ")
				.append("where a.attendanceDate =  '"+attendedDate+"' AND (a.p_AFlag='A' OR a.p_AType IS NOT NULL) ");
		 
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return  query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(StudentAttendanceDetailsBean.class ) )
				 .getResultList();

		
	}


	

	

	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<AttendanceReportBean1> getStandardWisePresentReport(Integer schoolId, String attendedDate,
			Integer standardId) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select cast(count(sm.stud_id) as char) as totalCount , cast(count(case when sm.stud_gender='Male' then 1 end) as char) as maleCount,  cast(count(case when sm.stud_gender ='Female' then 1 end) as char) as femalCount ")
		.append("from student_master sm ")
		.append("left join student_standard_renew ssr on (sm.stud_id = ssr.studentId and ssr.isDel='0') ")
		.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' ")
		.append("where a.attendanceDate = '"+attendedDate+"' ")
		.append("and a.schoolid= '"+schoolId+"' and ssr.standardId='"+standardId+"' AND a.p_AFlag='P' ");
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(AttendanceReportBean1.class ) )
				 .getResultList();

	}


	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<AttendanceReportBean1> getStandardWiseAbsentList(Integer schoolId, String attendedDate,
			Integer standardId) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select cast(count(sm.stud_id) as char) as totalCount , cast(count(case when sm.stud_gender='Male' then 1 end) as char) as maleCount,  cast(count(case when sm.stud_gender ='Female' then 1 end) as char) as femalCount ")
		.append("from student_master sm ")
		.append("left join student_standard_renew ssr on (sm.stud_id = ssr.studentId and ssr.isDel='0') ")
		.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' ")
		.append("where a.attendanceDate = '"+attendedDate+"' ")
		.append("and a.schoolid= '"+schoolId+"' and ssr.standardId='"+standardId+"'  AND a.p_AFlag='A' ");
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return  query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(AttendanceReportBean1.class ) )
				 .getResultList();

	}


	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<AttendanceReportBean1> getStandardWiseExemptedList(Integer schoolId, String attendedDate,
			Integer standardId) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select cast(count(sm.stud_id) as char) as totalCount , cast(count(case when sm.stud_gender='Male' then 1 end) as char) as maleCount,  cast(count(case when sm.stud_gender ='Female' then 1 end) as char) as femalCount ")
		.append("from student_master sm ")
		.append("left join student_standard_renew ssr on (sm.stud_id = ssr.studentId and ssr.isDel='0') ")
		.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' ")
		.append("where a.attendanceDate = '"+attendedDate+"' ")
		.append("and a.schoolid= '"+schoolId+"' and ssr.standardId='"+standardId+"' AND (a.p_AFlag='A' OR a.p_AType IS NOT NULL) ");
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return  query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(AttendanceReportBean1.class ) )
				 .getResultList();

	}
	
	
	


	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<StudentDetailsBean> getTotalPresentAttendanceDetails(String attendedDate, Integer schoolId,
			Integer standardId, Integer divisionId) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select CONCAT(IFNULL(sm.studInitialName,''),' ',IFNULL(sm.studFName,''),' ',IFNULL(sm.studMName,''),' ',IFNULL(sm.studLName,'')) as studentName , cast(sm.Student_RegNo as char) as studentRegNo , ssr.rollNo  as studentRollNo, sm.grBookId as studentgrBookId  ")
				.append("from student_master sm ")
				.append("left join student_standard_renew ssr on  (sm.stud_id = ssr.studentId and ssr.isDel='0')")
				.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' ")
				.append("where a.attendanceDate = '"+attendedDate+"' ")
				.append("and ssr.standardId='"+standardId+"' and ssr.divisionId='"+divisionId+"' AND a.p_AFlag='P' ");
				
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(StudentDetailsBean.class ) )
				 .getResultList();

	}	
	
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<StudentDetailsBean> getTotalAbsentAttendanceDetails(String attendedDate, Integer schoolId,
			Integer standardId, Integer divisionId) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select CONCAT(IFNULL(sm.studInitialName,''),' ',IFNULL(sm.studFName,''),' ',IFNULL(sm.studMName,''),' ',IFNULL(sm.studLName,'')) as studentName , cast(sm.Student_RegNo as char) as studentRegNo , ssr.rollNo  as studentRollNo, sm.grBookId as studentgrBookId  ")
				.append("from student_master sm ")
				.append("left join student_standard_renew ssr on  (sm.stud_id = ssr.studentId and ssr.isDel='0')")
				.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' where a.attendanceDate = '"+attendedDate+"' ")
				.append("and ssr.standardId='"+standardId+"' and ssr.divisionId='"+divisionId+"' AND a.p_AFlag='A' ");
				
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(StudentDetailsBean.class ) )
				 .getResultList();

	}


	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<AttendanceReportBean1> getDivisionWisePresentReport(Integer schoolId, String attendedDate, Integer standardId, Integer divisionId) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select cast(count(sm.stud_id) as char) as totalCount , cast(count(case when sm.stud_gender='Male' then 1 end) as char) as maleCount,  cast(count(case when sm.stud_gender ='Female' then 1 end) as char) as femalCount ")
		.append("from student_master sm ")
		.append("left join student_standard_renew ssr on (sm.stud_id = ssr.studentId and ssr.isDel='0') ")
		.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' ")
		.append("where a.attendanceDate = '"+attendedDate+"' ")
		.append("and a.schoolid= '"+schoolId+"' and ssr.standardId='"+standardId+"' and ssr.divisionId='"+divisionId+"' AND a.p_AFlag='P' ");
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(AttendanceReportBean1.class ) )
				 .getResultList();

	}


	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<AttendanceReportBean1> getDivisionWiseAbsentList(Integer schoolId, String attendedDate,
			Integer standardId, Integer divisionId) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select cast(count(sm.stud_id) as char) as totalCount , cast(count(case when sm.stud_gender='Male' then 1 end) as char) as maleCount,  cast(count(case when sm.stud_gender ='Female' then 1 end) as char) as femalCount ")
		.append("from student_master sm ")
		.append("left join student_standard_renew ssr on (sm.stud_id = ssr.studentId and ssr.isDel='0') ")
		.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' ")
		.append("where a.attendanceDate = '"+attendedDate+"' ")
		.append("and a.schoolid= '"+schoolId+"' and ssr.standardId='"+standardId+"' and ssr.divisionId='"+divisionId+"' AND a.p_AFlag='A' ");
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(AttendanceReportBean1.class ) )
				 .getResultList();

	}


	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<AttendanceReportBean1> getDivisionWiseExemptedList(Integer schoolId, String attendedDate,
			Integer standardId, Integer divisionId) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select cast(count(sm.stud_id) as char) as totalCount , cast(count(case when sm.stud_gender='Male' then 1 end) as char) as maleCount,  cast(count(case when sm.stud_gender ='Female' then 1 end) as char) as femalCount ")
		.append("from student_master sm ")
		.append("left join student_standard_renew ssr on (sm.stud_id = ssr.studentId and ssr.isDel='0') ")
		.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' ")
		.append("where a.attendanceDate = '"+attendedDate+"' ")
		.append("and a.schoolid= '"+schoolId+"' and ssr.standardId='"+standardId+"'and ssr.divisionId='"+divisionId+"' AND (a.p_AFlag='A' OR a.p_AType IS NOT NULL) ");
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(AttendanceReportBean1.class ) )
				 .getResultList();

	}


	public List<AttendanceReportBean1> getGroupWisePresentReport(String sansthaKey, String attendedDate,
			Integer groupOfSchoolId, List<Integer> schoolList) {
		StringBuilder attendenceListQuery = new StringBuilder();
		attendenceListQuery.append("select cast(count(sm.stud_id) as char) as totalCount , cast(count(case when sm.stud_gender='Male' then 1 end) as char) as maleCount,  cast(count(case when sm.stud_gender ='Female' then 1 end) as char) as femalCount ")
		.append("from student_master sm ")
		.append("left join student_standard_renew ssr on (sm.stud_id = ssr.studentId and ssr.isDel='0') ")
		.append("left join attendance_classwise_catalog a on sm.stud_id=a.stud_id and ssr.renewstudentId=a.renewstudentId and a.isDel='0' ")
		.append("left join school_master ssm on (ssm.schoolid = a.schoolid and ssm.isDel='0') ")
		.append("left join group_of_school gs on (ssm.groupOfSchoolId = gs.groupOfSchoolId and gs.isDel='0') ")
		.append("where a.attendanceDate = '"+attendedDate+"' ")
		.append("and a.isDel='0' and ssm.sansthaKey = '"+sansthaKey+"' and a.p_AFlag='A' and sm.isDel='0' and sm.isAdmissionCancelled='1' and ssr.alreadyRenew=1 and (ssr.college_leavingdate is null or ssr.college_leavingdate='')  AND (ssr.isOnlineAdmission='0' OR  (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) ");
		
		if(groupOfSchoolId!=0) {
			attendenceListQuery.append("and gs.groupOfSchoolId='"+groupOfSchoolId+"' ");
		}
		else {
			attendenceListQuery.append("and gs.groupOfSchoolId is null ");
		}
		
		Query query = entitymanager.createNativeQuery(attendenceListQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(AttendanceReportBean1.class ) )
				 .getResultList();
	}


	public List<AttendanceReportBean> getgroupWiseAttendance(String sansthaKey, String attendedDate) {
		// TODO Auto-generated method stub
		StringBuffer groupWiseAttendanceQuery = new StringBuffer();
		groupWiseAttendanceQuery.append("select gos.groupOfSchoolId as groupOfSchoolId, gos.groupOfSchoolName as groupOfSchoolName,cast(count(case when att.p_AFlag='A' AND att.p_AType IS NULL then 1 end) as char) as absentCountInWords, ")
		.append("cast(count(case when att.p_AFlag='P' then 1 end) as char) as presentCountInWords,cast(count(case when att.p_AFlag='A' AND att.p_AType IS NOT NULL then 1 end) as char) as exemptedCountInWords ")
		.append("from attendance_classwise_catalog att ")
		.append("LEFT JOIN standardmaster st ON att.stdId=st.standardId AND att.schoolid=st.schoolid AND st.isDel='0' ")
		.append("LEFT JOIN school_master sch ON sch.schoolid=att.schoolid AND sch.isDel='0' ")
		.append("LEFT JOIN group_of_school gos ON gos.groupOfSchoolId=sch.groupOfSchoolId AND gos.isDel='0' ")
		.append("where att.attendanceDate = '"+attendedDate+"' AND att.isDel='0' AND sch.sansthaKey='"+sansthaKey+"' ")
		.append("GROUP BY 1,2 ORDER BY groupOfSchoolId ");
		
		Query query = entitymanager.createNativeQuery(groupWiseAttendanceQuery.toString());
		List<AttendanceReportBean> list = query.unwrap(org.hibernate.query.Query.class).setResultTransformer(Transformers.aliasToBean(AttendanceReportBean.class)).list();
		return list;
	}


	public List<AttendanceReportBean> getStandardAndDivisionAttendance(Integer schoolId, String attendedDate,
			Integer standardId) {
		// TODO Auto-generated method stub
		StringBuffer groupWiseAttendanceQuery = new StringBuffer();
		groupWiseAttendanceQuery.append("SELECT st.standardName as stdandardName,st.standardId as standardId,di.divisionName as divisionName,di.divisionId as divisionId,cast(count(case when att.p_AFlag='A' AND att.p_AType IS NULL then 1 end) as char) as absentCountInWords, ")
		.append("cast(count(case when att.p_AFlag='P' then 1 end) as char) as presentCountInWords,cast(count(case when att.p_AFlag='A' AND att.p_AType IS NOT NULL then 1 end) as char) as exemptedCountInWords ")
		.append("from attendance_classwise_catalog att ")
		.append("LEFT JOIN standardmaster st ON att.stdId=st.standardId AND att.schoolid=st.schoolid AND st.isDel='0' ")
		.append("LEFT JOIN division di ON di.divisionId=att.divId AND di.isDel='0' AND di.schoolid=att.schoolid ")
		.append("where att.attendanceDate = '"+attendedDate+"' AND att.isDel='0' AND att.stdId="+standardId+" AND att.schoolid="+schoolId+" ")
		.append("GROUP BY 1,2 ORDER BY di.divisionNam ");
		
		Query query = entitymanager.createNativeQuery(groupWiseAttendanceQuery.toString());
		List<AttendanceReportBean> list = query.unwrap(org.hibernate.query.Query.class).setResultTransformer(Transformers.aliasToBean(AttendanceReportBean.class)).list();
		return list;
	}


	public List<AttendanceReportBean> getStandardWiseAttendance(Integer schoolId, String attendedDate) {
		// TODO Auto-generated method stub
		StringBuffer groupWiseAttendanceQuery = new StringBuffer();
		groupWiseAttendanceQuery.append("SELECT st.standardName as stdandardName,st.standardId as standardId,cast(count(case when att.p_AFlag='A' AND att.p_AType IS NULL then 1 end) as char) as absentCountInWords, ")
		.append("cast(count(case when att.p_AFlag='P' then 1 end) as char) as presentCountInWords,cast(count(case when att.p_AFlag='A' AND att.p_AType IS NOT NULL then 1 end) as char) as exemptedCountInWords ")
		.append("from attendance_classwise_catalog att ")
		.append("LEFT JOIN standardmaster st ON att.stdId=st.standardId AND att.schoolid=st.schoolid AND st.isDel='0' ")
		.append("where att.attendanceDate = '"+attendedDate+"' AND att.isDel='0' AND att.schoolid="+schoolId+" ")
		.append("GROUP BY 1,2 ORDER BY st.standardId ");
		
		Query query = entitymanager.createNativeQuery(groupWiseAttendanceQuery.toString());
		List<AttendanceReportBean> list = query.unwrap(org.hibernate.query.Query.class).setResultTransformer(Transformers.aliasToBean(AttendanceReportBean.class)).list();
		return list;
	}


	public List<AttendanceReportBean> getSchoolWiseAttendance(String sansthaKey, String attendedDate,
			Integer groupOfSchoolId) {
		// TODO Auto-generated method stub
		StringBuffer groupWiseAttendanceQuery = new StringBuffer();
		groupWiseAttendanceQuery.append("SELECT sc.schoolid as schoolid,ifnull(sc.shortSchoolName,'') AS schoolName,sc.schoolName as longSchoolName,sc.groupOfSchoolId as groupOfSchoolId,cast(count(case when att.p_AFlag='A' AND att.p_AType IS NULL then 1 end) as char) as absentCountInWords, ")
		.append("cast(count(case when att.p_AFlag='P' then 1 end) as char) as presentCountInWords,cast(count(case when att.p_AFlag='A' AND att.p_AType IS NOT NULL then 1 end) as char) as exemptedCountInWords ")
		.append("from attendance_classwise_catalog att ")
		.append("LEFT JOIN standardmaster st ON att.stdId=st.standardId AND att.schoolid=st.schoolid AND st.isDel='0' "
				+ "LEFT JOIN school_master sc ON sc.schoolid=att.schoolid AND sc.isDel='0' "
				+ "LEFT JOIN group_of_school gp ON gp.groupOfSchoolId=sc.groupOfSchoolId AND gp.isDel='0' ")
		.append("where att.attendanceDate = '"+attendedDate+"' AND att.isDel='0' AND sc.sansthaKey="+sansthaKey+" AND gp.groupOfSchoolId="+groupOfSchoolId+" ")
		.append("GROUP BY 1,2 ORDER BY sc.schoolid ");
		
		Query query = entitymanager.createNativeQuery(groupWiseAttendanceQuery.toString());
		List<AttendanceReportBean> list = query.unwrap(org.hibernate.query.Query.class).setResultTransformer(Transformers.aliasToBean(AttendanceReportBean.class)).list();
		return list;
	}


	public List<AttendanceReportBean> getSchoolWiseAttendance(String sansthaKey, String attendedDate) {
		// TODO Auto-generated method stub
		StringBuffer groupWiseAttendanceQuery = new StringBuffer();
		groupWiseAttendanceQuery.append("SELECT sc.schoolid as schoolid,ifnull(sc.shortSchoolName,'') AS schoolName,sc.schoolName as longSchoolName,sc.groupOfSchoolId as groupOfSchoolId,cast(count(case when att.p_AFlag='A' AND att.p_AType IS NULL then 1 end) as char) as absentCountInWords, ")
		.append("cast(count(case when att.p_AFlag='P' then 1 end) as char) as presentCountInWords,cast(count(case when att.p_AFlag='A' AND att.p_AType IS NOT NULL then 1 end) as char) as exemptedCountInWords ")
		.append("from attendance_classwise_catalog att ")
		.append("LEFT JOIN standardmaster st ON att.stdId=st.standardId AND att.schoolid=st.schoolid AND st.isDel='0' "
				+ "LEFT JOIN school_master sc ON sc.schoolid=att.schoolid AND sc.isDel='0' "
				+ "LEFT JOIN group_of_school gp ON gp.groupOfSchoolId=sc.groupOfSchoolId AND gp.isDel='0' ")
		.append("where att.attendanceDate = '"+attendedDate+"' AND att.isDel='0' AND sc.sansthaKey="+sansthaKey+" AND gp.groupOfSchoolId IS null ")
		.append("GROUP BY 1,2 ORDER BY sc.schoolid ");
		
		Query query = entitymanager.createNativeQuery(groupWiseAttendanceQuery.toString());
		List<AttendanceReportBean> list = query.unwrap(org.hibernate.query.Query.class).setResultTransformer(Transformers.aliasToBean(AttendanceReportBean.class)).list();
		return list;
	}


	public List<PrincipalMenuBean> getMenuList(Integer menuId, String isReactFlag) {
		// TODO Auto-generated method stub
		String queryStr="SELECT a.trustMenuId as menuId,a.menuName as menuName,a.imageFilePath as iconPath,a.isWebViewForReact,a.isWebViewForAndroid,a.webViewURL,a.parentMenuId "
				+ "FROM trust_menu_master a "
				+ "LEFT JOIN android_menu_master_group c ON c.androidMenuGroupId=a.androidMenuGroupId "
				+ "WHERE c.androidMenuGroupId="+menuId+" AND a.isReact IN ("+isReactFlag+")  " ;
		Query query = entitymanager.createNativeQuery(queryStr);
		
		List<PrincipalMenuBean> list = query.unwrap( org.hibernate.query.Query.class )
			 .setResultTransformer(Transformers.aliasToBean(PrincipalMenuBean.class ) )
			 .getResultList();
		return list;
	}

//	public List<PrincipalMenuBean> getMenuListForReact(Integer menuId, String isReactFlag) {
//		// TODO Auto-generated method stub
//		String queryStr="SELECT a.trustMenuId as menuId,a.menuName as menuName,a.imageFilePath as iconPath,a.isWebViewForReact,a.isWebViewForAndroid,a.webViewURL "
//				+ "FROM trust_menu_master a "
//				+ "LEFT JOIN android_menu_master_group c ON c.androidMenuGroupId=a.androidMenuGroupId "
//				+ "WHERE c.androidMenuGroupId="+menuId+" AND a.isReact IN ("+isReactFlag+") and a.isWebViewForReact='1' " ;
//		Query query = entitymanager.createNativeQuery(queryStr);
//		
//		List<PrincipalMenuBean> list = query.unwrap( org.hibernate.query.Query.class )
//			 .setResultTransformer(Transformers.aliasToBean(PrincipalMenuBean.class ) )
//			 .getResultList();
//		return list;
//	}

	public List<PrincipalMenuBean> getMenuList1(Integer menuId, String isReactFlag,String sansthaKey) {
		// TODO Auto-generated method stub
		String queryStr="SELECT a.trustMenuId as menuId,a.menuName as menuName,a.imageFilePath as iconPath,a.isWebViewForReact,a.isWebViewForAndroid,a.webViewURL,a.parentMenuId "
				+ "FROM trust_menu_master a "
				+ "LEFT JOIN android_menu_master_group c ON c.androidMenuGroupId=a.androidMenuGroupId "
				+ "LEFT JOIN trust_menu t ON t.menuId=a.trustMenuId "
				+ "WHERE c.androidMenuGroupId="+menuId+" AND a.isReact IN ("+isReactFlag+") and t.sansthaKey="+sansthaKey+"  " ;
		Query query = entitymanager.createNativeQuery(queryStr);
		
		List<PrincipalMenuBean> list = query.unwrap( org.hibernate.query.Query.class )
			 .setResultTransformer(Transformers.aliasToBean(PrincipalMenuBean.class ) )
			 .getResultList();
		return list;
	}


	public List<PrincipalMenuBean> getGlobalMenuList1(Integer menuId, String isReactFlag) {
		String queryStr="SELECT a.trustMenuId as menuId,a.menuName as menuName,a.imageFilePath as iconPath,a.isWebViewForReact,a.isWebViewForAndroid,a.webViewURL,a.parentMenuId "
				+ "FROM trust_menu_master a "
				+ "LEFT JOIN android_menu_master_group c ON c.androidMenuGroupId=a.androidMenuGroupId "
				+ "WHERE c.androidMenuGroupId="+menuId+" and a.isGlobal=1 AND a.isReact IN ("+isReactFlag+")  " ;
		Query query = entitymanager.createNativeQuery(queryStr);
		
		List<PrincipalMenuBean> list = query.unwrap( org.hibernate.query.Query.class )
			 .setResultTransformer(Transformers.aliasToBean(PrincipalMenuBean.class ) )
			 .getResultList();
		return list;
	}


	public List<Integer> getMenuListNotForSchool(String sansthaKey) {
		String queryStr="SELECT a.menuId as menuId "
				+ "FROM trust_menu_not_for_school a "
				+ "WHERE a.sansthaKey="+sansthaKey+"  " ;
		Query query = entitymanager.createNativeQuery(queryStr);
		
		List<Integer> list = query.unwrap( org.hibernate.query.Query.class )
			 .getResultList();
		return list;
	}
	
}	
	
