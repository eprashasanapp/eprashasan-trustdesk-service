 package com.ingenio.trust.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.bean.FeeBean;


@Repository
@SuppressWarnings({"unchecked","deprecation"})
public class FeeRepositoryImpl {
  
  @PersistenceContext
  private EntityManager entitymanager;
	
	public List<FeeBean>  getTotalFee(String sansthaKey,String fromDate,String toDate, Integer groupId) {
		StringBuilder feeReportQuery = new StringBuilder();

//			feeReportQuery.append("SELECT yearId,yearName,schoolid AS schoolId, IFNULL(schoolName,'') AS schoolName,longSchoolName, CAST(assignedFee AS CHAR) AS assignedFee,  ");
//			feeReportQuery.append("CAST(assignedFine AS CHAR) AS assignedFine, CAST(clearanceFee AS CHAR) AS clearanceFee, CAST(IFNULL(fineClearance,0) AS CHAR) AS clearanceFine, CAST(paidFee AS CHAR) AS paidFee, CAST(discount AS CHAR) AS discount, CAST(IFNULL(totalPaidFine,0) AS CHAR) AS paidFine, CAST((assignedFee - (paidFee + discount) - clearanceFee) AS CHAR) AS remainingFee, CAST(IFNULL((assignedFine- totalPaidFine - fineClearance),0) AS CHAR) AS remainingFine   ");
//			feeReportQuery.append("FROM ( ");
//			feeReportQuery.append("SELECT *  ");
//			feeReportQuery.append("FROM ( ");
//			feeReportQuery.append("SELECT feeSetMultiID,yearId,yearName,schoolid,schoolName,longSchoolName,clearanceFee,ifnull(totalAssignFee,0) AS assignedFee ,headId,subheadId, ");
//			feeReportQuery.append("(ifnull(totalPaidFee,0)+ifnull(utilizeFee,0))-IFNULL(discount,0) AS paidFee,ifnull(remianFee,0) AS remianFee,IFNULL(assignedFine,0) AS assignedFine,IFNULL(discount,0) AS discount ");
//			feeReportQuery.append("FROM  ");
//			feeReportQuery.append("(SELECT feeSetMultiID,yearId,yearName,schoolid,schoolName,longSchoolName,clearanceFee,ifnull(sum(totalAssignFee),0) AS totalAssignFee ,headId,subheadId,  ");
//			feeReportQuery.append("ifnull(sum(totalPaidFee),0) AS totalPaidFee,ifnull(sum(utilizeFee),0) AS utilizeFee,IFNULL(sum(discount),0) AS discount,if((ifnull(sum(totalPaidFee),0)+ifnull(sum(utilizeFee),0))-IFNULL(sum(discount),0)>ifnull(SUM(totalAssignFee),0),0,ifnull(sum(totalAssignFee),0)-(ifnull(sum(totalPaidFee),0)+ifnull(sum(utilizeFee),0))-IFNULL(sum(discount),0)) AS remianFee,SUM(IFNULL(fine,0)) AS assignedFine  ");
//			feeReportQuery.append("FROM (SELECT g.feeSetMultiID,e.yearId,e.year AS yearName,m.schoolid,m.shortSchoolName AS schoolName,m.schoolName AS longSchoolName, ");
//			feeReportQuery.append("SUM(IFNULL(k.clearanceFee,0)) AS clearanceFee, ifnull(sum(g.assignFee),0) AS totalAssignFee,g.headId,g.subheadId  ");
//			feeReportQuery.append("FROM fee_setting_multi g  ");
//			feeReportQuery.append("LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID AND d.isDel='0' ");
//			feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID AND c.isDel='0'  ");
//			feeReportQuery.append("LEFT JOIN yearmaster e ON e.yearId=d.yearID AND e.isDel='0' ");
//			feeReportQuery.append("LEFT JOIN fee_clearancetable k ON k.feeSetMultiID=g.feeSetMultiID AND k.isDel='0' ");
//			feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=g.schoolid  ");
//			feeReportQuery.append("LEFT JOIN fee_subhead_master n ON n.subheadID=g.subheadID  ");
//			feeReportQuery.append("LEFT JOIN student_master r ON r.stud_id=c.studRenewID ");
//			feeReportQuery.append("LEFT JOIN student_standard_renew ssr ON ssr.studentId=r.stud_id ");
//			feeReportQuery.append("WHERE g.isConsolation=0 AND m.sansthaKey='"+sansthaKey+"' AND g.isDel='0'   AND n.isScholarshipFlag='0' AND r.isDel='0' AND r.isAdmissionCancelled='1' AND ssr.alreadyRenew=1 AND (ssr.college_leavingdate IS NULL OR ssr.college_leavingdate='')   ");
//			if(groupId !=0) {
//				feeReportQuery.append("and m.groupOfSchoolId='"+groupId+"' " ); 
//			}
//			else {
//				feeReportQuery.append("and m.groupOfSchoolId is null "); 
//			}
//			feeReportQuery.append("GROUP BY c.studRenewID,d.yearId,g.schoolid,d.standardId,g.feeSetMultiID)vg  ");
//			feeReportQuery.append("LEFT JOIN ");
//			feeReportQuery.append("(SELECT g.feeSetMultiID AS feeSetMultiID1,d.yearId AS yearId1,l.standardId AS standardId1,i.feesClearance AS feesClearance, i.fineClearance AS fineClearance, g.headID AS headId1, g.subheadID AS subheadId1, IFNULL((CASE WHEN (IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE()) THEN IFNULL(g.fine* DATEDIFF(CURRENT_DATE(), STR_TO_DATE(g.dueDate,'%d-%m-%Y')),0) WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE() THEN IFNULL(g.fine,0) WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE() THEN IFNULL(g.fine,0) WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') THEN IFNULL(g.fine* DATEDIFF(STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y'), STR_TO_DATE(g.dueDate,'%d-%m-%Y')),0) WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') THEN 0 END),0) AS fine, IFNULL(sum(i.payFee),0) AS totalPaidFee, IFNULL(sum(i.discount),0) AS discount ");
//			feeReportQuery.append("FROM fee_setting_multi g  ");
//			feeReportQuery.append("LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID  ");
//			feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID  ");
//			feeReportQuery.append("LEFT JOIN fee_head_master f ON f.headID=g.headID ");
//			feeReportQuery.append("LEFT JOIN fee_subhead_master h ON h.subheadID=g.subheadID  ");
//			feeReportQuery.append("LEFT JOIN yearmaster e ON e.yearId=d.yearID ");
//			feeReportQuery.append("LEFT JOIN fee_paidfeetable i ON i.feeSetMultiID=g.feeSetMultiID  ");
//			if(StringUtils.isBlank(toDate)) {
//				feeReportQuery.append(" and STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') <= STR_TO_DATE('"+fromDate+"','%d-%m-%Y') " );
//			}
//			else {
//				feeReportQuery.append("and STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y') " );
//			}
//			feeReportQuery.append("LEFT JOIN fee_receipt i1 ON i1.receiptID=i.receiptID ");
//			feeReportQuery.append("LEFT JOIN student_master r ON r.stud_id=c.studRenewID ");
//			feeReportQuery.append("LEFT JOIN student_standard_renew ssr ON ssr.studentId=r.stud_id ");
//			feeReportQuery.append("LEFT JOIN standardmaster l ON l.standardId=d.standardID ");
//			feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=g.schoolid ");
//			feeReportQuery.append("WHERE g.isConsolation=0  AND m.sansthaKey='"+sansthaKey+"' AND g.isDel='0'   AND (i1.isDel='0' OR (i1.isDel='1' AND i1.isApproval='0')) AND r.isDel='0' AND r.isAdmissionCancelled='1' AND ssr.alreadyRenew=1 AND (ssr.college_leavingdate IS NULL OR ssr.college_leavingdate='') AND (ssr.isOnlineAdmission='0' OR (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) AND h.isScholarshipFlag='0' AND (((i1.payTypeOrPrintFlag IS NULL OR i1.payTypeOrPrintFlag='2') AND i1.returnFlag='1') OR (i1.payTypeOrPrintFlag IS NULL OR i1.payTypeOrPrintFlag='1'))  ");
//			if(groupId !=0) {
//				feeReportQuery.append("and m.groupOfSchoolId='"+groupId+"' " ); 
//			}
//			else {
//				feeReportQuery.append("and m.groupOfSchoolId is null "); 
//			}
//			feeReportQuery.append("GROUP BY c.studRenewID,d.yearId,g.schoolid,d.standardId,g.feeSetMultiID)vh ON  vg.feeSetMultiID=vh.feeSetMultiID1 ");
//			feeReportQuery.append("LEFT JOIN ");
//			feeReportQuery.append("(SELECT g.feeSetMultiID AS feeSetMultiID2,d.yearId AS yearId2, g.headID AS headID2, g.subheadID AS subheadID2, IFNULL(i.utilizeFee,0) AS utilizeFee  ");
//			feeReportQuery.append("FROM fee_setting_multi g ");
//			feeReportQuery.append("LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID  ");
//			feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID  ");
//			feeReportQuery.append("LEFT JOIN fee_head_master f ON f.headID=g.headID ");
//			feeReportQuery.append("LEFT JOIN fee_subhead_master h ON h.subheadID=g.subheadID ");
//			feeReportQuery.append("LEFT JOIN yearmaster e ON e.yearId=d.yearID ");
//			feeReportQuery.append("LEFT JOIN utilize_excess_fee i ON i.feeSetMultiID=g.feeSetMultiID AND STR_TO_DATE(i.uitilizefeeDate,'%d-%m-%Y') <= STR_TO_DATE('"+fromDate+"','%d-%m-%Y') ");
//			feeReportQuery.append("LEFT JOIN reciept_utilization_of_excces_fee i1 ON i1.voucherID=i.receiptID  ");
//			feeReportQuery.append("LEFT JOIN student_master r ON r.stud_id=c.studRenewID  ");
//			feeReportQuery.append("LEFT JOIN student_standard_renew ssr ON ssr.studentId=r.stud_id ");
//			feeReportQuery.append("LEFT JOIN standardmaster l ON l.standardId=d.standardID ");
//			feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=g.schoolid ");
//			feeReportQuery.append("WHERE g.isConsolation=0  AND m.sansthaKey='"+sansthaKey+"' AND g.isDel='0'   AND r.isDel='0' AND r.isAdmissionCancelled='1' AND ssr.alreadyRenew=1 AND (ssr.college_leavingdate IS NULL OR ssr.college_leavingdate='') AND (ssr.isOnlineAdmission='0' OR (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) AND h.isScholarshipFlag='0'  ");
//			if(groupId !=0) {
//				feeReportQuery.append("and m.groupOfSchoolId='"+groupId+"' " ); 
//			}
//			else {
//				feeReportQuery.append("and m.groupOfSchoolId is null "); 
//			}
//			feeReportQuery.append("GROUP BY c.studRenewID,d.yearId,g.schoolid,d.standardId,g.headId,g.subheadId,g.feeSetMultiID)kh ON vg.headId=kh.headID2 AND vg.subheadId=kh.subheadID2 AND vg.feeSetMultiId=kh.feeSetMultiID2 ");
//			feeReportQuery.append("GROUP BY yearId,schoolId)yu ");
//			feeReportQuery.append("GROUP BY yearId,schoolId)gf  ");
//			feeReportQuery.append(" LEFT JOIN (select yearId1,schoolId1,standardID1,headId1,subheadId1, " 
//				+ "feeSetMultiID1,SUM(totalPaidFine) as totalPaidFine,SUM(fineClearance) as fineClearance " 
//				+ " from( "
//				+ "SELECT m.schoolid AS schoolId1,d.standardID as standardID1,"
//				+ "d.yearID as yearId1,g.feeSetMultiID as feeSetMultiID1,g.headId as headId1, g.subheadId as subheadId1, "
//				+ "SUM(IFNULL(j.dueFee,0)) AS totalPaidFine, "
//				+ "IFNULL(k.dueFee,0) AS fineClearance FROM fee_setting_multi g LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID and d.isDel='0' "
//				+ "LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID  and c.isDel='0' "
//				+ "LEFT JOIN yearmaster e ON e.yearId=d.yearID and e.isDel='0' "
//				+ "LEFT JOIN fee_paid_duetable j ON j.feeSetMultiID=g.feeSetMultiID and j.isDel='0' " );
//				if(StringUtils.isBlank(toDate)) {
//					feeReportQuery.append(" and STR_TO_DATE(j.dueDate,'%d-%m-%Y') <= STR_TO_DATE('"+fromDate+"','%d-%m-%Y') " );
//				}
//				else {
//					feeReportQuery.append("and STR_TO_DATE(j.dueDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y') ");
//				}
//			feeReportQuery.append("LEFT JOIN fee_duetable k ON k.feeSetMultiID=g.feeSetMultiID and k.isDel='0' "
//				+ "LEFT JOIN school_master m on m.schoolid=g.schoolid  "
//				+ "WHERE g.isConsolation=0 and m.sansthaKey='"+sansthaKey+"' and g.isDel='0' " );
//			
//			if(groupId !=0) {
//				feeReportQuery.append("and m.groupOfSchoolId='"+groupId+"' " ); 
//			}
//			else {
//				feeReportQuery.append("and m.groupOfSchoolId is null "); 
//			}
//		
//			feeReportQuery.append("GROUP BY yearId1,schoolid1,standardId1,headId1,subheadId1,feeSetMultiID1)a1 GROUP BY  yearId1,schoolId1)kj "
//				+ "ON kj.yearId1=yearId AND kj.schoolid1=schoolid)ghd "
//				+ "ORDER BY yearId,schoolid ASC ");
		
		feeReportQuery.append("SELECT yearId,yearName,schoolid AS schoolId, IFNULL(schoolName,'') AS schoolName,longSchoolName, CAST(assignedFee AS CHAR) AS assignedFee, CAST(assignedFine AS CHAR) AS assignedFine, CAST(clearanceFee AS CHAR) AS clearanceFee, CAST(IFNULL(fineClearance,0) AS CHAR) AS clearanceFine, CAST(paidFee AS CHAR) AS paidFee, CAST(discount AS CHAR) AS discount, CAST(IFNULL(totalPaidFine,0)+IFNULL(totlPaidFine1,0) AS CHAR) AS paidFine, CAST((assignedFee - (paidFee + discount) - clearanceFee) AS CHAR) AS remainingFee, CAST(IFNULL((assignedFine- ifnull(totalPaidFine,0) - fineClearance),0) AS CHAR) AS remainingFine,cast(ifnull(otherExcessFee,0) AS CHAR) AS excessFee,CAST(ifnull(otherFee,0) AS CHAR) AS otherFee  "); 
		feeReportQuery.append("FROM ( "); 
		feeReportQuery.append("SELECT *  "); 
		feeReportQuery.append("FROM (  "); 
		feeReportQuery.append("SELECT feeSetMultiID,yearId,yearName,schoolid,schoolName,longSchoolName,clearanceFee, IFNULL(totalAssignFee,0) AS assignedFee,headId,subheadId, (IFNULL(totalPaidFee,0)+ IFNULL(utilizeFee,0))- IFNULL(discount,0) AS paidFee, IFNULL(remianFee,0) AS remianFee, IFNULL(assignedFine,0) AS assignedFine, IFNULL(discount,0) AS discount "); 
		feeReportQuery.append("FROM (  "); 
		feeReportQuery.append("SELECT feeSetMultiID,yearId,yearName,schoolid,schoolName,longSchoolName,clearanceFee, IFNULL(SUM(totalAssignFee),0) AS totalAssignFee,headId,subheadId, IFNULL(SUM(totalPaidFee),0) AS totalPaidFee, IFNULL(SUM(utilizeFee),0) AS utilizeFee, IFNULL(SUM(discount),0) AS discount,if((IFNULL(SUM(totalPaidFee),0)+ IFNULL(SUM(utilizeFee),0))- IFNULL(SUM(discount),0)> IFNULL(SUM(totalAssignFee),0),0, IFNULL(SUM(totalAssignFee),0)-(IFNULL(SUM(totalPaidFee),0)+ IFNULL(SUM(utilizeFee),0))- IFNULL(SUM(discount),0)) AS remianFee, SUM(IFNULL(fine,0)) AS assignedFine  "); 
		feeReportQuery.append("FROM (  "); 
		feeReportQuery.append("SELECT g.feeSetMultiID,e.yearId,e.year AS yearName,m.schoolid,m.shortSchoolName AS schoolName,m.schoolName AS longSchoolName, SUM(IFNULL(k.clearanceFee,0)) AS clearanceFee, IFNULL(SUM(g.assignFee),0) AS totalAssignFee,g.headId,g.subheadId  "); 
		feeReportQuery.append("FROM fee_setting_multi g "); 
		feeReportQuery.append("LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID AND d.isDel='0'  "); 
		feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID AND c.isDel='0' "); 
		feeReportQuery.append("LEFT JOIN yearmaster e ON e.yearId=d.yearID AND e.isDel='0' "); 
		feeReportQuery.append("LEFT JOIN fee_clearancetable k ON k.feeSetMultiID=g.feeSetMultiID AND k.isDel='0' "); 
		feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=g.schoolid  "); 
		feeReportQuery.append("LEFT JOIN fee_subhead_master n ON n.subheadID=g.subheadID  "); 
		feeReportQuery.append("LEFT JOIN student_master r ON r.stud_id=c.studRenewID  "); 
		feeReportQuery.append("LEFT JOIN student_standard_renew ssr ON ssr.studentId=r.stud_id  "); 
		feeReportQuery.append("WHERE g.isConsolation=0 AND m.sansthaKey='"+sansthaKey+"' AND g.isDel='0' AND n.isScholarshipFlag='0' AND r.isDel='0' AND r.isAdmissionCancelled='1' AND ssr.alreadyRenew=1 AND (ssr.college_leavingdate IS NULL OR ssr.college_leavingdate='')   ");
		if(groupId !=0) {
			feeReportQuery.append("and m.groupOfSchoolId='"+groupId+"' " ); 
		}
		else {
			feeReportQuery.append("and m.groupOfSchoolId is null "); 
		}
		feeReportQuery.append("GROUP BY c.studRenewID,d.yearId,g.schoolid,d.standardId,g.feeSetMultiID)vg "); 
		feeReportQuery.append("LEFT JOIN ( "); 
		feeReportQuery.append("SELECT g.feeSetMultiID AS feeSetMultiID1,d.yearId AS yearId1,l.standardId AS standardId1,i.feesClearance AS feesClearance, i.fineClearance AS fineClearance, g.headID AS headId1, g.subheadID AS subheadId1, IFNULL((CASE WHEN (IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE()) THEN IFNULL(g.fine* DATEDIFF(CURRENT_DATE(), STR_TO_DATE(g.dueDate,'%d-%m-%Y')),0) WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE() THEN IFNULL(g.fine,0) WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE() THEN IFNULL(g.fine,0) WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') THEN IFNULL(g.fine* DATEDIFF(STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y'), STR_TO_DATE(g.dueDate,'%d-%m-%Y')),0) WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') THEN 0 END),0) AS fine, IFNULL(SUM(i.payFee),0) AS totalPaidFee, IFNULL(SUM(i.discount),0)+IFNULL(d.preDiscount,0) AS discount "); 
		feeReportQuery.append("FROM fee_setting_multi g  "); 
		feeReportQuery.append("LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID  "); 
		feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID  "); 
		feeReportQuery.append("LEFT JOIN fee_head_master f ON f.headID=g.headID "); 
		feeReportQuery.append("LEFT JOIN fee_subhead_master h ON h.subheadID=g.subheadID  "); 
		feeReportQuery.append("LEFT JOIN yearmaster e ON e.yearId=d.yearID "); 
		feeReportQuery.append("LEFT JOIN fee_paidfeetable i ON i.feeSetMultiID=g.feeSetMultiID  ");
		if(StringUtils.isBlank(toDate)) {
			feeReportQuery.append(" and STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') <= STR_TO_DATE('"+fromDate+"','%d-%m-%Y') " );
		}
		else {
			feeReportQuery.append("and STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y') " );
		}
		feeReportQuery.append("LEFT JOIN fee_receipt i1 ON i1.receiptID=i.receiptID  "); 
		feeReportQuery.append("LEFT JOIN student_master r ON r.stud_id=c.studRenewID "); 
		feeReportQuery.append("LEFT JOIN student_standard_renew ssr ON ssr.studentId=r.stud_id   "); 
		feeReportQuery.append("LEFT JOIN standardmaster l ON l.standardId=d.standardID "); 
		feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=g.schoolid  "); 
		feeReportQuery.append("WHERE g.isConsolation=0 AND m.sansthaKey='"+sansthaKey+"' AND g.isDel='0' AND (i1.isDel='0' OR (i1.isDel='1' AND i1.isApproval='0')) AND r.isDel='0' AND r.isAdmissionCancelled='1' AND ssr.alreadyRenew=1 AND (ssr.college_leavingdate IS NULL OR ssr.college_leavingdate='') AND (ssr.isOnlineAdmission='0' OR (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) AND h.isScholarshipFlag='0' AND if((i1.payTypeOrPrintFlag='2' AND SUBSTRING(i.payType,9,2) IN('02','03')),(((i1.payTypeOrPrintFlag IS NULL OR i1.payTypeOrPrintFlag='2') AND i1.returnFlag='1') AND (i1.reconciliationFlag='1' AND i1.bounceClearFlag='1')), ((i1.payTypeOrPrintFlag IS NULL OR i1.payTypeOrPrintFlag='2') AND i1.returnFlag='1') OR (i1.payTypeOrPrintFlag IS NULL OR i1.payTypeOrPrintFlag='1'))  ");
		if(groupId !=0) {
			feeReportQuery.append("and m.groupOfSchoolId='"+groupId+"' " ); 
		}
		else {
			feeReportQuery.append("and m.groupOfSchoolId is null "); 
		}
		feeReportQuery.append("GROUP BY c.studRenewID,d.yearId,g.schoolid,d.standardId,g.feeSetMultiID)vh ON vg.feeSetMultiID=vh.feeSetMultiID1  "); 
		feeReportQuery.append("LEFT JOIN ( "); 
		feeReportQuery.append("SELECT g.feeSetMultiID AS feeSetMultiID2,d.yearId AS yearId2, g.headID AS headID2, g.subheadID AS subheadID2, IFNULL(i.utilizeFee,0) AS utilizeFee  "); 
		feeReportQuery.append("FROM fee_setting_multi g "); 
		feeReportQuery.append("LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID  "); 
		feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID  "); 
		feeReportQuery.append("LEFT JOIN fee_head_master f ON f.headID=g.headID "); 
		feeReportQuery.append("LEFT JOIN fee_subhead_master h ON h.subheadID=g.subheadID  "); 
		feeReportQuery.append("LEFT JOIN yearmaster e ON e.yearId=d.yearID  "); 
		feeReportQuery.append("LEFT JOIN utilize_excess_fee i ON i.feeSetMultiID=g.feeSetMultiID AND STR_TO_DATE(i.uitilizefeeDate,'%d-%m-%Y') <= STR_TO_DATE('16-05-2022','%d-%m-%Y') "); 
		feeReportQuery.append("LEFT JOIN reciept_utilization_of_excces_fee i1 ON i1.voucherID=i.receiptID   "); 
		feeReportQuery.append("LEFT JOIN student_master r ON r.stud_id=c.studRenewID  "); 
		feeReportQuery.append("LEFT JOIN student_standard_renew ssr ON ssr.studentId=r.stud_id  "); 
		feeReportQuery.append("LEFT JOIN standardmaster l ON l.standardId=d.standardID  "); 
		feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=g.schoolid  "); 
		feeReportQuery.append("WHERE g.isConsolation=0 AND m.sansthaKey='"+sansthaKey+"' AND g.isDel='0' AND r.isDel='0' AND r.isAdmissionCancelled='1' AND ssr.alreadyRenew=1 AND (ssr.college_leavingdate IS NULL OR ssr.college_leavingdate='') AND (ssr.isOnlineAdmission='0' OR (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) AND h.isScholarshipFlag='0'   ");
		if(groupId !=0) {
			feeReportQuery.append("and m.groupOfSchoolId='"+groupId+"' " ); 
		}
		else {
			feeReportQuery.append("and m.groupOfSchoolId is null "); 
		}
		feeReportQuery.append("GROUP BY c.studRenewID,d.yearId,g.schoolid,d.standardId,g.headId,g.subheadId,g.feeSetMultiID)kh ON vg.headId=kh.headID2 AND vg.subheadId=kh.subheadID2 AND vg.feeSetMultiId=kh.feeSetMultiID2 "); 
		feeReportQuery.append("GROUP BY yearId,schoolId)yu   "); 
		feeReportQuery.append("GROUP BY yearId,schoolId)gf  "); 
		feeReportQuery.append("LEFT JOIN (  "); 
		feeReportQuery.append("SELECT yearId1,schoolId1,standardID1,headId1,subheadId1, feeSetMultiID1, SUM(totalPaidFine) AS totalPaidFine, SUM(fineClearance) AS fineClearance  "); 
		feeReportQuery.append("FROM(  "); 
		feeReportQuery.append("SELECT m.schoolid AS schoolId1,d.standardID AS standardID1,d.yearID AS yearId1,g.feeSetMultiID AS feeSetMultiID1,g.headId AS headId1, g.subheadId AS subheadId1, SUM(IFNULL(j.dueFee,0)) AS totalPaidFine, IFNULL(k.dueFee,0) AS fineClearance  "); 
		feeReportQuery.append("FROM fee_setting_multi g  "); 
		feeReportQuery.append("LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID AND d.isDel='0'   "); 
		feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID AND c.isDel='0'  "); 
		feeReportQuery.append("LEFT JOIN yearmaster e ON e.yearId=d.yearID AND e.isDel='0' "); 
		feeReportQuery.append("LEFT JOIN fee_paid_duetable j ON j.feeSetMultiID=g.feeSetMultiID AND j.isDel='0'   ");
		if(StringUtils.isBlank(toDate)) {
			feeReportQuery.append(" and STR_TO_DATE(j.dueDate,'%d-%m-%Y') <= STR_TO_DATE('"+fromDate+"','%d-%m-%Y') " );
		}
		else {
			feeReportQuery.append("and STR_TO_DATE(j.dueDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y') ");
		}
		feeReportQuery.append("LEFT JOIN fee_duetable k ON k.feeSetMultiID=g.feeSetMultiID AND k.isDel='0' "); 
		feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=g.schoolid "); 
		feeReportQuery.append("WHERE g.isConsolation=0 AND m.sansthaKey='"+sansthaKey+"' AND g.isDel='0'  ");
		if(groupId !=0) {
			feeReportQuery.append("and m.groupOfSchoolId='"+groupId+"' " ); 
		}
		else {
			feeReportQuery.append("and m.groupOfSchoolId is null "); 
		}
		feeReportQuery.append("GROUP BY yearId1,schoolid1,standardId1,headId1,subheadId1,feeSetMultiID1)a1   "); 
		feeReportQuery.append("GROUP BY yearId1,schoolId1)kj ON kj.yearId1=yearId AND kj.schoolid1=schoolid  "); 
		feeReportQuery.append("LEFT JOIN ( "); 
		feeReportQuery.append("SELECT otherExcessFee,yearId9,schoolId9 FROM (SELECT SUM(IFNULL(otherExcessFee,0)) AS otherExcessFee,yearId9,studId9, schoolId as schoolId9   "); 
		feeReportQuery.append("FROM ( "); 
		feeReportQuery.append("SELECT if((  "); 
		feeReportQuery.append("SELECT DISTINCT a.receiptID   "); 
		feeReportQuery.append("FROM fee_deletepaidfeetable b  "); 
		feeReportQuery.append("WHERE b.receiptID=a.receiptId),0, (a.excessFees)) AS otherExcessFee,a.yearId AS yearId9,d.renewstudentId AS studId9,gs.groupOfSchoolId AS groupId,m.schoolid AS schoolId  "); 
		feeReportQuery.append("FROM fee_excess_studentwise a "); 
		feeReportQuery.append("LEFT JOIN fee_receipt c ON c.receiptID=a.receiptId "); 
		feeReportQuery.append("LEFT JOIN student_standard_renew d ON d.studentId=a.studId "); 
		feeReportQuery.append("LEFT JOIN yearmaster d1 ON d1.yearId=a.yearId   "); 
		feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=a.schoolid AND m.isDel='0'   "); 
		feeReportQuery.append("LEFT JOIN group_of_school gs ON gs.groupOfSchoolId=m.groupOfSchoolId  "); 
		feeReportQuery.append("WHERE (c.isDel='0' OR (c.isDel='1' AND c.isApproval='0')) AND m.sansthaKey='"+sansthaKey+"'    ");
		if(groupId !=0) {
			feeReportQuery.append("and m.groupOfSchoolId='"+groupId+"' " ); 
		}
		else {
			feeReportQuery.append("and m.groupOfSchoolId is null "); 
		}
		feeReportQuery.append(" )k GROUP BY studId9,yearId9,schoolId9)k "); 
		feeReportQuery.append("GROUP BY yearId9,schoolId9)ks ON ks.yearId9=yearId AND ks.schoolId9=schoolid  "); 
		feeReportQuery.append("LEFT JOIN ( "); 
		feeReportQuery.append("SELECT IFNULL(sum(totlPaidFine1),0) AS totlPaidFine1,schoolId AS schoolId2,yearId AS yearId2  FROM (SELECT IFNULL(SUM(a.payFee),0.00) AS totlPaidFine1,m.schoolid AS schoolId,a.yearId AS yearId  "); 
		feeReportQuery.append("FROM fee_paidfeetable a   "); 
		feeReportQuery.append("LEFT JOIN fee_head_master p ON p.headID=a.headID  "); 
		feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.studFeeID=a.studFeeID   "); 
		feeReportQuery.append("LEFT JOIN student_standard_renew d ON d.studentId=c.studRenewID  "); 
		feeReportQuery.append("LEFT JOIN yearmaster d1 ON d1.yearId=a.yearId  "); 
		feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=a.schoolid AND m.isDel='0' "); 
		feeReportQuery.append("LEFT JOIN group_of_school gs ON gs.groupOfSchoolId=m.groupOfSchoolId  "); 
		feeReportQuery.append("WHERE  a.isDel='0' AND p.isLateFeeCharges='1' AND m.sansthaKey='"+sansthaKey+"' ");
		if(groupId !=0) {
			feeReportQuery.append("and m.groupOfSchoolId='"+groupId+"' " ); 
		}
		else {
			feeReportQuery.append("and m.groupOfSchoolId is null "); 
		}
		feeReportQuery.append("GROUP BY c.studRenewID,a.headID,a.subheadID,a.yearId,m.schoolid)k GROUP BY yearId2,schoolId2)kl on kl.schoolId2=schoolid AND kl.yearId2=yearId  "); 
		feeReportQuery.append("LEFT JOIN (SELECT SUM(otherFee) AS otherFee,schoolId AS schoolId3,yearId AS yearId3 FROM (SELECT IFNULL(SUM(a.payFee),0.00) AS otherFee,m.schoolid AS schoolId,a.yearId AS yearId  "); 
		feeReportQuery.append("FROM fee_paidfeetable a  "); 
		feeReportQuery.append("LEFT JOIN fee_receipt b ON a.receiptID=b.receiptID   "); 
		feeReportQuery.append("LEFT JOIN fee_head_master p ON p.headID=a.headID "); 
		feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.studFeeID=a.studFeeID  "); 
		feeReportQuery.append("LEFT JOIN student_standard_renew d ON d.studentId=c.studRenewID  "); 
		feeReportQuery.append("LEFT JOIN yearmaster d1 ON d1.yearId=a.yearId   "); 
		feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=a.schoolid AND m.isDel='0'  "); 
		feeReportQuery.append("LEFT JOIN group_of_school gs ON gs.groupOfSchoolId=m.groupOfSchoolId  "); 
		feeReportQuery.append("WHERE  a.isDel='0'  AND b.typeOfReceipt='1' OR b.typeOfReceipt='2' AND m.sansthaKey='"+sansthaKey+"'   "); 
		if(groupId !=0) {
			feeReportQuery.append("and m.groupOfSchoolId='"+groupId+"' " ); 
		}
		else {
			feeReportQuery.append("and m.groupOfSchoolId is null "); 
		}
		feeReportQuery.append("GROUP BY c.studRenewID,a.headID,a.subheadID,m.schoolid,a.yearId)k  "); 
		feeReportQuery.append("GROUP BY schoolId3,yearId3)km ON km.schoolId3=schoolid AND km.yearId3=yearId "); 
		feeReportQuery.append(")ghd  "); 
		feeReportQuery.append("ORDER BY yearId,schoolid ASC    "); 
		Query query = entitymanager.createNativeQuery(feeReportQuery.toString());
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(FeeBean.class ) )
				 .getResultList();
		

	}

	public List<FeeBean> getPayTypeList(String sansthaKey, List<String> yearName, String fromDate, String toDate, Integer groupId) {
		StringBuilder feeReportQuery = new StringBuilder();
//		feeReportQuery.append("SELECT payTypeId, payTypeName, CAST(paidFee AS CHAR) AS paidFee, CAST(IFNULL(totalPaidFine,0) AS CHAR) AS paidFine " + 
//				"FROM (SELECT * FROM (SELECT year,payType as payTypeId,payTypeName,receiptID, SUM(IFNULL(totalPaidFee,0)) AS paidFee FROM ( " + 
//				"SELECT y1.year, i.payType,y.payTypeName,j.receiptID, IFNULL(i.payFee,0) - IFNULL(i.discount,0) AS totalPaidFee " + 
//				"FROM fee_studentfee n " + 
//				"LEFT JOIN fee_setting m on m.feeSettingID=n.feeSettingID " + 
//				"left join fee_paidfeetable i on i.studFeeID=n.studFeeID " + 
//				"left join fee_receipt j on j.receiptID=i.receiptID " + 
//				"left join fee_pay_type y on y.payTypeID=i.payType " + 
//				"left join school_master s on s.schoolid=m.schoolid " + 
//				"left join yearmaster y1 on y1.yearId=m.yearID " + 
//				"WHERE s.sansthaKey='"+sansthaKey+"' ");
//		feeReportQuery.append(" and y1.year in ( ");	
//		for(int i=0;i<yearName.size();i++) {
//			if(i!=0) {
//				feeReportQuery.append(",");
//			}
//			feeReportQuery.append("'"+yearName.get(i)+"' ");
//		}
//		feeReportQuery.append(") ");
//		if(groupId !=0) {
//			feeReportQuery.append("and s.groupOfSchoolId='"+groupId+"' "); 
//		}
//		else {
//			feeReportQuery.append("and s.groupOfSchoolId is null "); 
//		}
//		feeReportQuery.append(" AND STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y'))vg " + 
//				"group by payTypeName )gf " + 
		
		feeReportQuery.append("SELECT payTypeId, payTypeName, CAST(paidFee AS CHAR) AS paidFee, CAST(IFNULL(totalPaidFine,0) AS CHAR) AS paidFine   "); 
		feeReportQuery.append("FROM ( "); 
		feeReportQuery.append("SELECT *   "); 
		feeReportQuery.append("FROM (  "); 
		feeReportQuery.append("SELECT YEAR,groupOfSchoolId,payType AS payTypeId,payTypeName,receiptID, SUM(IFNULL(totalPaidFee,0))+ SUM(IFNULL(utilizeFee,0)) AS paidFee "); 
		feeReportQuery.append("FROM ( "); 
		feeReportQuery.append("SELECT y1.year AS YEAR,s.groupOfSchoolId, y.payTypeID AS payType,y.payTypeName,j.receiptID, IFNULL(i.payFee,0) - IFNULL(i.discount,0) AS totalPaidFee  "); 
		feeReportQuery.append("FROM fee_setting_multi g  "); 
		feeReportQuery.append("LEFT JOIN fee_setting m ON m.feeSettingID=g.feeSettingID AND m.isDel='0'  "); 
		feeReportQuery.append("LEFT JOIN fee_paidfeetable i ON i.feeSetMultiID=g.feeSetMultiID  "); 
		feeReportQuery.append("LEFT JOIN fee_receipt j ON j.receiptID=i.receiptID AND j.isDel='0' "); 
		feeReportQuery.append("LEFT JOIN school_master s ON s.schoolid=m.schoolid  "); 
		feeReportQuery.append("LEFT JOIN yearmaster y1 ON y1.yearId=m.yearID  "); 
		feeReportQuery.append("LEFT JOIN fee_pay_type y ON y.payTypeID=i.payType   "); 
		feeReportQuery.append("LEFT JOIN school_master s1 ON s1.schoolid=m.schoolid "); 
		feeReportQuery.append("WHERE s.sansthaKey='"+sansthaKey+"' AND (j.isDel='0' OR (j.isDel='1' AND j.isApproval='0'))  AND (STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') AND STR_TO_DATE('"+toDate+"','%d-%m-%Y')) AND (((j.payTypeOrPrintFlag IS NULL OR j.payTypeOrPrintFlag='2') AND j.returnFlag='1') OR (j.payTypeOrPrintFlag IS NULL OR j.payTypeOrPrintFlag='1')) ");
		if(groupId !=0) {
			feeReportQuery.append("and s1.groupOfSchoolId='"+groupId+"' "); 
		}
		else {
			feeReportQuery.append("and s1.groupOfSchoolId is null "); 
		}
		
		
		feeReportQuery.append(" and y1.year in ( ");	
		for(int i=0;i<yearName.size();i++) {
			if(i!=0) {
				feeReportQuery.append(",");
			}
			feeReportQuery.append("'"+yearName.get(i)+"' ");
		}
		feeReportQuery.append(") ");
		feeReportQuery.append("GROUP BY y.payTypeName "); 
		feeReportQuery.append(")vg "); 
		feeReportQuery.append("LEFT JOIN ( "); 
		feeReportQuery.append("SELECT y.payTypeName AS payTypeName1, IFNULL(g1.utilizeFee,0) AS utilizeFee   "); 
		feeReportQuery.append("FROM fee_setting_multi g  "); 
		feeReportQuery.append("LEFT JOIN fee_setting m ON m.feeSettingID=g.feeSettingID AND m.isDel='0'  "); 
		feeReportQuery.append("LEFT JOIN school_master s ON s.schoolid=m.schoolid  "); 
		feeReportQuery.append("LEFT JOIN yearmaster y1 ON y1.yearId=m.yearID "); 
		feeReportQuery.append("LEFT JOIN utilize_excess_fee g1 ON g.feeSetMultiID=g1.feeSetMultiID  "); 
		feeReportQuery.append("LEFT JOIN reciept_utilization_of_excces_fee g2 ON g2.voucherID=g1.receiptID  "); 
		feeReportQuery.append("LEFT JOIN fee_pay_type y ON y.payTypeID=g1.payType  "); 
		feeReportQuery.append("LEFT JOIN school_master s1 ON s1.schoolid=m.schoolid  "); 
		feeReportQuery.append("WHERE s.sansthaKey='"+sansthaKey+"'   AND (STR_TO_DATE(g1.uitilizefeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') AND STR_TO_DATE('"+toDate+"','%d-%m-%Y'))  "); 
		if(groupId !=0) {
			feeReportQuery.append("and s1.groupOfSchoolId='"+groupId+"' "); 
		}
		else {
			feeReportQuery.append("and s1.groupOfSchoolId is null "); 
		}
		
		
		feeReportQuery.append(" and y1.year in ( ");	
		for(int i=0;i<yearName.size();i++) {
			if(i!=0) {
				feeReportQuery.append(",");
			}
			feeReportQuery.append("'"+yearName.get(i)+"' ");
		}
		feeReportQuery.append(") ");
		feeReportQuery.append("GROUP BY y.payTypeName)k ON k.payTypeName1=vg.payTypeName  "); 
		feeReportQuery.append("GROUP BY payTypeName)gf "); 
		feeReportQuery.append("LEFT JOIN (Select SUM(totalPaidFine) as totalPaidFine, payType,payTypeName1 from ( " + 
		"SELECT y1.year as year1,i.payType,y.payTypeName as payTypeName1,  k.receiptID as receiptID1, " + 
		"(IFNULL(j.dueFee,0)) AS totalPaidFine " + 
		"FROM fee_studentfee n " + 
		"LEFT JOIN fee_setting m on m.feeSettingID=n.feeSettingID " + 
		"LEFT JOIN fee_paidfeetable i on i.studFeeID=n.studFeeID "+
		"LEFT JOIN fee_paid_duetable j ON i.receiptID = j.receiptID "+
		"left join fee_pay_type y on y.payTypeID=i.payType " + 
		"left join fee_receipt k on k.receiptID=i.receiptID " + 
		"left join school_master s on s.schoolid=m.schoolid " + 
		"LEFT JOIN yearmaster y1 ON y1.yearId=m.yearID "+
		" WHERE STR_TO_DATE(j.dueDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y') " +
		"AND s.sansthaKey='"+sansthaKey+"' and y1.year in ( ");
		
		for(int i=0;i<yearName.size();i++) {
			if(i!=0) {
				feeReportQuery.append(",");
			}
			feeReportQuery.append("'"+yearName.get(i)+"' ");
		}
		feeReportQuery.append(") ");
		if(groupId !=0) {
			feeReportQuery.append("and s.groupOfSchoolId='"+groupId+"' "); 
		}
		else {
			feeReportQuery.append("and s.groupOfSchoolId is null "); 
		}
		feeReportQuery.append("group by i.receiptID,j.feeSetMultiID)a group by payTypeName1 " + 
				")kj ON kj.payTypeName1=payTypeName)ghd ORDER BY payTypeId ASC ");
		Query query = entitymanager.createNativeQuery(feeReportQuery.toString());
	
	return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(FeeBean.class ) )
				 .getResultList();
		

	}

	public List<FeeBean> getYearList(String sansthaKey, List<String> yearName, String fromDate, String toDate, Integer groupId) {
		StringBuilder feeReportQuery = new StringBuilder();
//	 feeReportQuery.append("SELECT  yearId,yearName,CAST(paidFee AS CHAR) AS paidFee, CAST(IFNULL(totalPaidFine,0) AS CHAR) AS paidFine " + 
//				"FROM (SELECT * FROM ( SELECT year,yearId,yearName,receiptID,SUM(IFNULL(totalPaidFee,0)) AS paidFee FROM ( "
//				+ "SELECT y.year, m.yearId,y.year as yearName,j.receiptID, IFNULL(i.payFee,0) - IFNULL(i.discount,0) AS totalPaidFee FROM fee_studentfee n "
//				+ "LEFT JOIN fee_setting m on m.feeSettingID=n.feeSettingID left join fee_paidfeetable i on i.studFeeID=n.studFeeID "
//				+ "left join fee_receipt j on j.receiptID=i.receiptID left join yearmaster y on y.yearId=m.yearID "
//				+ "left join school_master s on s.schoolId=m.schoolid  "
//				+ "WHERE s.sansthaKey='"+sansthaKey+"' ");
//		if(groupId !=0) {
//			feeReportQuery.append("and s.groupOfSchoolId='"+groupId+"' "); 
//		}
//		else {
//			feeReportQuery.append("and s.groupOfSchoolId is null "); 
//		}
//		feeReportQuery.append("AND STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y') )vg "
		feeReportQuery.append("SELECT yearId,yearName, CAST(paidFee AS CHAR) AS paidFee, CAST(IFNULL(totalPaidFine,0) AS CHAR) AS paidFine "); 
		feeReportQuery.append("FROM ( "); 
		feeReportQuery.append("SELECT * "); 
		feeReportQuery.append("FROM (  "); 
		feeReportQuery.append("SELECT YEAR AS yearName,receiptID, yearId,SUM(IFNULL(totalPaidFee,0))+ SUM(IFNULL(utilizeFee,0)) AS paidFee  "); 
		feeReportQuery.append("FROM ( "); 
		feeReportQuery.append("SELECT y1.year AS YEAR,s.groupOfSchoolId, y1.yearId, y.payTypeID AS payType,y.payTypeName,j.receiptID, IFNULL(i.payFee,0) - IFNULL(i.discount,0) AS totalPaidFee  "); 
		feeReportQuery.append("FROM fee_setting_multi g "); 
		feeReportQuery.append("LEFT JOIN fee_setting m ON m.feeSettingID=g.feeSettingID AND m.isDel='0'  "); 
		feeReportQuery.append("LEFT JOIN fee_paidfeetable i ON i.feeSetMultiID=g.feeSetMultiID  "); 
		feeReportQuery.append("LEFT JOIN fee_receipt j ON j.receiptID=i.receiptID AND j.isDel='0' "); 
		feeReportQuery.append("LEFT JOIN school_master s ON s.schoolid=m.schoolid  "); 
		feeReportQuery.append("LEFT JOIN yearmaster y1 ON y1.yearId=m.yearID  "); 
		feeReportQuery.append("LEFT JOIN fee_pay_type y ON y.payTypeID=i.payType "); 
		feeReportQuery.append("WHERE s.sansthaKey='"+sansthaKey+"' AND (j.isDel='0' OR (j.isDel='1' AND j.isApproval='0')) AND (STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') AND STR_TO_DATE('"+toDate+"','%d-%m-%Y')) AND (((j.payTypeOrPrintFlag IS NULL OR j.payTypeOrPrintFlag='2') AND j.returnFlag='1') OR (j.payTypeOrPrintFlag IS NULL OR j.payTypeOrPrintFlag='1')) "); 
		if(groupId !=0) {
			feeReportQuery.append("and s.groupOfSchoolId='"+groupId+"' "); 
		}
		else {
			feeReportQuery.append("and s.groupOfSchoolId is null "); 
		}
		feeReportQuery.append("GROUP BY y1.year  "); 
		feeReportQuery.append(")vg "); 
		feeReportQuery.append("LEFT JOIN ( "); 
		feeReportQuery.append("SELECT y.payTypeName AS payTypeName1, IFNULL(g1.utilizeFee,0) AS utilizeFee  "); 
		feeReportQuery.append("FROM fee_setting_multi g "); 
		feeReportQuery.append("LEFT JOIN fee_setting m ON m.feeSettingID=g.feeSettingID AND m.isDel='0' "); 
		feeReportQuery.append("LEFT JOIN school_master s ON s.schoolid=m.schoolid  "); 
		feeReportQuery.append("LEFT JOIN yearmaster y1 ON y1.yearId=m.yearID "); 
		feeReportQuery.append("LEFT JOIN utilize_excess_fee g1 ON g.feeSetMultiID=g1.feeSetMultiID "); 
		feeReportQuery.append("LEFT JOIN reciept_utilization_of_excces_fee g2 ON g2.voucherID=g1.receiptID "); 
		feeReportQuery.append("LEFT JOIN fee_pay_type y ON y.payTypeID=g1.payType "); 
		feeReportQuery.append("WHERE s.sansthaKey='"+sansthaKey+"'   AND (STR_TO_DATE(g1.uitilizefeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') AND STR_TO_DATE('"+toDate+"','%d-%m-%Y'))  "); 
		if(groupId !=0) {
			feeReportQuery.append("and s.groupOfSchoolId='"+groupId+"' "); 
		}
		else {
			feeReportQuery.append("and s.groupOfSchoolId is null "); 
		}
		feeReportQuery.append("GROUP BY y1.year)k ON k.payTypeName1=vg.payTypeName  "); 
		feeReportQuery.append("GROUP BY yearName)gf "); 
		feeReportQuery.append("LEFT JOIN (SELECT  y.year as year1,m.yearId as yearId1,k.receiptID as receiptID1, SUM(IFNULL(j.dueFee,0)) AS totalPaidFine "
		+ "FROM fee_studentfee n LEFT JOIN fee_setting m on m.feeSettingID=n.feeSettingID "
		+ "left join fee_paid_duetable j on j.studFeeID=n.studFeeID left join fee_receipt k on k.receiptID=j.receiptID "
		+ "left join yearmaster y on y.yearId=m.yearID "
		+ "left join school_master s on s.schoolId=m.schoolid  "
		+ "WHERE STR_TO_DATE(j.dueDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y') " 
		+ "AND s.sansthaKey='"+sansthaKey+"' ");
		if(groupId !=0) {
			feeReportQuery.append("and s.groupOfSchoolId='"+groupId+"' "); 
		}
		else {
			feeReportQuery.append("and s.groupOfSchoolId is null "); 
		}
		feeReportQuery.append("group by year1 )kj ON kj.yearId1=yearId  )ghd ORDER BY yearId1 ASC ");
		Query query = entitymanager.createNativeQuery(feeReportQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(FeeBean.class ) )
				 .getResultList();
		

	}
	
	public List<FeeBean> getChequeBounceChargesSchoolWise(Integer schoolId, Integer yearId, String fromDate, String toDate,Integer groupId) {
		StringBuilder feeReportQuery = new StringBuilder();
		 feeReportQuery.append("select cast(IFNULL(sum(a.bounceCharges),0) as char) as assignedChequeBounceCharges,"
				+ "cast(IFNULL(sum(a.bouncePaidCharges),0) as char) as paidChequeBounceCharges  "
				+ "from fee_bouncecharges a " + 
				"left join fee_receipt b on a.receiptID=b.receiptID " + 
				"left join fee_paidfeetable c on c.receiptID=b.receiptID " + 
				"left join fee_studentfee d on d.studFeeID=c.studFeeID " + 
				"left join fee_setting e on e.feeSettingID=d.feeSettingID " + 
				"left join school_master s on s.groupOfSchoolId=a.schoolId " +
				"where e.schoolID='"+schoolId+"' and e.yearID='"+yearId+"' and s.groupOfSchoolId='"+groupId+"'  ");
		if(StringUtils.isBlank(toDate)) {
			feeReportQuery.append("and STR_TO_DATE(c.paidfeeDate,'%d-%m-%Y') <= STR_TO_DATE('"+fromDate+"','%d-%m-%Y') ");
		}
		else {
			feeReportQuery.append("and STR_TO_DATE(c.paidfeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y') ");
		}
		Query query = entitymanager.createNativeQuery(feeReportQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(FeeBean.class ) )
				 .getResultList();
		

	}
	
	public List<FeeBean> getChequeBounceChargesGroupWise(Integer groupId, List<String> yearName, String fromDate, String toDate) {
		StringBuilder feeReportQuery = new StringBuilder();
		 feeReportQuery.append("select cast(IFNULL(sum(a.bounceCharges),0) as char) as assignedChequeBounceCharges,"
				+ "cast(IFNULL(sum(a.bouncePaidCharges),0) as char) as paidChequeBounceCharges  "
				+ "from fee_bouncecharges a " + 
				"left join fee_receipt b on a.receiptID=b.receiptID " + 
				"left join fee_paidfeetable c on c.receiptID=b.receiptID " + 
				"left join fee_studentfee d on d.studFeeID=c.studFeeID " + 
				"left join fee_setting e on e.feeSettingID=d.feeSettingID " + 
				"left join school_master s on s.groupOfSchoolId=a.schoolId " +
				"left join yearmaster y on  y.yearId=e.yearId  "+
				"where s.groupOfSchoolId='"+groupId+"' and y.year in ('"+yearName+"'  ");
		for(int i=0;i<yearName.size();i++) {
			if(i!=0) {
				feeReportQuery.append(",");
			}
			feeReportQuery.append("'"+yearName.get(i)+"' ");
		}
		feeReportQuery.append(")");
		if(StringUtils.isBlank(toDate)) {
			feeReportQuery.append(" and STR_TO_DATE(c.paidfeeDate,'%d-%m-%Y') <= STR_TO_DATE('"+fromDate+"','%d-%m-%Y') ");
		}
		else {
			feeReportQuery.append("and STR_TO_DATE(c.paidfeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y') ");
		}
		Query query = entitymanager.createNativeQuery(feeReportQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(FeeBean.class ) )
				 .getResultList();
		

	}

	public List<FeeBean> getTotalFeeGroupWise(String sansthaKey, String fromDate, String toDate,List<String> yearName) {
		StringBuilder feeReportQuery = new StringBuilder();

//				feeReportQuery.append("SELECT yearId,yearName, CAST(IFNULL(groupId,'0') AS CHAR) AS groupId, IFNULL(groupName,'Other') AS groupName, CAST(assignedFee AS CHAR) AS assignedFee, CAST(assignedFine AS CHAR) AS assignedFine, CAST(clearanceFee AS CHAR) AS clearanceFee, CAST(IFNULL(fineClearance,0) AS CHAR) AS clearanceFine, CAST(paidFee AS CHAR) AS paidFee, CAST(discount AS CHAR) AS discount, CAST(IFNULL(totalPaidFine,0) AS CHAR) AS paidFine, CAST((assignedFee - (paidFee + discount) - clearanceFee) AS CHAR) AS remainingFee, CAST(IFNULL((assignedFine- totalPaidFine - fineClearance),0) AS CHAR) AS remainingFine  "); 
//				feeReportQuery.append("FROM ( "); 
//				feeReportQuery.append("SELECT *  "); 
//				feeReportQuery.append("FROM (  "); 
//				feeReportQuery.append("SELECT feeSetMultiID,yearId,yearName,schoolid,schoolName,longSchoolName,clearanceFee,ifnull(totalAssignFee,0) AS assignedFee ,headId,subheadId, "); 
//				feeReportQuery.append("(ifnull(totalPaidFee,0)+ifnull(utilizeFee,0))-IFNULL(discount,0) AS paidFee,ifnull(remianFee,0) AS remianFee,IFNULL(assignedFine,0) AS assignedFine,IFNULL(discount,0) AS discount,groupId,groupName "); 
//				feeReportQuery.append("FROM  "); 
//				feeReportQuery.append("(SELECT feeSetMultiID,yearId,yearName,schoolid,schoolName,longSchoolName,clearanceFee,ifnull(sum(totalAssignFee),0) AS totalAssignFee ,headId,subheadId, "); 
//				feeReportQuery.append("ifnull(sum(totalPaidFee),0) AS totalPaidFee,ifnull(sum(utilizeFee),0) AS utilizeFee,IFNULL(sum(discount),0) AS discount,if((ifnull(sum(totalPaidFee),0)+ifnull(sum(utilizeFee),0))-IFNULL(sum(discount),0)>ifnull(SUM(totalAssignFee),0),0,ifnull(sum(totalAssignFee),0)-(ifnull(sum(totalPaidFee),0)+ifnull(sum(utilizeFee),0))-IFNULL(sum(discount),0)) AS remianFee,SUM(IFNULL(fine,0)) AS assignedFine,groupId,groupName "); 
//				feeReportQuery.append("FROM (SELECT g.feeSetMultiID,e.yearId,e.year AS yearName,m.schoolid,m.shortSchoolName AS schoolName,m.schoolName AS longSchoolName, "); 
//				feeReportQuery.append("SUM(IFNULL(k.clearanceFee,0)) AS clearanceFee, ifnull(sum(g.assignFee),0) AS totalAssignFee,g.headId,g.subheadId,gs.groupOfSchoolId AS groupId,gs.groupOfSchoolName AS groupName  "); 
//				feeReportQuery.append("FROM fee_setting_multi g "); 
//				feeReportQuery.append("LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID AND d.isDel='0' "); 
//				feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID AND c.isDel='0'  "); 
//				feeReportQuery.append("LEFT JOIN yearmaster e ON e.yearId=d.yearID AND e.isDel='0' "); 
//				feeReportQuery.append("LEFT JOIN fee_clearancetable k ON k.feeSetMultiID=g.feeSetMultiID AND k.isDel='0'  "); 
//				feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=g.schoolid "); 
//				feeReportQuery.append("LEFT JOIN fee_subhead_master n ON n.subheadID=g.subheadID  "); 
//				feeReportQuery.append("LEFT JOIN group_of_school gs ON gs.groupOfSchoolId=m.groupOfSchoolId "); 
//				feeReportQuery.append("LEFT JOIN student_master r ON r.stud_id=c.studRenewID "); 
//				feeReportQuery.append("LEFT JOIN student_standard_renew ssr ON ssr.studentId=r.stud_id "); 
//				feeReportQuery.append("WHERE g.isConsolation=0 AND m.sansthaKey='"+sansthaKey+"' AND g.isDel='0'  AND n.isScholarshipFlag='0' AND r.isDel='0' AND r.isAdmissionCancelled='1' AND ssr.alreadyRenew=1 AND (ssr.college_leavingdate IS NULL OR ssr.college_leavingdate='')  ");
//				feeReportQuery.append("AND e.YEAR IN (");
//				for(int i=0;i<yearName.size();i++) {
//					if(i!=0) {
//						feeReportQuery.append(",");
//					}
//					feeReportQuery.append(" '"+yearName.get(i)+"' ");
//				}
//				feeReportQuery.append(")GROUP BY gs.groupOfSchoolId,c.studRenewID,d.yearId,g.schoolid,d.standardId,g.feeSetMultiID)vg  "); 
//				feeReportQuery.append("LEFT JOIN  "); 
//				feeReportQuery.append("(SELECT g.feeSetMultiID AS feeSetMultiID1,d.yearId AS yearId1,l.standardId AS standardId1,i.feesClearance AS feesClearance, i.fineClearance AS fineClearance, g.headID AS headId1, g.subheadID AS subheadId1, IFNULL((CASE WHEN (IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE()) THEN IFNULL(g.fine* DATEDIFF(CURRENT_DATE(), STR_TO_DATE(g.dueDate,'%d-%m-%Y')),0) WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE() THEN IFNULL(g.fine,0) WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE() THEN IFNULL(g.fine,0) WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') THEN IFNULL(g.fine* DATEDIFF(STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y'), STR_TO_DATE(g.dueDate,'%d-%m-%Y')),0) WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') THEN 0 END),0) AS fine, IFNULL(sum(i.payFee),0) AS totalPaidFee, IFNULL(sum(i.discount),0) AS discount "); 
//				feeReportQuery.append("FROM fee_setting_multi g  "); 
//				feeReportQuery.append("LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID  "); 
//				feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID  "); 
//				feeReportQuery.append("LEFT JOIN fee_head_master f ON f.headID=g.headID  "); 
//				feeReportQuery.append("LEFT JOIN fee_subhead_master h ON h.subheadID=g.subheadID "); 
//				feeReportQuery.append("LEFT JOIN yearmaster e ON e.yearId=d.yearID  "); 
//				feeReportQuery.append("LEFT JOIN fee_paidfeetable i ON i.feeSetMultiID=g.feeSetMultiID ");
//				if(StringUtils.isBlank(toDate)) {
//					feeReportQuery.append("and STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') <= STR_TO_DATE('"+fromDate+"','%d-%m-%Y') ");
//				}
//				else {
//					feeReportQuery.append("and STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y') ");
//				}; 
//				feeReportQuery.append("LEFT JOIN fee_receipt i1 ON i1.receiptID=i.receiptID "); 
//				feeReportQuery.append("LEFT JOIN student_master r ON r.stud_id=c.studRenewID  "); 
//				feeReportQuery.append("LEFT JOIN student_standard_renew ssr ON ssr.studentId=r.stud_id "); 
//				feeReportQuery.append("LEFT JOIN standardmaster l ON l.standardId=d.standardID "); 
//				feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=g.schoolid "); 
//				feeReportQuery.append("LEFT JOIN group_of_school gs ON gs.groupOfSchoolId=m.groupOfSchoolId "); 
//				feeReportQuery.append("WHERE g.isConsolation=0  AND m.sansthaKey='"+sansthaKey+"' AND g.isDel='0'  AND (i1.isDel='0' OR (i1.isDel='1' AND i1.isApproval='0')) AND r.isDel='0' AND r.isAdmissionCancelled='1' AND ssr.alreadyRenew=1 AND (ssr.college_leavingdate IS NULL OR ssr.college_leavingdate='') AND (ssr.isOnlineAdmission='0' OR (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) AND h.isScholarshipFlag='0' AND (((i1.payTypeOrPrintFlag IS NULL OR i1.payTypeOrPrintFlag='2') AND i1.returnFlag='1') OR (i1.payTypeOrPrintFlag IS NULL OR i1.payTypeOrPrintFlag='1'))  ");
//				feeReportQuery.append("AND e.YEAR IN (");
//				for(int i=0;i<yearName.size();i++) {
//					if(i!=0) {
//						feeReportQuery.append(",");
//					}
//					feeReportQuery.append(" '"+yearName.get(i)+"' ");
//				}
//				feeReportQuery.append(")GROUP BY gs.groupOfSchoolId,c.studRenewID,d.yearId,g.schoolid,d.standardId,g.feeSetMultiID)vh ON  vg.feeSetMultiID=vh.feeSetMultiID1  "); 
//				feeReportQuery.append("LEFT JOIN "); 
//				feeReportQuery.append("(SELECT g.feeSetMultiID AS feeSetMultiID2,d.yearId AS yearId2, g.headID AS headID2, g.subheadID AS subheadID2, IFNULL(i.utilizeFee,0) AS utilizeFee  "); 
//				feeReportQuery.append("FROM fee_setting_multi g "); 
//				feeReportQuery.append("LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID  "); 
//				feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID  "); 
//				feeReportQuery.append("LEFT JOIN fee_head_master f ON f.headID=g.headID  "); 
//				feeReportQuery.append("LEFT JOIN fee_subhead_master h ON h.subheadID=g.subheadID  "); 
//				feeReportQuery.append("LEFT JOIN yearmaster e ON e.yearId=d.yearID  "); 
//				feeReportQuery.append("LEFT JOIN utilize_excess_fee i ON i.feeSetMultiID=g.feeSetMultiID AND STR_TO_DATE(i.uitilizefeeDate,'%d-%m-%Y') <= STR_TO_DATE('"+fromDate+"','%d-%m-%Y') "); 
//				feeReportQuery.append("LEFT JOIN reciept_utilization_of_excces_fee i1 ON i1.voucherID=i.receiptID "); 
//				feeReportQuery.append("LEFT JOIN student_master r ON r.stud_id=c.studRenewID "); 
//				feeReportQuery.append("LEFT JOIN student_standard_renew ssr ON ssr.studentId=r.stud_id  "); 
//				feeReportQuery.append("LEFT JOIN standardmaster l ON l.standardId=d.standardID  "); 
//				feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=g.schoolid "); 
//				feeReportQuery.append("LEFT JOIN group_of_school gs ON gs.groupOfSchoolId=m.groupOfSchoolId  "); 
//				feeReportQuery.append("WHERE g.isConsolation=0  AND m.sansthaKey='"+sansthaKey+"' AND g.isDel='0'   AND r.isDel='0' AND r.isAdmissionCancelled='1' AND ssr.alreadyRenew=1 AND (ssr.college_leavingdate IS NULL OR ssr.college_leavingdate='') AND (ssr.isOnlineAdmission='0' OR (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) AND h.isScholarshipFlag='0' ");
//				feeReportQuery.append("AND e.YEAR IN (");
//				for(int i=0;i<yearName.size();i++) {
//					if(i!=0) {
//						feeReportQuery.append(",");
//					}
//					feeReportQuery.append(" '"+yearName.get(i)+"' ");
//				}
//				feeReportQuery.append(")GROUP BY gs.groupOfSchoolId,c.studRenewID,d.yearId,g.schoolid,d.standardId,g.headId,g.subheadId,g.feeSetMultiID)kh ON vg.headId=kh.headID2 AND vg.subheadId=kh.subheadID2 AND vg.feeSetMultiId=kh.feeSetMultiID2  "); 
//				feeReportQuery.append("GROUP BY groupId)yu  "); 
//				feeReportQuery.append("GROUP BY groupId)gf	"); 
//				feeReportQuery.append("LEFT JOIN (select groupId1,yearId1,schoolId1,standardID1,headId1,subheadId1, " 
//				+ "feeSetMultiID1,SUM(totalPaidFine) as totalPaidFine,SUM(fineClearance) as fineClearance "  
//				+ "from( SELECT e.year as yearId1,m.groupOfSchoolId AS groupId1,"
//				+ "g.schoolId as schoolId1,g.headId as headId1,g.subheadId as subheadID1,g.feeSetMultiID as feeSetMultiID1,d.standardId as standardId1, "
//				+ "SUM(IFNULL(j.dueFee,0)) AS totalPaidFine, "
//				+ "IFNULL(k.dueFee,0) AS fineClearance FROM fee_setting_multi g LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID and d.isDel='0' "
//				+ "LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID  and c.isDel='0' "
//				+ "LEFT JOIN yearmaster e ON e.yearId=d.yearID and e.isDel='0' "
//				+ "LEFT JOIN fee_paid_duetable j ON j.feeSetMultiID=g.feeSetMultiID and j.isDel='0' ");
//				if(StringUtils.isBlank(toDate)) {
//					feeReportQuery.append("and STR_TO_DATE(j.dueDate,'%d-%m-%Y') <= STR_TO_DATE('"+fromDate+"','%d-%m-%Y') ");
//				}
//				else {
//					feeReportQuery.append("and STR_TO_DATE(j.dueDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y') ");
//				}
//			feeReportQuery.append("LEFT JOIN fee_duetable k ON k.feeSetMultiID=g.feeSetMultiID and k.isDel='0' "
//				+ "LEFT JOIN school_master m on m.schoolid=g.schoolid  and m.isDel='0' "
//				+ "LEFT JOIN group_of_school gs on gs.groupOfSchoolId=m.groupOfSchoolId "
//				+ "WHERE g.isConsolation=0 and m.sansthaKey='"+sansthaKey+"' and g.isDel='0' AND e.YEAR IN ( " );
//				
//				for(int i=0;i<yearName.size();i++) {
//					if(i!=0) {
//						feeReportQuery.append(",");
//					}
//					feeReportQuery.append(" '"+yearName.get(i)+"' ");
//				}
//			feeReportQuery.append(" ) GROUP BY yearId1,schoolid1,standardId1,headId1,subheadId1,feeSetMultiID1)a1 GROUP BY  groupId1)kj "
//				+ "ON  kj.groupId1=groupId)ghd "
//				+ "ORDER BY groupId ASC ");
		
		feeReportQuery.append("SELECT yearId,yearName, CAST(IFNULL(groupId,'0') AS CHAR) AS groupId, IFNULL(groupName,'Other') AS groupName, CAST(assignedFee AS CHAR) AS assignedFee, CAST(assignedFine AS CHAR) AS assignedFine, CAST(clearanceFee AS CHAR) AS clearanceFee, CAST(IFNULL(fineClearance,0) AS CHAR) AS clearanceFine, CAST(paidFee AS CHAR) AS paidFee, CAST(discount AS CHAR) AS discount, CAST(IFNULL(totalPaidFine,0)+ifnull(totlPaidFine1,0) AS CHAR) AS paidFine, CAST((assignedFee - (paidFee + discount) - clearanceFee) AS CHAR) AS remainingFee, CAST(IFNULL((assignedFine- ifnull(totalPaidFine,0) - fineClearance),0) AS CHAR) AS remainingFine,cast(ifnull(otherExcessFee,0) AS CHAR) AS excessFee,cast(ifnull(otherFee,0) as char) AS otherFee  ");
		feeReportQuery.append("FROM ( ");
		feeReportQuery.append("SELECT * ");
		feeReportQuery.append("FROM (  ");
		feeReportQuery.append("SELECT feeSetMultiID,yearId,yearName,schoolid,schoolName,longSchoolName,clearanceFee, IFNULL(totalAssignFee,0) AS assignedFee,headId,subheadId, (IFNULL(totalPaidFee,0)+ IFNULL(utilizeFee,0))- IFNULL(discount,0) AS paidFee, IFNULL(remianFee,0) AS remianFee, IFNULL(assignedFine,0) AS assignedFine, IFNULL(discount,0) AS discount,groupId,groupName  ");
		feeReportQuery.append("FROM ( ");
		feeReportQuery.append("SELECT feeSetMultiID,yearId,yearName,schoolid,schoolName,longSchoolName,clearanceFee, IFNULL(SUM(totalAssignFee),0) AS totalAssignFee,headId,subheadId, IFNULL(SUM(totalPaidFee),0) AS totalPaidFee, IFNULL(SUM(utilizeFee),0) AS utilizeFee, IFNULL(SUM(discount),0) AS discount,if((IFNULL(SUM(totalPaidFee),0)+ IFNULL(SUM(utilizeFee),0))- IFNULL(SUM(discount),0)> IFNULL(SUM(totalAssignFee),0),0, IFNULL(SUM(totalAssignFee),0)-(IFNULL(SUM(totalPaidFee),0)+ IFNULL(SUM(utilizeFee),0))- IFNULL(SUM(discount),0)) AS remianFee, SUM(IFNULL(fine,0)) AS assignedFine,groupId,groupName  ");
		feeReportQuery.append("FROM ( ");
		feeReportQuery.append("SELECT g.feeSetMultiID,e.yearId,e.year AS yearName,m.schoolid,m.shortSchoolName AS schoolName,m.schoolName AS longSchoolName, SUM(IFNULL(k.clearanceFee,0)) AS clearanceFee, IFNULL(SUM(g.assignFee),0) AS totalAssignFee,g.headId,g.subheadId,gs.groupOfSchoolId AS groupId,gs.groupOfSchoolName AS groupName ");
		feeReportQuery.append("FROM fee_setting_multi g ");
		feeReportQuery.append("LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID AND d.isDel='0'  ");
		feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID AND c.isDel='0'  ");
		feeReportQuery.append("LEFT JOIN yearmaster e ON e.yearId=d.yearID AND e.isDel='0' ");
		feeReportQuery.append("LEFT JOIN fee_clearancetable k ON k.feeSetMultiID=g.feeSetMultiID AND k.isDel='0'  ");
		feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=g.schoolid ");
		feeReportQuery.append("LEFT JOIN fee_subhead_master n ON n.subheadID=g.subheadID  ");
		feeReportQuery.append("LEFT JOIN group_of_school gs ON gs.groupOfSchoolId=m.groupOfSchoolId ");
		feeReportQuery.append("LEFT JOIN student_master r ON r.stud_id=c.studRenewID ");
		feeReportQuery.append("LEFT JOIN student_standard_renew ssr ON ssr.studentId=r.stud_id ");
		feeReportQuery.append("WHERE g.isConsolation=0 AND m.sansthaKey='"+sansthaKey+"' AND g.isDel='0' AND n.isScholarshipFlag='0' AND r.isDel='0' AND r.isAdmissionCancelled='1' AND ssr.alreadyRenew=1 AND (ssr.college_leavingdate IS NULL OR ssr.college_leavingdate='') AND e.YEAR IN (   ");
		for(int i=0;i<yearName.size();i++) {
			if(i!=0) {
				feeReportQuery.append(",");
			}
			feeReportQuery.append(" '"+yearName.get(i)+"' ");
		}
		feeReportQuery.append(" ) GROUP BY gs.groupOfSchoolId,c.studRenewID,d.yearId,g.schoolid,d.standardId,g.feeSetMultiID)vg  ");
		feeReportQuery.append("LEFT JOIN (  ");
		feeReportQuery.append("SELECT g.feeSetMultiID AS feeSetMultiID1,d.yearId AS yearId1,l.standardId AS standardId1,i.feesClearance AS feesClearance, i.fineClearance AS fineClearance, g.headID AS headId1, g.subheadID AS subheadId1, IFNULL((CASE WHEN (IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE()) THEN IFNULL(g.fine* DATEDIFF(CURRENT_DATE(), STR_TO_DATE(g.dueDate,'%d-%m-%Y')),0) WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE() THEN IFNULL(g.fine,0) WHEN IFNULL(i.feesClearance,0)=0 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= CURRENT_DATE() THEN 0 WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=0 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< CURRENT_DATE() THEN IFNULL(g.fine,0) WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')< STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') THEN IFNULL(g.fine* DATEDIFF(STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y'), STR_TO_DATE(g.dueDate,'%d-%m-%Y')),0) WHEN IFNULL(i.feesClearance,0)=1 AND g.fineIncreament=1 AND STR_TO_DATE(g.dueDate,'%d-%m-%Y')>= STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') THEN 0 END),0) AS fine, IFNULL(SUM(i.payFee),0) AS totalPaidFee, IFNULL(SUM(i.discount),0)+IFNULL(d.preDiscount,0) AS discount ");
		feeReportQuery.append("FROM fee_setting_multi g  ");
		feeReportQuery.append("LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID  ");
		feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID ");
		feeReportQuery.append("LEFT JOIN fee_head_master f ON f.headID=g.headID ");
		feeReportQuery.append("LEFT JOIN fee_subhead_master h ON h.subheadID=g.subheadID  ");
		feeReportQuery.append("LEFT JOIN yearmaster e ON e.yearId=d.yearID ");
		feeReportQuery.append("LEFT JOIN fee_paidfeetable i ON i.feeSetMultiID=g.feeSetMultiID  ");
		if(StringUtils.isBlank(toDate)) {
			feeReportQuery.append("and STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') <= STR_TO_DATE('"+fromDate+"','%d-%m-%Y') ");
		}
		else {
			feeReportQuery.append("and STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y') ");
		} 
		feeReportQuery.append("LEFT JOIN fee_receipt i1 ON i1.receiptID=i.receiptID ");
		feeReportQuery.append("LEFT JOIN student_master r ON r.stud_id=c.studRenewID  ");
		feeReportQuery.append("LEFT JOIN student_standard_renew ssr ON ssr.studentId=r.stud_id ");
		feeReportQuery.append("LEFT JOIN standardmaster l ON l.standardId=d.standardID  ");
		feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=g.schoolid ");
		feeReportQuery.append("LEFT JOIN group_of_school gs ON gs.groupOfSchoolId=m.groupOfSchoolId  ");
		feeReportQuery.append("WHERE g.isConsolation=0 AND m.sansthaKey='"+sansthaKey+"' AND f.isLateFeeCharges!='1' AND g.isDel='0' AND (i1.isDel='0' OR (i1.isDel='1' AND i1.isApproval='0')) AND r.isDel='0' AND r.isAdmissionCancelled='1' AND ssr.alreadyRenew=1 AND (ssr.college_leavingdate IS NULL OR ssr.college_leavingdate='') AND (ssr.isOnlineAdmission='0' OR (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) AND h.isScholarshipFlag='0' AND (((i1.payTypeOrPrintFlag IS NULL OR i1.payTypeOrPrintFlag='2') AND i1.returnFlag='1') OR (i1.payTypeOrPrintFlag IS NULL OR i1.payTypeOrPrintFlag='1')) AND e.YEAR IN ( ");
		for(int i=0;i<yearName.size();i++) {
			if(i!=0) {
				feeReportQuery.append(",");
			}
			feeReportQuery.append(" '"+yearName.get(i)+"' ");
		}
		feeReportQuery.append(" ) GROUP BY gs.groupOfSchoolId,c.studRenewID,d.yearId,g.schoolid,d.standardId,g.feeSetMultiID)vh ON vg.feeSetMultiID=vh.feeSetMultiID1  ");
		feeReportQuery.append("LEFT JOIN ( ");
		feeReportQuery.append("SELECT g.feeSetMultiID AS feeSetMultiID2,d.yearId AS yearId2, g.headID AS headID2, g.subheadID AS subheadID2, IFNULL(i.utilizeFee,0) AS utilizeFee  ");
		feeReportQuery.append("FROM fee_setting_multi g  ");
		feeReportQuery.append("LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID ");
		feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID  ");
		feeReportQuery.append("LEFT JOIN fee_head_master f ON f.headID=g.headID  ");
		feeReportQuery.append("LEFT JOIN fee_subhead_master h ON h.subheadID=g.subheadID  ");
		feeReportQuery.append("LEFT JOIN yearmaster e ON e.yearId=d.yearID  ");
		feeReportQuery.append("LEFT JOIN utilize_excess_fee i ON i.feeSetMultiID=g.feeSetMultiID AND STR_TO_DATE(i.uitilizefeeDate,'%d-%m-%Y') <= STR_TO_DATE('16-05-2022','%d-%m-%Y')   ");
		feeReportQuery.append("LEFT JOIN reciept_utilization_of_excces_fee i1 ON i1.voucherID=i.receiptID  ");
		feeReportQuery.append("LEFT JOIN student_master r ON r.stud_id=c.studRenewID  ");
		feeReportQuery.append("LEFT JOIN student_standard_renew ssr ON ssr.studentId=r.stud_id ");
		feeReportQuery.append("LEFT JOIN standardmaster l ON l.standardId=d.standardID ");
		feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=g.schoolid ");
		feeReportQuery.append("LEFT JOIN group_of_school gs ON gs.groupOfSchoolId=m.groupOfSchoolId ");
		feeReportQuery.append("WHERE g.isConsolation=0 AND m.sansthaKey='"+sansthaKey+"' AND g.isDel='0' AND r.isDel='0' AND r.isAdmissionCancelled='1' AND ssr.alreadyRenew=1 AND (ssr.college_leavingdate IS NULL OR ssr.college_leavingdate='') AND (ssr.isOnlineAdmission='0' OR (ssr.isOnlineAdmission='1' AND ssr.isApproval='1') OR (ssr.isRegistration='1' AND ssr.isRegistrationApprovedFlag='1')) AND h.isScholarshipFlag='0' AND e.YEAR IN ( ");
		for(int i=0;i<yearName.size();i++) {
			if(i!=0) {
				feeReportQuery.append(",");
			}
			feeReportQuery.append(" '"+yearName.get(i)+"' ");
		}
		feeReportQuery.append(" ) GROUP BY gs.groupOfSchoolId,c.studRenewID,d.yearId,g.schoolid,d.standardId,g.headId,g.subheadId,g.feeSetMultiID)kh ON vg.headId=kh.headID2 AND vg.subheadId=kh.subheadID2 AND vg.feeSetMultiId=kh.feeSetMultiID2  ");
		feeReportQuery.append("GROUP BY groupId)yu ");
		feeReportQuery.append("GROUP BY groupId)gf  ");
		feeReportQuery.append("LEFT JOIN ( ");
		feeReportQuery.append("SELECT groupId1,yearId1,schoolId1,standardID1,headId1,subheadId1, feeSetMultiID1, SUM(totalPaidFine) AS totalPaidFine, SUM(fineClearance) AS fineClearance  ");
		feeReportQuery.append("FROM( ");
		feeReportQuery.append("SELECT e.year AS yearId1,m.groupOfSchoolId AS groupId1,g.schoolId AS schoolId1,g.headId AS headId1,g.subheadId AS subheadID1,g.feeSetMultiID AS feeSetMultiID1,d.standardId AS standardId1, SUM(IFNULL(j.dueFee,0)) AS totalPaidFine, IFNULL(k.dueFee,0) AS fineClearance  ");
		feeReportQuery.append("FROM fee_setting_multi g  ");
		feeReportQuery.append("LEFT JOIN fee_setting d ON d.feeSettingID=g.feeSettingID AND d.isDel='0'  ");
		feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.feeSettingID=d.feeSettingID AND c.isDel='0'  ");
		feeReportQuery.append("LEFT JOIN yearmaster e ON e.yearId=d.yearID AND e.isDel='0' ");
		feeReportQuery.append("LEFT JOIN fee_paid_duetable j ON j.feeSetMultiID=g.feeSetMultiID   ");
		if(StringUtils.isBlank(toDate)) {
			feeReportQuery.append("and STR_TO_DATE(j.dueDate,'%d-%m-%Y') <= STR_TO_DATE('"+fromDate+"','%d-%m-%Y') ");
		}
		else {
			feeReportQuery.append("and STR_TO_DATE(j.dueDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y') ");
		}
		feeReportQuery.append("LEFT JOIN fee_duetable k ON k.feeSetMultiID=g.feeSetMultiID AND k.isDel='0' ");
		feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=g.schoolid AND m.isDel='0' ");
		feeReportQuery.append("LEFT JOIN group_of_school gs ON gs.groupOfSchoolId=m.groupOfSchoolId  ");
		feeReportQuery.append("WHERE g.isConsolation=0 AND m.sansthaKey='"+sansthaKey+"' AND g.isDel='0' AND e.YEAR IN ( ");
		for(int i=0;i<yearName.size();i++) {
			if(i!=0) {
				feeReportQuery.append(",");
			}
			feeReportQuery.append(" '"+yearName.get(i)+"' ");
		}
		feeReportQuery.append(" ) GROUP BY yearId1,schoolid1,standardId1,headId1,subheadId1,feeSetMultiID1)a1 ");
		feeReportQuery.append("GROUP BY groupId1)kj ON kj.groupId1=groupId ");
		feeReportQuery.append("LEFT JOIN (  ");
		feeReportQuery.append("SELECT ifnull(sum(otherExcessFee),0) AS otherExcessFee,groupId as groupId2 FROM (SELECT SUM(IFNULL(otherExcessFee,0)) AS otherExcessFee,yearId9,studId9,groupId  ");
		feeReportQuery.append("FROM ( ");
		feeReportQuery.append("SELECT if((  ");
		feeReportQuery.append("SELECT DISTINCT a.receiptID  ");
		feeReportQuery.append("FROM fee_deletepaidfeetable b  ");
		feeReportQuery.append("WHERE b.receiptID=a.receiptId),0, (a.excessFees)) AS otherExcessFee,a.yearId AS yearId9,d.renewstudentId AS studId9,gs.groupOfSchoolId AS groupId  ");
		feeReportQuery.append("FROM fee_excess_studentwise a  ");
		feeReportQuery.append("LEFT JOIN fee_receipt c ON c.receiptID=a.receiptId  ");
		feeReportQuery.append("LEFT JOIN student_standard_renew d ON d.studentId=a.studId  ");
		feeReportQuery.append("LEFT JOIN yearmaster d1 ON d1.yearId=a.yearId  ");
		feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=a.schoolid AND m.isDel='0'  ");
		feeReportQuery.append("LEFT JOIN group_of_school gs ON gs.groupOfSchoolId=m.groupOfSchoolId  ");
		feeReportQuery.append("WHERE (c.isDel='0' OR (c.isDel='1' AND c.isApproval='0')) AND m.sansthaKey='"+sansthaKey+"' AND d1.YEAR IN ( ");
		for(int i=0;i<yearName.size();i++) {
			if(i!=0) {
				feeReportQuery.append(",");
			}
			feeReportQuery.append(" '"+yearName.get(i)+"' ");
		}
		feeReportQuery.append(" ) GROUP BY studId9)k )kw GROUP BY groupId2) k1 ON k1.groupId2=groupId ");
		feeReportQuery.append("LEFT JOIN (  ");
		feeReportQuery.append("SELECT IFNULL(sum(totlPaidFine1),0) AS totlPaidFine1,groupId3  FROM (SELECT IFNULL(SUM(a.payFee),0.00) AS totlPaidFine1,gs.groupOfSchoolId AS groupId3  ");
		feeReportQuery.append("FROM fee_paidfeetable a ");
		feeReportQuery.append("LEFT JOIN fee_head_master p ON p.headID=a.headID ");
		feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.studFeeID=a.studFeeID  ");
		feeReportQuery.append("LEFT JOIN student_standard_renew d ON d.studentId=c.studRenewID  ");
		feeReportQuery.append("LEFT JOIN yearmaster d1 ON d1.yearId=a.yearId  ");
		feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=a.schoolid AND m.isDel='0' ");
		feeReportQuery.append("LEFT JOIN group_of_school gs ON gs.groupOfSchoolId=m.groupOfSchoolId  ");
		feeReportQuery.append("WHERE  a.isDel='0' AND p.isLateFeeCharges='1' and m.sansthaKey='"+sansthaKey+"' AND d1.YEAR IN ( ");
		for(int i=0;i<yearName.size();i++) {
			if(i!=0) {
				feeReportQuery.append(",");
			}
			feeReportQuery.append(" '"+yearName.get(i)+"' ");
		}
		feeReportQuery.append(" ) GROUP BY c.studRenewID,a.headID,a.subheadID)k GROUP BY groupId3)k2 ON k2.groupId3=groupId ");
		feeReportQuery.append("LEFT JOIN (SELECT SUM(otherFee) AS otherFee,groupId AS groupId4 FROM (SELECT IFNULL(SUM(a.payFee),0.00) AS otherFee,gs.groupOfSchoolId AS groupId   ");
		feeReportQuery.append("FROM fee_paidfeetable a ");
		feeReportQuery.append("LEFT JOIN fee_receipt b ON a.receiptID=b.receiptID ");
		feeReportQuery.append("LEFT JOIN fee_head_master p ON p.headID=a.headID  ");
		feeReportQuery.append("LEFT JOIN fee_studentfee c ON c.studFeeID=a.studFeeID  ");
		feeReportQuery.append("LEFT JOIN student_standard_renew d ON d.studentId=c.studRenewID  ");
		feeReportQuery.append("LEFT JOIN yearmaster d1 ON d1.yearId=a.yearId ");
		feeReportQuery.append("LEFT JOIN school_master m ON m.schoolid=a.schoolid AND m.isDel='0' ");
		feeReportQuery.append("LEFT JOIN group_of_school gs ON gs.groupOfSchoolId=m.groupOfSchoolId  ");
		feeReportQuery.append("WHERE  a.isDel='0'  AND b.typeOfReceipt='1' OR b.typeOfReceipt='2' and m.sansthaKey='"+sansthaKey+"' AND d1.YEAR IN ( ");
		for(int i=0;i<yearName.size();i++) {
			if(i!=0) {
				feeReportQuery.append(",");
			}
			feeReportQuery.append(" '"+yearName.get(i)+"' ");
		}
		feeReportQuery.append(" ) GROUP BY c.studRenewID,a.headID,a.subheadID)k   ");
		feeReportQuery.append("GROUP BY groupId)k3 ON k3.groupId4=groupId  ");
		feeReportQuery.append(")ghd ");
		feeReportQuery.append("ORDER BY groupId ASC ");
		System.out.println(feeReportQuery);
		Query query = entitymanager.createNativeQuery(feeReportQuery.toString());
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(FeeBean.class ) )
				 .getResultList();

	}


	public List<FeeBean> getPayTypeListGroupWise(String sansthaKey, List<String> yearName, String fromDate, String toDate) {
		StringBuilder feeReportQuery = new StringBuilder();
//		feeReportQuery.append("SELECT payTypeId, payTypeName, CAST(paidFee AS CHAR) AS paidFee, CAST(IFNULL(totalPaidFine,0) AS CHAR) AS paidFine " + 
//				"FROM (SELECT * FROM (SELECT year,groupOfSchoolId,payType as payTypeId,payTypeName,receiptID, SUM(IFNULL(totalPaidFee,0)) AS paidFee FROM ( " + 
//				"SELECT y1.year,s.groupOfSchoolId,  y.payTypeID AS payType,y.payTypeName,j.receiptID, (IFNULL(i.payFee,0)+ IFNULL(g1.utilizeFee,0)) - IFNULL(i.discount,0) AS totalPaidFee " + 
//				"FROM fee_setting_multi g " + 
//				"LEFT JOIN fee_setting m ON m.feeSettingID=g.feeSettingID AND m.isDel='0' " + 
//				"LEFT JOIN fee_paidfeetable i ON i.feeSetMultiID=g.feeSetMultiID " + 
//				"LEFT JOIN fee_receipt j ON j.receiptID=i.receiptID AND j.isDel='0' " + 
//				"left join school_master s on s.schoolid=m.schoolid " + 
//				"left join yearmaster y1 on y1.yearId=m.yearID LEFT JOIN utilize_excess_fee g1 ON g.feeSetMultiID=g1.feeSetMultiID   left join reciept_utilization_of_excces_fee g2 ON g2.voucherID=g1.receiptID " + 
//				"left join fee_pay_type y ON y.payTypeID=i.payType OR y.payTypeID=g1.payType " + 
//				"WHERE s.sansthaKey='"+sansthaKey+"' AND (j.isDel='0' OR g2.isDel='0') ");
//				feeReportQuery.append("and y1.year IN ( ");
//				for(int i=0;i<yearName.size();i++) {
//					if(i!=0) {
//						feeReportQuery.append(",");
//					}
//					feeReportQuery.append("'"+yearName.get(i)+"' ");
//				}
//				feeReportQuery.append(" )  AND ((STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') between STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y')) OR (STR_TO_DATE(g1.uitilizefeeDate,'%d-%m-%Y') between STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y'))))vg " + 
//				"group by payTypeName)gf " + 
				feeReportQuery.append("SELECT payTypeId, payTypeName, CAST(paidFee AS CHAR) AS paidFee, CAST(IFNULL(totalPaidFine,0) AS CHAR) AS paidFine  ");
				feeReportQuery.append("FROM (  ");
				feeReportQuery.append("SELECT *   ");
				feeReportQuery.append("FROM (  ");
				feeReportQuery.append("SELECT YEAR,groupOfSchoolId,payType AS payTypeId,payTypeName,receiptID, SUM(IFNULL(totalPaidFee,0))+ SUM(IFNULL(utilizeFee,0)) AS paidFee  ");
				feeReportQuery.append("FROM (  ");
				feeReportQuery.append("SELECT y1.year AS YEAR,s.groupOfSchoolId, y.payTypeID AS payType,y.payTypeName,j.receiptID, IFNULL(i.payFee,0) - IFNULL(i.discount,0) AS totalPaidFee ");
				feeReportQuery.append("FROM fee_setting_multi g ");
				feeReportQuery.append("LEFT JOIN fee_setting m ON m.feeSettingID=g.feeSettingID AND m.isDel='0'   ");
				feeReportQuery.append("LEFT JOIN fee_paidfeetable i ON i.feeSetMultiID=g.feeSetMultiID   ");
				feeReportQuery.append("LEFT JOIN fee_receipt j ON j.receiptID=i.receiptID AND j.isDel='0'  ");
				feeReportQuery.append("LEFT JOIN school_master s ON s.schoolid=m.schoolid  ");
				feeReportQuery.append("LEFT JOIN yearmaster y1 ON y1.yearId=m.yearID  ");
				feeReportQuery.append("LEFT JOIN fee_pay_type y ON y.payTypeID=i.payType   ");
				feeReportQuery.append("WHERE s.sansthaKey='"+sansthaKey+"' AND (j.isDel='0' OR (j.isDel='1' AND j.isApproval='0'))  AND (STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') AND STR_TO_DATE('"+toDate+"','%d-%m-%Y')) AND (((j.payTypeOrPrintFlag IS NULL OR j.payTypeOrPrintFlag='2') AND j.returnFlag='1') OR (j.payTypeOrPrintFlag IS NULL OR j.payTypeOrPrintFlag='1')) ");
				feeReportQuery.append("and y1.year IN ( ");
				for(int i=0;i<yearName.size();i++) {
					if(i!=0) {
						feeReportQuery.append(",");
					}
					feeReportQuery.append("'"+yearName.get(i)+"' ");
				}
				feeReportQuery.append(")GROUP BY y.payTypeName   ");
				feeReportQuery.append(")vg  ");
				feeReportQuery.append("LEFT JOIN (  ");
				feeReportQuery.append("SELECT y.payTypeName AS payTypeName1, IFNULL(g1.utilizeFee,0) AS utilizeFee  ");
				feeReportQuery.append("FROM fee_setting_multi g ");
				feeReportQuery.append("LEFT JOIN fee_setting m ON m.feeSettingID=g.feeSettingID AND m.isDel='0'  ");
				feeReportQuery.append("LEFT JOIN school_master s ON s.schoolid=m.schoolid  ");
				feeReportQuery.append("LEFT JOIN yearmaster y1 ON y1.yearId=m.yearID  ");
				feeReportQuery.append("LEFT JOIN utilize_excess_fee g1 ON g.feeSetMultiID=g1.feeSetMultiID ");
				feeReportQuery.append("LEFT JOIN reciept_utilization_of_excces_fee g2 ON g2.voucherID=g1.receiptID  ");
				feeReportQuery.append("LEFT JOIN fee_pay_type y ON y.payTypeID=g1.payType  ");
				feeReportQuery.append("WHERE s.sansthaKey='"+sansthaKey+"'  AND (STR_TO_DATE(g1.uitilizefeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') AND STR_TO_DATE('"+toDate+"','%d-%m-%Y')) ");
				feeReportQuery.append("and y1.year IN ( ");
				for(int i=0;i<yearName.size();i++) {
					if(i!=0) {
						feeReportQuery.append(",");
					}
					feeReportQuery.append("'"+yearName.get(i)+"' ");
				}
				feeReportQuery.append(")GROUP BY y.payTypeName)k ON k.payTypeName1=vg.payTypeName  ");
				feeReportQuery.append("GROUP BY payTypeName)gf ");
				feeReportQuery.append("LEFT JOIN (Select SUM(totalPaidFine) as totalPaidFine, payType,payTypeName1 from ( " + 
				"SELECT y1.year as year1, i.payType,y.payTypeName as payTypeName1, k.receiptID as receiptID1, " + 
				"(IFNULL(j.dueFee,0)) AS totalPaidFine " + 
				"FROM fee_studentfee n " + 
				"LEFT JOIN fee_setting m on m.feeSettingID=n.feeSettingID " + 
				"LEFT JOIN fee_paidfeetable i on i.studFeeID=n.studFeeID "+
				"LEFT JOIN fee_paid_duetable j ON i.receiptID = j.receiptID "+
				"left join fee_receipt k on k.receiptID=i.receiptID " + 
				"left join fee_pay_type y on y.payTypeID=i.payType " + 
				"left join school_master s on s.schoolid=m.schoolid " + 
				"LEFT JOIN yearmaster y1 ON y1.yearId=m.yearID "+
				" WHERE STR_TO_DATE(j.dueDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y') "+
				"and s.sansthaKey='"+sansthaKey+"' and  k.isDel='0' and y1.year IN (" ) ;
				for(int i=0;i<yearName.size();i++) {
					if(i!=0) {
						feeReportQuery.append(",");
					}
					feeReportQuery.append("'"+yearName.get(i)+"' ");
				}
				feeReportQuery.append(" ) ");
				feeReportQuery.append("group by i.receiptID,j.feeSetMultiID)a group by payTypeName1 " + 
				")kj ON kj.payTypeName1=payTypeName)ghd ORDER BY payTypeId ASC ");
		Query query = entitymanager.createNativeQuery(feeReportQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(FeeBean.class ) )
				 .getResultList();
		

	}

	public List<FeeBean> getYearListGroupWise(String sansthaKey, String yearName, String fromDate, String toDate) {
		StringBuilder feeReportQuery = new StringBuilder();
//		feeReportQuery.append(" SELECT  yearId,yearName,CAST(paidFee AS CHAR) AS paidFee, CAST(IFNULL(totalPaidFine,0) AS CHAR) AS paidFine " + 
//				"FROM (SELECT * FROM ( SELECT year,yearId,yearName,receiptID,SUM(IFNULL(totalPaidFee,0)) AS paidFee FROM ( "
//				+ "SELECT y.year, m.yearId,y.year as yearName,j.receiptID,  (IFNULL(i.payFee,0) + IFNULL(g1.utilizeFee,0)) - IFNULL(i.discount,0) AS totalPaidFee FROM fee_setting_multi g  "
//				+ "LEFT JOIN fee_setting m ON m.feeSettingID=g.feeSettingID AND m.isDel='0' left join fee_paidfeetable i ON i.feeSetMultiID=g.feeSetMultiID  "
//				+ "left join fee_receipt j on j.receiptID=i.receiptID left join yearmaster y on y.yearId=m.yearID "
//				+ "left join school_master s on s.schoolId=m.schoolid  "
//				+ "LEFT JOIN group_of_school gs on gs.groupOfSchoolId=s.groupOfSchoolId LEFT JOIN utilize_excess_fee g1 ON g.feeSetMultiID=g1.feeSetMultiID   left join reciept_utilization_of_excces_fee g2 ON g2.voucherID=g1.receiptID "
//				+ "WHERE s.sansthaKey='"+sansthaKey+"' AND (j.isDel='0' OR g2.isDel='0')  ");
//			feeReportQuery.append("AND ((STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') between STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y')) OR (STR_TO_DATE(g1.uitilizefeeDate,'%d-%m-%Y') between STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y'))))vg "
			feeReportQuery.append("SELECT yearId,yearName, CAST(paidFee AS CHAR) AS paidFee, CAST(IFNULL(totalPaidFine,0) AS CHAR) AS paidFine ");  
			feeReportQuery.append("FROM ( ");  
			feeReportQuery.append("SELECT *   ");  
			feeReportQuery.append("FROM (  ");  
			feeReportQuery.append("SELECT YEAR AS yearName,receiptID, yearId,SUM(IFNULL(totalPaidFee,0))+ SUM(IFNULL(utilizeFee,0)) AS paidFee  ");  
			feeReportQuery.append("FROM ( ");  
			feeReportQuery.append("SELECT y1.year AS YEAR,s.groupOfSchoolId, y1.yearId, y.payTypeID AS payType,y.payTypeName,j.receiptID, IFNULL(i.payFee,0) - IFNULL(i.discount,0) AS totalPaidFee    ");  
			feeReportQuery.append("FROM fee_setting_multi g ");  
			feeReportQuery.append("LEFT JOIN fee_setting m ON m.feeSettingID=g.feeSettingID AND m.isDel='0'  ");  
			feeReportQuery.append("LEFT JOIN fee_paidfeetable i ON i.feeSetMultiID=g.feeSetMultiID  ");  
			feeReportQuery.append("LEFT JOIN fee_receipt j ON j.receiptID=i.receiptID AND j.isDel='0'  ");  
			feeReportQuery.append("LEFT JOIN school_master s ON s.schoolid=m.schoolid  ");  
			feeReportQuery.append("LEFT JOIN yearmaster y1 ON y1.yearId=m.yearID ");  
			feeReportQuery.append("LEFT JOIN fee_pay_type y ON y.payTypeID=i.payType  ");  
			feeReportQuery.append("WHERE s.sansthaKey='"+sansthaKey+"' AND (j.isDel='0' OR (j.isDel='1' AND j.isApproval='0')) AND (STR_TO_DATE(i.paidfeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') AND STR_TO_DATE('"+toDate+"','%d-%m-%Y')) AND (((j.payTypeOrPrintFlag IS NULL OR j.payTypeOrPrintFlag='2') AND j.returnFlag='1') OR (j.payTypeOrPrintFlag IS NULL OR j.payTypeOrPrintFlag='1')) ");  
			feeReportQuery.append("GROUP BY y1.year ");  
			feeReportQuery.append(")vg  ");  
			feeReportQuery.append("LEFT JOIN (  ");  
			feeReportQuery.append("SELECT y.payTypeName AS payTypeName1, IFNULL(g1.utilizeFee,0) AS utilizeFee  ");  
			feeReportQuery.append("FROM fee_setting_multi g ");  
			feeReportQuery.append("LEFT JOIN fee_setting m ON m.feeSettingID=g.feeSettingID AND m.isDel='0'  ");  
			feeReportQuery.append("LEFT JOIN school_master s ON s.schoolid=m.schoolid ");  
			feeReportQuery.append("LEFT JOIN yearmaster y1 ON y1.yearId=m.yearID  ");  
			feeReportQuery.append("LEFT JOIN utilize_excess_fee g1 ON g.feeSetMultiID=g1.feeSetMultiID ");  
			feeReportQuery.append("LEFT JOIN reciept_utilization_of_excces_fee g2 ON g2.voucherID=g1.receiptID  ");  
			feeReportQuery.append("LEFT JOIN fee_pay_type y ON y.payTypeID=g1.payType  ");  
			feeReportQuery.append("WHERE s.sansthaKey='"+sansthaKey+"'   AND (STR_TO_DATE(g1.uitilizefeeDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') AND STR_TO_DATE('"+toDate+"','%d-%m-%Y'))  ");  
			feeReportQuery.append("GROUP BY y1.year)k ON k.payTypeName1=vg.payTypeName ");  
			feeReportQuery.append("GROUP BY yearName)gf  ");  
			feeReportQuery.append(" LEFT JOIN (SELECT  y.year as year1,m.yearId as yearId1,k.receiptID as receiptID1, SUM(IFNULL(j.dueFee,0)) AS totalPaidFine "
				+ "FROM fee_studentfee n LEFT JOIN fee_setting m on m.feeSettingID=n.feeSettingID "
				+ "left join fee_paid_duetable j on j.studFeeID=n.studFeeID left join fee_receipt k on k.receiptID=j.receiptID "
				+ "left join yearmaster y on y.yearId=m.yearID " 
				+ "left join school_master s on s.schoolId=m.schoolid  "
				+ " WHERE s.sansthaKey='"+sansthaKey+"' and k.isDel='0' AND STR_TO_DATE(j.dueDate,'%d-%m-%Y') BETWEEN STR_TO_DATE('"+fromDate+"','%d-%m-%Y') and STR_TO_DATE('"+toDate+"','%d-%m-%Y') " 
				+ "group by year1 )kj ON kj.yearId1=yearId  )ghd ORDER BY yearId1 ASC ");
		Query query = entitymanager.createNativeQuery(feeReportQuery.toString());
	
		return query.unwrap( org.hibernate.query.Query.class )
				 .setResultTransformer(Transformers.aliasToBean(FeeBean.class ) )
				 .getResultList();
		

	}
}
