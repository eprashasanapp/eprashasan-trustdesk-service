package com.ingenio.trust.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.bean.SummaryBean;
import com.ingenio.trust.bean.SummaryMenuBean;
import com.ingenio.trust.model.StudentMasterModel;



@Repository
public interface SummaryDetailRepository extends JpaRepository<StudentMasterModel, Integer> {

	

	@Query(value = "select new com.ingenio.trust.bean.SummaryBean(COALESCE(cm.categoryId,0) as summaryId,COALESCE(cm.categoryNameGL,'NA') as summaryName,COALESCE(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end),0) as totalCount,COALESCE(count(case when sm.studGender='Male' then 1 end),0) as boysCount, COALESCE(count(case when sm.studGender='Female' then 1 end),0) as girlsCount) from StudentMasterModel sm " 
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0' and cm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) group by 2 order by summaryId ")
	List<SummaryBean> getCategoryWiseSchoolStrength(String sansthaKey, List<String> yearList, String renewdate);


	@Query(value = "select new com.ingenio.trust.bean.SummaryBean(COALESCE(cm.concessionId,0) as summaryId,COALESCE(cm.concessionTypeGL,'NA') as summaryName, count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm " 
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ConcessionMaster cm on (cm.concessionId = ssr.concession and cm.isDel='0' and cm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) group by 2 order by summaryId ")
    List<SummaryBean> getConcessionWiseSchoolStrength(String sansthaKey, List<String> yearList, String renewdate);


	@Query(value = "select new com.ingenio.trust.bean.SummaryBean(COALESCE(mm.minorityId,0) as summaryId,COALESCE(mm.minorityTypeGL,'NA')as summaryName,count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm " 
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = sm.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) group by 2 order by summaryId ")
   List<SummaryBean> getMinorityWiseSchoolStrength(String sansthaKey, List<String> yearList, String renewDate);


	@Query(value = "select new com.ingenio.trust.bean.SummaryBean(COALESCE(rl.religionId,0) as summaryId,COALESCE(rl.religionNameGL,'NA') as summaryName,count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm " 
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0' and rl.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) group by 2 order by summaryId ")
    List<SummaryBean> getReligionWiseSchoolStrength(String sansthaKey, List<String> yearList, String renewdate);


	@Query(value = "select new com.ingenio.trust.bean.SummaryMenuBean(sm.summaryMenuId , sm.summaryName ) from SummaryMenuModel sm order by sm.summaryMenuId " )
	List<SummaryMenuBean> getSummaryMenuList();

	@Query(value = "select new com.ingenio.trust.bean.SummaryBean(COALESCE(rl.religionId,0) as summaryId,COALESCE(rl.religionNameGL,'NA') as summaryName,count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm " 
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ReligionMasterModel rl on (rl.religionId = sm.religion.religionId and ssr.isDel='0' and rl.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and ssm.schoolid IN (:schoolList)  group by 2 order by summaryId ")
	List<SummaryBean> getReligionWiseSchoolStrength(String sansthaKey, List<String> yearList, String renewdate,
			List<Integer> schoolList);

	@Query(value = "select new com.ingenio.trust.bean.SummaryBean(COALESCE(cm.concessionId,0) as summaryId,COALESCE(cm.concessionTypeGL,'NA') as summaryName, count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm " 
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join ConcessionMaster cm on (cm.concessionId = ssr.concession and cm.isDel='0' and cm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and ssm.schoolid IN (:schoolList) group by 2 order by summaryId ")
	List<SummaryBean> getConcessionWiseSchoolStrength(String sansthaKey, List<String> yearList, String renewdate,
			List<Integer> schoolList);

	@Query(value = "select new com.ingenio.trust.bean.SummaryBean(COALESCE(mm.minorityId,0) as summaryId,COALESCE(mm.minorityTypeGL,'NA')as summaryName,count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end) as totalCount,count(case when sm.studGender='Male' then 1 end) as boysCount, count(case when sm.studGender='Female' then 1 end) as girlsCount) from StudentMasterModel sm " 
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join MinorityMasterModel mm on (mm.minorityId = sm.status and mm.isDel='0' and mm.schoolId.schoolid = sm.schoolMasterModel.schoolid ) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewDate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewDate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList) and ssm.schoolid IN (:schoolList) group by 2 order by summaryId ")
	List<SummaryBean> getMinorityWiseSchoolStrength(String sansthaKey, List<String> yearList, String renewDate,
			List<Integer> schoolList);

	@Query(value = "select new com.ingenio.trust.bean.SummaryBean(COALESCE(cm.categoryId,0) as summaryId,COALESCE(cm.categoryNameGL,'NA') as summaryName,COALESCE(count(case when (sm.studGender='Male' or sm.studGender='Female') then 1 end),0) as totalCount,COALESCE(count(case when sm.studGender='Male' then 1 end),0) as boysCount, COALESCE(count(case when sm.studGender='Female' then 1 end),0) as girlsCount) from StudentMasterModel sm " 
			+ "left join StudentStandardRenewModel ssr on (sm.studentId = ssr.studentMasterModel.studentId and ssr.isDel='0' and  sm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join SchoolMasterModel ssm on (ssm.schoolid = ssr.schoolMasterModel.schoolid and ssr.isDel='0') "
			+ "left join CategoryMasterModel cm on (sm.category.categoryId = cm.categoryId and cm.isDel='0' and cm.schoolMasterModel.schoolid=ssr.schoolMasterModel.schoolid) "
			+ "left join YearMasterModel ym on ( ym.yearId=ssr.yearMasterModel.yearId and ym.schoolMasterModel.schoolid = ssr.schoolMasterModel.schoolid ) "
			+ "where ssr.alreadyRenew='1' and ((ssr.collegeLeavingDate is null or ssr.collegeLeavingDate='') or "
			+ "(str_to_date(ssr.collegeLeavingDate,'%d-%m-%Y') > str_to_date(:renewdate,'%d-%m-%Y'))) "
			+ "and (str_to_date(ssr.renewAdmissionDate,'%d-%m-%Y') <= str_to_date(:renewdate,'%d-%m-%Y'))  "
			+ "and ssm.sansthaKey=:sansthaKey and sm.isDel='0' and sm.isAdmissionCancelled=1 and sm.isApproval='0' "
			+ "and ym.year IN (:yearList)  and ssm.schoolid IN (:schoolList)  group by 2 order by summaryId ")
	List<SummaryBean> getCategoryWiseSchoolStrength(String sansthaKey, List<String> yearList, String renewdate,
			List<Integer> schoolList);

}








