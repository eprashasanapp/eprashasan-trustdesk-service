package com.ingenio.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ingenio.trust.model.LibBookTypeModel;

@Repository
public interface LibBookTypeRepository  extends JpaRepository<LibBookTypeModel, Integer>{

	
    @Query("From LibBookTypeModel where schoolId = :schoolId AND bookTypeName = :bookTypeName")
    LibBookTypeModel getEntityByName(@Param("schoolId") Integer schoolId, @Param("bookTypeName") String bookTypeName);

}
