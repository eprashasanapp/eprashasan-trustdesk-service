package com.ingenio.trust.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.trust.bean.FeeBean;
import com.ingenio.trust.bean.ResponseBodyBean;
import com.ingenio.trust.service.PaidFeeService;

@RestController
@CrossOrigin
public class PaidFeeController {

	@Autowired
	PaidFeeService paidFeeService;
	
    @GetMapping(value="/paidFeeSchoolWise")
    public ResponseBodyBean<FeeBean> getPaidFeeStandardWise(@RequestParam("sansthaKey") String sansthaKey,
										  @RequestParam("yearName") String yearName,
										  @RequestParam("feeFromDate") String feeFromDate,
										  @RequestParam("feeToDate") String feeToDate,
										  @RequestParam("groupId") Integer groupId, @RequestParam("mobileNo") String mobileNo,
								  			@RequestParam("userId") String userId,@RequestParam("profileRole") String profileRole){
		ResponseBodyBean<FeeBean> responseBodyBean = new ResponseBodyBean<>();
		List<FeeBean> feeCountList = new ArrayList<>();
		try {
			feeCountList =paidFeeService.getPaidFeeStandardWise(sansthaKey,yearName,feeFromDate,feeToDate,groupId,mobileNo,profileRole);
			if(CollectionUtils.isNotEmpty(feeCountList)) {
				responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,feeCountList);
			}
			else {
				responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;
	}
   
    @GetMapping(value="/paidFeeGroupWise")
    public ResponseBodyBean<FeeBean> getPaidFeeGroupWise(@RequestParam("sansthaKey") String sansthaKey,
   										  @RequestParam("yearName") String yearName,
   										  @RequestParam("feeFromDate") String feeFromDate,
   										  @RequestParam("feeToDate") String feeToDate, @RequestParam("mobileNo") String mobileNo,
   							  			@RequestParam("userId") String userId,@RequestParam("profileRole") String profileRole){
   		ResponseBodyBean<FeeBean> responseBodyBean = new ResponseBodyBean<>();
   		List<FeeBean> feeCountList = new ArrayList<>();
   		try {
   			feeCountList =paidFeeService.getPaidFeeGroupWise(sansthaKey,yearName,feeFromDate,feeToDate,mobileNo,profileRole);
   			if(CollectionUtils.isNotEmpty(feeCountList)) {
   				responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,feeCountList);
   			}
   			else {
   				responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
   			}
   		} catch (Exception e) {
   			e.printStackTrace();
   			responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
   		}
   		return responseBodyBean;
   	}
      
  
}
