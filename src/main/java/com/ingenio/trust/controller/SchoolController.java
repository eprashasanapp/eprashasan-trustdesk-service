package com.ingenio.trust.controller;

import java.util.ArrayList;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.trust.bean.AppliedSchoolFilterParam;
import com.ingenio.trust.bean.AppliedSchoolPage;
import com.ingenio.trust.bean.EligibleSchoolPage;
import com.ingenio.trust.bean.IntakeFilterParam;
import com.ingenio.trust.bean.ResponseBodyBean;
import com.ingenio.trust.service.AppliedSchoolService;
import com.ingenio.trust.service.EligibleSchoolService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin
public class SchoolController {

	@Autowired
	AppliedSchoolService appliedSchoolService;
	
	@Autowired
	EligibleSchoolService eligibleSchoolService;
	
    @GetMapping(path = "/getAppliedToUnit")
    @ApiResponses(value = {
    		  @ApiResponse(code = 200, message = "Successfully retrieved"),
    		  @ApiResponse(code = 404, message = "Not found - The Data was not found")
    		})
    @ApiOperation(value = "Get getAppliedToUnit Details", notes = "Get AppliedToUnit Details")
    public ResponseBodyBean<AppliedSchoolPage> getAppliedSchoolPageDetails(
           @RequestParam(value ="studentId",  required = false) Integer studentId,
           @RequestParam(value ="fromSchoolId",  required = false) Integer fromSchoolId,
           @RequestParam(value ="appliedSchoolId",  required = false) Integer appliedSchoolId,
           @RequestParam(value ="size",  required = true) Integer size,
           @RequestParam(value ="page",  required = true) Integer page
          ) {
       	AppliedSchoolFilterParam queryParams = new AppliedSchoolFilterParam();
       	queryParams.setAppliedSchoolId(appliedSchoolId);
       	queryParams.setFromSchoolId(fromSchoolId);
       	queryParams.setPage(page);
       	queryParams.setStudentId(studentId);
       	queryParams.setSize(size);
       	ResponseBodyBean<AppliedSchoolPage> responseBodyBean = new ResponseBodyBean<>();
       	try {
       	AppliedSchoolPage bTCardPage = appliedSchoolService.getAppliedSchoolPageDetails(queryParams);
    		if(CollectionUtils.isNotEmpty(bTCardPage.getData())) {
				responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,bTCardPage);
			}
			else {
				responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;
    }
    
    
    
    @GetMapping(path = "/geteligibleschoollist")
    @ApiResponses(value = {
    		  @ApiResponse(code = 200, message = "Successfully retrieved"),
    		  @ApiResponse(code = 404, message = "Not found - The Data was not found")
    		})
    @ApiOperation(value = "Get geteligibleschoollist	 Details", notes = "Get geteligibleschoollist	 Details")
    public ResponseBodyBean<EligibleSchoolPage> getIntakeOfSchool(
           @RequestParam(value ="categoryName",  required = true) String categoryName,
           @RequestParam(value ="districtId",  required = false) Integer districtId,
           @RequestParam(value ="talukaId",  required = false) Integer talukaId,
           @RequestParam(value ="stateId",  required = false) Integer stateId,
           @RequestParam(value ="gender",  required = false) Integer gender,
           @RequestParam(value ="sansthakey",  required = false) Integer sansthakey,
           @RequestParam(value ="schoolId",  required = false) Integer schoolId,
           @RequestParam(value ="streamName",  required = true) String streamName,
           @RequestParam(value ="size",  required = true) Integer size,
           @RequestParam(value ="page",  required = true) Integer page
          ) {
    	IntakeFilterParam queryParams = new IntakeFilterParam();
       	queryParams.setCategoryName(categoryName);queryParams.setSansthakey(sansthakey);
       	queryParams.setDistrictId(districtId);queryParams.setSchoolId(schoolId);
       	queryParams.setPage(page);queryParams.setStateId(stateId);
       	queryParams.setGender(gender);queryParams.setStreamName(streamName);
       	queryParams.setSize(size);queryParams.setTalukaId(talukaId);
       	ResponseBodyBean<EligibleSchoolPage> responseBodyBean = new ResponseBodyBean<>();
       	try {
       		EligibleSchoolPage bTCardPage = eligibleSchoolService.getEligibleSchoolPageDetails(queryParams);
    		if(CollectionUtils.isNotEmpty(bTCardPage.getData())) {
				responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,bTCardPage);
			}
			else {
				responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;
    }


}
