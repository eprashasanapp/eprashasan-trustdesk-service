package com.ingenio.trust.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.trust.bean.BTCardRecordDetails;
import com.ingenio.trust.bean.BTCardRecordFilterParam;
import com.ingenio.trust.bean.BTCardRecordPage;
import com.ingenio.trust.service.BTCardService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;

@RestController
@CrossOrigin
public class BTCardController {

	@Autowired
	BTCardService bTCardService;
	
    @GetMapping(path = "/getBTCardDetailsHistory")
    @ApiResponse(code = 200, message = "Ok")
    @ApiOperation(value = "Get BT Card history Details", notes = "Get BT card history Details")
    public ResponseEntity<BTCardRecordPage> getBTCardRenewedRecordDetails(
            @Valid BTCardRecordFilterParam queryParams) {
    	BTCardRecordPage bTCardPage = bTCardService.getBTCardRenewedRecordDetails(queryParams);
        return ResponseEntity.status(HttpStatus.OK).body(bTCardPage);
    }
    
    @GetMapping(path = "/getBTCardDetails")
    @ApiResponse(code = 200, message = "Ok")
    @ApiOperation(value = "Get BT Card Details", notes = "Get BT card Details")
    public ResponseEntity<BTCardRecordPage> getBTCardDetails(
            @Valid BTCardRecordFilterParam queryParams) {
    	BTCardRecordPage bTCardPage = bTCardService.getBTCardDetails(queryParams);
        return ResponseEntity.status(HttpStatus.OK).body(bTCardPage);
    }
    
    @PostMapping(path = "/addBTCard")
    @ApiResponse(code = 200, message = "Ok")
    @ApiOperation(value = "add addBTCard for Library", notes = "add addBTCard for Library")
    public ResponseEntity<BTCardRecordDetails> addBTCard(
            @RequestBody BTCardRecordDetails bTCardDetails) {
    	BTCardRecordDetails bTCardRecordDetails = bTCardService.addBTCard(bTCardDetails);
        return ResponseEntity.status(HttpStatus.OK).body(bTCardRecordDetails);
    }
    
}
