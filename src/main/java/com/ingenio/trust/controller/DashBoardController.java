package com.ingenio.trust.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.trust.bean.DashBoardBean;
import com.ingenio.trust.bean.ResponseBodyBean;
import com.ingenio.trust.bean.StudentCountBean;
import com.ingenio.trust.service.StudentCountService;



@RestController 
@CrossOrigin
public class DashBoardController {
	
	
	@Autowired 
	private StudentCountService studentCountService;
	
	  @GetMapping(value="/yearForSanstha")
	  public ResponseBodyBean<String> getYearForSanstha(@RequestParam("sansthaKey") String sansthaKey,
				@RequestParam("renewAdmissionDate") String renewDate , @RequestParam("mobileNo") String mobileNo,
	  			@RequestParam("userId") String userId,@RequestParam("profileRole") String profileRole){
			ResponseBodyBean<String> responseBodyBean = new ResponseBodyBean<>();
			List<String> studentCountList = new ArrayList<>();
			try {
				
				studentCountList =studentCountService.getYearForSanstha(sansthaKey,renewDate);
				if(CollectionUtils.isNotEmpty(studentCountList)) {
					responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,studentCountList);
				}
				else {
					responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
				}
			} catch (Exception e) {
				e.printStackTrace();
				responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			}
			return responseBodyBean;
		}
	    
	  
    @GetMapping(value="/dashboardCount")
    public ResponseBodyBean<DashBoardBean> getDashboardCount(@RequestParam("sansthaKey") String sansthaKey,
			@RequestParam("yearName") String yearName,@RequestParam("renewAdmissionDate") String renewDate ,@RequestParam("language") String language){

    	ResponseBodyBean<DashBoardBean> responseBodyBean = new ResponseBodyBean<>();
		List<DashBoardBean> studentCountList = new ArrayList<>();
		try {			
			studentCountList =studentCountService.getDashboardCount(sansthaKey,yearName,renewDate ,language);
			if(CollectionUtils.isNotEmpty(studentCountList)) {
				responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,studentCountList);
			}
			else {
				responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;
	}
    
    
//    @GetMapping(value="/newdashboardCount")
//    public ResponseBodyBean<DashBoardBean> getNewDashboardCount(@RequestParam("sansthaKey") String sansthaKey,
//  			@RequestParam("yearName") String yearName,@RequestParam("renewAdmissionDate") String renewDate ,
//  			@RequestParam("language") String language,@RequestParam("mobileNo") String mobileNo,
//  			@RequestParam("userId") String userId,@RequestParam("profileRole") String profileRole,
//  			@RequestParam(value ="isReact", required=false,defaultValue="0")Integer isReact){
//
//      	ResponseBodyBean<DashBoardBean> responseBodyBean = new ResponseBodyBean<>();
//  		List<DashBoardBean> studentCountList = new ArrayList<>();
//  		try {
//  			System.out.println("mobileNo"+mobileNo + "userId" + userId + "profileRole"+profileRole + "yearName"+yearName + "sanstha" +sansthaKey);
//  			studentCountList =studentCountService.getNewDashboardCount(sansthaKey,yearName,renewDate ,language,mobileNo,profileRole,isReact);
//  			
//  			  			
//  			if(CollectionUtils.isNotEmpty(studentCountList)) {
//  				responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,studentCountList);
//  			}
//  			else {
//  				responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
//  			}
//  		} catch (Exception e) {
//  			e.printStackTrace();
//  			System.out.println("Student Count List-----------------------------------------"+studentCountList);
//  			responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
//  		}
//  		return responseBodyBean;
//  	}
    
    @GetMapping(value="/newdashboardCount")
    public ResponseBodyBean<DashBoardBean> getNewDashboardCount1(@RequestParam(value="sansthaKey",required=false) String sansthaKey,
  			@RequestParam("yearName") String yearName,@RequestParam("renewAdmissionDate") String renewDate ,
  			@RequestParam("language") String language,@RequestParam("mobileNo") String mobileNo,
  			@RequestParam(value="userId",required = false) String userId,@RequestParam(value="profileRole",required = false) String profileRole,
  			@RequestParam(value ="isReact", required=false,defaultValue="0")Integer isReact){

      	ResponseBodyBean<DashBoardBean> responseBodyBean = new ResponseBodyBean<>();
  		List<DashBoardBean> studentCountList = new ArrayList<>();
  		try {
  			System.out.println("mobileNo"+mobileNo + "userId" + userId + "profileRole"+profileRole + "yearName"+yearName + "sanstha" +sansthaKey);
  			studentCountList =studentCountService.getNewDashboardCount1(sansthaKey,yearName,renewDate ,language,mobileNo,profileRole,isReact);
  			
  			  			
  			if(CollectionUtils.isNotEmpty(studentCountList)) {
  				responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,studentCountList);
  			}
  			else {
  				responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
  			}
  		} catch (Exception e) {
  			e.printStackTrace();
  			System.out.println("Student Count List-----------------------------------------"+studentCountList);
  			responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
  		}
  		return responseBodyBean;
  	}
 
     @GetMapping(value="/groupOfSchoolWiseStrength")
     public ResponseBodyBean<StudentCountBean> getgroupOfSchoolWiseStrength(@RequestParam("sansthaKey") String sansthaKey,
 			@RequestParam("yearName") String yearName,
 			@RequestParam("renewAdmissionDate") String renewDate ,@RequestParam("mobileNo") String mobileNo,
  			@RequestParam("userId") String userId, @RequestParam("profileRole") String profileRole){
 		ResponseBodyBean<StudentCountBean> responseBodyBean = new ResponseBodyBean<>();
 		List<StudentCountBean> studentCountList = new ArrayList<>();
 		try {
 			
 			studentCountList =studentCountService.getgroupOfSchoolWiseStrength(sansthaKey,yearName,renewDate,mobileNo,profileRole);
 			if(CollectionUtils.isNotEmpty(studentCountList)) {
 				responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,studentCountList);
 			}
 			else {
 				responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
 			}
 		} catch (Exception e) {
 			e.printStackTrace();
 			responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
 		}
 		return responseBodyBean;
 	}
     
     
     
   
    // http://13.233.210.164:8050/trust-service/newdashboardCount?sansthaKey=0378&yearName=2020-2021&renewAdmissionDate=15-04-2021&language=en&profileRole=&userId=&mobileNo=9890349548 
     
     
    
    
}
