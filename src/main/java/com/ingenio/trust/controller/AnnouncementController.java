package com.ingenio.trust.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Produces;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ingenio.trust.bean.AnnouncementRequestBean;
import com.ingenio.trust.bean.AnnouncementResponseBean;
import com.ingenio.trust.bean.ResponseBodyBean;
import com.ingenio.trust.bean.SansthaUserBean;
import com.ingenio.trust.bean.SchoolBean;
import com.ingenio.trust.bean.StaffDetailsBean;
import com.ingenio.trust.bean.UserBean;
import com.ingenio.trust.service.AnnouncementService;




@Controller
@CrossOrigin
public class AnnouncementController {
	
	
	@Autowired
	AnnouncementService announcementService;
	

	@GetMapping(value="/schoolList")
	@ResponseBody ResponseBodyBean<SchoolBean> getSchoolList(@RequestParam("sansthaKey") String sansthaKey,
			@RequestParam("sansthaUserId") Integer sansthaUserId){
		List<SchoolBean> schoolResponseBean = new ArrayList<>();
		try {
			schoolResponseBean=announcementService.getSchoolList(sansthaKey,sansthaUserId);
			if(schoolResponseBean != null) {
				return new ResponseBodyBean<SchoolBean>("200",HttpStatus.OK,schoolResponseBean);
			}
			else {
				return new ResponseBodyBean<SchoolBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<SchoolBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	
	@GetMapping(value="/sasthaUserList")
	@ResponseBody ResponseBodyBean<SansthaUserBean> getSansthaUserList(@RequestParam("sansthaKey") String sansthaKey){
		List<SansthaUserBean> sansthaUserList = new ArrayList<>();
		try {
			sansthaUserList=announcementService.getSansthaUserList(sansthaKey);
			if(sansthaUserList != null) {
				return new ResponseBodyBean<SansthaUserBean>("200",HttpStatus.OK,sansthaUserList);
			}
			else {
				return new ResponseBodyBean<SansthaUserBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<SansthaUserBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@GetMapping(value="/principalList")
	@ResponseBody ResponseBodyBean<StaffDetailsBean> getSchoolWisePrincipalList(@RequestParam("sansthaKey") String sansthaKey,@RequestParam("sansthaUserId") Integer sansthaUserId){
		List<StaffDetailsBean> principalList = new ArrayList<>();
		try {
			principalList=announcementService.getSchoolWisePrincipalList(sansthaKey,sansthaUserId);
			if(principalList != null) {
				return new ResponseBodyBean<StaffDetailsBean>("200",HttpStatus.OK,principalList);
			}
			else {
				return new ResponseBodyBean<StaffDetailsBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StaffDetailsBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	

	@GetMapping(value="/allUserList")
	@ResponseBody ResponseBodyBean<UserBean> getAllUserList(@RequestParam("sansthaKey") String sansthaKey,@RequestParam("sansthaUserId") Integer sansthaUserId){
		UserBean userBean =new UserBean();
		try {
			List<UserBean> userList=announcementService.getAllUserList(sansthaKey,sansthaUserId);
			if(CollectionUtils.isNotEmpty(userList)) {
				return new ResponseBodyBean<UserBean>("200",HttpStatus.OK,userList);
			}
			else {
				return new ResponseBodyBean<UserBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<UserBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	
	@PostMapping(value="/saveSansthaAnnouncement")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> saveSansthaAnnouncement(@RequestBody AnnouncementRequestBean announcementRequestBean){
		try {
				Integer saveId = announcementService.saveSansthaAnnouncement(announcementRequestBean);
				if(saveId>0) {
					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,saveId);
				}
				else {
					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
				}			
		
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@GetMapping(value="/myAnnouncement")
	public @ResponseBody ResponseBodyBean<AnnouncementResponseBean> getMyAnnouncement(@RequestParam("sansthauserId") Integer sansthauserId,
			@RequestParam("yearId") Integer yearId,@RequestParam("offset") Integer offset,
			@RequestParam(value = "announcementDate", required = false) String announcementDate){
		List<AnnouncementResponseBean> assignedAnnouncementList = new ArrayList<AnnouncementResponseBean>();
		try {
			assignedAnnouncementList=announcementService.getMyAnnouncement(sansthauserId,yearId,announcementDate,offset);
			if(CollectionUtils.isNotEmpty(assignedAnnouncementList) && assignedAnnouncementList.get(0).getAnnouncementId()!=null) {
//				for(int i=0;i<assignedAnnouncementList.size();i++) {
//				announcementService.updateAnnouncementStatus(assignedAnnouncementList.get(0).getAnnouncementAssignedtoId(), 0, "1");
//				}
				return new ResponseBodyBean<AnnouncementResponseBean>("200",HttpStatus.OK,assignedAnnouncementList);
			}
			else {
				return new ResponseBodyBean<AnnouncementResponseBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementResponseBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	
	@PostMapping(value = "/attachmentsWithId")
	@Produces("application/json")
	public @ResponseBody ResponseBodyBean<Integer> saveAttachmentsWithId(@RequestParam MultipartFile file,@RequestParam String fileName,
			@RequestParam("yearName") String yearName , @RequestParam Integer sansthaAnnouncmentId, @RequestParam Integer sansthakey){
		try {
			Integer saveId = announcementService.saveAttachmentsWithId(file,fileName,yearName ,sansthaAnnouncmentId,sansthakey);
			if(saveId>0) {
				return new ResponseBodyBean<Integer>("200",HttpStatus.OK,saveId);
			}
			else {
				return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}	

//		@PostMapping(value="/deleteAnnouncement")
//	@Produces("application/json")
//	public @ResponseBody ResponseBodyBean<Integer> deleteAnnouncement(@RequestParam("announcementId") Integer announcementId){
//			try{
//				Integer deleteId = announcementService.deleteAnnouncement(announcementId);
//				if(deleteId!=0) {
//					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,deleteId);
//				}
//				else {
//					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,0);
//				}
//				
//			  
//	        } catch (Exception e) {
//				e.printStackTrace();
//				return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,0);
//			}
//	}
//	
//	@PostMapping(value="/deleteAttachment")
//	@Produces("application/json")
//	public @ResponseBody ResponseBodyBean<Integer> deleteAttachment(@RequestParam("announcementAttachmentId") Integer announcementAttachmentId){
//			try{
//				Integer deleteId = announcementService.deleteAttachment(announcementAttachmentId);
//				if(deleteId!=0) {
//					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,deleteId);
//				}
//				else {
//					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,0);
//				}
//				
//			  
//	        } catch (Exception e) {
//				e.printStackTrace();
//				return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,0);
//			}
//	}
//	
	@GetMapping(value="/attachmentList")
	public @ResponseBody ResponseBodyBean<AnnouncementResponseBean> getAttachmentList(@RequestParam("announcementId") Integer announcementId){
		List<AnnouncementResponseBean> assignedAnnouncementList = new ArrayList<AnnouncementResponseBean>();
		try {
			assignedAnnouncementList=announcementService.getAttachmentList(announcementId);
			if(CollectionUtils.isNotEmpty(assignedAnnouncementList)) {
				return new ResponseBodyBean<AnnouncementResponseBean>("200",HttpStatus.OK,assignedAnnouncementList);
			}
			else {
				return new ResponseBodyBean<AnnouncementResponseBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<AnnouncementResponseBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
//	}

}
