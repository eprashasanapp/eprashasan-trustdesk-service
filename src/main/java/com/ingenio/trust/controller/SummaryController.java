package com.ingenio.trust.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.trust.bean.ResponseBodyBean;
import com.ingenio.trust.bean.StudentCountBean;
import com.ingenio.trust.bean.SummaryMenuBean;
import com.ingenio.trust.service.SummaryDetailService;

@RestController
@CrossOrigin
public class SummaryController {

	@Autowired
	private SummaryDetailService summaryDetailService;

	@GetMapping(value = "/SansthaWiseSummaryDetails")
	public ResponseBodyBean<StudentCountBean> getSansthaWiseSummaryDetails(@RequestParam("sansthaKey") String sansthaKey,
			@RequestParam("profileRole") String profileRole, @RequestParam("yearName") String yearName,
			@RequestParam("renewAdmissionDate") String renewDate, @RequestParam("summaryType") String summaryType,
			@RequestParam("mobileNo") String mobileNo,
  			@RequestParam("userId") String userId){
		ResponseBodyBean<StudentCountBean> responseBodyBean = new ResponseBodyBean<StudentCountBean>();
		List<StudentCountBean> studentCountList = new ArrayList<>();
		try {

			studentCountList = summaryDetailService.getSansthaWiseSummaryDetails(sansthaKey, yearName, renewDate,
					summaryType,mobileNo,profileRole);
			if (CollectionUtils.isNotEmpty(studentCountList)) {
				responseBodyBean = new ResponseBodyBean<>("200", HttpStatus.OK, studentCountList);
			} else {
				responseBodyBean = new ResponseBodyBean<>("404", HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean = new ResponseBodyBean<>("500", HttpStatus.INTERNAL_SERVER_ERROR,
					new ArrayList<>());
		}
		return responseBodyBean;
	}
	
	
	@GetMapping(value="/groupWiseSummary")
	public ResponseBodyBean<StudentCountBean>groupWiseSummaryList(@RequestParam("sansthaKey") String sansthaKey,
			@RequestParam("yearName") String yearName,
			@RequestParam("summaryType") String summaryType,
			@RequestParam("summaryId") String summaryId,
			@RequestParam("renewAdmissionDate") String renewDate, @RequestParam("mobileNo") String mobileNo,
  			@RequestParam("userId") String userId,@RequestParam("profileRole") String profileRole){
		ResponseBodyBean<StudentCountBean> responseBodyBean = new ResponseBodyBean<StudentCountBean>();
		List<StudentCountBean> studentCountList = new ArrayList<>();
		try {
			
			studentCountList =summaryDetailService.groupWiseSummaryList(sansthaKey,yearName,summaryType,summaryId,
					renewDate,mobileNo,profileRole);
			if(CollectionUtils.isNotEmpty(studentCountList)) {
				responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,studentCountList);
			}
			else {
				responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;
	}
	
	
	@GetMapping(value="/SummaryWiseSchoolList")
	public ResponseBodyBean<StudentCountBean>getSummaryWiseSchoolList(@RequestParam("sansthaKey") String sansthaKey,
			@RequestParam("yearName") String yearName,
			@RequestParam("summaryType") String summaryType,
			@RequestParam("summaryId") String summaryId,
			@RequestParam("groupOfSchoolId") Integer groupOfSchoolId,
			@RequestParam("renewAdmissionDate") String renewDate, @RequestParam("mobileNo") String mobileNo,
  			@RequestParam("userId") String userId,@RequestParam("profileRole") String profileRole){
		ResponseBodyBean<StudentCountBean> responseBodyBean = new ResponseBodyBean<>();
		List<StudentCountBean> studentCountList = new ArrayList<>();
		try {
			
			studentCountList =summaryDetailService.getSummaryWiseSchoolList(sansthaKey,yearName,summaryType,summaryId,
					renewDate,groupOfSchoolId,mobileNo,profileRole);
			if(CollectionUtils.isNotEmpty(studentCountList)) {
				responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,studentCountList);
			}
			else {
				responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;
	}
	
	
	
	  @GetMapping(value="/summaryMenuList")
	  public ResponseBodyBean<SummaryMenuBean> getSummaryMenuList( @RequestParam("mobileNo") String mobileNo,
	  			@RequestParam("userId") String userId,@RequestParam("profileRole") String profileRole){
	  		ResponseBodyBean<SummaryMenuBean> responseBodyBean = new ResponseBodyBean<>();
	  		List<SummaryMenuBean> studentCountList = new ArrayList<>();
	  		try {
	  			
	  			studentCountList =summaryDetailService.getSummaryMenuList();
	  			if(CollectionUtils.isNotEmpty(studentCountList)) {
	  				responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,studentCountList);
	  			}
	  			else {
	  				responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
	  			}
	  		} catch (Exception e) {
	  			e.printStackTrace();
	  			responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
	  		}
	  		return responseBodyBean;
	  	}
	      
    
	  @GetMapping(value="/testing123")
	  public String testing() {
		  return "new changes checking ci cd";
	  }

}
