package com.ingenio.trust.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.trust.bean.AttendanceReportBean;
import com.ingenio.trust.bean.ResponseBodyBean;
import com.ingenio.trust.service.AttendanceReportService;

@RestController
@CrossOrigin
public class AttendanceCountController {
	
	@Autowired 
	private AttendanceReportService attendanceReportService;
	
	@GetMapping(value="/schoolWiseAttendance")
		public ResponseBodyBean<AttendanceReportBean>getSchoolWiseAttendance(@RequestParam("sansthaKey") String sansthaKey,
				@RequestParam("groupOfSchoolId") Integer groupOfSchoolId,
				@RequestParam("attendedDate") String attendedDate, @RequestParam("mobileNo") String mobileNo,
	  			@RequestParam("userId") String userId,@RequestParam("profileRole") String profileRole){
			ResponseBodyBean<AttendanceReportBean> responseBodyBean = new ResponseBodyBean<>();
			List<AttendanceReportBean> attendanceReportList = new ArrayList<>();
			try {
				
				attendanceReportList =attendanceReportService.getSchoolWiseAttendance(sansthaKey,attendedDate,
						groupOfSchoolId,mobileNo,profileRole);
				if(CollectionUtils.isNotEmpty(attendanceReportList)) {
					responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,attendanceReportList);
				}
				else {
					responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
				}
			} catch (Exception e) {
				e.printStackTrace();
				responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
			}
			return responseBodyBean;
		}
	
	@GetMapping(value="/groupWiseAttendance")
	public ResponseBodyBean<AttendanceReportBean>getgGoupWiseAttendance(@RequestParam("sansthaKey") String sansthaKey,
			@RequestParam("profileRole") String profileRole,
			@RequestParam("attendedDate") String attendedDate, @RequestParam("mobileNo") String mobileNo,
  			@RequestParam("userId") String userId){
		ResponseBodyBean<AttendanceReportBean> responseBodyBean = new ResponseBodyBean<>();
		List<AttendanceReportBean> attendanceReportList = new ArrayList<>();
		try {
			
			attendanceReportList =attendanceReportService.getgGoupWiseAttendance(sansthaKey,attendedDate,mobileNo,profileRole);
			if(CollectionUtils.isNotEmpty(attendanceReportList)) {
				responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,attendanceReportList);
			}
			else {
				responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;
	}

}
