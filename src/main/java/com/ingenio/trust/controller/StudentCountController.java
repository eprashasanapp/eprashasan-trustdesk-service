package com.ingenio.trust.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.trust.bean.ResponseBodyBean;
import com.ingenio.trust.bean.SchoolBean;
import com.ingenio.trust.bean.StaffDetailsNewBean;
import com.ingenio.trust.bean.StudentCountBean;
import com.ingenio.trust.service.StudentCountService;



@RestController
@CrossOrigin
public class StudentCountController {
	
	
	@Autowired 
	private StudentCountService studentCountService;
    
    @GetMapping(value="/SchoolWiseStrength")
    public ResponseBodyBean<StudentCountBean> getSchoolWiseStrength(@RequestParam("sansthaKey") String sansthaKey,
			@RequestParam("groupOfSchoolId") Integer groupOfSchoolId,
			@RequestParam("yearName") String yearName,
			@RequestParam("renewAdmissionDate") String renewDate, @RequestParam("mobileNo") String mobileNo,
  			@RequestParam("userId") String userId,@RequestParam("profileRole") String profileRole){
		ResponseBodyBean<StudentCountBean> responseBodyBean = new ResponseBodyBean<>();
		List<StudentCountBean> studentCountList = new ArrayList<>();
		try {
			
			studentCountList =studentCountService.getSchoolWiseStrength(sansthaKey,yearName,renewDate,groupOfSchoolId,mobileNo,profileRole);
			if(CollectionUtils.isNotEmpty(studentCountList)) {
				responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,studentCountList);
			}
			else {
				responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
		return responseBodyBean;
	}
    
    @GetMapping(value="/schoolWiseStaffDetails")
    public ResponseBodyBean<StaffDetailsNewBean> getSchoolWiseStaffDetails(@RequestParam("schoolid") Integer schoolid) {

    	
    	try {
			
    		List<StaffDetailsNewBean>staffDetails= studentCountService.getSchoolWiseStaffDetails(schoolid);
			if(CollectionUtils.isNotEmpty(staffDetails)) {
				return new ResponseBodyBean<StaffDetailsNewBean>("200",HttpStatus.OK,staffDetails);
			}
			else {
				 return new ResponseBodyBean<StaffDetailsNewBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<StaffDetailsNewBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
    }
    @GetMapping(value="/getGroupOfSchool")
   
    
    public ResponseBodyBean<SchoolBean> getGroupOfSchool(@RequestParam("sansthaKey") String sansthaKey,@RequestParam(value="groupOfSchoolId",required = false,defaultValue = "0") Integer groupOfSchoolId)
    {
    	try {
			
    		List<SchoolBean> schoolBean=studentCountService.getAllSchools(sansthaKey,groupOfSchoolId);
			if(CollectionUtils.isNotEmpty(schoolBean)) {
				return new ResponseBodyBean<SchoolBean>("200",HttpStatus.OK,schoolBean);
			}
			else {
				return  new ResponseBodyBean<SchoolBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<SchoolBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
    }
   
  }
