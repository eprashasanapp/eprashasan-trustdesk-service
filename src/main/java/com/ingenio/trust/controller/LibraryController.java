package com.ingenio.trust.controller;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.validation.Valid;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ingenio.trust.bean.BookIssuedDetails;
import com.ingenio.trust.bean.BookIssuedFilterParam;
import com.ingenio.trust.bean.BookIssuedPage;
import com.ingenio.trust.bean.ExcelBean;
import com.ingenio.trust.bean.LibBookAccessISBNFilterParam;
import com.ingenio.trust.bean.LibBookAccessISBNPage;
import com.ingenio.trust.bean.LibSchoolTypeFilterParam;
import com.ingenio.trust.bean.LibSchoolTypePage;
import com.ingenio.trust.bean.LibStudentAdmissionFilterParam;
import com.ingenio.trust.bean.LibStudentAdmissionPage;
import com.ingenio.trust.bean.LibStudentAdmissionRequest;
import com.ingenio.trust.bean.LibStudentAdmissionResponse;
import com.ingenio.trust.bean.UpdateBookIssued;
import com.ingenio.trust.service.AttachmentService;
import com.ingenio.trust.service.LibBookAccessISBNService;
import com.ingenio.trust.service.LibBookIssuedService;
import com.ingenio.trust.service.LibSchoolTypeService;
import com.ingenio.trust.service.LibraryService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Component
@CrossOrigin
public class LibraryController {

	private static final int STRING = 0;

	private static final int NUMERIC = 0;

	@Autowired
	LibraryService libraryService;
	
	@Autowired
	LibBookAccessISBNService libBookAccessISBNService;
	
	@Autowired
	LibBookIssuedService bookIssuedService;
	
	@Autowired
	LibSchoolTypeService libSchoolTypeService;
	
	@Autowired
	AttachmentService attachmentService;
	
    @PostMapping(path = "/addStudentInLibrary", consumes = "application/json")
    @ApiOperation(value = "Add/Update Students in Library", notes = "Add/Update Students in Library")
    public ResponseEntity<LibStudentAdmissionResponse> addStudentInLibrary(
            @ApiParam(value = "StudentsInfo object for Library") @Valid @RequestBody LibStudentAdmissionRequest addStudentLibRequest){
    	 libraryService.addStudentToLib(addStudentLibRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    } 
    
    @GetMapping(path = "/getAdmittedStudentInLibrary")
    @ApiResponse(code = 200, message = "Ok")
    @ApiOperation(value = "Get admittted Students in Library", notes = "Get admitted Students in Library")
    public ResponseEntity<LibStudentAdmissionPage> getAdmittedStudentInLibrary(
            @Valid LibStudentAdmissionFilterParam queryParams) {
    	LibStudentAdmissionPage libStudentAdmissionPage = libraryService.getAdmittedStudentsFromLib(queryParams);
        return ResponseEntity.status(HttpStatus.OK).body(libStudentAdmissionPage);
    }
    
    @GetMapping(path = "/getBookDetailsByAcession")
    @ApiResponse(code = 200, message = "Ok")
    @ApiOperation(value = "get Book Details By Acession Library", notes = "get Book Details By Acession Library")
    public ResponseEntity<LibBookAccessISBNPage> getBookDetailsByAcession(
            @Valid LibBookAccessISBNFilterParam queryParams) {
    	LibBookAccessISBNPage bookDetailsPage = libBookAccessISBNService.getLibBookAccessISBN(queryParams);
        return ResponseEntity.status(HttpStatus.OK).body(bookDetailsPage);
    }
    
    @GetMapping(path = "/getBookIssuedDetails")
    @ApiResponse(code = 200, message = "Ok")
    @ApiOperation(value = "get Book Details from Library", notes = "get Book Details from Library")
    public ResponseEntity<BookIssuedPage> getBookIssuedDetails(
            @Valid BookIssuedFilterParam queryParams) {
    	BookIssuedPage bookIssuedPage = bookIssuedService.getBookIssuedDetails(queryParams);
        return ResponseEntity.status(HttpStatus.OK).body(bookIssuedPage);
    }
    
    @PostMapping(path = "/addIssuedBook")
    @ApiResponse(code = 200, message = "Ok")
    @ApiOperation(value = "add issued book in Library", notes = "add issued book in Library")
    public ResponseEntity<BookIssuedDetails> addBookIssued(
            @RequestBody BookIssuedDetails bookIssue) {
    	BookIssuedDetails bookIssuedDetails = bookIssuedService.addBookIssuedDetails(bookIssue);
        return ResponseEntity.status(HttpStatus.OK).body(bookIssuedDetails);
    }
    
    @PatchMapping(path = "/updateIssuedBook")
    @ApiResponse(code = 200, message = "Ok")
    @ApiOperation(value = "update bulk issued book in Library", notes = "update bulk issued book in Library")
    public ResponseEntity<UpdateBookIssued> updateIssuedBook(
            @RequestBody UpdateBookIssued bookIssue) {
    	bookIssue = bookIssuedService.updateBookIssuedDetails(bookIssue);
        return ResponseEntity.status(HttpStatus.OK).body(bookIssue);
    }
    
    @GetMapping(path = "/getLibSchoolType")
    @ApiResponse(code = 200, message = "Ok")
    @ApiOperation(value = "get School Type Details from Library", notes = "get School Type Details from Library")
    public ResponseEntity<LibSchoolTypePage> getLibSchoolType(
            @Valid LibSchoolTypeFilterParam queryParams) {
    	 LibSchoolTypePage libSchoolTypePage = libSchoolTypeService.getSchoolType(queryParams);
        return ResponseEntity.status(HttpStatus.OK).body(libSchoolTypePage);
    }
    
    @ApiOperation(value = "upload book inventory in library", response = Iterable.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "upload book inventory in library proceed.") })
    @PostMapping(path = "/uploadbookinventoryinlibrary/{schoolId}/attachments", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = "application/json")
    public ResponseEntity attachmentsSendTestEmailForTemplate(@PathVariable Integer schoolId,
            @ApiParam(value = "File") @RequestParam("files") MultipartFile file) throws Exception {
    	 File convFile = new File(System.getProperty("java.io.tmpdir")+"/"+file.getOriginalFilename());
    	 file.transferTo(convFile);
    	FileInputStream inputStream = new FileInputStream(convFile);
    	Workbook baeuldungWorkBook = new XSSFWorkbook(inputStream);
    	List<ExcelBean> list = new ArrayList<ExcelBean>();
    	for (Sheet sheet : baeuldungWorkBook) {
    		Iterator<Row> itr = sheet.iterator();
    		while (itr.hasNext())                 
    		{  
    		Row row = itr.next();  
    		Iterator<Cell> cellIterator = row.cellIterator();    
    		while (cellIterator.hasNext())   
    		{
    		ExcelBean eb = new ExcelBean();
    		eb.setAccessionDate(getCellValue(cellIterator.next()));
    		eb.setStatus(getCellValue(cellIterator.next()));
    		eb.setAccNo(getCellValue(cellIterator.next()));
    		eb.setSeriesCode(getCellValue(cellIterator.next()));
    		eb.setClassNo(getCellValue(cellIterator.next()));
    		eb.setAuthor(getCellValue(cellIterator.next()));
    		eb.setTitleName(getCellValue(cellIterator.next()));
    		eb.setEdition(getCellValue(cellIterator.next()));
    		eb.setVolume(getCellValue(cellIterator.next()));
    		eb.setPublisherName(getCellValue(cellIterator.next()));
    		eb.setCity(getCellValue(cellIterator.next()));
    		eb.setPubYear(getCellValue(cellIterator.next()));
    		eb.setPages(getCellValue(cellIterator.next()));
    		eb.setPrePages(getCellValue(cellIterator.next()));
    		eb.setVendorCity(getCellValue(cellIterator.next()));
    		eb.setInvoiceNo(getCellValue(cellIterator.next()));
    		eb.setInvoiceDate(getCellValue(cellIterator.next()));
    		eb.setSubSubjectName(getCellValue(cellIterator.next()));
    		eb.setPrintPrice(getCellValue(cellIterator.next()));
    		eb.setPurPrice(getCellValue(cellIterator.next()));
    		eb.setRackName(getCellValue(cellIterator.next()));
    		list.add(eb);
    		}
    		}
    	}
    	attachmentService.uploadBookInventoryinLibrary(schoolId, list);
    	convFile.deleteOnExit();
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    
    @SuppressWarnings("deprecation")
	public String getCellValue(Cell cell) { //TODO deprecated code ...need to remove during code refactoring
    	 switch (cell.getCellType()) {
         case Cell.CELL_TYPE_STRING:
        	 return cell.getStringCellValue();
         case Cell.CELL_TYPE_NUMERIC:
        	 return String.valueOf(cell.getNumericCellValue());
     }
		return null;
    }
    

//    @SuppressWarnings("deprecation")
//    public String getCellValue(Cell cell) {
//        if (cell == null) {
//            return null;
//        }
//        
//        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
//            return cell.getStringCellValue();
//        } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
//            return String.valueOf(cell.getNumericCellValue());
//        } else {
//            throw new IllegalArgumentException("Unsupported cell type: " + cell.getCellType());
//        }
//    }

    
} 
