package com.ingenio.trust.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.trust.bean.ResponseBodyBean;
import com.ingenio.trust.bean.StaffDetailsBean;
import com.ingenio.trust.service.StaffDetailService;

@RestController 
@CrossOrigin
public class StaffController {
	
	

	
	@Autowired 
	private StaffDetailService staffDetailService;
	
	
	 @GetMapping(value="/totalSansthaWiseStaffDetails")
	 public ResponseBodyBean<StaffDetailsBean>getTotalStaffSansthaWiseDetails(@RequestParam("sansthaKey") String sansthaKey,
	  			@RequestParam("label") String label,
	  			@RequestParam("groupOfSchoolId") Integer groupOfSchoolId,
	  			@RequestParam("yearName") String yearname,
	  			@RequestParam("renewAdmissionDate") String renewDate, @RequestParam("mobileNo") String mobileNo,
	  			@RequestParam("userId") String userId,@RequestParam("profileRole") String profileRole){
	  		ResponseBodyBean<StaffDetailsBean> responseBodyBean = new ResponseBodyBean<>();
	  		List<StaffDetailsBean> studentDetailsList = new ArrayList<>();
	  		try {
	  		
	  			studentDetailsList =staffDetailService.getSansthaWiseStaffDetails(sansthaKey,yearname,renewDate,groupOfSchoolId);
	  			if(CollectionUtils.isNotEmpty(studentDetailsList)) {
	  				responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,studentDetailsList);
	  			}
	  			else {
	  				responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
	  			}
	  		} catch (Exception e) {
	  			e.printStackTrace();
	  			responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
	  		}
	  		return responseBodyBean;
	  	}

	 @GetMapping(value="/groupWiseStaffDetails")
	 public	ResponseBodyBean<StaffDetailsBean>getgroupWiseStaffDetails(@RequestParam("sansthaKey") String sansthaKey,
	  			@RequestParam("label") String label,
	  			@RequestParam("yearName") String yearname,
	  			@RequestParam("renewAdmissionDate") String renewDate, @RequestParam("mobileNo") String mobileNo,
	  			@RequestParam("userId") String userId,@RequestParam("profileRole") String profileRole){
	  		ResponseBodyBean<StaffDetailsBean> responseBodyBean = new ResponseBodyBean<>();
	  		List<StaffDetailsBean> studentDetailsList = new ArrayList<>();
	  		try {
	  		
	  			studentDetailsList =staffDetailService.getgroupWiseStaffDetails(sansthaKey,yearname,renewDate);
	  			if(CollectionUtils.isNotEmpty(studentDetailsList)) {
	  				responseBodyBean = new ResponseBodyBean<>("200",HttpStatus.OK,studentDetailsList);
	  			}
	  			else {
	  				responseBodyBean =  new ResponseBodyBean<>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
	  			}
	  		} catch (Exception e) {
	  			e.printStackTrace();
	  			responseBodyBean =  new ResponseBodyBean<>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
	  		}
	  		return responseBodyBean;
	  	}

  
}
