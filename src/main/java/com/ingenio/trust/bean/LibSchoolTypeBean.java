package com.ingenio.trust.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LibSchoolTypeBean {

	private Integer schoolTypeId;
	
	private String SchoolType;
	
	private Integer schoolId;
	
	private Integer createdBy;
	
	private String createdDate;
	
	private Integer status;
	
	private String updatedDate;
	
	private Integer userId;
	
	private Date cDate;
	
	private String isDel;
	
	private Date delDate;
	
	private Integer deleteBy;
	
	private String isEdit;
	
	private Date editDate;
	
	private Integer editBy;
	
	private String deviceType;
	
	private String ipAddress;
	
	private String macAddress;
	
	private String sinkingFlag;
	
	private Integer fromSchoolId;
	
}
