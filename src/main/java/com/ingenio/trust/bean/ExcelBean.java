package com.ingenio.trust.bean;

import lombok.Data;

@Data
public class ExcelBean {

	private String accessionDate;
	private String status;
	private String accNo;
	private String seriesCode;
	private String classNo;
	private String author;
	private String titleName;
	private String edition;
	private String volume;
	private String publisherName;
	private String city;
	private String pubYear;
	private String pages;
	private String prePages;
	private String vendorCity;
	private String invoiceNo;
	private String invoiceDate;
	private String subSubjectName;
	private String printPrice;
	private String purPrice;
	private String rackName;

}
