package com.ingenio.trust.bean;

import java.util.Date;
import java.util.List;

public class AnnouncementRequestBean {

	private Integer yearId;
	private Integer staffId;
	private Integer userId;
	private String IPAddress;
	private String deviceType;
	private String announcementTitle;
	private String announcement;
	private String smsTitle;
	private String smsMessage;
	private Integer schoolId;
	private Integer sansthaId;
	private Integer sansthaUserId;
	private Integer announcementId;
	private Date startDate;
	private Integer apiFlag;
	private List<AnnouncementResponseBean> annoucementResponseList;
	private String teacherName;
	private String time;
	private String flag;
	private Integer sansthaKey;
	
	

	public Integer getSansthaKey() {
		return sansthaKey;
	}

	public void setSansthaKey(Integer sansthaKey) {
		this.sansthaKey = sansthaKey;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public Integer getSansthaId() {
		return sansthaId;
	}

	public void setSansthaId(Integer sansthaId) {
		this.sansthaId = sansthaId;
	}

	public Integer getSansthaUserId() {
		return sansthaUserId;
	}

	public void setSansthaUserId(Integer sansthaUserId) {
		this.sansthaUserId = sansthaUserId;
	}

	public Integer getYearId() {
		return yearId;
	}

	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getIPAddress() {
		return IPAddress;
	}

	public void setIPAddress(String iPAddress) {
		IPAddress = iPAddress;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getAnnouncementTitle() {
		return announcementTitle;
	}

	public void setAnnouncementTitle(String announcementTitle) {
		this.announcementTitle = announcementTitle;
	}

	public String getAnnouncement() {
		return announcement;
	}

	public void setAnnouncement(String announcement) {
		this.announcement = announcement;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public Integer getAnnouncementId() {
		return announcementId;
	}

	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Integer getApiFlag() {
		return apiFlag;
	}

	public void setApiFlag(Integer apiFlag) {
		this.apiFlag = apiFlag;
	}

	public List<AnnouncementResponseBean> getAnnoucementResponseList() {
		return annoucementResponseList;
	}

	public void setAnnoucementResponseList(List<AnnouncementResponseBean> annoucementResponseList) {
		this.annoucementResponseList = annoucementResponseList;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getSmsTitle() {
		return smsTitle;
	}

	public void setSmsTitle(String smsTitle) {
		this.smsTitle = smsTitle;
	}

	public String getSmsMessage() {
		return smsMessage;
	}

	public void setSmsMessage(String smsMessage) {
		this.smsMessage = smsMessage;
	}

}
