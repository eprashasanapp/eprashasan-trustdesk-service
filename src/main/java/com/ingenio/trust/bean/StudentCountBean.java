package com.ingenio.trust.bean;

import java.util.List;



public class StudentCountBean {
	
	
	private Long totalCount;
	private Long girlsCount;
	private Long boysCount;
	private String totalCountInWords;
	private String girlsCountInWords;
	private String boysCountInWords;
	private String standardName;
	private Integer standardId;
	private String divisionName;
	private Integer divisionId;
	private List<StudentCountBean> schoolWiseStudentList;
	private List<StudentCountBean> standardwiseStudentList;
	private List<StudentCountBean> divisionwiseList;
	private List<StudentDetailsBean> studentDetailsBean;
	private List<PrincipalMenuBean> principalMenuList;
	private AttendanceReportBean attendanceReportBean;
	
	private List<SummaryBean> summaryWiseList;
	private List<AgeWiseSummeryReportBean> ageWiseList;
	private List<GroupOfSchoolBean> groupWiseList;
	
	private Integer schoolid;
	private String schoolName;
	
	private String schoolLogo;
	private Integer schoolBoardId;
	private String boardName;
	private String boardType; 
	private Integer langId;
	private String langName;
	private String schoolTypeName; 
	private Integer schoolTypeId;
	private String schoolAdreess;
	private Integer stateId;
	private String state;
	private Integer districtId;
	private String district;
	private Integer talukaId;
	private String taluka;
	
	private Integer schoolLogoId;
	private String schoolLogoPath; 	
	private String shortSchoolName;
	private String longSchoolName;
	private Integer yearId;
	
	private Integer groupOfSchoolId;
	private String groupOfSchoolName;
	private Integer staffCount;
	
	private List<StudentCountBean> SummaryWiseGroupList;
	PrincipalDetailsBean principalDetailsBean; 
	List<StaffDetailsNewBean> staffDetailsBean;
	
	public List<StudentCountBean> getSummaryWiseGroupList() {
		return SummaryWiseGroupList;
	}

	public void setSummaryWiseGroupList(List<StudentCountBean> summaryWiseGroupList) {
		SummaryWiseGroupList = summaryWiseGroupList;
	}

	public StudentCountBean() {
		super();
	}	
	
	public StudentCountBean(String studentStdandard,Integer stdandardId, Long totalCount, Long boysCount, Long girlsCount) {
		super();
		this.standardName = studentStdandard;
		this.standardId=stdandardId;
		this.totalCount = totalCount;
		this.girlsCount = girlsCount;
		this.boysCount = boysCount;
		}

	public StudentCountBean(Long totalCount, Long boysCount, Long girlsCount) {
		super();
		this.totalCount = totalCount;
		this.girlsCount = girlsCount;
		this.boysCount = boysCount;
	}
	
	public StudentCountBean(Integer schoolid,String schoolName,Long totalCount, Long boysCount, Long girlsCount) {
		super();
		this.schoolName=schoolName;
		this.schoolid=schoolid;
		this.totalCount = totalCount;
		this.girlsCount = girlsCount;
		this.boysCount = boysCount;
	}
	
	public StudentCountBean(Integer groupOfSchoolId,String groupOfSchoolName,Integer schoolid,Long totalCount, Long boysCount, Long girlsCount) {
		super();
		this.groupOfSchoolId=groupOfSchoolId;
		this.groupOfSchoolName=groupOfSchoolName;
		this.schoolid=schoolid;
		this.totalCount = totalCount;
		this.girlsCount = girlsCount;
		this.boysCount = boysCount;
	}
	
	public Integer getGroupOfSchoolId() {
		return groupOfSchoolId;
	}

	public void setGroupOfSchoolId(Integer groupOfSchoolId) {
		this.groupOfSchoolId = groupOfSchoolId;
	}

	public String getGroupOfSchoolName() {
		return groupOfSchoolName;
	}

	public void setGroupOfSchoolName(String groupOfSchoolName) {
		this.groupOfSchoolName = groupOfSchoolName;
	}

	public StudentCountBean(Integer schoolid,String schoolName, String longSchoolName,Long totalCount, Long boysCount, Long girlsCount) {
		super();
		this.schoolName=schoolName;
		this.schoolid=schoolid;
		this.longSchoolName=longSchoolName;
		this.totalCount = totalCount;
		this.girlsCount = girlsCount;
		this.boysCount = boysCount;
	}
	
	
	public StudentCountBean(Integer schoolid,String schoolName, String longSchoolName,Long totalCount, Long boysCount, Long girlsCount,
			Integer schoolBoardId,String boardType, Integer langId,String langName, 
			String schoolTypeName,Integer schoolTypeId,
			String schoolAdreess,Integer stateId,String state, Integer districtId,String district,Integer talukaId,String taluka,Integer schoolLogoId,String schoolLogoPath) {
		super();
		this.schoolName=schoolName;
		this.schoolid=schoolid;
		this.longSchoolName=longSchoolName;
		this.totalCount = totalCount;
		this.girlsCount = girlsCount;
		this.boysCount = boysCount;
		this.schoolBoardId=schoolBoardId;
		this.boardType=boardType;
		this.langId=langId;
		this.langName=langName;
		this.schoolTypeName=schoolTypeName;
		this.schoolTypeId=schoolTypeId;
		this.schoolAdreess=schoolAdreess;
		this.stateId=stateId;
		this.state=state;
		this.districtId=districtId;
		this.district=district;
		this.taluka=taluka;
		this.schoolLogoId=schoolLogoId;
		this.schoolLogoPath=schoolLogoPath;
	}
	
	public StudentCountBean(String studentStdandard,Integer stdandardId, String studentDivision, Long totalCount, Long boysCount,Long girlsCount) {
		super();
		this.standardName = studentStdandard;
		this.standardId=stdandardId;
		this.divisionName = studentDivision;
		this.totalCount = totalCount;
		this.girlsCount = girlsCount;
		this.boysCount = boysCount;
		}
	
	public StudentCountBean(String studentStdandard,Integer stdandardId, String studentDivision,Integer divisionId, Long totalCount,Long boysCount, Long girlsCount) {
		super();
		this.standardName = studentStdandard;
		this.standardId=stdandardId;
		this.divisionName = studentDivision;
		this.divisionId = divisionId;
		this.totalCount = totalCount;
		this.girlsCount = girlsCount;
		this.boysCount = boysCount;
		}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	public Long getGirlsCount() {
		return girlsCount;
	}

	public void setGirlsCount(Long girlsCount) {
		this.girlsCount = girlsCount;
	}

	public Long getBoysCount() {
		return boysCount;
	}

	public void setBoysCount(Long boysCount) {
		this.boysCount = boysCount;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public List<StudentCountBean> getStandardwiseStudentList() {
		return standardwiseStudentList;
	}

	public void setStandardwiseStudentList(List<StudentCountBean> standardwiseStudentList) {
		this.standardwiseStudentList = standardwiseStudentList;
	}

	public List<StudentCountBean> getDivisionwiseList() {
		return divisionwiseList;
	}

	public void setDivisionwiseList(List<StudentCountBean> divisionwiseList) {
		this.divisionwiseList = divisionwiseList;
	}

	public List<StudentDetailsBean> getStudentDetailsBean() {
		return studentDetailsBean;
	}

	public void setStudentDetailsBean(List<StudentDetailsBean> studentDetailsBean) {
		this.studentDetailsBean = studentDetailsBean;
	}

	public List<PrincipalMenuBean> getPrincipalMenuList() {
		return principalMenuList;
	}

	public void setPrincipalMenuList(List<PrincipalMenuBean> principalMenuList) {
		this.principalMenuList = principalMenuList;
	}

	public AttendanceReportBean getAttendanceReportBean() {
		return attendanceReportBean;
	}

	public void setAttendanceReportBean(AttendanceReportBean attendanceReportBean) {
		this.attendanceReportBean = attendanceReportBean;
	}

	public Integer getSchoolid() {
		return schoolid;
	}

	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getTotalCountInWords() {
		return totalCountInWords;
	}

	public void setTotalCountInWords(String totalCountInWords) {
		this.totalCountInWords = totalCountInWords;
	}

	public String getGirlsCountInWords() {
		return girlsCountInWords;
	}

	public void setGirlsCountInWords(String girlsCountInWords) {
		this.girlsCountInWords = girlsCountInWords;
	}

	public String getBoysCountInWords() {
		return boysCountInWords;
	}

	public void setBoysCountInWords(String boysCountInWords) {
		this.boysCountInWords = boysCountInWords;
	}

	

	public String getShortSchoolName() {
		return shortSchoolName;
	}

	public void setShortSchoolName(String shortSchoolName) {
		this.shortSchoolName = shortSchoolName;
	}

	public List<StudentCountBean> getSchoolWiseStudentList() {
		return schoolWiseStudentList;
	}

	public void setSchoolWiseStudentList(List<StudentCountBean> schoolWiseStudentList) {
		this.schoolWiseStudentList = schoolWiseStudentList;
	}

	public List<AgeWiseSummeryReportBean> getAgeWiseList() {
		return ageWiseList;
	}

	public void setAgeWiseList(List<AgeWiseSummeryReportBean> ageWiseList) {
		this.ageWiseList = ageWiseList;
	}

	public Integer getYearId() {
		return yearId;
	}

	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}

	public List<GroupOfSchoolBean> getGroupWiseList() {
		return groupWiseList;
	}

	public void setGroupWiseList(List<GroupOfSchoolBean> groupWiseList) {
		this.groupWiseList = groupWiseList;
	}

	public List<SummaryBean> getSummaryWiseList() {
		return summaryWiseList;
	}

	public void setSummaryWiseList(List<SummaryBean> summaryWiseList) {
		this.summaryWiseList = summaryWiseList;
	}

	public String getLongSchoolName() {
		return longSchoolName;
	}

	public void setLongSchoolName(String longSchoolName) {
		this.longSchoolName = longSchoolName;
	}

	public PrincipalDetailsBean getPrincipalDetailsBean() {
		return principalDetailsBean;
	}

	public void setPrincipalDetailsBean(PrincipalDetailsBean principalDetailsBean) {
		this.principalDetailsBean = principalDetailsBean;
	}

	public List<StaffDetailsNewBean> getStaffDetailsBean() {
		return staffDetailsBean;
	}

	public void setStaffDetailsBean(List<StaffDetailsNewBean> staffDetailsBean) {
		this.staffDetailsBean = staffDetailsBean;
	}

	public String getSchoolLogo() {
		return schoolLogo;
	}

	public void setSchoolLogo(String schoolLogo) {
		this.schoolLogo = schoolLogo;
	}

	public String getBoardName() {
		return boardName;
	}

	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}

	public String getBoardType() {
		return boardType;
	}

	public void setBoardType(String boardType) {
		this.boardType = boardType;
	}

	

	public String getLangName() {
		return langName;
	}

	public void setLangName(String langName) {
		this.langName = langName;
	}

	public String getSchoolTypeName() {
		return schoolTypeName;
	}

	public void setSchoolTypeName(String schoolTypeName) {
		this.schoolTypeName = schoolTypeName;
	}

	public Integer getSchoolTypeId() {
		return schoolTypeId;
	}

	public void setSchoolTypeId(Integer schoolTypeId) {
		this.schoolTypeId = schoolTypeId;
	}

	public String getSchoolAdreess() {
		return schoolAdreess;
	}

	public void setSchoolAdreess(String schoolAdreess) {
		this.schoolAdreess = schoolAdreess;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getTaluka() {
		return taluka;
	}

	public void setTaluka(String taluka) {
		this.taluka = taluka;
	}

	public Integer getSchoolBoardId() {
		return schoolBoardId;
	}

	public void setSchoolBoardId(Integer schoolBoardId) {
		this.schoolBoardId = schoolBoardId;
	}

	public Integer getLangId() {
		return langId;
	}

	public void setLangId(Integer langId) {
		this.langId = langId;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public Integer getTalukaId() {
		return talukaId;
	}

	public void setTalukaId(Integer talukaId) {
		this.talukaId = talukaId;
	}

	public Integer getSchoolLogoId() {
		return schoolLogoId;
	}

	public void setSchoolLogoId(Integer schoolLogoId) {
		this.schoolLogoId = schoolLogoId;
	}

	public String getSchoolLogoPath() {
		return schoolLogoPath;
	}

	public void setSchoolLogoPath(String schoolLogoPath) {
		this.schoolLogoPath = schoolLogoPath;
	}

	public Integer getStaffCount() {
		return staffCount;
	}

	public void setStaffCount(Integer staffCount) {
		this.staffCount = staffCount;
	}

	
	
}
