package com.ingenio.trust.bean;

public class PrincipalDetailsBean {
	    
	private Integer staffId;
	private String initialName;
	private String firstName;
	private String secondName;
	private String lastName;
	private String	principalregId;
	private String  principalDateOfBirt;
	private String	principalMobileNo;
	private Integer designationId;
	private String	designationName;
	private Integer staffTypeId;
	private String staffTypeName;

	private String	principalPhoto;
	private Integer	principalAppuserID;
	private  String	principalName;
	
	
	
	public PrincipalDetailsBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PrincipalDetailsBean(Integer staffId, String initialName, String firstName, String secondName,
			String lastName, String principalregId, String principalDateOfBirt, String principalMobileNo,
			Integer designationId, String designationName, Integer staffTypeId, String staffTypeName) {
		super();
		this.staffId = staffId;
		this.initialName = initialName;
		this.firstName = firstName;
		this.secondName = secondName;
		this.lastName = lastName;
		this.principalregId = principalregId;
		this.principalDateOfBirt = principalDateOfBirt;
		this.principalMobileNo = principalMobileNo;
		this.designationId = designationId;
		this.designationName = designationName;
		this.staffTypeId = staffTypeId;
		this.staffTypeName = staffTypeName;
	}
	public String getInitialName() {
		return initialName;
	}
	public void setInitialName(String initialName) {
		this.initialName = initialName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPrincipalName() {
		return principalName;
	}
	public void setPrincipalName(String principalName) {
		this.principalName = principalName;
	}
	public String getPrincipalregId() {
		return principalregId;
	}
	public void setPrincipalregId(String principalregId) {
		this.principalregId = principalregId;
	}
	public String getPrincipalMobileNo() {
		return principalMobileNo;
	}
	public void setPrincipalMobileNo(String principalMobileNo) {
		this.principalMobileNo = principalMobileNo;
	}
	public String getPrincipalPhoto() {
		return principalPhoto;
	}
	public void setPrincipalPhoto(String principalPhoto) {
		this.principalPhoto = principalPhoto;
	}
	public Integer getPrincipalAppuserID() {
		return principalAppuserID;
	}
	public void setPrincipalAppuserID(Integer principalAppuserID) {
		this.principalAppuserID = principalAppuserID;
	}
	public String getPrincipalDateOfBirt() {
		return principalDateOfBirt;
	}
	public void setPrincipalDateOfBirt(String principalDateOfBirt) {
		this.principalDateOfBirt = principalDateOfBirt;
	}
	public Integer getDesignationId() {
		return designationId;
	}
	public void setDesignationId(Integer designationId) {
		this.designationId = designationId;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public Integer getStaffTypeId() {
		return staffTypeId;
	}
	public void setStaffTypeId(Integer staffTypeId) {
		this.staffTypeId = staffTypeId;
	}
	public String getStaffTypeName() {
		return staffTypeName;
	}
	public void setStaffTypeName(String staffTypeName) {
		this.staffTypeName = staffTypeName;
	}
	public Integer getStaffId() {
		return staffId;
	}
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	
	
}
