package com.ingenio.trust.bean;

import java.util.Date;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LibStudentAdmissionInfo {
	
	@ApiModelProperty(value = "Enter student_admission_id")
	private Integer studentAdmissionId;
	
	@NotEmpty
	@ApiModelProperty(value = "Enter schoolid (School Library of Student)")
	private Integer schoolId;
	
	@ApiModelProperty(value = "Enter address")
	private String address;
	
	@ApiModelProperty(value = "Enter adhar_number")
	private String adharNumber;
	
	@ApiModelProperty(value = "Enter created_by")
	private Integer createdBy;
	
	@ApiModelProperty(value = "Enter created_date")
	private String createdDate;
	
	@ApiModelProperty(value = "Enter dob")
	private String dob;
	
	@NotEmpty
	@ApiModelProperty(value = "Enter division_id")
	private Integer divisionId;
	
	@ApiModelProperty(value = "Enter email")
	private String email;
	
	@ApiModelProperty(value = "Enter mobile_number")
	private String mobileNumber;
	
	@NotEmpty
	@ApiModelProperty(value = "Enter registration_number")
	private String registrationNumber;
	
	@NotEmpty
	@ApiModelProperty(value = "Enter school_type_id")
	private Integer schoolTypeId;
	
	@NotEmpty
	@ApiModelProperty(value = "Enter standard_id")
	private Integer standardId;
	
	@ApiModelProperty(value = "Enter status")
	private Integer status;
	
	@ApiModelProperty(value = "Enter student_name")
	private String studentName;
	
	@ApiModelProperty(value = "Enter updated_date")
	private String updatedDate;
	
	@NotEmpty
	@ApiModelProperty(value = "Enter year_id")
	private Integer yearId;
	
	@NotEmpty
	@ApiModelProperty(value = "Enter userId")
	private Integer userId;
	
	@ApiModelProperty(value = "Enter cDate")
	private Date cDate;
	
	@ApiModelProperty(value = "Enter isDel")
	private String isDel;
	
	@ApiModelProperty(value = "Enter delDate")
	private Date delDate;
	
	@ApiModelProperty(value = "Enter deleteBy")
	private Integer deleteBy;
	
	@ApiModelProperty(value = "Enter isEdit")
	private String isEdit;
	
	@ApiModelProperty(value = "Enter editDate")
	private Date editDate;
	
	@ApiModelProperty(value = "Enter editBy")
	private Integer editBy;
	
	@ApiModelProperty(value = "Enter deviceType")
	private String deviceType;
	
	@ApiModelProperty(value = "Enter ipAddress")
	private String ipAddress;
	
	@ApiModelProperty(value = "Enter macAddress")
	private String macAddress;
	
	@ApiModelProperty(value = "Enter sinkingFlag")
	private char sinkingFlag;
	
	@NotEmpty
	@ApiModelProperty(value = "Enter studentId")
	private Integer studentId;
	
	@NotEmpty
	@ApiModelProperty(value = "Enter studentRenewId")
	private Integer studentRenewId;
	
	@ApiModelProperty(value = "Enter isGuest")
	private String isGuest;
	
	@ApiModelProperty(value = "Enter studentschoolId(current school of Student)")
	private Integer studentschoolId;
	
}
