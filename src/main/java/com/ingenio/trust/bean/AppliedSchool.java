package com.ingenio.trust.bean;

import lombok.Data;

@Data
public class AppliedSchool {

	private String id;

	private Integer fromSchoolId;

	private Integer appliedSchoolId;

	private String studentId;

	private Integer createdBy;

	private String createdDate;

	private double preference;

	private Integer approvalstatus;

	private String categoryName;

	private Integer streamName;

	private String className;

	private String updatedDate;
	
	private AppliedSchoolDetails appliedSchoolDetails;

}
