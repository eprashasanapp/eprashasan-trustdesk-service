package com.ingenio.trust.bean;

import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class LibBookAccessISBNFilterParam {

	
	@ApiParam(value = "Enter schoolid (School Library of Student)")
	private Integer schoolId;
	
	@ApiParam(value = "Enter categoryId")
	private Integer categoryId;
	
	@ApiParam(value = "Enter isbn number")
	private Integer isbnNumber;
	
	@ApiParam(value = "Enter accession No")
//	private Integer accessionNo;
	private String accessionNo;
	
	@ApiParam(value = "Number of rows per request - for pagination")
    private int size;

    @ApiParam(value = "Page number for pagination")
    private int page;
    
}
