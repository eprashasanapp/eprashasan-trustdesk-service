package com.ingenio.trust.bean;

public class SummaryMenuBean {
	
	
	private String summaryName;
	private Integer summaryMenuId;

	
	public SummaryMenuBean() {
		super();
	}


	public String getSummaryName() {
		return summaryName;
	}


	public void setSummaryName(String summaryName) {
		this.summaryName = summaryName;
	}


	public Integer getSummaryMenuId() {
		return summaryMenuId;
	}


	public void setSummaryMenuId(Integer summaryMenuId) {
		this.summaryMenuId = summaryMenuId;
	}


	public SummaryMenuBean(Integer summaryMenuId, String summaryName) {
		super();
		this.summaryName = summaryName;
		this.summaryMenuId = summaryMenuId;
	}



	
	
}
