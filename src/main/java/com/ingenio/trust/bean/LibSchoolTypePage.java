package com.ingenio.trust.bean;

import java.util.List;

import lombok.Data;

@Data
public class LibSchoolTypePage {

	private List<LibSchoolTypeBean> data;

    private Paging paging;
    
}
