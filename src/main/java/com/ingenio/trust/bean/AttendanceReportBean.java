package com.ingenio.trust.bean;

import java.util.List;

public class AttendanceReportBean {

	private Long presentCount;
	private Long absentCount;
	private Long exemptedCount;
	private String presentCountInWords;
	private String absentCountInWords;
	private String exemptedCountInWords;
	private String totalCountInWords;
	private String label;
	private Long count;
	private Long totalCount;
	private String stdandardName;
	private String divisionName;
	private Integer divisionId;
	private Integer standardId;
	private Integer schoolid;
	private String schoolName;
	private String longSchoolName;
	private Integer groupOfSchoolId;
	private String groupOfSchoolName;
	private Integer yearId;	
	private String percentAttendance;
	private List<AttendanceReportBean1> presentList;
	private List<AttendanceReportBean1> absentList;
	private List<AttendanceReportBean1> exemptedList;
	private List<AttendanceReportBean> standardList;
	private List<AttendanceReportBean> schoolList;
	private List<AttendanceReportBean> divisionList;
	private List<PrincipalMenuBean> principalMenuList;
	private List<StudentDetailsBean> presentStudentList;
	private List<StudentDetailsBean> absentStudentList;
	private List<AttendanceReportBean> GroupWiseAttendanceList;

	public AttendanceReportBean() {
		super();
	}

	public AttendanceReportBean(Long absentCount, Long presentCount, Long exemptedCount) {
		super();
		this.presentCount = presentCount;
		this.absentCount = absentCount;
		this.exemptedCount = exemptedCount;
	}

	public AttendanceReportBean(String studentStdandard, Integer standardId, Long presentCount, Long absentCount,
			Long exemptedCount) {
		super();
		this.stdandardName = studentStdandard;
		this.standardId = standardId;
		this.presentCount = presentCount;
		this.absentCount = absentCount;
		this.exemptedCount = exemptedCount;
	}

	public AttendanceReportBean(Integer groupOfSchoolId, String groupOfSchoolName, Long absentCount, Long presentCount,
			Long exemptedCount) {
		super();
		this.setGroupOfSchoolId(groupOfSchoolId);
		this.setGroupOfSchoolName(groupOfSchoolName);
		this.presentCount = presentCount;
		this.absentCount = absentCount;
		this.exemptedCount = exemptedCount;
	}
	
	public AttendanceReportBean(Integer schoolid, String schoolName,String longSchoolName, Integer groupOfSchoolId, Long absentCount, Long presentCount,
			Long exemptedCount) {
		super();
		this.schoolid = schoolid;
		this.schoolName = schoolName;
		this.longSchoolName = longSchoolName;
		this.setGroupOfSchoolId(groupOfSchoolId);
		this.presentCount = presentCount;
		this.absentCount = absentCount;
		this.exemptedCount = exemptedCount;
	}

	public AttendanceReportBean(String studentStdandard, Integer standardId, String studentDivision, Integer divisionId,
			Long presentCount, Long absentCount, Long exemptedCount) {
		super();
		this.stdandardName = studentStdandard;
		this.standardId = standardId;
		this.divisionName = studentDivision;
		this.divisionId = divisionId;
		this.presentCount = presentCount;
		this.absentCount = absentCount;
		this.exemptedCount = exemptedCount;
	}

	public Long getPresentCount() {
		return presentCount;
	}

	public void setPresentCount(Long presentCount) {
		this.presentCount = presentCount;
	}

	public Long getAbsentCount() {
		return absentCount;
	}

	public void setAbsentCount(Long absentCount) {
		this.absentCount = absentCount;
	}

	public Long getExemptedCount() {
		return exemptedCount;
	}

	public void setExemptedCount(Long exemptedCount) {
		this.exemptedCount = exemptedCount;
	}

	public String getLabel() {
		return label;
	}

	public List<AttendanceReportBean> getSchoolList() {
		return schoolList;
	}

	public void setSchoolList(List<AttendanceReportBean> schoolList) {
		this.schoolList = schoolList;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	public List<AttendanceReportBean1> getPresentList() {
		return presentList;
	}

	public void setPresentList(List<AttendanceReportBean1> presentList) {
		this.presentList = presentList;
	}

	public List<AttendanceReportBean1> getAbsentList() {
		return absentList;
	}

	public void setAbsentList(List<AttendanceReportBean1> absentList) {
		this.absentList = absentList;
	}

	public List<AttendanceReportBean1> getExemptedList() {
		return exemptedList;
	}

	public void setExemptedList(List<AttendanceReportBean1> exemptedList) {
		this.exemptedList = exemptedList;
	}

	public String getStdandardName() {
		return stdandardName;
	}

	public void setStdandardName(String stdandardName) {
		this.stdandardName = stdandardName;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public List<AttendanceReportBean> getStandardList() {
		return standardList;
	}

	public void setStandardList(List<AttendanceReportBean> standardList) {
		this.standardList = standardList;
	}

	public List<AttendanceReportBean> getDivisionList() {
		return divisionList;
	}

	public void setDivisionList(List<AttendanceReportBean> divisionList) {
		this.divisionList = divisionList;
	}

	public List<PrincipalMenuBean> getPrincipalMenuList() {
		return principalMenuList;
	}

	public void setPrincipalMenuList(List<PrincipalMenuBean> principalMenuList) {
		this.principalMenuList = principalMenuList;
	}

	public List<StudentDetailsBean> getPresentStudentList() {
		return presentStudentList;
	}

	public void setPresentStudentList(List<StudentDetailsBean> presentStudentList) {
		this.presentStudentList = presentStudentList;
	}

	public List<StudentDetailsBean> getAbsentStudentList() {
		return absentStudentList;
	}

	public void setAbsentStudentList(List<StudentDetailsBean> absentStudentList) {
		this.absentStudentList = absentStudentList;
	}

	public Integer getSchoolid() {
		return schoolid;
	}

	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getPresentCountInWords() {
		return presentCountInWords;
	}

	public void setPresentCountInWords(String presentCountInWords) {
		this.presentCountInWords = presentCountInWords;
	}

	public String getAbsentCountInWords() {
		return absentCountInWords;
	}

	public void setAbsentCountInWords(String absentCountInWords) {
		this.absentCountInWords = absentCountInWords;
	}

	public String getExemptedCountInWords() {
		return exemptedCountInWords;
	}

	public void setExemptedCountInWords(String exemptedCountInWords) {
		this.exemptedCountInWords = exemptedCountInWords;
	}

	public Integer getYearId() {
		return yearId;
	}

	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}

	public String getTotalCountInWords() {
		return totalCountInWords;
	}

	public void setTotalCountInWords(String totalCountInWords) {
		this.totalCountInWords = totalCountInWords;
	}

	public Integer getGroupOfSchoolId() {
		return groupOfSchoolId;
	}

	public void setGroupOfSchoolId(Integer groupOfSchoolId) {
		this.groupOfSchoolId = groupOfSchoolId;
	}

	public String getGroupOfSchoolName() {
		return groupOfSchoolName;
	}

	public void setGroupOfSchoolName(String groupOfSchoolName) {
		this.groupOfSchoolName = groupOfSchoolName;
	}

	public String getLongSchoolName() {
		return longSchoolName;
	}

	public void setLongSchoolName(String longSchoolName) {
		this.longSchoolName = longSchoolName;
	}

	public List<AttendanceReportBean> getGroupWiseAttendanceList() {
		return GroupWiseAttendanceList;
	}

	public void setGroupWiseAttendanceList(List<AttendanceReportBean> groupWiseAttendanceList) {
		GroupWiseAttendanceList = groupWiseAttendanceList;
	}

	public String getPercentAttendance() {
		return percentAttendance;
	}

	public void setPercentAttendance(String percentAttendance) {
		this.percentAttendance = percentAttendance;
	}


}
