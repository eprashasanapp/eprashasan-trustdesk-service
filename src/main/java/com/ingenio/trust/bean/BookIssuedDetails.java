package com.ingenio.trust.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookIssuedDetails {

	private Integer issueBookId;
	private int nameId;
	private int type;
	private int forBookType;
	private int btCardId;
	private int bookInventoryId;
	private int bookIsbnAccessId;
	private int isIssued;
	private Integer transactionNo;
	private Date issuedDate = new java.sql.Date(new java.util.Date().getTime());;
	private String returnDate;
	private Date createdDate = new java.sql.Date(new java.util.Date().getTime());;
	
	private String updatedDate;
	private int status;
	private String actualReturnDate;
	private Integer schoolId;
	private Integer userId;
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private Date delDate;
	private Integer deleteBy;
	private String isEdit = "0";
	private Date editDate;
	private Integer editBy;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag = "0";
	
	private String onlineOfline;
	private String requestedDate;
	private String lapsDate;
	private String acceptedDate;
	private String rejectedDate;
	private int requestedBy;
	private int acceptedBy;
	private int rejectedBy;
	private int issuedBy;
	private int returnBy;
	
}
