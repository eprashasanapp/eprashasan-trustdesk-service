package com.ingenio.trust.bean;

public class ReligionBean {
	
	private Integer religionId;
	private String religionName;
	
	
	public ReligionBean(Integer religionId, String religionName) {
		super();
		this.religionId = religionId;
		this.religionName = religionName;
	}


	public Integer getReligionId() {
		return religionId;
	}


	public void setReligionId(Integer religionId) {
		this.religionId = religionId;
	}


	public String getReligionName() {
		return religionName;
	}


	public void setReligionName(String religionName) {
		this.religionName = religionName;
	}

	
	
	
}
