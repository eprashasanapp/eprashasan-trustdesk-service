package com.ingenio.trust.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AppliedSchoolFilterParam {
	
	@ApiModelProperty(name = "from_school_id")
	private Integer fromSchoolId;
	
	@ApiModelProperty(name = "applied_school_id")
	private Integer appliedSchoolId;
	
	@ApiModelProperty(name = "student_id")
	private Integer studentId;
	
	@ApiModelProperty(value = "Number of rows per request - for pagination")
    private int size;

	@ApiModelProperty(value = "Page number for pagination")
    private int page;
}
