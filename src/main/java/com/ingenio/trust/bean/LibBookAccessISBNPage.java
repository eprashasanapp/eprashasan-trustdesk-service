package com.ingenio.trust.bean;

import java.util.List;

import lombok.Data;

@Data
public class LibBookAccessISBNPage {

	private List<LibBookAccessISBN> data;

    private Paging paging;
    
}
