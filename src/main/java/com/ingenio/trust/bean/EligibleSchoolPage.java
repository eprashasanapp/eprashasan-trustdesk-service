package com.ingenio.trust.bean;

import java.util.List;

import lombok.Data;

@Data
public class EligibleSchoolPage {

	private List<EligibleSchool> data;

    private Paging paging;
    
}
