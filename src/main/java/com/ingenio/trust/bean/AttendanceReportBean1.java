package com.ingenio.trust.bean;

public class AttendanceReportBean1 {
	
	private String totalCount;
	private String maleCount;
	private String femalCount;
	private String totalCountInWords;
	private String maleCountInWords;
	private String femalCountInWords;
	
	
	public String getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}
	public String getMaleCount() {
		return maleCount;
	}
	public void setMaleCount(String maleCount) {
		this.maleCount = maleCount;
	}
	public String getFemalCount() {
		return femalCount;
	}
	public void setFemalCount(String femalCount) {
		this.femalCount = femalCount;
	}
	public String getTotalCountInWords() {
		return totalCountInWords;
	}
	public void setTotalCountInWords(String totalCountInWords) {
		this.totalCountInWords = totalCountInWords;
	}
	public String getMaleCountInWords() {
		return maleCountInWords;
	}
	public void setMaleCountInWords(String maleCountInWords) {
		this.maleCountInWords = maleCountInWords;
	}
	public String getFemalCountInWords() {
		return femalCountInWords;
	}
	public void setFemalCountInWords(String femalCountInWords) {
		this.femalCountInWords = femalCountInWords;
	}
	
	
	
}
