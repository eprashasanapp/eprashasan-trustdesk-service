package com.ingenio.trust.bean;

import java.util.List;

import lombok.Data;

@Data
public class BookIssuedPage {

	private List<BookIssuedDetails> data;

    private Paging paging;
    
}
