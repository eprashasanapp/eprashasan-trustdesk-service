package com.ingenio.trust.bean;

import java.util.List;

public class FeeBean {
	
	private Integer yearId;
	private String yearName;
	private String assignedTotalInWords;
	private String assignedTotal;
	private String assignedFeeInWords;
	private String assignedFee;
	private String assignedFineInWords;
	private String assignedFine;
	private String assignedChequeBounceCharges;
	private String assignedChequeBounceChargesInWords;
	
	private String paidTotalInWords;
	private String paidTotal;
	private String paidFeeInWords;
	private String paidFee;
	private String paidFineInWords;
	private String paidFine;
	private String paidChequeBounceCharges;
	private String paidChequeBounceChargesInWords;
	private String discount;
	
	private String currentPaidTotalInWords;
	private String currentPaidTotal;
	private String currentPaidFeeInWords;
	private String currentPaidFee;
	private String currentPaidFineInWords;
	private String currentPaidFine;
	private String currentPaidChequeBounceCharges;
	private String currentPaidChequeBounceChargesInWords;
	
	private String clearanceTotalInWords;
	private String clearanceTotal;
	private String clearanceFeeInWords;
	private String clearanceFee;
	private String clearanceFineInWords;
	private String clearanceFine;
	private String clearanceChequeBounceCharges;
	private String clearanceChequeBounceChargesInWords;
	
	private String remainingTotalInWords;
	private String remainingTotal;
	private String remainingFeeInWords;
	private String remainingFee;
	private String remainingFineInWords;
	private String remainingFine;
	private String remainingChequeBounceCharges;
	private String remainingChequeBounceChargesInWords;
	
	private String paidClearanceTotal;
	private String paidClearanceTotalInWords;

	private Integer standardId;
	private String standardName;
	private String divisionId;
	private String divisionName;
	private Integer payTypeId;
	private String payTypeName;
	private Integer headId;
	private String headName;
	private Integer subheadId;
	private String subheadName;
	private String studentRegNo;
	private String rollNo;
	private String studentName;
	private String payTypeTotal;
	private Integer schoolId;
	private String schoolName;
	private String longSchoolName;
	
	private List<FeeBean> divisionList; 
	private List<FeeBean> standardList; 
	private List<FeeBean> payTypeList;
	private List<FeeBean> yearList;
	private List<FeeBean> studentDetailList;
	private List<FeeBean> groupList;
	private List<FeeBean> schoolList;
	private String groupId;
	private String groupName;
	private String otherFee;
	private String excessFee;
	
	private List<StudentDetailsBean> studentDetailsList;

	public Integer getYearId() {
		return yearId;
	}

	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}

	public String getYearName() {
		return yearName;
	}

	public void setYearName(String yearName) {
		this.yearName = yearName;
	}

	public String getAssignedTotalInWords() {
		return assignedTotalInWords;
	}

	public void setAssignedTotalInWords(String assignedTotalInWords) {
		this.assignedTotalInWords = assignedTotalInWords;
	}

	public String getAssignedTotal() {
		return assignedTotal;
	}

	public void setAssignedTotal(String assignedTotal) {
		this.assignedTotal = assignedTotal;
	}

	public String getAssignedFeeInWords() {
		return assignedFeeInWords;
	}

	public void setAssignedFeeInWords(String assignedFeeInWords) {
		this.assignedFeeInWords = assignedFeeInWords;
	}

	public String getAssignedFee() {
		return assignedFee;
	}

	public void setAssignedFee(String assignedFee) {
		this.assignedFee = assignedFee;
	}

	public String getAssignedFineInWords() {
		return assignedFineInWords;
	}

	public void setAssignedFineInWords(String assignedFineInWords) {
		this.assignedFineInWords = assignedFineInWords;
	}

	public String getAssignedFine() {
		return assignedFine;
	}

	public void setAssignedFine(String assignedFine) {
		this.assignedFine = assignedFine;
	}

	public String getAssignedChequeBounceCharges() {
		return assignedChequeBounceCharges;
	}

	public void setAssignedChequeBounceCharges(String assignedChequeBounceCharges) {
		this.assignedChequeBounceCharges = assignedChequeBounceCharges;
	}

	public String getAssignedChequeBounceChargesInWords() {
		return assignedChequeBounceChargesInWords;
	}

	public void setAssignedChequeBounceChargesInWords(String assignedChequeBounceChargesInWords) {
		this.assignedChequeBounceChargesInWords = assignedChequeBounceChargesInWords;
	}

	public String getPaidTotalInWords() {
		return paidTotalInWords;
	}

	public void setPaidTotalInWords(String paidTotalInWords) {
		this.paidTotalInWords = paidTotalInWords;
	}

	public String getPaidTotal() {
		return paidTotal;
	}

	public void setPaidTotal(String paidTotal) {
		this.paidTotal = paidTotal;
	}

	public String getPaidFeeInWords() {
		return paidFeeInWords;
	}

	public void setPaidFeeInWords(String paidFeeInWords) {
		this.paidFeeInWords = paidFeeInWords;
	}

	public String getPaidFee() {
		return paidFee;
	}

	public void setPaidFee(String paidFee) {
		this.paidFee = paidFee;
	}

	public String getPaidFineInWords() {
		return paidFineInWords;
	}

	public void setPaidFineInWords(String paidFineInWords) {
		this.paidFineInWords = paidFineInWords;
	}

	public String getPaidFine() {
		return paidFine;
	}

	public void setPaidFine(String paidFine) {
		this.paidFine = paidFine;
	}

	public String getPaidChequeBounceCharges() {
		return paidChequeBounceCharges;
	}

	public void setPaidChequeBounceCharges(String paidChequeBounceCharges) {
		this.paidChequeBounceCharges = paidChequeBounceCharges;
	}

	public String getPaidChequeBounceChargesInWords() {
		return paidChequeBounceChargesInWords;
	}

	public void setPaidChequeBounceChargesInWords(String paidChequeBounceChargesInWords) {
		this.paidChequeBounceChargesInWords = paidChequeBounceChargesInWords;
	}

	public String getCurrentPaidTotalInWords() {
		return currentPaidTotalInWords;
	}

	public void setCurrentPaidTotalInWords(String currentPaidTotalInWords) {
		this.currentPaidTotalInWords = currentPaidTotalInWords;
	}

	public String getCurrentPaidTotal() {
		return currentPaidTotal;
	}

	public void setCurrentPaidTotal(String currentPaidTotal) {
		this.currentPaidTotal = currentPaidTotal;
	}

	public String getCurrentPaidFeeInWords() {
		return currentPaidFeeInWords;
	}

	public void setCurrentPaidFeeInWords(String currentPaidFeeInWords) {
		this.currentPaidFeeInWords = currentPaidFeeInWords;
	}

	public String getCurrentPaidFee() {
		return currentPaidFee;
	}

	public void setCurrentPaidFee(String currentPaidFee) {
		this.currentPaidFee = currentPaidFee;
	}

	public String getCurrentPaidFineInWords() {
		return currentPaidFineInWords;
	}

	public void setCurrentPaidFineInWords(String currentPaidFineInWords) {
		this.currentPaidFineInWords = currentPaidFineInWords;
	}

	public String getCurrentPaidFine() {
		return currentPaidFine;
	}

	public void setCurrentPaidFine(String currentPaidFine) {
		this.currentPaidFine = currentPaidFine;
	}

	public String getCurrentPaidChequeBounceCharges() {
		return currentPaidChequeBounceCharges;
	}

	public void setCurrentPaidChequeBounceCharges(String currentPaidChequeBounceCharges) {
		this.currentPaidChequeBounceCharges = currentPaidChequeBounceCharges;
	}

	public String getCurrentPaidChequeBounceChargesInWords() {
		return currentPaidChequeBounceChargesInWords;
	}

	public void setCurrentPaidChequeBounceChargesInWords(String currentPaidChequeBounceChargesInWords) {
		this.currentPaidChequeBounceChargesInWords = currentPaidChequeBounceChargesInWords;
	}

	public String getClearanceTotalInWords() {
		return clearanceTotalInWords;
	}

	public void setClearanceTotalInWords(String clearanceTotalInWords) {
		this.clearanceTotalInWords = clearanceTotalInWords;
	}

	public String getClearanceTotal() {
		return clearanceTotal;
	}

	public void setClearanceTotal(String clearanceTotal) {
		this.clearanceTotal = clearanceTotal;
	}

	public String getClearanceFeeInWords() {
		return clearanceFeeInWords;
	}

	public void setClearanceFeeInWords(String clearanceFeeInWords) {
		this.clearanceFeeInWords = clearanceFeeInWords;
	}

	public String getClearanceFee() {
		return clearanceFee;
	}

	public void setClearanceFee(String clearanceFee) {
		this.clearanceFee = clearanceFee;
	}

	public String getClearanceFineInWords() {
		return clearanceFineInWords;
	}

	public void setClearanceFineInWords(String clearanceFineInWords) {
		this.clearanceFineInWords = clearanceFineInWords;
	}

	public String getClearanceFine() {
		return clearanceFine;
	}

	public void setClearanceFine(String clearanceFine) {
		this.clearanceFine = clearanceFine;
	}

	public String getClearanceChequeBounceCharges() {
		return clearanceChequeBounceCharges;
	}

	public void setClearanceChequeBounceCharges(String clearanceChequeBounceCharges) {
		this.clearanceChequeBounceCharges = clearanceChequeBounceCharges;
	}

	public String getClearanceChequeBounceChargesInWords() {
		return clearanceChequeBounceChargesInWords;
	}

	public void setClearanceChequeBounceChargesInWords(String clearanceChequeBounceChargesInWords) {
		this.clearanceChequeBounceChargesInWords = clearanceChequeBounceChargesInWords;
	}

	public String getRemainingTotalInWords() {
		return remainingTotalInWords;
	}

	public void setRemainingTotalInWords(String remainingTotalInWords) {
		this.remainingTotalInWords = remainingTotalInWords;
	}

	public String getRemainingTotal() {
		return remainingTotal;
	}

	public void setRemainingTotal(String remainingTotal) {
		this.remainingTotal = remainingTotal;
	}

	public String getRemainingFeeInWords() {
		return remainingFeeInWords;
	}

	public void setRemainingFeeInWords(String remainingFeeInWords) {
		this.remainingFeeInWords = remainingFeeInWords;
	}

	public String getRemainingFee() {
		return remainingFee;
	}

	public void setRemainingFee(String remainingFee) {
		this.remainingFee = remainingFee;
	}

	public String getRemainingFineInWords() {
		return remainingFineInWords;
	}

	public void setRemainingFineInWords(String remainingFineInWords) {
		this.remainingFineInWords = remainingFineInWords;
	}

	public String getRemainingFine() {
		return remainingFine;
	}

	public void setRemainingFine(String remainingFine) {
		this.remainingFine = remainingFine;
	}

	public String getRemainingChequeBounceCharges() {
		return remainingChequeBounceCharges;
	}

	public void setRemainingChequeBounceCharges(String remainingChequeBounceCharges) {
		this.remainingChequeBounceCharges = remainingChequeBounceCharges;
	}

	public String getRemainingChequeBounceChargesInWords() {
		return remainingChequeBounceChargesInWords;
	}

	public void setRemainingChequeBounceChargesInWords(String remainingChequeBounceChargesInWords) {
		this.remainingChequeBounceChargesInWords = remainingChequeBounceChargesInWords;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public String getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(String divisionId) {
		this.divisionId = divisionId;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public Integer getPayTypeId() {
		return payTypeId;
	}

	public void setPayTypeId(Integer payTypeId) {
		this.payTypeId = payTypeId;
	}

	public String getPayTypeName() {
		return payTypeName;
	}

	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	public Integer getHeadId() {
		return headId;
	}

	public void setHeadId(Integer headId) {
		this.headId = headId;
	}

	public String getHeadName() {
		return headName;
	}

	public void setHeadName(String headName) {
		this.headName = headName;
	}

	public Integer getSubheadId() {
		return subheadId;
	}

	public void setSubheadId(Integer subheadId) {
		this.subheadId = subheadId;
	}

	public String getSubheadName() {
		return subheadName;
	}

	public void setSubheadName(String subheadName) {
		this.subheadName = subheadName;
	}

	public String getStudentRegNo() {
		return studentRegNo;
	}

	public void setStudentRegNo(String studentRegNo) {
		this.studentRegNo = studentRegNo;
	}

	public String getRollNo() {
		return rollNo;
	}

	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getPayTypeTotal() {
		return payTypeTotal;
	}

	public void setPayTypeTotal(String payTypeTotal) {
		this.payTypeTotal = payTypeTotal;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public List<FeeBean> getDivisionList() {
		return divisionList;
	}

	public void setDivisionList(List<FeeBean> divisionList) {
		this.divisionList = divisionList;
	}

	public List<FeeBean> getStandardList() {
		return standardList;
	}

	public void setStandardList(List<FeeBean> standardList) {
		this.standardList = standardList;
	}

	public List<FeeBean> getPayTypeList() {
		return payTypeList;
	}

	public void setPayTypeList(List<FeeBean> payTypeList) {
		this.payTypeList = payTypeList;
	}

	public List<FeeBean> getYearList() {
		return yearList;
	}

	public void setYearList(List<FeeBean> yearList) {
		this.yearList = yearList;
	}

	public List<FeeBean> getStudentDetailList() {
		return studentDetailList;
	}

	public void setStudentDetailList(List<FeeBean> studentDetailList) {
		this.studentDetailList = studentDetailList;
	}

	public List<FeeBean> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<FeeBean> groupList) {
		this.groupList = groupList;
	}

	public List<FeeBean> getSchoolList() {
		return schoolList;
	}

	public void setSchoolList(List<FeeBean> schoolList) {
		this.schoolList = schoolList;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<StudentDetailsBean> getStudentDetailsList() {
		return studentDetailsList;
	}

	public void setStudentDetailsList(List<StudentDetailsBean> studentDetailsList) {
		this.studentDetailsList = studentDetailsList;
	}

	public String getPaidClearanceTotal() {
		return paidClearanceTotal;
	}

	public void setPaidClearanceTotal(String paidClearanceTotal) {
		this.paidClearanceTotal = paidClearanceTotal;
	}

	public String getPaidClearanceTotalInWords() {
		return paidClearanceTotalInWords;
	}

	public void setPaidClearanceTotalInWords(String paidClearanceTotalInWords) {
		this.paidClearanceTotalInWords = paidClearanceTotalInWords;
	}

	public String getLongSchoolName() {
		return longSchoolName;
	}

	public void setLongSchoolName(String longSchoolName) {
		this.longSchoolName = longSchoolName;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getOtherFee() {
		return otherFee;
	}

	public void setOtherFee(String otherFee) {
		this.otherFee = otherFee;
	}

	public String getExcessFee() {
		return excessFee;
	}

	public void setExcessFee(String excessFee) {
		this.excessFee = excessFee;
	}

}
