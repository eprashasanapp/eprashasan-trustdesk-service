package com.ingenio.trust.bean;

import java.util.Date;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BTCardRecordDetails {
	
	@ApiModelProperty(value = "Enter btCardRecordId")
	private Integer btCardRecordId;
	
	@NotEmpty
	@ApiModelProperty(value = "Enter schoolid (School Library of Student)")
	private Integer schoolId;
	
	private Integer schoolTypeId;
	
	@ApiModelProperty(value = "Enter btCardId")
	private String btCardId;
	
	@ApiModelProperty(value = "Enter btNumber")
	private String btNumber;
	
	@ApiModelProperty(value = "Enter created_by")
	private Integer createdBy;
	
	@ApiModelProperty(value = "Enter created_date")
	private String createdDate;
	
	@ApiModelProperty(value = "Enter fees")
	private double fees;
	
	@NotEmpty
	@ApiModelProperty(value = "Enter nameId")
	@JsonProperty("libAdmissionId")
	private Integer nameId;
	
	@ApiModelProperty(value = "Enter securityDeposit")
	private String securityDeposit;
	
	@ApiModelProperty(value = "Enter type")
	private Integer type;
	
	@NotEmpty
	@ApiModelProperty(value = "Enter validity")
	private String validity;
	
	@NotEmpty
	@ApiModelProperty(value = "Enter standard_id")
	private Integer standardId;
	
	@ApiModelProperty(value = "Enter status")
	private Integer status;
	
	@ApiModelProperty(value = "Enter student_name")
	private String studentName;
	
	@ApiModelProperty(value = "Enter updated_date")
	private String updatedDate;
	
	@NotEmpty
	@ApiModelProperty(value = "Enter userId")
	private Integer userId;
	
	@ApiModelProperty(value = "Enter cDate")
	private Date cDate;
	
	@ApiModelProperty(value = "Enter isDel")
	private String isDel;
	
	@ApiModelProperty(value = "Enter delDate")
	private Date delDate;
	
	@ApiModelProperty(value = "Enter deleteBy")
	private Integer deleteBy;
	
	@ApiModelProperty(value = "Enter isEdit")
	private String isEdit;
	
	@ApiModelProperty(value = "Enter editDate")
	private Date editDate;
	
	@ApiModelProperty(value = "Enter editBy")
	private Integer editBy;
	
	@ApiModelProperty(value = "Enter deviceType")
	private char deviceType;
	
	@ApiModelProperty(value = "Enter ipAddress")
	private String ipAddress;
	
	@ApiModelProperty(value = "Enter macAddress")
	private String macAddress;
	
	@ApiModelProperty(value = "Enter sinkingFlag")
	private String sinkingFlag;
	
}
