package com.ingenio.trust.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LibBookAccessISBN {

	private Integer bookAccessISBNId;

	private Integer schoolId;

	private String accessNumber;
	
	private String categoryId;

	private LibBookInventory bookDetails;

	private String isbnNumber;

	private Integer type;

	private Integer status;

	private Integer userId;

	private Date cDate;

	private String isDel;

	private Date delDate;

	private Integer deleteBy;

	private String isEdit;

	private Date editDate;

	private Integer editBy;

	private String deviceType;

	private String ipAddress;

	private String macAddress;

	private String sinkingFlag;
}
