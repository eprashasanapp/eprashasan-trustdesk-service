package com.ingenio.trust.bean;

import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class BookIssuedFilterParam {

	
	@ApiParam(value = "Enter schoolid (School Library of Student)")
	private Integer schoolId;
	
	@ApiParam(value = "Enter nameId")
	private Integer nameId;
	
	@ApiParam(value = "Enter accession No")
	private Integer accessionNo;
	
	@ApiParam(value = "Enter bookInventoryId")
	private Integer bookInventoryId;
	
	@ApiParam(value = "Number of rows per request - for pagination")
    private int size;

    @ApiParam(value = "Page number for pagination")
    private int page;
    
}
