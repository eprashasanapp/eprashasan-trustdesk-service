package com.ingenio.trust.bean;

import java.util.List;

public class StudentDetailsBean {
	
	
	private String studentName;
	private String studentRegNo;
	private String studentGender;
	private String studentCast;
	private String studentCategory;
	private String studentReligion;
	private Integer studentStatus;
	private String studentConcession;
	private Integer renewStudentId;
	private String studentStdandard;
	private String studentDivision;
	private String studentYear;
	private Integer studentRollNo;
	private Integer studentgrBookId;
	private String studentPhoto;
	
	private List <StudentDetailsBean> presentStudentList;
	private List <StudentDetailsBean> absentStudentList;

	
	public StudentDetailsBean() {
		super();
	}

	public StudentDetailsBean(String studentName, String studentRegNo, String studentGender, String studetnCast,
			String studentCategory, String studentReligion, Integer studentStatus, String studentConcession,
			Integer renewStudentId, String studentStdandard, String studetnDivision, String studetnYear) {
		super();
		this.studentName = studentName;
		this.studentRegNo = studentRegNo;
		this.studentGender = studentGender;
		this.studentCast = studetnCast;
		this.studentCategory = studentCategory;
		this.studentReligion = studentReligion;
		this.studentStatus = studentStatus;
		this.studentConcession = studentConcession;
		this.renewStudentId = renewStudentId;
		this.studentStdandard = studentStdandard;
		this.studentDivision = studetnDivision;
		this.studentYear = studetnYear;

	}
	
	
	public StudentDetailsBean(String studentName, String studentRegNo, String studentGender, String studetnCast,
			 Integer studentStatus, Integer studentgrBookId, String studentConcession,
			Integer renewStudentId,String studentStdandard, String studetnDivision, String studetnYear,Integer studentRollNo) {
		super();
		this.studentName = studentName;
		this.studentRegNo = studentRegNo;
		this.studentGender = studentGender;
		this.studentCast = studetnCast;
		this.studentStatus = studentStatus;
		this.studentgrBookId =studentgrBookId;
		this.studentConcession = studentConcession;
		this.renewStudentId = renewStudentId;
		this.studentStdandard = studentStdandard;
		this.studentDivision = studetnDivision;
		this.studentYear = studetnYear;
		this.studentRollNo=studentRollNo;
	}
	
	
	public StudentDetailsBean(String studentName, String studentRegNo,Integer studentRollNo,Integer studentgrBookId) {
		super();
		this.studentName = studentName;
		this.studentRegNo = studentRegNo;
		this.studentRollNo=studentRollNo;
		this.studentgrBookId=studentgrBookId;
	}

	
	
	
	public StudentDetailsBean(String studentName) {
		super();
		this.studentName = studentName;
		}
	
	
	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentRegNo() {
		return studentRegNo;
	}

	public void setStudentRegNo(String studentRegNo) {
		this.studentRegNo = studentRegNo;
	}

	public String getStudentGender() {
		return studentGender;
	}

	public void setStudentGender(String studentGender) {
		this.studentGender = studentGender;
	}

	public String getStudetnCast() {
		return studentCast;
	}

	public void setStudetnCast(String studetnCast) {
		this.studentCast = studetnCast;
	}

	public String getStudentCategory() {
		return studentCategory;
	}

	public void setStudentCategory(String studentCategory) {
		this.studentCategory = studentCategory;
	}

	public String getStudentReligion() {
		return studentReligion;
	}

	public void setStudentReligion(String studentReligion) {
		this.studentReligion = studentReligion;
	}

	public Integer getStudentStatus() {
		return studentStatus;
	}

	public void setStudentStatus(Integer studentStatus) {
		this.studentStatus = studentStatus;
	}

	public String getStudentConcession() {
		return studentConcession;
	}

	public void setStudentConcession(String studentConcession) {
		this.studentConcession = studentConcession;
	}

	public Integer getRenewStudentId() {
		return renewStudentId;
	}

	public void setRenewStudentId(Integer renewStudentId) {
		this.renewStudentId = renewStudentId;
	}

	public String getStudentStdandard() {
		return studentStdandard;
	}

	public void setStudentStdandard(String studentStdandard) {
		this.studentStdandard = studentStdandard;
	}

	public String getStudetnDivision() {
		return studentDivision;
	}

	public void setStudetnDivision(String studetnDivision) {
		this.studentDivision = studetnDivision;
	}

	public String getStudetnYear() {
		return studentYear;
	}

	public void setStudetnYear(String studetnYear) {
		this.studentYear = studetnYear;
	}

	public Integer getStudentRollNo() {
		return studentRollNo;
	}

	public void setStudentRollNo(Integer studentRollNo) {
		this.studentRollNo = studentRollNo;
	}

	public List<StudentDetailsBean> getPresentStudentList() {
		return presentStudentList;
	}

	public void setPresentStudentList(List<StudentDetailsBean> presentStudentList) {
		this.presentStudentList = presentStudentList;
	}

	public List<StudentDetailsBean> getAbsentStudentList() {
		return absentStudentList;
	}

	public void setAbsentStudentList(List<StudentDetailsBean> absentStudentList) {
		this.absentStudentList = absentStudentList;
	}

	public Integer getStudentgrBookId() {
		return studentgrBookId;
	}

	public void setStudentgrBookId(Integer studentgrBookId) {
		this.studentgrBookId = studentgrBookId;
	}

	public String getStudentPhoto() {
		return studentPhoto;
	}

	public void setStudentPhoto(String studentPhoto) {
		this.studentPhoto = studentPhoto;
	}
	
	
	
}
