package com.ingenio.trust.bean;

import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class LibStudentAdmissionFilterParam {

	@ApiParam(value = "Enter student_admission_id")
	private Integer studentAdmissionId;
	
	@ApiParam(value = "Enter schoolid (School Library of Student)")
	private Integer schoolId;
	
	@ApiParam(value = "Enter studentId")
	private Integer studentId;
	
	@ApiParam(value = "Enter studentRenewId")
	private Integer studentRenewId;
	
	@ApiParam(value = "Enter isGuest")
	private Boolean isGuest;
	
	@ApiParam(value = "Enter studentschoolId(current school of Student)")
	private Integer studentschoolId;
	
	@ApiParam(value = "Enter status")
	private Integer status;
	
	@ApiParam(value = "Enter school_type_id")
	private Integer schoolTypeId;
	
	@ApiParam(value = "Number of rows per request - for pagination")
    private int size;

    @ApiParam(value = "Page number for pagination")
    private int page;
	
	
}
