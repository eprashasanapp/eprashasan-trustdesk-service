package com.ingenio.trust.bean;

import java.util.List;

import org.springframework.http.HttpStatus;

public class ResponseBodyBean<T> {
	
	private String statusCode;
	private HttpStatus stausMessage;
	private List<T> responseList;
	private T responseData;

	
	public ResponseBodyBean() {
		super();
	}

	public ResponseBodyBean(String statusCode, HttpStatus stausMessage, List<T> responseList) {
		super();
		this.statusCode = statusCode;
		this.stausMessage = stausMessage;
		this.responseList = responseList;
	}
	
	public ResponseBodyBean(String statusCode, HttpStatus stausMessage, T responseData) {
		super();
		this.statusCode = statusCode;
		this.stausMessage = stausMessage;
		this.responseData = responseData;
	}

	
	public List<T> getResponseList() {
		return responseList;
	}
	
	public void setResponseList(List<T> responseList) {
		this.responseList = responseList;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public HttpStatus getStausMessage() {
		return stausMessage;
	}

	public void setStausMessage(HttpStatus stausMessage) {
		this.stausMessage = stausMessage;
	}

	public T getResponseData() {
		return responseData;
	}

	public void setResponseData(T responseData) {
		this.responseData = responseData;
	}
}
