package com.ingenio.trust.bean;

public class GroupOfSchoolBean {

	private Integer groupOfSchoolId;
	private String groupOfSchoolName;
	private Long totalCount;
	private Long girlsCount;
	private Long boysCount;
	private String totalCountInWords;
	private String girlsCountInWords;
	private String boysCountInWords;
	private String presentCountInWords;
	private String absentCountInWords;
	private Long presentCount;
	private Long absentCount;
	private Long exemptedCount;
	
	

	public Long getExemptedCount() {
		return exemptedCount;
	}

	public void setExemptedCount(Long exemptedCount) {
		this.exemptedCount = exemptedCount;
	}

	public GroupOfSchoolBean() {
		super();
	}

	public GroupOfSchoolBean(Integer groupOfSchoolId, String groupOfSchoolName, Long totalCount, Long boysCount,
			Long girlsCount) {
		super();
		this.groupOfSchoolId = groupOfSchoolId;
		this.groupOfSchoolName = groupOfSchoolName;
		this.totalCount = totalCount;
		this.girlsCount = girlsCount;
		this.boysCount = boysCount;

	}
	


	public Integer getGroupOfSchoolId() {
		return groupOfSchoolId;
	}

	public void setGroupOfSchoolId(Integer groupOfSchoolId) {
		this.groupOfSchoolId = groupOfSchoolId;
	}

	public String getGroupOfSchoolName() {
		return groupOfSchoolName;
	}

	public void setGroupOfSchoolName(String groupOfSchoolName) {
		this.groupOfSchoolName = groupOfSchoolName;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	public Long getGirlsCount() {
		return girlsCount;
	}

	public void setGirlsCount(Long girlsCount) {
		this.girlsCount = girlsCount;
	}

	public Long getBoysCount() {
		return boysCount;
	}

	public void setBoysCount(Long boysCount) {
		this.boysCount = boysCount;
	}

	public String getTotalCountInWords() {
		return totalCountInWords;
	}

	public void setTotalCountInWords(String totalCountInWords) {
		this.totalCountInWords = totalCountInWords;
	}

	public String getGirlsCountInWords() {
		return girlsCountInWords;
	}

	public void setGirlsCountInWords(String girlsCountInWords) {
		this.girlsCountInWords = girlsCountInWords;
	}

	public String getBoysCountInWords() {
		return boysCountInWords;
	}

	public void setBoysCountInWords(String boysCountInWords) {
		this.boysCountInWords = boysCountInWords;
	}

	public String getPresentCountInWords() {
		return presentCountInWords;
	}

	public void setPresentCountInWords(String presentCountInWords) {
		this.presentCountInWords = presentCountInWords;
	}

	public String getAbsentCountInWords() {
		return absentCountInWords;
	}

	public void setAbsentCountInWords(String absentCountInWords) {
		this.absentCountInWords = absentCountInWords;
	}

	public Long getPresentCount() {
		return presentCount;
	}

	public void setPresentCount(Long presentCount) {
		this.presentCount = presentCount;
	}

	public Long getAbsentCount() {
		return absentCount;
	}

	public void setAbsentCount(Long absentCount) {
		this.absentCount = absentCount;
	}
	

}