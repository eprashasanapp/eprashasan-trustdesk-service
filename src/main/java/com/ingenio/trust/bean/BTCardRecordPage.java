package com.ingenio.trust.bean;

import java.util.List;

import lombok.Data;

@Data
public class BTCardRecordPage {

	private List<BTCardRecordDetails> data;

    private Paging paging;
    
}
