package com.ingenio.trust.bean;

import java.util.List;

import lombok.Data;
@Data
public class LibStudentAdmissionRequest {

	List<LibStudentAdmissionInfo> studentsInfo;
}
