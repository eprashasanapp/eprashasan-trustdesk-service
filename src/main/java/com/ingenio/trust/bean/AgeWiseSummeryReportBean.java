package com.ingenio.trust.bean;


public class AgeWiseSummeryReportBean {
	
	
	private String standardName;
	private Integer standardId;
	
	private String divisionName;
	private Integer divisionId;
	

	private String gender;
	private String totalCount;
	private String male;
	private String female;
	
	
	private String age1;
	private String age2;
	private String age3;	
	private String age4;
	private String age5;
	private String age6;
	private String age7;
	private String age8;
	private String age9;
	private String age10;
	private String age11;
	private String age12;
	private String age13;
	private String age14;
	private String age15;
	private String age16;
	private String age17;
	private String age18;
	private String age19;
	private String age20;
	private String age21;
	private String age22;
	private String age23;
	private String age24;
	private String age25;
	
	
	private String age0M;
	private String age1M;
	private String age2M;
	private String age3M;
	private String age4M;
	private String age5M;
	private String age6M;
	private String age7M;
	private String age8M;
	private String age9M;
	private String age10M;
	private String age11M;
	private String age12M;
	private String age13M;
	private String age14M;
	private String age15M;
	private String age16M;
	private String age17M;
	private String age18M;
	private String age19M;
	private String age20M;
	private String age21M;
	private String age22M;
	private String age23M;
	private String age24M;
	private String age25M;
	
	
	private String age0F;
	private String age1F;
	private String age2F;
	private String age3F;
	private String age4F;
	private String age5F;
	private String age6F;
	private String age7F;
	private String age8F;
	private String age9F;
	private String age10F;
	private String age11F;
	private String age12F;
	private String age13F;
	private String age14F;
	private String age15F;
	private String age16F;
	private String age17F;
	private String age18F;
	private String age19F;
	private String age20F;
	private String age21F;
	private String age22F;
	private String age23F;
	private String age24F;
	private String age25F;
	
	private String age0MF;
	private String age1MF;
	private String age2MF;
	private String age3MF;
	private String age4MF;
	private String age5MF;
	private String age6MF;
	private String age7MF;
	private String age8MF;
	private String age9MF;
	private String age10MF;
	private String age11MF;
	private String age12MF;
	private String age13MF;
	private String age14MF;
	private String age15MF;
	private String age16MF;
	private String age17MF;
	private String age18MF;
	private String age19MF;
	private String age20MF;
	private String age21MF;
	private String age22MF;
	private String age23MF;
	private String age24MF;
	private String age25MF;
	
	private String age;
	private String bdate;
	
	private Integer standardPriority;
	
	
	
	
	public Integer getStandardPriority() {
		return standardPriority;
	}
	public void setStandardPriority(Integer standardPriority) {
		this.standardPriority = standardPriority;
	}
	public String getAge1() {
		return age1;
	}
	public void setAge1(String age1) {
		this.age1 = age1;
	}
	public String getAge2() {
		return age2;
	}
	public void setAge2(String age2) {
		this.age2 = age2;
	}
	public String getAge3() {
		return age3;
	}
	public void setAge3(String age3) {
		this.age3 = age3;
	}
	public String getAge4() {
		return age4;
	}
	public void setAge4(String age4) {
		this.age4 = age4;
	}
	public String getAge19() {
		return age19;
	}
	public void setAge19(String age19) {
		this.age19 = age19;
	}
	public String getAge20() {
		return age20;
	}
	public void setAge20(String age20) {
		this.age20 = age20;
	}
	public String getAge21() {
		return age21;
	}
	public void setAge21(String age21) {
		this.age21 = age21;
	}
	public String getAge22() {
		return age22;
	}
	public void setAge22(String age22) {
		this.age22 = age22;
	}
	public String getAge23() {
		return age23;
	}
	public void setAge23(String age23) {
		this.age23 = age23;
	}
	public String getAge24() {
		return age24;
	}
	public void setAge24(String age24) {
		this.age24 = age24;
	}
	public String getAge25() {
		return age25;
	}
	public void setAge25(String age25) {
		this.age25 = age25;
	}
	public String getAge1M() {
		return age1M;
	}
	public void setAge1M(String age1m) {
		age1M = age1m;
	}
	public String getAge2M() {
		return age2M;
	}
	public void setAge2M(String age2m) {
		age2M = age2m;
	}
	public String getAge3M() {
		return age3M;
	}
	public void setAge3M(String age3m) {
		age3M = age3m;
	}
	public String getAge4M() {
		return age4M;
	}
	public void setAge4M(String age4m) {
		age4M = age4m;
	}
	public String getAge19M() {
		return age19M;
	}
	public void setAge19M(String age19m) {
		age19M = age19m;
	}
	public String getAge20M() {
		return age20M;
	}
	public void setAge20M(String age20m) {
		age20M = age20m;
	}
	public String getAge21M() {
		return age21M;
	}
	public void setAge21M(String age21m) {
		age21M = age21m;
	}
	public String getAge22M() {
		return age22M;
	}
	public void setAge22M(String age22m) {
		age22M = age22m;
	}
	public String getAge23M() {
		return age23M;
	}
	public void setAge23M(String age23m) {
		age23M = age23m;
	}
	public String getAge24M() {
		return age24M;
	}
	public void setAge24M(String age24m) {
		age24M = age24m;
	}
	public String getAge25M() {
		return age25M;
	}
	public void setAge25M(String age25m) {
		age25M = age25m;
	}
	public String getAge1F() {
		return age1F;
	}
	public void setAge1F(String age1f) {
		age1F = age1f;
	}
	public String getAge2F() {
		return age2F;
	}
	public void setAge2F(String age2f) {
		age2F = age2f;
	}
	public String getAge3F() {
		return age3F;
	}
	public void setAge3F(String age3f) {
		age3F = age3f;
	}
	public String getAge4F() {
		return age4F;
	}
	public void setAge4F(String age4f) {
		age4F = age4f;
	}
	public String getAge19F() {
		return age19F;
	}
	public void setAge19F(String age19f) {
		age19F = age19f;
	}
	public String getAge20F() {
		return age20F;
	}
	public void setAge20F(String age20f) {
		age20F = age20f;
	}
	public String getAge21F() {
		return age21F;
	}
	public void setAge21F(String age21f) {
		age21F = age21f;
	}
	public String getAge22F() {
		return age22F;
	}
	public void setAge22F(String age22f) {
		age22F = age22f;
	}
	public String getAge23F() {
		return age23F;
	}
	public void setAge23F(String age23f) {
		age23F = age23f;
	}
	public String getAge24F() {
		return age24F;
	}
	public void setAge24F(String age24f) {
		age24F = age24f;
	}
	public String getAge25F() {
		return age25F;
	}
	public void setAge25F(String age25f) {
		age25F = age25f;
	}
	public String getAge1MF() {
		return age1MF;
	}
	public void setAge1MF(String age1mf) {
		age1MF = age1mf;
	}
	public String getAge2MF() {
		return age2MF;
	}
	public void setAge2MF(String age2mf) {
		age2MF = age2mf;
	}
	public String getAge3MF() {
		return age3MF;
	}
	public void setAge3MF(String age3mf) {
		age3MF = age3mf;
	}
	public String getAge4MF() {
		return age4MF;
	}
	public void setAge4MF(String age4mf) {
		age4MF = age4mf;
	}
	public String getAge19MF() {
		return age19MF;
	}
	public void setAge19MF(String age19mf) {
		age19MF = age19mf;
	}
	public String getAge20MF() {
		return age20MF;
	}
	public void setAge20MF(String age20mf) {
		age20MF = age20mf;
	}
	public String getAge21MF() {
		return age21MF;
	}
	public void setAge21MF(String age21mf) {
		age21MF = age21mf;
	}
	public String getAge22MF() {
		return age22MF;
	}
	public void setAge22MF(String age22mf) {
		age22MF = age22mf;
	}
	public String getAge23MF() {
		return age23MF;
	}
	public void setAge23MF(String age23mf) {
		age23MF = age23mf;
	}
	public String getAge24MF() {
		return age24MF;
	}
	public void setAge24MF(String age24mf) {
		age24MF = age24mf;
	}
	public String getAge25MF() {
		return age25MF;
	}
	public void setAge25MF(String age25mf) {
		age25MF = age25mf;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getBdate() {
		return bdate;
	}
	public void setBdate(String bdate) {
		this.bdate = bdate;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public Integer getStandardId() {
		return standardId;
	}
	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}
	public String getMale() {
		return male;
	}
	public void setMale(String male) {
		this.male = male;
	}
	public String getFemale() {
		return female;
	}
	public void setFemale(String female) {
		this.female = female;
	}
	public String getAge5() {
		return age5;
	}
	public void setAge5(String age5) {
		this.age5 = age5;
	}
	public String getAge6() {
		return age6;
	}
	public void setAge6(String age6) {
		this.age6 = age6;
	}
	public String getAge7() {
		return age7;
	}
	public void setAge7(String age7) {
		this.age7 = age7;
	}
	public String getAge8() {
		return age8;
	}
	public void setAge8(String age8) {
		this.age8 = age8;
	}
	public String getAge9() {
		return age9;
	}
	public void setAge9(String age9) {
		this.age9 = age9;
	}
	public String getAge10() {
		return age10;
	}
	public void setAge10(String age10) {
		this.age10 = age10;
	}
	public String getAge11() {
		return age11;
	}
	public void setAge11(String age11) {
		this.age11 = age11;
	}
	public String getAge12() {
		return age12;
	}
	public void setAge12(String age12) {
		this.age12 = age12;
	}
	public String getAge13() {
		return age13;
	}
	public void setAge13(String age13) {
		this.age13 = age13;
	}
	public String getAge14() {
		return age14;
	}
	public void setAge14(String age14) {
		this.age14 = age14;
	}
	public String getAge15() {
		return age15;
	}
	public void setAge15(String age15) {
		this.age15 = age15;
	}
	public String getAge16() {
		return age16;
	}
	public void setAge16(String age16) {
		this.age16 = age16;
	}
	public String getAge17() {
		return age17;
	}
	public void setAge17(String age17) {
		this.age17 = age17;
	}
	public String getAge18() {
		return age18;
	}
	public void setAge18(String age18) {
		this.age18 = age18;
	}
	public String getAge5M() {
		return age5M;
	}
	public void setAge5M(String age5m) {
		age5M = age5m;
	}
	public String getAge6M() {
		return age6M;
	}
	public void setAge6M(String age6m) {
		age6M = age6m;
	}
	public String getAge7M() {
		return age7M;
	}
	public void setAge7M(String age7m) {
		age7M = age7m;
	}
	public String getAge8M() {
		return age8M;
	}
	public void setAge8M(String age8m) {
		age8M = age8m;
	}
	public String getAge9M() {
		return age9M;
	}
	public void setAge9M(String age9m) {
		age9M = age9m;
	}
	public String getAge10M() {
		return age10M;
	}
	public void setAge10M(String age10m) {
		age10M = age10m;
	}
	public String getAge11M() {
		return age11M;
	}
	public void setAge11M(String age11m) {
		age11M = age11m;
	}
	public String getAge12M() {
		return age12M;
	}
	public void setAge12M(String age12m) {
		age12M = age12m;
	}
	public String getAge13M() {
		return age13M;
	}
	public void setAge13M(String age13m) {
		age13M = age13m;
	}
	public String getAge14M() {
		return age14M;
	}
	public void setAge14M(String age14m) {
		age14M = age14m;
	}
	public String getAge15M() {
		return age15M;
	}
	public void setAge15M(String age15m) {
		age15M = age15m;
	}
	public String getAge16M() {
		return age16M;
	}
	public void setAge16M(String age16m) {
		age16M = age16m;
	}
	public String getAge17M() {
		return age17M;
	}
	public void setAge17M(String age17m) {
		age17M = age17m;
	}
	public String getAge18M() {
		return age18M;
	}
	public void setAge18M(String age18m) {
		age18M = age18m;
	}
	public String getAge5F() {
		return age5F;
	}
	public void setAge5F(String age5f) {
		age5F = age5f;
	}
	public String getAge6F() {
		return age6F;
	}
	public void setAge6F(String age6f) {
		age6F = age6f;
	}
	public String getAge7F() {
		return age7F;
	}
	public void setAge7F(String age7f) {
		age7F = age7f;
	}
	public String getAge8F() {
		return age8F;
	}
	public void setAge8F(String age8f) {
		age8F = age8f;
	}
	public String getAge9F() {
		return age9F;
	}
	public void setAge9F(String age9f) {
		age9F = age9f;
	}
	public String getAge10F() {
		return age10F;
	}
	public void setAge10F(String age10f) {
		age10F = age10f;
	}
	public String getAge11F() {
		return age11F;
	}
	public void setAge11F(String age11f) {
		age11F = age11f;
	}
	public String getAge12F() {
		return age12F;
	}
	public void setAge12F(String age12f) {
		age12F = age12f;
	}
	public String getAge13F() {
		return age13F;
	}
	public void setAge13F(String age13f) {
		age13F = age13f;
	}
	public String getAge14F() {
		return age14F;
	}
	public void setAge14F(String age14f) {
		age14F = age14f;
	}
	public String getAge15F() {
		return age15F;
	}
	public void setAge15F(String age15f) {
		age15F = age15f;
	}
	public String getAge16F() {
		return age16F;
	}
	public void setAge16F(String age16f) {
		age16F = age16f;
	}
	public String getAge17F() {
		return age17F;
	}
	public void setAge17F(String age17f) {
		age17F = age17f;
	}
	public String getAge18F() {
		return age18F;
	}
	public void setAge18F(String age18f) {
		age18F = age18f;
	}
	public String getAge5MF() {
		return age5MF;
	}
	public void setAge5MF(String age5mf) {
		age5MF = age5mf;
	}
	public String getAge6MF() {
		return age6MF;
	}
	public void setAge6MF(String age6mf) {
		age6MF = age6mf;
	}
	public String getAge7MF() {
		return age7MF;
	}
	public void setAge7MF(String age7mf) {
		age7MF = age7mf;
	}
	public String getAge8MF() {
		return age8MF;
	}
	public void setAge8MF(String age8mf) {
		age8MF = age8mf;
	}
	public String getAge9MF() {
		return age9MF;
	}
	public void setAge9MF(String age9mf) {
		age9MF = age9mf;
	}
	public String getAge10MF() {
		return age10MF;
	}
	public void setAge10MF(String age10mf) {
		age10MF = age10mf;
	}
	public String getAge11MF() {
		return age11MF;
	}
	public void setAge11MF(String age11mf) {
		age11MF = age11mf;
	}
	public String getAge12MF() {
		return age12MF;
	}
	public void setAge12MF(String age12mf) {
		age12MF = age12mf;
	}
	public String getAge13MF() {
		return age13MF;
	}
	public void setAge13MF(String age13mf) {
		age13MF = age13mf;
	}
	public String getAge14MF() {
		return age14MF;
	}
	public void setAge14MF(String age14mf) {
		age14MF = age14mf;
	}
	public String getAge15MF() {
		return age15MF;
	}
	public void setAge15MF(String age15mf) {
		age15MF = age15mf;
	}
	public String getAge16MF() {
		return age16MF;
	}
	public void setAge16MF(String age16mf) {
		age16MF = age16mf;
	}
	public String getAge17MF() {
		return age17MF;
	}
	public void setAge17MF(String age17mf) {
		age17MF = age17mf;
	}
	public String getAge18MF() {
		return age18MF;
	}
	public void setAge18MF(String age18mf) {
		age18MF = age18mf;
	}
	public String getDivisionName() {
		return divisionName;
	}
	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}
	public Integer getDivisionId() {
		return divisionId;
	}
	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}
	public String getAge0M() {
		return age0M;
	}
	public void setAge0M(String age0m) {
		age0M = age0m;
	}
	public String getAge0F() {
		return age0F;
	}
	public void setAge0F(String age0f) {
		age0F = age0f;
	}
	public String getAge0MF() {
		return age0MF;
	}
	public void setAge0MF(String age0mf) {
		age0MF = age0mf;
	}
}
