package com.ingenio.trust.bean;

import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class LibSchoolTypeFilterParam {

	@ApiParam(value = "Enter schoolid (School Library of Student)")
	private Integer schoolId;
	
	@ApiParam(value = "Enter status")
	private Integer status;
	
	@ApiParam(value = "Enter school_type_id")
	private Integer schoolTypeId;
	
	@ApiParam(value = "Number of rows per request - for pagination")
    private int size;

    @ApiParam(value = "Page number for pagination")
    private int page;
	
}
