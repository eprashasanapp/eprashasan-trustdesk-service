package com.ingenio.trust.bean;

public class StudentAttendanceDetailsBean {
	
	
	private String studentName;
	
	
	public StudentAttendanceDetailsBean() {
		super();
	}


	
	public StudentAttendanceDetailsBean(String studentName) {
		super();
		this.studentName = studentName;
		}
	
	
	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	
	
}
