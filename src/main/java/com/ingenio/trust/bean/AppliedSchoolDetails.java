package com.ingenio.trust.bean;

import lombok.Data;

@Data
public class AppliedSchoolDetails {
	private Integer schoolid;
	private String schoolName;
	private String shortSchoolName;
	private String schoolSaunsthaName;
	private String address;
}
