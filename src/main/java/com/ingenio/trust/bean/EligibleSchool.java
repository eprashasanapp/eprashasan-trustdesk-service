package com.ingenio.trust.bean;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class EligibleSchool {

	@ApiModelProperty(value = "Enter student_admission_id")
	private Integer studentAdmissionId;
	
	@NotEmpty
	@ApiModelProperty(value = "Eligible schoolid")
	private Integer schoolid;
	
	@ApiModelProperty(value = "Eligible schoolname")
	private String schoolNameDecrypted;
	
	@ApiModelProperty(value = "Eligible schoolSaunsthaName")
	private String sansthaNameDecrypted;
	
	@ApiModelProperty(value = "Eligible schoolAddressDecrypted")
	private String schoolAddressDecrypted;
	
	@ApiModelProperty(value = "Eligible school logo")
	private String logo;
	
	@ApiModelProperty(value = "Eligible streamId")
	private Integer streamId;
	
	@ApiModelProperty(value = "Eligible categoryId")
	private Integer categoryId;
	
	@ApiModelProperty(value = "Eligible intake/quota")
	private Integer intake;
	
}
