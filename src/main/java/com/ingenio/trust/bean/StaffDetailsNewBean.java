package com.ingenio.trust.bean;

public class StaffDetailsNewBean {
	private Integer staffId;
	private String initialName;
	private String firstName;
	private String secondName;
	private String lastName;
	private String	staffregId;
	private String  staffDateOfBirt;
	private String	staffMobileNo;
	private Integer designationId;
	private String	designationName;
	private Integer staffTypeId;
	private String staffTypeName;

	private String	staffPhoto;
	private Integer	staffAppuserID;
	private  String	staffName;
	
	public StaffDetailsNewBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getDesignationId() {
		return designationId;
	}
	public void setDesignationId(Integer designationId) {
		this.designationId = designationId;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public Integer getStaffTypeId() {
		return staffTypeId;
	}
	public void setStaffTypeId(Integer staffTypeId) {
		this.staffTypeId = staffTypeId;
	}
	public String getStaffTypeName() {
		return staffTypeName;
	}
	public void setStaffTypeName(String staffTypeName) {
		this.staffTypeName = staffTypeName;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getStaffregId() {
		return staffregId;
	}
	public void setStaffregId(String staffregId) {
		this.staffregId = staffregId;
	}
	public String getStaffMobileNo() {
		return staffMobileNo;
	}
	public void setStaffMobileNo(String staffMobileNo) {
		this.staffMobileNo = staffMobileNo;
	}
	public String getStaffPhoto() {
		return staffPhoto;
	}
	public void setStaffPhoto(String staffPhoto) {
		this.staffPhoto = staffPhoto;
	}
	public Integer getStaffAppuserID() {
		return staffAppuserID;
	}
	public void setStaffAppuserID(Integer staffAppuserID) {
		this.staffAppuserID = staffAppuserID;
	}
	public String getStaffDateOfBirt() {
		return staffDateOfBirt;
	}
	public void setStaffDateOfBirt(String staffDateOfBirt) {
		this.staffDateOfBirt = staffDateOfBirt;
	}
	public Integer getStaffId() {
		return staffId;
	}
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	public String getInitialName() {
		return initialName;
	}
	public void setInitialName(String initialName) {
		this.initialName = initialName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public StaffDetailsNewBean(Integer staffId, String initialName, String firstName, String secondName,
			String lastName, String staffregId, String staffDateOfBirt, String staffMobileNo, Integer designationId,
			String designationName, Integer staffTypeId, String staffTypeName) {
		super();
		this.staffId = staffId;
		this.initialName = initialName;
		this.firstName = firstName;
		this.secondName = secondName;
		this.lastName = lastName;
		this.staffregId = staffregId;
		this.staffDateOfBirt = staffDateOfBirt;
		this.staffMobileNo = staffMobileNo;
		this.designationId = designationId;
		this.designationName = designationName;
		this.staffTypeId = staffTypeId;
		this.staffTypeName = staffTypeName;
	}
	
	
	
}

