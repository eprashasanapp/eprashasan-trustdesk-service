package com.ingenio.trust.bean;

import java.util.Date;
import java.util.List;

public class AnnouncementResponseBean {

	private String roleName;
	private List<AnnoucementLabelBean> displayList;
	private Integer announcementId;
	private String announcement;
	private String announcementTitle;
	private Date startDate;
	private Long totalStudentCount;
	private Long deliveredCount;
	private Long unseenCount;
	private Long seenCount;
	private Long completeCount;
	private Long noOfAttachments;
	private String announcedBy;
	private String announcedByImgUrl;
	private String announcedByRole;
	private String assignmentStatus;
	private Integer announcementAssignedtoId;
	private List<AnnouncementResponseBean> attachmentList;
	private Integer schoolId;
	private Integer sansthaId;
	private Integer sansthaUserId;
	private String registrationNumber;
	private Integer staffId;
	private boolean showcount;
	private String filePath;
	private Integer attachmentId;
	private String flag;
	private String role;
	private String name;
	private String staffName;
	
	

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Integer getSansthaId() {
		return sansthaId;
	}

	public void setSansthaId(Integer sansthaId) {
		this.sansthaId = sansthaId;
	}

	public Integer getSansthaUserId() {
		return sansthaUserId;
	}

	public void setSansthaUserId(Integer sansthaUserId) {
		this.sansthaUserId = sansthaUserId;
	}

	

	public AnnouncementResponseBean() {

	}

	public AnnouncementResponseBean(Integer announcementId, String announcement, String announcementTitle,
			Date startDate, Long totalStudentCount, Long deliveredCount, Long unseenCount, Long seenCount,
			Long completeCount, Long noOfAttachments) {
		super();
		this.announcementId = announcementId;
		this.announcement = announcement;
		this.announcementTitle = announcementTitle;
		this.startDate = startDate;
		this.totalStudentCount = totalStudentCount;
		this.deliveredCount = deliveredCount;
		this.unseenCount = unseenCount;
		this.seenCount = seenCount;
		this.completeCount = completeCount;
		this.noOfAttachments = noOfAttachments;
	}
	
	
	public AnnouncementResponseBean(Integer announcementId, String announcement, String announcementTitle,
			Date startDate ,Long noOfAttachments ,String announcedByRole ,Integer sansthaUserId , Integer staffId ,String name ,String staffName) {
		super();
		this.announcementId = announcementId;
		this.announcement = announcement;
		this.announcementTitle = announcementTitle;
		this.startDate = startDate;
		this.noOfAttachments = noOfAttachments;
		this.announcedByRole = announcedByRole;
		this.sansthaUserId=sansthaUserId;
		this.staffId=staffId;
		this.name = name;
		this.staffName=staffName;
	}

	public AnnouncementResponseBean(Integer announcementId, String announcement, String announcementTitle,
			Date startDate, Long noOfAttachments, String announcedBy, String announcedByRole, String assignmentStatus,
			Integer announcementAssignedtoId, Integer schoolId, String registrationNumber, Integer staffId) {
		super();
		this.announcementId = announcementId;
		this.announcement = announcement;
		this.announcementTitle = announcementTitle;
		this.startDate = startDate;
		this.noOfAttachments = noOfAttachments;
		this.announcedBy = announcedBy;
		this.announcedByRole = announcedByRole;
		this.assignmentStatus = assignmentStatus;
		this.announcementAssignedtoId = announcementAssignedtoId;
		this.schoolId = schoolId;
		this.registrationNumber = registrationNumber;
		this.setStaffId(staffId);
	}

	public AnnouncementResponseBean(String filePath, Integer attachmentId) {
		super();
		this.filePath = filePath;
		this.attachmentId = attachmentId;
	}

	
	
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<AnnoucementLabelBean> getDisplayList() {
		return displayList;
	}

	public void setDisplayList(List<AnnoucementLabelBean> displayList) {
		this.displayList = displayList;
	}

	public Integer getAnnouncementId() {
		return announcementId;
	}

	public void setAnnouncementId(Integer announcementId) {
		this.announcementId = announcementId;
	}

	public String getAnnouncement() {
		return announcement;
	}

	public void setAnnouncement(String announcement) {
		this.announcement = announcement;
	}

	public String getAnnouncementTitle() {
		return announcementTitle;
	}

	public void setAnnouncementTitle(String announcementTitle) {
		this.announcementTitle = announcementTitle;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Long getTotalStudentCount() {
		return totalStudentCount;
	}

	public void setTotalStudentCount(Long totalStudentCount) {
		this.totalStudentCount = totalStudentCount;
	}
	
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getDeliveredCount() {
		return deliveredCount;
	}

	public void setDeliveredCount(Long deliveredCount) {
		this.deliveredCount = deliveredCount;
	}

	public Long getUnseenCount() {
		return unseenCount;
	}

	public void setUnseenCount(Long unseenCount) {
		this.unseenCount = unseenCount;
	}

	public Long getSeenCount() {
		return seenCount;
	}

	public void setSeenCount(Long seenCount) {
		this.seenCount = seenCount;
	}

	public Long getCompleteCount() {
		return completeCount;
	}

	public void setCompleteCount(Long completeCount) {
		this.completeCount = completeCount;
	}

	public Long getNoOfAttachments() {
		return noOfAttachments;
	}

	public void setNoOfAttachments(Long noOfAttachments) {
		this.noOfAttachments = noOfAttachments;
	}

	public String getAnnouncedBy() {
		return announcedBy;
	}

	public void setAnnouncedBy(String announcedBy) {
		this.announcedBy = announcedBy;
	}

	public String getAssignmentStatus() {
		return assignmentStatus;
	}

	public void setAssignmentStatus(String assignmentStatus) {
		this.assignmentStatus = assignmentStatus;
	}

	public Integer getAnnouncementAssignedtoId() {
		return announcementAssignedtoId;
	}

	public void setAnnouncementAssignedtoId(Integer announcementAssignedtoId) {
		this.announcementAssignedtoId = announcementAssignedtoId;
	}

	public String getAnnouncedByRole() {
		return announcedByRole;
	}

	public void setAnnouncedByRole(String announcedByRole) {
		this.announcedByRole = announcedByRole;
	}

	public List<AnnouncementResponseBean> getAttachmentList() {
		return attachmentList;
	}

	public void setAttachmentList(List<AnnouncementResponseBean> attachmentList) {
		this.attachmentList = attachmentList;
	}

	public String getAnnouncedByImgUrl() {
		return announcedByImgUrl;
	}

	public void setAnnouncedByImgUrl(String announcedByImgUrl) {
		this.announcedByImgUrl = announcedByImgUrl;
	}

	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public boolean isShowcount() {
		return showcount;
	}

	public void setShowcount(boolean showcount) {
		this.showcount = showcount;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Integer getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(Integer attachmentId) {
		this.attachmentId = attachmentId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
}
