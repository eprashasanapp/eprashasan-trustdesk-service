package com.ingenio.trust.bean;

import java.util.List;

public class PrincipalMenuBean {

	private String menuName;
	private Integer menuId;
	private String iconPath;
	private String groupName;
	private Integer groupdId;
	private String isWebViewForReact;
	private String isWebViewForAndroid;
	private String webViewURL;
	private Integer parentMenuId;
	private List<PrincipalMenuBean> principalMenuList;

	public PrincipalMenuBean() {
		super();
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public Integer getMenuId() {
		return menuId;
	}

	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}

	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getGroupdId() {
		return groupdId;
	}

	public void setGroupdId(Integer groupdId) {
		this.groupdId = groupdId;
	}

	public List<PrincipalMenuBean> getPrincipalMenuList() {
		return principalMenuList;
	}

	public void setPrincipalMenuList(List<PrincipalMenuBean> principalMenuList) {
		this.principalMenuList = principalMenuList;
	}

	public PrincipalMenuBean(Integer menuId, String menuName) {
		super();
		this.menuName = menuName;
		this.menuId = menuId;
	}

	public PrincipalMenuBean(Integer menuId,String menuName, String iconPath) {
		super();
		this.menuId = menuId;
		this.menuName = menuName;
		this.iconPath = iconPath;
	}
	public PrincipalMenuBean(Integer menuId,String menuName, String iconPath,String isWebViewForReact,String isWebViewForAndroid,String webViewURL,Integer parentMenuId) {
		super();
		this.menuId = menuId;
		this.menuName = menuName;
		this.iconPath = iconPath;
		this.isWebViewForReact=isWebViewForReact;
		this.isWebViewForAndroid=isWebViewForAndroid;
		this.webViewURL=webViewURL;
		this.parentMenuId=parentMenuId;
	}

	public String getIsWebViewForReact() {
		return isWebViewForReact;
	}

	public void setIsWebViewForReact(String isWebViewForReact) {
		this.isWebViewForReact = isWebViewForReact;
	}

	public String getIsWebViewForAndroid() {
		return isWebViewForAndroid;
	}

	public void setIsWebViewForAndroid(String isWebViewForAndroid) {
		this.isWebViewForAndroid = isWebViewForAndroid;
	}

	public String getWebViewURL() {
		return webViewURL;
	}

	public void setWebViewURL(String webViewURL) {
		this.webViewURL = webViewURL;
	}

	public Integer getParentMenuId() {
		return parentMenuId;
	}

	public void setParentMenuId(Integer parentMenuId) {
		this.parentMenuId = parentMenuId;
	}

	
	

}
