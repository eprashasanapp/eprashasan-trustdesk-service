package com.ingenio.trust.bean;

import com.ingenio.trust.model.LibCurrencyMaster;

import lombok.Data;

@Data
public class LibBookInventory {

	private int bookInventoryID;
	private String bookName;
	private String authorName;
	private String publicationName;
	private int quantity;
	private int bookTypeID;
	private int book_stream_id;
	private int standardID;
	private int divisionID;
	private String bookEdition;
	private int book_for;
	private double amountPerBook;
	private String bill_no;
	private String year_of_publisher;
    private	String no_of_pages;
    private String rack_No;
    private String bill_date;
    private LibCurrencyMaster CurrencyDetails;
	
}
