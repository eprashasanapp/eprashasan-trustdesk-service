package com.ingenio.trust.bean;

import java.util.List;

public class DashBoardBean {
	
	private List<PrincipalMenuBean> menuList;
	private AttendanceReportBean attendanceReportBean;
	private List<DashBoardBean> countList;
	private String title;
	private String label;
	private String count;
	private String countInWord;
	
	private String userActiveOrInActive;
	private String userActiveOrInActiveName;
	
	public List<PrincipalMenuBean> getMenuList() {
		return menuList;
	}
	public void setMenuList(List<PrincipalMenuBean> menuList) {
		this.menuList = menuList;
	}
	public AttendanceReportBean getAttendanceReportBean() {
		return attendanceReportBean;
	}
	public void setAttendanceReportBean(AttendanceReportBean attendanceReportBean) {
		this.attendanceReportBean = attendanceReportBean;
	}
	public List<DashBoardBean> getCountList() {
		return countList;
	}
	public void setCountList(List<DashBoardBean> countList) {
		this.countList = countList;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public String getCountInWord() {
		return countInWord;
	}
	public void setCountInWord(String countInWord) {
		this.countInWord = countInWord;
	}
	public String getUserActiveOrInActive() {
		return userActiveOrInActive;
	}
	public void setUserActiveOrInActive(String userActiveOrInActive) {
		this.userActiveOrInActive = userActiveOrInActive;
	}
	public String getUserActiveOrInActiveName() {
		return userActiveOrInActiveName;
	}
	public void setUserActiveOrInActiveName(String userActiveOrInActiveName) {
		this.userActiveOrInActiveName = userActiveOrInActiveName;
	}
	
}
