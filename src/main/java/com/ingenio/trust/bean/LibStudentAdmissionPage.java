package com.ingenio.trust.bean;

import java.util.List;

import lombok.Data;

@Data
public class LibStudentAdmissionPage {

	private List<LibStudentAdmissionInfo> data;

    private Paging paging;
    
}
