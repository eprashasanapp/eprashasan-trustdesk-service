package com.ingenio.trust.bean;

import lombok.Data;

@Data
public class Paging {

    private int totalPages;
    
    private int pageSize;
    
}