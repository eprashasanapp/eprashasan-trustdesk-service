package com.ingenio.trust.bean;

public class SansthaUserBean {
	
	
	private Integer sansthaUserId;
	private Integer sansthaId;
	private String sansthaUserRegNo;
	private String initialName;	
	private String firstName;	
	private String secondName;	
	private String lastName;	
	private String birthDate;	
	private String gender;
	private String sansthaName;
	
	public Integer getSansthaUserId() {
		return sansthaUserId;
	}
	public void setSansthaUserId(Integer sansthaUserId) {
		this.sansthaUserId = sansthaUserId;
	}
	public Integer getSansthaId() {
		return sansthaId;
	}
	public void setSansthaId(Integer sansthaId) {
		this.sansthaId = sansthaId;
	}
	public String getSansthaUserRegNo() {
		return sansthaUserRegNo;
	}
	public void setSansthaUserRegNo(String sansthaUserRegNo) {
		this.sansthaUserRegNo = sansthaUserRegNo;
	}
	public String getInitialName() {
		return initialName;
	}
	public void setInitialName(String initialName) {
		this.initialName = initialName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	private SansthaUserBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SansthaUserBean(Integer sansthaUserId, Integer sansthaId, String sansthaUserRegNo, String initialName,
			String firstName, String secondName, String lastName, String birthDate, String gender) {
		super();
		this.sansthaUserId = sansthaUserId;
		this.sansthaId = sansthaId;
		this.sansthaUserRegNo = sansthaUserRegNo;
		this.initialName = initialName;
		this.firstName = firstName;
		this.secondName = secondName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.gender = gender;
	}
	public String getSansthaName() {
		return sansthaName;
	}
	public void setSansthaName(String sansthaName) {
		this.sansthaName = sansthaName;
	}
	
	public SansthaUserBean(Integer sansthaId, String sansthaName ) {
		super();
		this.sansthaName = sansthaName;
		this.sansthaId = sansthaId;
	}
	

}
