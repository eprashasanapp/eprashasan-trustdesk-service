package com.ingenio.trust.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class IntakeFilterParam {
	
	@ApiModelProperty(name = "school_id")
	private Integer schoolId;
	
	@ApiModelProperty(name = "category Name")
	private String categoryName;
	
	@ApiModelProperty(name = "stream Name")
	private String streamName;
	
	@ApiModelProperty(name = "DistrictId")
	private Integer districtId;
	
	@ApiModelProperty(name = "taluka Id")
	private Integer talukaId;
	
	@ApiModelProperty(name = "state Id")
	private Integer stateId;
	
	@ApiModelProperty(name = "gender")
	private Integer gender;
	
	@ApiModelProperty(name = "sansthakey")
	private Integer sansthakey;
	
	@ApiModelProperty(name = "groupOfSchoolId")
	private Integer groupOfSchoolId;
	
	@ApiModelProperty(value = "Number of rows per request - for pagination")
    private int size;

	@ApiModelProperty(value = "Page number for pagination")
    private int page;
	
	@JsonIgnore
	private Integer streamId;
	
	@JsonIgnore
	private Integer categoryId;
}
