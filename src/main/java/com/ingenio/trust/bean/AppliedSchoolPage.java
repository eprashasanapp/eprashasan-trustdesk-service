package com.ingenio.trust.bean;

import java.util.List;

import lombok.Data;

@Data
public class AppliedSchoolPage {
	
	private List<AppliedSchool> data;

    private Paging paging;
}
