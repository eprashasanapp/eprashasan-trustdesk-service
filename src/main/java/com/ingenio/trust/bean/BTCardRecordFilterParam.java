package com.ingenio.trust.bean;

import java.util.List;

import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class BTCardRecordFilterParam {

	@ApiParam(value = "Enter List of BTNumber")
	private List<String> BTNumbers;
	
	@ApiParam(value = "Enter schoolid (School Library of Student)")
	private Integer schoolId;
	
	@ApiParam(value = "Enter status")
	private Integer status;
	
	@ApiParam(value = "Enter school_type_id")
	private Integer schoolTypeId;
	
	@ApiParam(value = "Enter type")
	private Integer type;
	
	@ApiParam(value = "Number of rows per request - for pagination")
    private int size;

    @ApiParam(value = "Page number for pagination")
    private int page;
	
}
