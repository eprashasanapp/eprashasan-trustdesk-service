package com.ingenio.trust.bean;

import java.util.List;

public class StaffDetailsBean {
	
	
	private String staffName;
	private Integer staffId;
	private String staffPhotoUrl;
	private String staffRegNo;
	private Integer yearId;
	private Long presentCount;
	private Long absentCount;
	private Long totalCount;
	private String presentCountInWords;
	private String absentCountInWords;
	private String totalCountInWords;
    List<StaffDetailsBean> presentStaffList; 
    List<StaffDetailsBean> absentStaffList; 
	private Integer schoolid;
	private String schoolName; 
	private String longSchoolName;
	private Integer groupOfSchoolId;
	private String groupOfSchoolName;
	
	private String firstName;
	private String lastName;
	


	
	List<StaffDetailsBean> schoolWiseList; 
	List<StaffDetailsBean> groupWiseList; 

	public String getStaffPhotoUrl() {
		return staffPhotoUrl;
	}

	public void setStaffPhotoUrl(String staffPhotoUrl) {
		this.staffPhotoUrl = staffPhotoUrl;
	}

	public String getStaffRegNo() {
		return staffRegNo;
	}

	public void setStaffRegNo(String staffRegNo) {
		this.staffRegNo = staffRegNo;
	}
	
	public List<StaffDetailsBean> getSchoolWiseList() {
		return schoolWiseList;
	}

	public void setSchoolWiseList(List<StaffDetailsBean> schoolWiseList) {
		this.schoolWiseList = schoolWiseList;
	}

	public List<StaffDetailsBean> getPresentStaffList() {
		return presentStaffList;
	}

	public void setPresentStaffList(List<StaffDetailsBean> presentStaffList) {
		this.presentStaffList = presentStaffList;
	}

	public List<StaffDetailsBean> getAbsentStaffList() {
		return absentStaffList;
	}

	public void setAbsentStaffList(List<StaffDetailsBean> absentStaffList) {
		this.absentStaffList = absentStaffList;
	}

	public StaffDetailsBean() {
		super();
	}
	

	public StaffDetailsBean(String staffName,Integer staffId,String staffRegNo) {
		super();
		this.staffName = staffName;
		this.staffId=staffId;
		this.staffRegNo=staffRegNo;
}
	
	public StaffDetailsBean(String groupOfSchoolName,Integer groupOfSchoolId,Long absentCount,Long totalCount,Long presentCount) {
		super();
		this.groupOfSchoolId=groupOfSchoolId;
		this.groupOfSchoolName = groupOfSchoolName;
		this.presentCount = presentCount;
		this.absentCount=absentCount;
		this.totalCount= totalCount;
}
	
	public StaffDetailsBean(Long absentCount,Long totalCount,Long presentCount) {
		super();
		this.presentCount = presentCount;
		this.absentCount=absentCount;
		this.totalCount= totalCount;
}
	public StaffDetailsBean(Long totalCount,Long presentCount) {
		super();
		this.presentCount = presentCount;
		this.totalCount= totalCount;
}

	public StaffDetailsBean(Integer schoolid ,String schoolName,String longSchoolName,Long absentCount,Long totalCount,Long presentCount) {
		super();
		this.schoolid=schoolid;
		this.schoolName=schoolName;
		this.presentCount = presentCount;
		this.absentCount=absentCount;
		this.longSchoolName=longSchoolName;
		this.totalCount= totalCount;
}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public Long getPresentCount() {
		return presentCount;
	}

	public void setPresentCount(Long presentCount) {
		this.presentCount = presentCount;
	}

	public Long getAbsentCount() {
		return absentCount;
	}

	public void setAbsentCount(Long absentCount) {
		this.absentCount = absentCount;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getSchoolid() {
		return schoolid;
	}

	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getPresentCountInWords() {
		return presentCountInWords;
	}

	public void setPresentCountInWords(String presentCountInWords) {
		this.presentCountInWords = presentCountInWords;
	}

	public String getAbsentCountInWords() {
		return absentCountInWords;
	}

	public void setAbsentCountInWords(String absentCountInWords) {
		this.absentCountInWords = absentCountInWords;
	}

	public String getTotalCountInWords() {
		return totalCountInWords;
	}

	public void setTotalCountInWords(String totalCountInWords) {
		this.totalCountInWords = totalCountInWords;
	}

	public Integer getYearId() {
		return yearId;
	}

	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}

	public Integer getGroupOfSchoolId() {
		return groupOfSchoolId;
	}

	public void setGroupOfSchoolId(Integer groupOfSchoolId) {
		this.groupOfSchoolId = groupOfSchoolId;
	}

	public String getGroupOfSchoolName() {
		return groupOfSchoolName;
	}

	public void setGroupOfSchoolName(String groupOfSchoolName) {
		this.groupOfSchoolName = groupOfSchoolName;
	}

	public List<StaffDetailsBean> getGroupWiseList() {
		return groupWiseList;
	}

	public void setGroupWiseList(List<StaffDetailsBean> groupWiseList) {
		this.groupWiseList = groupWiseList;
	}

	public String getLongSchoolName() {
		return longSchoolName;
	}

	public void setLongSchoolName(String longSchoolName) {
		this.longSchoolName = longSchoolName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public StaffDetailsBean( Integer schoolid, String schoolName, String firstName, String lastName,Integer staffId) {
		super();
		this.staffId = staffId;
		this.schoolid = schoolid;
		this.schoolName = schoolName;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	
}
