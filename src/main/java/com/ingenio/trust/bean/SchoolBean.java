package com.ingenio.trust.bean;

public class SchoolBean {
	
	

	private Integer schoolId;
	private String schoolName;
	private String shortSchoolName;	
	
	
	public Integer getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getShortSchoolName() {
		return shortSchoolName;
	}

	public void setShortSchoolName(String shortSchoolName) {
		this.shortSchoolName = shortSchoolName;
	}

	public SchoolBean() {
		super();
	}

	public SchoolBean(Integer schoolId, String schoolName, String shortSchoolName) {
		super();
		this.schoolId = schoolId;
		this.schoolName = schoolName;
		this.shortSchoolName = shortSchoolName;
	}

	public SchoolBean(Integer schoolId, String schoolName) {
		super();
		this.schoolId = schoolId;
		this.schoolName = schoolName;
	}

	

}
