package com.ingenio.trust.bean;

import java.util.List;

public class UserBean {
	
	List<SchoolBean> schoolList;
	List<SansthaUserBean> sansthaUserList;
	List<StaffDetailsBean> principalList;
	private String roleName;
	private List<AnnouncementLabelBean> displayList;	
	
	public List<SchoolBean> getSchoolList() {
		return schoolList;
	}
	public void setSchoolList(List<SchoolBean> schoolList) {
		this.schoolList = schoolList;
	}
	public List<SansthaUserBean> getSansthaUserList() {
		return sansthaUserList;
	}
	public void setSansthaUserList(List<SansthaUserBean> sansthaUserList) {
		this.sansthaUserList = sansthaUserList;
	}
	public List<StaffDetailsBean> getPrincipalList() {
		return principalList;
	}
	public void setPrincipalList(List<StaffDetailsBean> principalList) {
		this.principalList = principalList;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public List<AnnouncementLabelBean> getDisplayList() {
		return displayList;
	}
	public void setDisplayList(List<AnnouncementLabelBean> displayList) {
		this.displayList = displayList;
	}
	
	
	
	

}
