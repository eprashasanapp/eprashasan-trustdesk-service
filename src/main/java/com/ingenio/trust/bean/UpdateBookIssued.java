package com.ingenio.trust.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class UpdateBookIssued {

	List<Integer> ids = new ArrayList<Integer>();
	String message;
}
