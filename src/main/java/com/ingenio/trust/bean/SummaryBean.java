package com.ingenio.trust.bean;

public class SummaryBean {
	
private Integer summaryId;
private String summaryName;
private Long totalCount;
private Long girlsCount;
private Long boysCount;
private String totalCountInWords;
private String girlsCountInWords;
private String boysCountInWords;


public SummaryBean() {
	super();
}	


public SummaryBean(Integer summaryId, String summaryName, Long totalCount, Long boysCount, Long girlsCount) {
	super();
	this.summaryId = summaryId;
	this.summaryName = summaryName;
	this.totalCount = totalCount;
	this.girlsCount = girlsCount;
	this.boysCount = boysCount;
}




public Long getTotalCount() {
	return totalCount;
}


public void setTotalCount(Long totalCount) {
	this.totalCount = totalCount;
}


public Long getGirlsCount() {
	return girlsCount;
}


public void setGirlsCount(Long girlsCount) {
	this.girlsCount = girlsCount;
}


public Long getBoysCount() {
	return boysCount;
}


public void setBoysCount(Long boysCount) {
	this.boysCount = boysCount;
}


public Integer getSummaryId() {
	return summaryId;
}


public void setSummaryId(Integer summaryId) {
	this.summaryId = summaryId;
}


public String getSummaryName() {
	return summaryName;
}


public void setSummaryName(String summaryName) {
	this.summaryName = summaryName;
}


public String getTotalCountInWords() {
	return totalCountInWords;
}


public void setTotalCountInWords(String totalCountInWords) {
	this.totalCountInWords = totalCountInWords;
}


public String getGirlsCountInWords() {
	return girlsCountInWords;
}


public void setGirlsCountInWords(String girlsCountInWords) {
	this.girlsCountInWords = girlsCountInWords;
}


public String getBoysCountInWords() {
	return boysCountInWords;
}


public void setBoysCountInWords(String boysCountInWords) {
	this.boysCountInWords = boysCountInWords;
}





}