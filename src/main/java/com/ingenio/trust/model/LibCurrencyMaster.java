package com.ingenio.trust.model;

import lombok.Data;

@Data
public class LibCurrencyMaster {
	
	private Integer c_Id;
	private String currencyName;

}
