package com.ingenio.trust.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "lib_book_access_isbn")
public class LibBookAccessISBNModel {

	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	@Column(name = "book_access_isbn_id")
	private Integer bookAccessISBNId;

	@Column(name = "schoolid")
	private Integer schoolId;

	@Column(name = "access_number")
	private String accessNumber;

	@Column(name = "category_id")
	private String categoryId;

	@Column(name = "isbn_number")
	private String isbnNumber;

	@Column(name = "type")
	private Integer type;

	@Column(name = "status")
	private Integer status;

	@Column(name = "userId")
	private Integer userId;

	@Column(name = "cDate")
	private Date cDate;

	@Column(name = "isDel")
	private String isDel;

	@Column(name = "delDate")
	private Date delDate;

	@Column(name = "deleteBy")
	private Integer deleteBy;

	@Column(name = "isEdit")
	private String isEdit;

	@Column(name = "editDate")
	private Date editDate;

	@Column(name = "editBy")
	private Integer editBy;

	@Column(name = "deviceType")
	private String deviceType;

	@Column(name = "ipAddress")
	private String ipAddress;

	@Column(name = "macAddress")
	private String macAddress;

	@Column(name = "sinkingFlag")
	private String sinkingFlag;
	
	@Column(name = "availability_status")
	private String availabilitystatus;

	@ManyToOne(fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "book_inventory_id", nullable = true)
	private LibBookInventoryModel bookDetails;
}
