package com.ingenio.trust.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "android_menu_master_group")
public class AndroidMenuMasterGroupModel {

	private Integer androidMenuGroupId;	
	private String androidMenuGroupName;
	private String imageFilePath;
	private Date cDate = new Date(new java.util.Date().getTime());
	private String isDel = "0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private String isEdit = "0";
	private Date editDate;
	private AppUserRoleModel editBy;
	private String deviceType = "0";
	private String ipAddress;
	private String macAddress;	
	private String sinkingFlag = "0";
	

	@Id
	@Column(name = "androidMenuGroupId")
	public Integer getAndroidMenuGroupId() {
		return androidMenuGroupId;
	}

	public void setAndroidMenuGroupId(Integer androidMenuGroupId) {
		this.androidMenuGroupId = androidMenuGroupId;
	}

	@Column(name = "androidMenuGroupName",nullable = false, length = 30)
	@Length(max = 50)
	public String getAndroidMenuGroupName() {
		return androidMenuGroupName;
	}

	public void setAndroidMenuGroupName(String androidMenuGroupName) {
		this.androidMenuGroupName = androidMenuGroupName;
	}

	
	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name = "cDate", updatable = false)
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@Column(name = "isDel", columnDefinition = "default '0'")
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	@Column(name = "deleteBy")
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name = "isEdit", columnDefinition = "default '0'")
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	@Column(name = "editBy")
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}

	@Column(name = "deviceType", columnDefinition = "default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name = "ipAddress", length = 100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name = "macAddress", length = 50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name = "sinkingFlag", columnDefinition = "default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	@Column(name = "imageFilePath")
	@Length(max = 200)
	public String getImageFilePath() {
		return imageFilePath;
	}

	public void setImageFilePath(String imageFilePath) {
		this.imageFilePath = imageFilePath;
	}
	
}
