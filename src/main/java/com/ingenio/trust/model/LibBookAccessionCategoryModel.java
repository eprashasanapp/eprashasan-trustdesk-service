package com.ingenio.trust.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "lib_book_accession_category")
public class LibBookAccessionCategoryModel  {

	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer categoryId;
	private String categoryName;
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new Date(new java.util.Date().getTime());
	private String isDel="0";
	private String isEdit="0";
	private String sinkingFlag="0";
	private Integer schoolId;

}