package com.ingenio.trust.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import lombok.Data;

/**
 * @author Contractor
 *
 */
@Data
@Entity
@Table(name = "lib_book_supplier")
public class LibBookSupplierModel {
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	@Column(name = "suppiler_id")
	private int supplierID;
	@Column(name = "supplier_name")
	private String supplierName;
	@Column(name = "supplier_email")
	private String supplierEmail;
	@Column(name = "supplier_phone")
	private String supplierPhone;
	@Column(name = "supplier_address")
	private String supplierAddress;
	@Column(name = "created_by")
	private int createdBy;
	@Column(name = "created_date")
	private String createdDate;
	@Column(name = "updated_date")
	private String updatedDate;
	@Column(name = "status")
	private int status;
	@Column(name = "schoolId")
	private Integer schoolId;
	@Column(name = "userId")
	private Integer userId;
	@Column(name = "cDate")
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	@Column(name = "delDate")
	private Date delDate;
	private Integer deleteBy;
	private String isEdit = "0";
	private Date editDate;
	private Integer editBy;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag = "0";
	
}
