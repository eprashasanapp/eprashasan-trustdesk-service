package com.ingenio.trust.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "lib_student_admission")
public class LibStudentAdmissionModel {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "student_admission_id")
	private Integer studentAdmissionId;
	
	@Column(name = "schoolid")
	private Integer schoolId;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "adhar_number")
	private String adharNumber;
	
	@Column(name = "created_by")
	private Integer createdBy;
	
	@Column(name = "created_date")
	private String createdDate;
	
	@Column(name = "dob")
	private String dob;
	
	@Column(name = "division_id")
	private Integer divisionId;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "mobile_number")
	private String mobileNumber;
	
	@Column(name = "registration_number")
	private String registrationNumber;
	
	@Column(name = "school_type_id")
	private Integer schoolTypeId;
	
	@Column(name = "standard_id")
	private Integer standardId;
	
	@Column(name = "status")
	private Integer status;
	
	@Column(name = "student_name")
	private String studentName;
	
	@Column(name = "updated_date")
	private String updatedDate;
	
	@Column(name = "year_id")
	private Integer yearId;
	
	@Column(name = "userId")
	private Integer userId;
	
	@Column(name = "cDate")
	private Date cDate;
	
	@Column(name = "isDel")
	private String isDel;
	
	@Column(name = "delDate")
	private Date delDate;
	
	@Column(name = "deleteBy")
	private Integer deleteBy;
	
	@Column(name = "isEdit")
	private String isEdit;
	
	@Column(name = "editDate")
	private Date editDate;
	
	@Column(name = "editBy")
	private Integer editBy;
	
	@Column(name = "deviceType")
	private String deviceType;
	
	@Column(name = "ipAddress")
	private String ipAddress;
	
	@Column(name = "macAddress")
	private String macAddress;
	
	@Column(name = "sinkingFlag")
	private String sinkingFlag;
	
	@Column(name = "studentId")
	private Integer studentId;
	
	@Column(name = "studentRenewId")
	private Integer studentRenewId;
	
	@Column(name = "isGuest")
	private String isGuest;
	
	@Column(name = "studentschoolId")
	private Integer studentschoolId;
	
}
