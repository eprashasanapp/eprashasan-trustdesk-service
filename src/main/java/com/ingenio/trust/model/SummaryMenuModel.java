package com.ingenio.trust.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "android_desk_summary_master")

public class SummaryMenuModel {
	private static final long serialVersionUID = -9004909181952908073L;

	@Id
	@Column(name = "summaryMenuId", nullable = false)
	private Integer summaryMenuId;
	
	@Column(name = "summaryName", nullable = false)
	private String summaryName;
	
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name = "cDate", updatable= false)
	private Date cDate = new Date(new java.util.Date().getTime());
	
	@Column(name="delDate")
	private Date delDate;
	
	@Column(name = "editDate")
	private Date editDate;
	
	@Column(name = "ipAddress", length = 100)
	private String ipAddress;
	
	@Column(name="isDel", columnDefinition="default '0'")
	private String isDel="0";
	
	@Column(name = "isEdit", columnDefinition = "default '0'")
	private String isEdit = "0";
	
	@Column(name = "macAddress", length = 50)
	private String macAddress;
	

	@Column(name = "sinkingFlag", columnDefinition = "default '0'")
	private String sinkingFlag = "0";
	
	@Column(name = "deleteBy")
	private AppUserRoleModel deleteBy;

	
	@Column(name = "editBy")
	private AppUserRoleModel editBy;
	
	
	@Column(name = "userID")
	private AppUserRoleModel userId;
	
	


	
	
}