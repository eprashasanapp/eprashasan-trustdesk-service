package com.ingenio.trust.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "school_board")
public class SchoolBoardModel {
	
	private int schoolBoardId;
	private String schoolBoard;
	private SchoolTypeModel schoolTypeModel;
	private String sinkingFlag = "0";
	private Integer schoolOrCollege=0;
	private Integer schoolid;
	private String startMonth;
	private String endMonth;
	
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	public int getSchoolBoardId() {
		return schoolBoardId;
	}
	public void setSchoolBoardId(int schoolBoardId) {
		this.schoolBoardId = schoolBoardId;
	}
	
	@Column(name="schoolBoard")
	public String getSchoolBoard() {
		return schoolBoard;
	}
	public void setSchoolBoard(String schoolBoard) {
		this.schoolBoard = schoolBoard;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@JoinColumn(name = "schoolTypeId")
	public SchoolTypeModel getSchoolTypeModel() {
		return schoolTypeModel;
	}
	public void setSchoolTypeModel(SchoolTypeModel schoolTypeModel) {
		this.schoolTypeModel = schoolTypeModel;
	}
	@Column(name = "sinkingFlag", nullable = false)
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	public Integer getSchoolOrCollege() {
		return schoolOrCollege;
	}
	public void setSchoolOrCollege(Integer schoolOrCollege) {
		this.schoolOrCollege = schoolOrCollege;
	}
	public Integer getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}
	public String getStartMonth() {
		return startMonth;
	}
	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}
	public String getEndMonth() {
		return endMonth;
	}
	public void setEndMonth(String endMonth) {
		this.endMonth = endMonth;
	}
	
	
	

}
