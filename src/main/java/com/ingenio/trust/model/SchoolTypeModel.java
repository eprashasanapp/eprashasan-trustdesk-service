package com.ingenio.trust.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "school_type")
public class SchoolTypeModel {

	private int schoolTypeId;
	private String schoolType;
	private String sinkingFlag = "0";
	private Integer schoolOrCollege=0;
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	public int getSchoolTypeId() {
		return schoolTypeId;
	}
	public void setSchoolTypeId(int schoolTypeId) {
		this.schoolTypeId = schoolTypeId;
	}
	
	@Column(name="schoolType")
	public String getSchoolType() {
		return schoolType;
	}
	public void setSchoolType(String schoolType) {
		this.schoolType = schoolType;
	}
	
	@Column(name="sinkingFlag")	
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	public Integer getSchoolOrCollege() {
		return schoolOrCollege;
	}
	public void setSchoolOrCollege(Integer schoolOrCollege) {
		this.schoolOrCollege = schoolOrCollege;
	}
	
	
}
