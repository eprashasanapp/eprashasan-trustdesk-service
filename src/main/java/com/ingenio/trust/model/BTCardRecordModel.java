package com.ingenio.trust.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "lib_bt_card_record")
public class BTCardRecordModel {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "bt_card_record_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer btCardRecordId;
	
	@Column(name = "schoolid")
	private Integer schoolId;
	
	@Column(name = "bt_card_id")
	private String btCardId;
	
	@Column(name = "bt_number")
	private String btNumber;
	
	@Column(name = "created_by")
	private Integer createdBy;
	
	@Column(name = "created_date")
	private String createdDate;
	
	@Column(name = "fees")
	private double fees;
	
	@Column(name = "name_id")
	private Integer nameId;
	
	@Column(name = "security_deposit")
	private String securityDeposit;
	
	@Column(name = "type")
	private Integer type;
	
	@Column(name = "validity")
	private String validity;
	
	@Column(name = "school_type_id")
	private Integer schoolTypeId;
	
	@Column(name = "userId")
	private Integer userId;
	
	@Column(name = "status")
	private Integer status;
	
	@Column(name = "deviceType")
	private char deviceType;
	
	@Column(name = "updated_date")
	private String updatedDate;
	
	@Column(name = "cDate")
	private Date cDate;
	
	@Column(name = "isDel")
	private String isDel;
	
	@Column(name = "delDate")
	private Date delDate;
	
	@Column(name = "deleteBy")
	private Integer deleteBy;
	
	@Column(name = "isEdit")
	private String isEdit;
	
	@Column(name = "editDate")
	private Date editDate;
	
	@Column(name = "editBy")
	private Integer editBy;
	
	@Column(name = "ipAddress")
	private String ipAddress;
	
	@Column(name = "macAddress")
	private String macAddress;
	
	@Column(name = "sinkingFlag")
	private String sinkingFlag;
	
}
