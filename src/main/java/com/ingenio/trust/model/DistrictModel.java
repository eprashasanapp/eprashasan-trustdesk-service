package com.ingenio.trust.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "district_tab")
public class DistrictModel {
	private static final long serialVersionUID = 1L;
	
	private Integer districtId;
	private String district;
	private StateModel stateModel;
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "districtId", unique = true, nullable = false)
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stateId",nullable = false)
	public StateModel getStateModel() {
		return stateModel;
	}
	public void setStateModel(StateModel stateModel) {
		this.stateModel = stateModel;
	}
//	@Override
//	public String toString() {
//		return "DistrictModel [districtId=" + districtId + ", district=" + district + ", stateModel=" + stateModel
//				+ "]";
//	}
	
	
}
