package com.ingenio.trust.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "lib_book_inventory")
public class LibBookInventoryModel {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "book_inventory_id")
	private int bookInventoryID;
	@Column(name = "book_name")
	private String bookName;
	@Column(name = "author_name")
	private String authorName;
	@Column(name = "publication_name")
	private String publicationName;
	private int quantity;
	@Column(name = "book_type_id")
	private int bookTypeID;
	private int book_stream_id;
	@Column(name = "standard_id")
	private int standardID;
	@Column(name = "division_id")
	private int divisionID;
	@Column(name = "book_edition")
	private String bookEdition;
	private int book_for;
	
	@Column(name = "amount_per_book")
	private double amountPerBook;
	private String bill_no;
	private String year_of_publisher;
    private	String no_of_pages;
    private String rack_No;
    private String bill_date;
    
	@Column(name = "created_by")
	private int createdBy;
	@Column(name = "created_date")
	private String createdDate;
	@Column(name = "updated_date")
	private String updatedDate;
	private int status;
	private Integer schoolId;
	private Integer userId;
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private Date delDate;
	private String deleteBy;
	private String isEdit = "0";
	private Date editDate;
	private String editBy;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag = "0";
	
	
	@OneToMany(mappedBy = "bookDetails", orphanRemoval = true)
    private List<LibBookAccessISBNModel> libBookAccessISBNModels;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "currencyId", nullable = true)
	private LibCurrencyMasterModel CurrencyDetails;
	
}
