package com.ingenio.trust.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "applied_school")
public class AppliedSchoolModel {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private String id;
	
	@Column(name = "from_school_id")
	private Integer fromSchoolId;
	
	@Column(name = "applied_school_id")
	private Integer appliedSchoolId;
	
	@Column(name = "student_id")
	private String studentId;
	
	@Column(name = "created_by")
	private String createdBy;
	
	@Column(name = "created_date")
	private String createdDate;
	
	@Column(name = "preference")
	private String preference;
	
	@Column(name = "approvalstatus")
	private String approvalstatus ="0";
	
	@Column(name = "category_name")
	private String categoryName;
	
	@Column(name = "streamname")
	private String streamName;
	
	@Column(name = "class_name")
	private String className;
	
	@Column(name = "updated_date")
	private String updatedDate;
	
}
