package com.ingenio.trust.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;


/**
 * ConcessionMaster entity. @author MyEclipse Persistence Tools
 */
@Entity

@Table(name = "concession_master", uniqueConstraints = @UniqueConstraint(columnNames = "concession_type"))
public class ConcessionMaster  {

	// Fields
	private static final long serialVersionUID = 1L;
	private Integer concessionId;
	private String concessionType;
	private String concessionTypeGL;
	private Integer isDel=0;
	private String isEdit;
	private String catconcession;
	private SchoolMasterModel schoolMasterModel;
	private AppUserRoleModel userId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new Date(new java.util.Date().getTime());
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag="0";

	// Constructors

	/** default constructor */
	public ConcessionMaster() {
	}

	/** minimal constructor */
	public ConcessionMaster(String concessionType) {
		this.concessionType = concessionType;
	}

	/** full constructor */
	public ConcessionMaster(String concessionType, Integer isDel,
			String isEdit, String catconcession) {
		this.concessionType = concessionType;
		this.isDel = isDel;
		this.isEdit = isEdit;
		this.catconcession = catconcession;
	}

	// Property accessors
	@Id
	@Column(name = "concession_Id", unique = true, nullable = false)
	public Integer getConcessionId() {
		return this.concessionId;
	}

	public void setConcessionId(Integer concessionId) {
		this.concessionId = concessionId;
	}

	@Column(name = "concession_type", unique = true, nullable = false, length = 100)
	public String getConcessionType() {
		return this.concessionType;
	}

	public void setConcessionType(String concessionType) {
		this.concessionType = concessionType;
	}
	
	@Column(name = "concessionTypeGL")
	public String getConcessionTypeGL() {
		return concessionTypeGL;
	}

	public void setConcessionTypeGL(String concessionTypeGL) {
		this.concessionTypeGL = concessionTypeGL;
	}
	
	@Column(name = "isDel")
	public Integer getIsDel() {
		return isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}

	@Column(name = "isEdit", length = 100)
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name = "catconcession", length = 50)
	public String getCatconcession() {
		return this.catconcession;
	}

	public void setCatconcession(String catconcession) {
		this.catconcession = catconcession;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable= false)
	public AppUserRoleModel getUserId() {
		return userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="deviceType", columnDefinition="default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="ipAddress", length=100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="macAddress", length=50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name="sinkingFlag", columnDefinition="default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

}