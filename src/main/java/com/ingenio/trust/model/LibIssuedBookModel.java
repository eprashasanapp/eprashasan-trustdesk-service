package com.ingenio.trust.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import lombok.Data;
@Data
@Entity
@Table(name = "lib_issued_book")
public class LibIssuedBookModel {
	
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "issue_book_id")
	private Integer issueBookId;
	@Column(name = "name_id")
	private Integer nameId;
	private Integer type;
	@Column(name = "for_book_type")
	private Integer forBookType;
	@Column(name = "bt_card_id")
	private Integer btCardId;
	@Column(name = "book_inventory_id")
	private Integer bookInventoryId;
	@Column(name = "book_isbn_access_id")
	private Integer bookIsbnAccessId;
	@Column(name = "is_issued")
	private Integer isIssued;
	@Column(name = "issued_date")
	private Date issuedDate = new java.sql.Date(new java.util.Date().getTime());
	@Column(name = "return_date")
	private String returnDate;
	@Column(name = "created_date")
	private Date createdDate = new java.sql.Date(new java.util.Date().getTime());
	
	@Column(name = "transaction_no")
	private Integer transactionNo;
	
	@Column(name = "updated_date")
	private String updatedDate;
	private Integer status;
	@Column(name = "actual_return_date")
	private String actualReturnDate;
	private Integer schoolId;
	private Integer userId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private Date delDate;
	private Integer deleteBy;
	private String isEdit = "0";
	private Date editDate;
	private Integer editBy;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag = "0";
	
	private String onlineOfline;
	private String requestedDate;
	private String lapsDate;
	private String acceptedDate;
	private String rejectedDate;
	private Integer requestedBy;
	private Integer acceptedBy;
	private Integer rejectedBy;
	private Integer issuedBy;
	private Integer returnBy;
	
}
