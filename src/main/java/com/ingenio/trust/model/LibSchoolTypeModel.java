package com.ingenio.trust.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "lib_school_type")
public class LibSchoolTypeModel {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "school_type_id")
	private Integer schoolTypeId;
	
	@Column(name = "school_type")
	private String SchoolType;
	
	@Column(name = "schoolId")
	private Integer schoolId;
	
	@Column(name = "created_by")
	private Integer createdBy;
	
	@Column(name = "created_date")
	private String createdDate;
	
	@Column(name = "status")
	private Integer status;
	
	@Column(name = "updated_date")
	private String updatedDate;
	
	@Column(name = "userId")
	private Integer userId;
	
	@Column(name = "cDate")
	private Date cDate;
	
	@Column(name = "isDel")
	private String isDel;
	
	@Column(name = "delDate")
	private Date delDate;
	
	@Column(name = "deleteBy")
	private Integer deleteBy;
	
	@Column(name = "isEdit")
	private String isEdit;
	
	@Column(name = "editDate")
	private Date editDate;
	
	@Column(name = "editBy")
	private Integer editBy;
	
	@Column(name = "deviceType")
	private String deviceType;
	
	@Column(name = "ipAddress")
	private String ipAddress;
	
	@Column(name = "macAddress")
	private String macAddress;
	
	@Column(name = "sinkingFlag")
	private String sinkingFlag;
	
	@Column(name = "fromSchoolId")
	private Integer fromSchoolId;
	
}
