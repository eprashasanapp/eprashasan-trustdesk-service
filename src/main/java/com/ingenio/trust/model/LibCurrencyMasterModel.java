package com.ingenio.trust.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "lib_currency_master")
public class LibCurrencyMasterModel {
	
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "c_id")
	private Integer c_Id;
	
	@Column(name = "currencyName")
	private String currencyName;
	
	private Integer schoolId;
	private Integer userId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private Date delDate;
	private Integer deleteBy;
	private String isEdit = "0";
	private Date editDate;
	private Integer editBy;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag = "0";
	
	@OneToMany(mappedBy = "CurrencyDetails", orphanRemoval = true)
    private List<LibBookInventoryModel> libBookInventoryModel;

}
