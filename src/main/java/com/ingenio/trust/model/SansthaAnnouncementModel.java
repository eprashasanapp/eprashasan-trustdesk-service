package com.ingenio.trust.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "sanstha_announcement")

public class SansthaAnnouncementModel {

	private Integer sansthaAnnouncementId;
	private SansthaRegistrationModel sansthaRegistrationModel;
	private SansthaUserRegistrationModel sansthaUserRegistrationModel;
	private String announcementTitle;
	private String announcement;
	private AppUserRoleModel userId;
	private Timestamp cDate;
	private String isDel = "0";
	private String isEdit = "0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String isApproval = "0";
	private AppUserRoleModel approvalBy;
	private Date approvalDate;
	private String deviceType = "0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag = "0";
	private Date endDate;
	private Date startDate;

	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column(name = "sansthaAnnouncementId", nullable = false, columnDefinition = "INT(11) UNSIGNED")	
	public Integer getSansthaAnnouncementId() {
		return sansthaAnnouncementId;
	}

	public void setSansthaAnnouncementId(Integer sansthaAnnouncementId) {
		this.sansthaAnnouncementId = sansthaAnnouncementId;
	}

	@Column(name = "announcementTitle", nullable = false, length = 50)
	@Length(max = 50)
	public String getAnnouncementTitle() {
		return announcementTitle;
	}

	public void setAnnouncementTitle(String announcementTitle) {
		this.announcementTitle = announcementTitle;
	}

	@Column(name = "announcement", nullable = false, length = 500)
	@Length(max = 500)
	public String getAnnouncement() {
		return announcement;
	}

	public void setAnnouncement(String announcement) {
		this.announcement = announcement;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable = false, columnDefinition = "INT(11) UNSIGNED DEFAULT NULL")
	@ElementCollection(targetClass = AppUserRoleModel.class)
	public AppUserRoleModel getUserId() {
		return this.userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sansthaId")
	@ElementCollection(targetClass = SansthaRegistrationModel.class)
	public SansthaRegistrationModel getSansthaRegistrationModel() {
		return sansthaRegistrationModel;
	}

	public void setSansthaRegistrationModel(SansthaRegistrationModel sansthaRegistrationModel) {
		this.sansthaRegistrationModel = sansthaRegistrationModel;
	}

	@Column(name = "cDate", nullable = true, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	public Timestamp getCDate() {
		return this.cDate;
	}

	public void setCDate(Timestamp cDate) {
		this.cDate = cDate;
	}

	@Column(name = "isDel", columnDefinition = "CHAR default '0'", length = 1)
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name = "isEdit", columnDefinition = "CHAR default '0'", length = 1)
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name = "delDate")
	@Type(type = "date")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", columnDefinition = "INT(10) UNSIGNED ")
	@ElementCollection(targetClass = AppUserRoleModel.class)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name = "editDate")
	@Type(type = "date")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", columnDefinition = "INT(10) UNSIGNED DEFAULT NULL", referencedColumnName = "appuserroleid")
	@ElementCollection(targetClass = AppUserRoleModel.class)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}

	@Column(name = "isApproval", length = 1, columnDefinition = "CHAR")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", columnDefinition = "INT(10) UNSIGNED")
	@ElementCollection(targetClass = AppUserRoleModel.class)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}

	@Column(name = "approvalDate")
	@Type(type = "date")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	@Column(name = "deviceType", length = 1, columnDefinition = "CHAR")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name = "ipAddress", length = 100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name = "macAddress", length = 50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name = "sinkingFlag", length = 1, columnDefinition = "CHAR default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	@Column(name = "endDate")
	@Type(type = "date")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "startDate")
	@Type(type = "date")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "sansthaUserId") 
	@ElementCollection(targetClass = SansthaUserRegistrationModel.class)
	public SansthaUserRegistrationModel getSansthaUserRegistrationModel() {
		return sansthaUserRegistrationModel;
	}

	public void setSansthaUserRegistrationModel(SansthaUserRegistrationModel sansthaUserRegistrationModel) {
		this.sansthaUserRegistrationModel = sansthaUserRegistrationModel;
	}

}
