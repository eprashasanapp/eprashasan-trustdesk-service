package com.ingenio.trust.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;



@Entity
@Table(name="sanstha_user_registration")

public class SansthaUserRegistrationModel {
	
	private Integer sansthaUserId;
	private SansthaRegistrationModel sansthaRegistrationModel;
	private String sansthaUserRegNo;
	private String initialName;
	private String firstName;
	private String secondName;
	private String lastName;
	private String birthDate;
	private String gender;
	private YearMasterModel yearId;
	private CasteMasterModel casteMasterModel;
	private CategoryMasterModel categoryMasterModel;
	private ReligionMasterModel religionMasterModel;
	private String contactNoSms;
	private String contactNo;
	private String emailId;
	private String address;
	private String city;
	private String state;
	private String pincode;
	private String country;
	private String joiningDate;
	private AppUserRoleModel userId;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new Date(new java.util.Date().getTime());
	private String isDel = "0";
	private String isEdit = "0";
	private Date delDate;
	private AppUserRoleModel deleteBy;
	private Date editDate;
	private AppUserRoleModel editBy;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag = "0";
	
	
	@Id
	@Column(name = "sansthaUserId"  , nullable = false)
	public Integer getSansthaUserId() {
		return sansthaUserId;
	}

	public void setSansthaUserId(Integer sansthaUserId) {
		this.sansthaUserId = sansthaUserId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sansthaId")
	public SansthaRegistrationModel getSansthaRegistrationModel() {
		return sansthaRegistrationModel;
	}

	public void setSansthaRegistrationModel(SansthaRegistrationModel sansthaRegistrationModel) {
		this.sansthaRegistrationModel = sansthaRegistrationModel;
	}

	@Column(name = "sansthaUserRegNo")
	public String getSansthaUserRegNo() {
		return sansthaUserRegNo;
	}

	public void setSansthaUserRegNo(String sansthaUserRegNo) {
		this.sansthaUserRegNo = sansthaUserRegNo;
	}
	
	@Column(name = "initialName")
	public String getInitialName() {
		return initialName;
	}

	public void setInitialName(String initialName) {
		this.initialName = initialName;
	}

	@Column(name = "firstName")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "secondName")
	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	@Column(name = "lastName")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "birthDate")
	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	
	@Column(name = "gender")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yearId", nullable = false)
	public YearMasterModel getYearId() {
		return yearId;
	}


	public void setYearId(YearMasterModel yearId) {
		this.yearId = yearId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "casteId", nullable = false)
	public CasteMasterModel getCasteMasterModel() {
		return casteMasterModel;
	}


	public void setCasteMasterModel(CasteMasterModel casteMasterModel) {
		this.casteMasterModel = casteMasterModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "categoryId", nullable = false)
	public CategoryMasterModel getCategoryMasterModel() {
		return categoryMasterModel;
	}


	public void setCategoryMasterModel(CategoryMasterModel categoryMasterModel) {
		this.categoryMasterModel = categoryMasterModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "religionId", nullable = false)
	public ReligionMasterModel getReligionMasterModel() {
		return religionMasterModel;
	}
	
	

	public void setReligionMasterModel(ReligionMasterModel religionMasterModel) {
		this.religionMasterModel = religionMasterModel;
	}

	@Column(name = "contactNoSms")
	public String getContactNoSms() {
		return contactNoSms;
	}

	public void setContactNoSms(String contactNoSms) {
		this.contactNoSms = contactNoSms;
	}

	@Column(name = "contactNo")
	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	@Column(name = "emailId")
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@Column(name = "address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "city")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "state")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "pincode")
	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	@Column(name = "country")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "joiningDate")
	public String getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId", nullable = false)
	public AppUserRoleModel getUserId() {
		return userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@Column(name = "cDate")
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@Column(name = "isDel")
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name = "isEdit")
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}

	@Column(name = "deviceType")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name = "ipAddress")
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name = "macAddress")
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name = "sinkingFlag")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	public SansthaUserRegistrationModel() {
		super();
	}

	

	
}
