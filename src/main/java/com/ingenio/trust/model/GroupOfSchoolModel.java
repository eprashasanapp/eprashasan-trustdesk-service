package com.ingenio.trust.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;



@Entity
@Table(name = "group_of_school")

public class GroupOfSchoolModel {

	private int groupOfSchoolId;
	private String groupOfSchoolName;

	private SansthaRegistrationModel sansthaRegistrationModel;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new Date(new java.util.Date().getTime());
	private SchoolMasterModel schoolMasterModel;
	private AppUserRoleModel userId;
	private String isDel = "0";
	private Date delDate;
	private String isEdit = "0";
	private Date editDate;
	private String sinkingFlag = "0";
	private AppUserRoleModel deleteBy;
	private AppUserRoleModel editBy;
	private String isApproval = "0";
	private AppUserRoleModel approvalBy;
	private Date approvalDate;
	private String deviceType = "0";
	private String ipAddress;
	private String macAddress;

	@Id
	@Column(name = "groupOfSchoolId")
	public int getGroupOfSchoolId() {
		return groupOfSchoolId;
	}

	public void setGroupOfSchoolId(int groupOfSchoolId) {
		this.groupOfSchoolId = groupOfSchoolId;
	}

	@Column(name = "groupOfSchoolName")
	public String getGroupOfSchoolName() {
		return groupOfSchoolName;
	}

	public void setGroupOfSchoolName(String groupOfSchoolName) {
		this.groupOfSchoolName = groupOfSchoolName;
	}

	@Column(name = "cDate", updatable = false)
	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@Column(name = "isDel", columnDefinition = "default '0'")
	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	@Column(name = "isEdit", columnDefinition = "default '0'")
	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	@Column(name = "deviceType", columnDefinition = "default '0'")
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name = "ipAddress", length = 100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name = "macAddress", length = 50)
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Column(name = "sinkingFlag", columnDefinition = "default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}

	@Column(name = "isApproval", columnDefinition = "default '0'")
	public String getIsApproval() {
		return isApproval;
	}

	public void setIsApproval(String isApproval) {
		this.isApproval = isApproval;
	}

	@Column(name = "approvalDate")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sansthaId", nullable = false)
	public SansthaRegistrationModel getSansthaRegistrationModel() {
		return sansthaRegistrationModel;
	}

	public void setSansthaRegistrationModel(SansthaRegistrationModel sansthaRegistrationModel) {
		this.sansthaRegistrationModel = sansthaRegistrationModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId", nullable = false)
	public AppUserRoleModel getUserId() {
		return userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvalBy", nullable = false)
	public AppUserRoleModel getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(AppUserRoleModel approvalBy) {
		this.approvalBy = approvalBy;
	}
	
	

}