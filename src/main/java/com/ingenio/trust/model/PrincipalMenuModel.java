package com.ingenio.trust.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "principal_menu_master")

public class PrincipalMenuModel {


	private Integer principalMenuId;
	private String menuName;
	private Date cDate = new Date(new java.util.Date().getTime());
	private Date delDate;
	private Date editDate;
	private String ipAddress;
	private String isDel="0";
	private String isEdit = "0";
	private String macAddress;
	private String sinkingFlag = "0";
	private AppUserRoleModel deleteBy;	
	private AppUserRoleModel editBy;	
	private AppUserRoleModel userId;
	private AndroidMenuMasterGroupModel androidMenuMasterGroupModel;


	@Id
	@Column(name = "principalMenuId", nullable = false)
	public Integer getPrincipalMenuId() {
		return principalMenuId;
	}


	public void setPrincipalMenuId(Integer principalMenuId) {
		this.principalMenuId = principalMenuId;
	}

	@Column(name = "menuName", nullable = false)
	public String getMenuName() {
		return menuName;
	}


	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}


	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}


	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}


	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	@Column(name = "ipAddress", length = 100)
	public String getIpAddress() {
		return ipAddress;
	}


	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="isDel", columnDefinition="default '0'")
	public String getIsDel() {
		return isDel;
	}


	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	@Column(name = "isEdit", columnDefinition = "default '0'")
	public String getIsEdit() {
		return isEdit;
	}


	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	@Column(name = "macAddress", length = 50)
	public String getMacAddress() {
		return macAddress;
	}


	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}


	public String getSinkingFlag() {
		return sinkingFlag;
	}

	@Column(name = "sinkingFlag", columnDefinition = "default '0'")
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	@Column(name = "deleteBy")
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}


	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}


	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	@Column(name = "editBy")
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}

	@Column(name = "userID")
	public AppUserRoleModel getUserId() {
		return userId;
	}
	
	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "androidMenuGroupId", nullable = false)
	@Length(max = 30)
	public AndroidMenuMasterGroupModel getAndroidMenuMasterGroupModel() {
		return androidMenuMasterGroupModel;
	}


	public void setAndroidMenuMasterGroupModel(AndroidMenuMasterGroupModel androidMenuMasterGroupModel) {
		this.androidMenuMasterGroupModel = androidMenuMasterGroupModel;
	}



}