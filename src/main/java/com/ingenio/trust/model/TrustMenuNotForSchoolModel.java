package com.ingenio.trust.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "trust_menu_not_for_school")
public class TrustMenuNotForSchoolModel {

	private Integer tmenuNotForSchId;
	private String sansthaKey;
	private Integer menuId;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getTmenuNotForSchId() {
		return tmenuNotForSchId;
	}
	public void setTmenuNotForSchId(Integer tmenuNotForSchId) {
		this.tmenuNotForSchId = tmenuNotForSchId;
	}
	
	public Integer getMenuId() {
		return menuId;
	}
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	public String getSansthaKey() {
		return sansthaKey;
	}
	public void setSansthaKey(String sansthaKey) {
		this.sansthaKey = sansthaKey;
	}
	
	
	
}
