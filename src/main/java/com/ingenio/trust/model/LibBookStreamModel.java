package com.ingenio.trust.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "lib_book_stream")
public class LibBookStreamModel {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "book_stream_id")
	private int bookStreamId;
	@Column(name = "book_stream_name")
	private String bookStreamName;
	@Column(name = "created_by")
	private int createdBy;
	@Column(name = "created_date")
	private String createdDate;
	@Column(name = "updated_date")
	private String updatedDate;
	private int status;
	private Integer schoolId;
	private Integer userId;
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private Date delDate;
	private String deleteBy;
	private String isEdit = "0";
	private Date editDate;
	private String editBy;
	private String deviceType="0";
	private String ipAddress;
	private String macAddress;
	private String sinkingFlag = "0";
	
}
