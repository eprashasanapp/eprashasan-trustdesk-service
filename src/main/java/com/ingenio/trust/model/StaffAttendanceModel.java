package com.ingenio.trust.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;



@Entity
@Table(name = "staff_attendance")

public class StaffAttendanceModel {
	
	private int staffAttendanceID;
	private int staffID;
	private int presentFlag;
	private String date;
	private String inTime;
	private String outTime;
	private int period;
	private String createdDate;
	private int status;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new Date(new java.util.Date().getTime());
	private String isDel = "0";
	private String isEdit = "0";
	private String sinkingFlag = "0";
	private Date delDate;
	private Date editDate;
	private String deviceType = "0";
	private String ipAddress;
	private String macAddress;
	private SchoolMasterModel schoolMasterModel;
	private AppUserRoleModel userId;
	private AppUserRoleModel deleteBy;
	private AppUserRoleModel editBy;
	
	@Id
	@Column(name = "staff_attendance_id")
	public int getStaffAttendanceID() {
		return staffAttendanceID;
	}
	
	public void setStaffAttendanceID(int staffAttendanceID) {
		this.staffAttendanceID = staffAttendanceID;
	}
	
	@Column(name = "staff_id")
	public int getStaffID() {
		return staffID;
	}
	
	public void setStaffID(int staffID) {
		this.staffID = staffID;
	}
	
	@Column(name = "present_flag")
	public int getPresentFlag() {
		return presentFlag;
	}
	
	public void setPresentFlag(int presentFlag) {
		this.presentFlag = presentFlag;
	}
	
	@Column(name = "date")
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	@Column(name = "intime")
	public String getInTime() {
		return inTime;
	}
	
	public void setInTime(String inTime) {
		this.inTime = inTime;
	}
	
	@Column(name = "outtime")
	public String getOutTime() {
		return outTime;
	}
	
	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}
	
	@Transient
	public int getPeriod() {
		return period;
	}
	
	public void setPeriod(int period) {
		this.period = period;
	}
	
	@Column(name = "created_date")
	public String getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name = "status")
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable = false)
	public AppUserRoleModel getUserId() {
		return userId;
	}
	
	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy", nullable = false)
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy", nullable = false)
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name = "cDate", updatable = false)
	public Date getcDate() {
		return cDate;
	}
	
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name = "isDel", nullable = false)
	public String getIsDel() {
		return isDel;
	}
	
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	@Column(name = "isEdit", nullable = false)
	public String getIsEdit() {
		return isEdit;
	}
	
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name = "sinkingFlag", nullable = false)
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}
	
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}
	
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@Column(name = "deviceType", nullable = false)
	public String getDeviceType() {
		return deviceType;
	}
	
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	@Column(name = "ipAddress", nullable = false)
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name = "macAddress", nullable = false)
	public String getMacAddress() {
		return macAddress;
	}
	
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
}
