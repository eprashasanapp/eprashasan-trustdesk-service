package com.ingenio.trust.util;


import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Strings;

public class Utillity {
	
	public static String convertNumberToWord(String number) {
		String number1="";
		if(number!=null && number.length()>=5) {
			Double numberDouble =Double.parseDouble(number);
			number = "" + numberDouble.intValue(); 
			if(number.length()==6) {
			    number1 =  number.substring(0,2);
			    if(Integer.parseInt(number.substring(2,4)) !=0) {
				    number1 =  number1.concat(".").concat(number.substring(2,4));
			    }
				number1 =  number1.concat("K");
			}
			if(number.length()==6) {
			    number1 =  number.substring(0,1);
    			if(Integer.parseInt(number.substring(1,3)) !=0) {
    				number1 =  number1.concat(".").concat(number.substring(1,3));
    			}
				number1 = number1.concat("L");
			}
			if(number.length()==7) {
			    number1 =  number.substring(0,2);
			    if(Integer.parseInt(number.substring(2,4)) !=0) {
				    number1 =  number1.concat(".").concat(number.substring(2,4));
			    }
				number1 =  number1.concat("L");
			}
				if(number.length()==8) {
			    number1 =  number.substring(0,1);
    			if(Integer.parseInt(number.substring(1,3)) !=0) {
    				number1 =  number1.concat(".").concat(number.substring(1,3));
    			}
				number1 = number1.concat("Cr");
			}
			if(number.length()==9) {
			    number1 =  number.substring(0,2);
			    if(Integer.parseInt(number.substring(2,4)) !=0) {
				    number1 =  number1.concat(".").concat(number.substring(2,4));
			    }
				number1 =  number1.concat("Cr");
			}
			if(number1.equalsIgnoreCase("")) {
				return number;
			}
			return number1;
		}else {
			return number;
		}
	}
	
	public static String convertEnglishToOther(String num,String language) {
		StringBuilder newStr = new StringBuilder();
		if (!Strings.isNullOrEmpty(num)) {
			if (language.equals("1")) {
				return num;
			}
			else {
				if (language.equals("2")) {
					for (int i = 0; i < num.length(); i++ ) {
						char str = num.charAt(i);
						switch (str) {
							case '-':
								newStr.append("-");
								break;
								
							case '.':
								newStr.append(".");
								break;
							case '1':
								newStr.append("१");
								break;
							
							case '2':
								newStr.append("२");
								break;
							
							case '3':
								newStr.append("३");
								break;
							
							case '4':
								newStr.append("४");
								break;
							
							case '5':
								newStr.append("५");
								break;
							
							case '6':
								newStr.append("६");
								break;
							
							case '7':
								newStr.append("७");
								break;
							
							case '8':
								newStr.append("८");
								break;
							
							case '9':
								newStr.append("९");
								break;
							
							case '0':
								newStr.append("०");
								break;
							
							case '१':
								newStr.append("१");
								break;
							
							case '२':
								newStr.append("२");
								break;
							
							case '३':
								newStr.append("३");
								break;
							
							case '४':
								newStr.append("४");
								break;
							
							case '५':
								newStr.append( "५");
								break;
							
							case '६':
								newStr.append( "६");
								break;
							
							case '७':
								newStr.append( "७");
								break;
							
							case '८':
								newStr.append( "८");
								break;
							case '९':
								newStr.append( "९");
								break;
							
							case '०':
								newStr.append( "०");
								break;
							default :
								newStr.append(str);
						}
					}
				}
				
				if (language.equals("3")) {
					for (int i = 0; i < num.length(); i++ ) {
						char str = num.charAt(i);
						switch (str) {
							// Start English to Hindi
							case '1':
								newStr.append( "१");
								break;
							case '2':
								newStr.append( "२");
								break;
							case '3':
								newStr.append( "३");
								break;
							case '4':
								newStr.append( "४");
								break;
							case '5':
								newStr.append( "५");
								break;
							case '6':
								newStr.append( "६");
								break;
							case '7':
								newStr.append( "७");
								break;
							case '8':
								newStr.append( "८");
								break;
							case '9':
								newStr.append( "९");
								break;
							case '0':
								newStr.append( "०");
								break;
							case '१':
								newStr.append( "१");
								break;
							case '२':
								newStr.append( "२");
								break;
							case '३':
								newStr.append( "३");
								break;
							case '४':
								newStr.append( "४");
								break;
							case '५':
								newStr.append( "५");
								break;
							case '६':
								newStr.append( "६");
								break;
							case '७':
								newStr.append( "७");
								break;
							case '८':
								newStr.append( "८");
								break;
							case '९':
								newStr.append( "९");
								break;
							case '०':
								newStr.append( "०");
								break;
							default :
								newStr.append(str);
						}
					}
				}
				
				if (language.equals("4")) {
					for (int i = 0; i < num.length(); i++ ) {
						char str = num.charAt(i);
						switch (str) {
							// Start English to Gujrati
							case '1':
								newStr.append( "૧");
								break;
							
							case '2':
								newStr.append( "૨");
								break;
							
							case '3':
								newStr.append( "૩");
								break;
							
							case '4':
								newStr.append( "૪");
								break;
							
							case '5':
								newStr.append( "૫");
								break;
							
							case '6':
								newStr.append( "૬");
								break;
							
							case '7':
								newStr.append( "૭");
								break;
							
							case '8':
								newStr.append( "૮");
								break;
							
							case '9':
								newStr.append( "૯");
								break;
							
							case '0':
								newStr.append( "૦");
								break;
							
							case '૧':
								newStr.append( "૧");
								break;
							
							case '૨':
								newStr.append( "૨");
								break;
							
							case '૩':
								newStr.append( "૩");
								break;
							
							case '૪':
								newStr.append( "૪");
								break;
							
							case '૫':
								newStr.append( "૫");
								break;
							
							case '૬':
								newStr.append( "૬");
								break;
							
							case '૭':
								newStr.append( "૭");
								break;
							
							case '૮':
								newStr.append( "૮");
								break;
							case '૯':
								newStr.append( "૯");
								break;
							
							case '૦':
								newStr.append( "૦");
								break;
							default :
								newStr.append(str);
						}
					}
				}
			}
		}
		return newStr.toString();
	}
	
	public static String convertOtherToEnglish(String num) {
		StringBuilder newStr = new StringBuilder();
		if (StringUtils.isNotEmpty(num) && !num.equals("null")) {
			for (int i = 0; i < num.length(); i++ ) {
				char str = num.charAt(i);				
				switch (str) {
					case '-':
						newStr.append("-");
						break;
						
					case '.':
						newStr.append(".");
						break;			
					
					// Start Marathi to English
					case '१':
						newStr.append("1");
						break;
					
					case '२':
						newStr.append("2");
						break;
					
					case '३':
						newStr.append("3");
						break;
					
					case '४':
						newStr.append("4");
						break;
					
					case '५':
						newStr.append("5");
						break;
					
					case '६':
						newStr.append("6");
						break;
					
					case '७':
						newStr.append("7");
						break;
					
					case '८':
						newStr.append("8");
						break;
					
					case '९':
						newStr.append("9");
						break;
					
					case '०':
						newStr.append("0");
						break;
					
					// Start Gujrati to English
					case '૧':
						newStr.append("1");
						break;
					
					case '૨':
						newStr.append("2");
						break;
					
					case '૩':
						newStr.append("3");
						break;
					
					case '૪':
						newStr.append("4");
						break;
					
					case '૫':
						newStr.append("5");
						break;
					
					case '૬':
						newStr.append("6");
						break;
					
					case '૭':
						newStr.append("7");
						break;
					
					case '૮':
						newStr.append("8");
						break;
					
					case '૯':
						newStr.append("9");
						break;
					
					case '૦':
						newStr.append("0");
						break;
						
					default :
						newStr.append(str);
				}
			}
		}else {
			return "";
		}
		return newStr.toString();
	}

	private Utillity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
