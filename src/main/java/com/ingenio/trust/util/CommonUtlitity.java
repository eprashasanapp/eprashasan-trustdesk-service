package com.ingenio.trust.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ingenio.trust.model.AppLanguageModel;
import com.ingenio.trust.model.DynamicAcademicMonthsModel;
import com.ingenio.trust.model.SchoolMasterModel;
import com.ingenio.trust.model.YearMasterModel;
import com.ingenio.trust.repository.AppLanguageRepository;
import com.ingenio.trust.repository.DynamicAcademicMonthsRepository;
import com.ingenio.trust.repository.YearMasterRepository;

@Component
public class CommonUtlitity {
	
	@Autowired
	DynamicAcademicMonthsRepository dynamicAcademicMonthsRepository;
	
	@Autowired
	YearMasterRepository yearMasterRepository;
	
	@Autowired
	private AppLanguageRepository appLanguageRepository;
	
	private CommonUtlitity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getCurrentYear(Integer schoolId,String date) {
		int globleYearId = 0;
		try {
			
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(schoolId);
			List<DynamicAcademicMonthsModel> dynamicAcadYearBean = dynamicAcademicMonthsRepository.findBySchoolMasterModel(schoolMasterModel);
			if(CollectionUtils.isNotEmpty(dynamicAcadYearBean)) {
				String localDynamicAcadSdate=dynamicAcadYearBean.get(0).getFromMonth();
				String globalDynamicAcadSdate=localDynamicAcadSdate.split("-")[1];
				
				LocalDate now = LocalDate.now(); 
				if(StringUtils.isNotEmpty(date)) {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
					now = LocalDate.parse(date,formatter);
				}
				
				int year = now.getYear();
				int month =(now.getMonthValue()); 
				String currentAcYr="";
				if (month >= Integer.parseInt(globalDynamicAcadSdate) && month <= 12) {
					year++;
					currentAcYr = now.getYear() + "-" + year;
				} else {
					year--;
					currentAcYr = year + "-" + now.getYear();
				}
				
				List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
				List<String> yearList = new ArrayList<>();
				if (CollectionUtils.isNotEmpty(appLanguageList)) {
					for (int i = 0; i < appLanguageList.size(); i++) {
						yearList .add(Utillity.convertEnglishToOther(currentAcYr, "" + appLanguageList.get(i).getLanguageId()));
					}
				}
				List<YearMasterModel> list = yearMasterRepository.findBySchoolMasterModelOrderByYear(schoolMasterModel);
				for(int i=0;i<list.size();i++) {
					if (yearList.contains(list.get(i).getYear())) {
						globleYearId = list.get(i).getYearId();
					}
				}
			}
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return globleYearId;
	}
	
	public String getCurrentYearName(Integer schoolId,String date) {
		try {
			
			SchoolMasterModel schoolMasterModel = new SchoolMasterModel();
			schoolMasterModel.setSchoolid(schoolId);
			List<DynamicAcademicMonthsModel> dynamicAcadYearBean = dynamicAcademicMonthsRepository.findBySchoolMasterModel(schoolMasterModel);
			if(CollectionUtils.isNotEmpty(dynamicAcadYearBean)) {
				String localDynamicAcadSdate=dynamicAcadYearBean.get(0).getFromMonth();
				String globalDynamicAcadSdate=localDynamicAcadSdate.split("-")[1];
				
				LocalDate now = LocalDate.now(); 
				if(StringUtils.isNotEmpty(date)) {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
					now = LocalDate.parse(date,formatter);
				}
				
				int year = now.getYear();
				int month =(now.getMonthValue()); 
				String currentAcYr="";
				if (month >= Integer.parseInt(globalDynamicAcadSdate) && month <= 12) {
					year++;
					currentAcYr = now.getYear() + "-" + year;
				} else {
					year--;
					currentAcYr = year + "-" + now.getYear();
				}
				
				List<AppLanguageModel> appLanguageList = appLanguageRepository.findAll();
				List<String> yearList = new ArrayList<>();
				if (CollectionUtils.isNotEmpty(appLanguageList)) {
					for (int i = 0; i < appLanguageList.size(); i++) {
						yearList .add(Utillity.convertEnglishToOther(currentAcYr, "" + appLanguageList.get(i).getLanguageId()));
					}
				}
				List<YearMasterModel> list = yearMasterRepository.findBySchoolMasterModelOrderByYear(schoolMasterModel);
				for(int i=0;i<list.size();i++) {
					if (yearList.contains(list.get(i).getYear())) {
						return list.get(i).getYear();
					}
				}
			}
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return "";
	}


}
