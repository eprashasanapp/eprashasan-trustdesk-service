package com.ingenio.trust.util;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.lang3.StringUtils;


public class UTFConversionUtil {
	
	
	
	private UTFConversionUtil() {
		super();
		// TODO Auto-generated constructor stub
	}

	// Convert String parameter to UTF-8
	public static String convertInUTFFormat(String str) {
		if (StringUtils.isNotEmpty(str)&& !str.equals("null")) {
			try {
				str = new String(str.getBytes("iso-8859-1"), StandardCharsets.UTF_8);
				return str;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	public static String generatePrimaryKey(String globalDbSchoolKey,String incrementedId) {
		String newGenetatedKey=incrementedId;
		try {
			if(globalDbSchoolKey!=null && incrementedId.length()<10 ) {
				newGenetatedKey=globalDbSchoolKey+("0000000000".substring(0, ("0000000000".length()-(globalDbSchoolKey+incrementedId).length())))+incrementedId;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newGenetatedKey;
	}
}
