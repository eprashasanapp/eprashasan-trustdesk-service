package com.ingenio.trust.enums;

public enum SummaryType {
	
	Category("category"),
	Minority("minority"),
	Concession("concession"),
	Religion("religion"),
	Age("age");	

	private final String summaryType;

	public  String getSummaryType() {
		return summaryType;
	}

	private SummaryType(String summaryType) {
		this.summaryType = summaryType;
	}

	
	

}
